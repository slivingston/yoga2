# Coding style

## Language/Compilers

* Requires C++17. It is regularly tested on OSX with XCode's clang derivative, or on Linux with clang 9

## Dependencies

* [llvm 9.0.1](http://llvm.org) to emit code.
* [eigen3](http://eigen.tuxfamily.org/) for linear algebra
* [sqlite3](https://sqlite.org/index.html)
* [sdl2](https://www.libsdl.org/)
* [libuv](http://docs.libuv.org/en/v1.x/)
* [glm](https://glm.g-truc.net/0.9.9/index.html)
* [imgui](https://github.com/ocornut/imgui)
