#pragma once
#include "./packet_network_engine.h"

struct ChildPipe : PacketNetworkEngine {
  ChildPipe(
    vector<string> const &_execArgs,
    vector<string> const &_execEnv);

  vector<string> execArgs;
  vector<string> execEnv;
  UvProcess *child_proc{nullptr};

  void setupChild();
  void afterSetWarming(R &delay) override;
};

