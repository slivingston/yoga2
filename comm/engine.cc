
#include "./engine.h"

#include "common/uv_wrappers.h"
#include "../timeseq/trace.h"


map<string, EngineRegister *> *EngineRegister::registry;


RuntimeInfo::RuntimeInfo(shared_ptr<YogaCompilation> _reg)
  : reg(_reg)
{
  realTimeOffset = realtime();
  if (reg) {
    paramValues = reg->paramValues.data();
  }
  else {
    paramValues = nullptr;
  }
}


GenericEngine::GenericEngine()
    : engineName("GenericEngine"),
      label("GenericEngine")
{
  rti = make_shared<RuntimeInfo>(nullptr);
}

GenericEngine::~GenericEngine()
{
  if (engineStatus == Running || engineStatus == Draining) {
    // Too late to call .stop, because anything interesting happens in subclasses.
    cerr << "Warning: engine "s + label + " engineStatus=" + repr(engineStatus) + " in destructor\n";
  }
  if (0) cerr << "Delete " + label + "\n";
}

YogaContext GenericEngine::mkCtx(char const *desc)
{
  return rti->reg->mkCtx(desc);
}


/*
  Engine state machine.
  Initial -> Warming -> Running -> Draining? -> Stopped

  Warming is to allow for child or remote processes to start first
*/

void GenericEngine::warmup(R &delay)
{
  if (engineStatus == Initial) {
    engineStatus = Warming;
    afterSetWarming(delay);
  }
}

void GenericEngine::start()
{
  if (engineStatus == Initial) {
    engineStatus = Warming;
    R delay = 0.0;
    afterSetWarming(delay); // delay gets lost here
  }
  if (engineStatus == Warming) {
    beforeSetRunning();
    engineStatus = Running;
    afterSetRunning();
    auto ltrace = trace.lock();
    if (ltrace) ltrace->engineChangedStatus();
  }
}

void GenericEngine::stop()
{
  if (engineStatus == Running) {
    if (needsDrain()) {
      engineStatus = Draining;
      afterSetDraining();
    }
    else {
      engineStatus = Stopped;
      afterSetStopped();
      auto ltrace = trace.lock();
      if (ltrace) ltrace->engineChangedStatus();
    }
  }
}

void GenericEngine::endDrain()
{
  if (engineStatus == Draining) {
    engineStatus = Stopped;
    afterSetStopped();
    auto ltrace = trace.lock();
    if (ltrace) ltrace->engineChangedStatus();
  }
}


void GenericEngine::console(double ts, string const &msg)
{
  string fullMsg = label + ": " + msg;
  auto ltrace = trace.lock();
  if (ltrace) {
    ltrace->console(ts, fullMsg);
  }
  else {
    cerr << fullMsg + "\n";
  }
}

void GenericEngine::console(string const &msg)
{
  console(realtime() - rti->realTimeOffset, msg);
}

void GenericEngine::consolef(double ts, char const *format, ...)
{
  va_list ap;
  va_start(ap, format);
  char *str = nullptr;
  int strLen = vasprintf(&str, format, ap);
  va_end(ap);
  if (strLen < 0) return;

  string line(str, str + strLen);
  console(ts, line);
  free(str);
}

void GenericEngine::consolef(char const *format, ...)
{
  va_list ap;
  va_start(ap, format);
  char *str = nullptr;
  int strLen = vasprintf(&str, format, ap);
  va_end(ap);
  if (strLen < 0) return;

  string line(str, str + strLen);
  console(line);
  free(str);
}

void GenericEngine::requestUpdate()
{
  if (rti) rti->requestUpdate();
}

bool GenericEngine::setEngineConfig(YogaContext const &ctx, AstAnnoCall *spec)
{
  if (!spec->options->getValueForKey("verbose", verbose)) {
    return ctx.logError("Invalid verbose");
  }
  return true;
}
