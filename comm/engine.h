#pragma once
#include "common/std_headers.h"
#include "../timeseq/runtime_info.h"
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "../jit/context.h"
#include "../lib/core_types.h"
#include "common/host_debug.h"
#include "nlohmann-json/json_fwd.hpp"

struct GenericEngine;
struct Trace;
struct YogaTimeseq;
struct EngineUpdateContext;
struct TimeseqChange;
struct GradientSet;
struct AstAnnoCall;

struct EngineUpdateContext {
  EngineUpdateContext(double _now) : now(_now) {}
  double now;
  double rtFence{1.0e99};
  int updateCnt{0};
  GradFwdTrace *gradFwdTrace{nullptr};
};

/*
  An Engine is something that updates Timeseqs, either based on network IO or based on computatations from other
  Timeseqs. They are all subclasses of GenericEngine.

  Engines must provide implementations of:
    void start();
    void update(EngineUpdateContext &ctx);
    void stop();

  ctx.now is the realtime() at which the update started
  a0.xxx points to previous data in the *Out timelines.
     Example: a0.balance
  a1.xxx points to data from the *In timelines, at least one of which is more recent than the UpdateOld.
     Example: a1.config, a1.top, a1.kin, a1.kinSoon, a1.target
  a2.xxx points to new data, which doUpdate should fill in.
     Example: a2.balance
  dt is the difference between the timestamps of the latest data in UpdateOld, and the data in UpdateNew.

  It's reasonable to do things like:
    a2.state->age = a0.state->age + dt;

  By default it calls (the second version of ) doUpdate any time a *Cur is newer than an *Old, but if you add
  an argument like {minUpdateDt: 0.005} to the typereg.engine call, it will wait until some data in an *In
  is at least 5 mS newer than any of the *Outs, so it shouldn't run at more than 200 Hz.

*/
struct GenericEngine : enable_shared_from_this<GenericEngine> {

  enum EngineStatus {
    Initial,
    Warming,
    Running,
    Draining,
    Stopped,
  };

  GenericEngine();
  virtual ~GenericEngine();
  GenericEngine(GenericEngine const &) = delete;
  GenericEngine(GenericEngine &&) = delete;
  GenericEngine &operator=(GenericEngine const &) = delete;
  GenericEngine &operator=(GenericEngine &&) = delete;

  EngineStatus engineStatus{Initial};

  void warmup(R &delay);
  void start();
  void stop();
  void endDrain();

  bool isInitial() const { return engineStatus == Initial; }
  bool isWarming() const { return engineStatus == Warming; }
  bool isRunning() const { return engineStatus == Running; }
  bool isDraining() const { return engineStatus == Draining; }
  bool isStopped() const { return engineStatus == Stopped; }
  bool isActive() const { return engineStatus == Warming || engineStatus == Running || engineStatus == Draining; }
  virtual void afterSetWarming(R &delay) {}
  virtual void beforeSetRunning() {}
  virtual void afterSetRunning() {}
  virtual void afterSetDraining() {}
  virtual void afterSetStopped() {}
  virtual void update(EngineUpdateContext &ctx) { }
  virtual void debugUpdate(EngineUpdateContext &ctx, R ts, YogaValue &debugLocals, Trace *specTrace, apr_pool_t *mem) { }

  virtual bool needsDrain() const { return false; }
  virtual bool isNetwork() const { return false; }
  virtual size_t workOutstanding() const { return 0; }

  virtual void addToManifest(nlohmann::json &manifest) {}

  
  bool setEngineConfig(YogaContext const &ctx, AstAnnoCall *spec);

  /*
    Write "<label>: <msg> to the console. That means both to stdout, and a timeseq named "console" if one exists.
    Supply ts if available, or the current time will be used.
  */
  void console(double ts, string const &msg);
  void console(string const &msg);
  void consolef(double ts, char const *format, ...);
  void consolef(char const *format, ...);


  // Convert an external time (in seconds since 1970) to an internal time (seconds since trace started)
  double fromExternalTime(double t) { return max(0.001, t - rti->realTimeOffset); }
  // Convert an internal time (seconds since trace started) to an external time (in seconds since 1970)
  double toExternalTime(double t) { return t + rti->realTimeOffset; }

  // Schedule the update thread to run sometime soon. Safe to call from IO threads or wherever.
  void requestUpdate();

  YogaContext mkCtx(char const *desc);

  weak_ptr<Trace> trace;
  shared_ptr<RuntimeInfo> rti;
  int verbose{0};
  string engineName, label;
  WarningFilter warningFilter;
};


struct EngineRegister {
  EngineRegister(
    string const &_name,
    vector<string> const &_argTypeNames,
    std::function<bool (YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)> _create,
    bool _liveOnly=true);

  string name;
  vector<string> argTypeNames;
  std::function<bool (YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)> create;
  bool liveOnly;

  static EngineRegister *forName(string const &name);

  static map<string, EngineRegister *> *registry;
};
