#pragma once
#include "./engine.h"

#include <functional>
#include <tuple>
#include <type_traits>

struct PacketNetworkEngine;

#include "../lib/core_types.h"
#include "common/packetbuf.h"
#include "common/uv_wrappers.h"

using PacketFiller = std::function<void(packet &)>;

using PacketRpcCb = std::function<void(string const &err, packet &rx)>;

struct PacketApiCb {
  PacketApiCb(shared_ptr<PacketNetworkEngine> _engine, int _id)
    : engine(_engine), id(_id)
  {
  }
  PacketApiCb(shared_ptr<GenericEngine> _engine, int _id)
    : engine(static_pointer_cast<PacketNetworkEngine>(_engine)), id(_id)
  {
  }
  shared_ptr<PacketNetworkEngine> engine;
  int id;
  void err(string const &error);
  void ok(PacketFiller body);
  void ok();
  void stream(PacketFiller body);
};

using PacketApiFunction = std::function<
  void (packet &rx, PacketApiCb &&cb)
>;

struct PacketNetworkEngine : GenericEngine {

  PacketNetworkEngine();
  ~PacketNetworkEngine() override;
  PacketNetworkEngine(PacketNetworkEngine const &) = delete;
  PacketNetworkEngine(PacketNetworkEngine &&) = delete;
  PacketNetworkEngine &operator=(PacketNetworkEngine const &) = delete;
  PacketNetworkEngine &operator=(PacketNetworkEngine &&) = delete;

  void setupStreams();

  void afterSetStopped() override;
  void afterSetRunning() override;

  void update(EngineUpdateContext &ctx) override;

  bool isNetwork() const override { return true; }
  size_t workOutstanding() const override;

  virtual void setupHandlers();

  virtual void rxEof();
  virtual void rxPacket(packet &rx);
  void failOutstandingRpcs(string const &err);

  void txPacket(packet &tx);

  void addApiMethod(string const &method, PacketApiFunction const &f);

  void rpc(string const &method, PacketFiller fillBody, PacketRpcCb cb);

  void sendPing();

  string progress();

  UvStream *in_stream{nullptr};
  UvStream *out_stream{nullptr};
  UvStream *stderr_stream{nullptr};
  string remoteDesc;
  string stderr_buf;
  string tx_version;
  string rx_version;

  int id_cnt{42};
  // method name -> func
  std::unordered_map<string, PacketApiFunction> api;
  std::unordered_map<int, PacketRpcCb> replyCallbacks;
  int repliesOutstanding{0};
  int clientRequestsSent{0};
  int clientRepliesReceived{0};
  int clientErrorsReceived{0};
  bool streamsReady{false};

  deque<std::function<void()>> onRxEof;
  deque<std::function<void()>> onReady;

  bool noEofExit{false};
};
