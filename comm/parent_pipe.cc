#include "./parent_pipe.h"
#include "common/uv_wrappers.h"


ParentPipe::ParentPipe()
{
}

void ParentPipe::afterSetRunning()
{
  setupStdio();
  PacketNetworkEngine::afterSetRunning();
}

void ParentPipe::setupStdio()
{
  if (engineName == "GenericEngine") {
    engineName = "parent";
  }
  remoteDesc = "stdio."s + to_string(getpid());
  label = engineName + "(" + remoteDesc + ")";
  if (0) console("capturing stdio");
  in_stream = new UvStream();
  out_stream = new UvStream();
  in_stream->tty_init(0, 1);
  out_stream->tty_init(1, 0);
  setupStreams();
}
