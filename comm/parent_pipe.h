#pragma once
#include "./packet_network_engine.h"

struct ParentPipe : PacketNetworkEngine {
  ParentPipe();

  void afterSetRunning() override;

  void setupStdio();
};

