#include "./ssc_network_engine.h"

#include "common/packetbuf_types.h"
#include "../timeseq/trace.h"

SscNetworkEngine::SscNetworkEngine() : sock() {}

SscNetworkEngine::SscNetworkEngine(SscNetworkConfig const &_config) : config(_config), sock() {}

SscNetworkEngine::~SscNetworkEngine() {}

static int sscLocalPortCounter = 10001;

void SscNetworkEngine::setupSockets()
{
  if (config.localPort == "") {
    config.localPort = to_string(sscLocalPortCounter++);
  }

  uvGetAddrInfo(
    config.localHostname,
    config.localPort,
    addrinfoForUdp(),
    [this, thisp = shared_from_this()](int st, struct addrinfo *result) {
      if (st < 0) {
        throw uv_error("UvGetAddrInfo("s + config.localHostname + ":"s + config.localPort + ")"s, st);
      }
      assert(result->ai_family == PF_INET);
      auto localAddr_in = new sockaddr_in(*(sockaddr_in *)result->ai_addr);
      if (config.localHostname == "") {
        localAddr_in->sin_addr.s_addr = INADDR_ANY;
      }
      localAddr = reinterpret_cast<sockaddr *>(localAddr_in);
      localAddrDesc = sockaddr_desc(localAddr);

      uvGetAddrInfo(
        config.remoteHostname,
        config.remotePort,
        addrinfoForUdp(),
        [this, thisp](int st, struct addrinfo *result) {
          if (st < 0) {
            throw uv_error("UvGetAddrInfo("s + config.remoteHostname + ":"s + config.remotePort + ")"s, st);
          }
          if (engineStatus != Running) return;

          assert(result->ai_family == PF_INET);
          auto remoteAddr_in = new sockaddr_in(*(sockaddr_in *)result->ai_addr);
          remoteAddr = reinterpret_cast<sockaddr *>(remoteAddr_in);
          remoteAddrDesc = sockaddr_desc(remoteAddr);

          sock.udp_init();
          int bindStatus = sock.udp_bind(localAddr);
          if (bindStatus < 0) {
            throw uv_error("bind to "s + localAddrDesc, bindStatus);
          }

          sock.udp_recv_start(
            [this, thisp](ssize_t nr, const uv_buf_t *buf, sockaddr const *sa, u_int sa_len) {
              if (nr == UV_EOF) {
                rxEof();
                sock.udp_recv_stop();
              }
              else if (nr < 0) {
                throw uv_error("SscNetworkEngine("s + remoteAddrDesc + "): read udp"s, nr);
              }
              else {
                if (!rxAnyFlag) {
                  rxAnyFlag = true;
                  if (verbose >= 0) console(
                    "Receiving on ("s + localAddrDesc + 
                    " <=> "s + remoteAddrDesc + 
                    " boardInstance=" + repr(config.boardInstance) +
                    ")"s);
                }
                packet rx(reinterpret_cast<U8 const *>(buf->base), (size_t)nr);
                rxPkt(rx);
              }
            });
            if (verbose >= 0) console("Connected ("s + localAddrDesc + " <=> "s + remoteAddrDesc + ")"s);
            // Kick off updates now that we have our socket set up
            if (rti) rti->requestUpdate();
        });
    });
}

void SscNetworkEngine::afterSetRunning()
{
  setupAccessors();
  setupHandlers();
  setupSockets();
}

void SscNetworkEngine::update(EngineUpdateContext &ctx)
{
  if (!sock.is_active()) return;

  packet tx(2048);
  if (ctx.now - lastUpdateRequestTs > 0.2) {
    tx.add((U8)'U');
    lastUpdateRequestTs = ctx.now;
  }
  if (tx.size() > 0) {
    txPkt(tx);
  }
  GenericEngine::update(ctx);
}

void SscNetworkEngine::afterSetStopped()
{
  sock.udp_recv_stop();
  sock.close();
}

void SscNetworkEngine::setupHandlers()
{
  rxHandlers[(U8)'!'] = [this](double rxTime, packet &rx, bool &error) {
    string msg(reinterpret_cast<char *>(rx.rd_ptr()), rx.remaining());
    console(rxTime, msg);
    rx.get_skip(rx.remaining());
  };

  rxHandlers[(U8)'m'] = [this](double rxTime, packet &rx, bool &error) {
    string msg(reinterpret_cast<char *>(rx.rd_ptr()), rx.remaining());
    console(fromExternalTime(rxTime), msg);
    rx.get_skip(rx.remaining());
  };

  rxHandlers[(U8)'u'] = [this](double rxTime, packet &rx, bool &error) {
    U64 pktTicks;
    rx.get(pktTicks);
    while (!error && rx.remaining() > 0) {
      U8 subtype;
      rx.get(subtype);
      if (subtype == 0) {
        if (rx.remaining() > 0 && warningFilter("packetNotAtEnd")) {
          console(
            rxTime, 
            "packet not at end. Remaining="s + to_string(rx.remaining()) + 
            " from "s + remoteAddrDesc);
        }
        return;
      }

      auto &uh = updateHandlers[subtype];
      if (uh) {
        uh(rxTime, rx, pktTicks, error);
      }
      else {
        if (warningFilter("badSubtype")) {
          console(
            rxTime, 
            "bad update subtype="s + repr_02x(subtype) + 
            " from "s + remoteAddrDesc);
          rx.dump();
        }
        error = true;
      }
    }
  };
}

void SscNetworkEngine::setupAccessors()
{
}

void SscNetworkEngine::rxPkt(packet &rx)
{
  double rxTime = realtime();
  bool error = false;
  while (!error && rx.remaining() > 0) {
    U8 type;
    rx.get(type);
    auto &ph = rxHandlers[type];
    if (!ph) {
      if (warningFilter("badType")) {
        console(rxTime, "bad pkt type="s + repr_02x(type) + " from "s + remoteAddrDesc);
        rx.dump();
      }
      error = true;
    }
    else {
      ph(rxTime, rx, error);
    }
  }
  if (rti) rti->requestUpdate();
}

void SscNetworkEngine::rxEof() {}

void SscNetworkEngine::txPkt(packet &tx)
{
  if (!sock.stream) return;

  /* FIXME: this can throw if we can't reach the netowrk, such as when we're testing on a laptop.
    Handle more gracefully */
  sock.udp_send((char *)tx.ptr(), tx.size(), remoteAddr, [this](int status) {
    if (status == UV_EAI_CANCELED) {
      console("udp_send: cancelled"s);
      return;
    }
    if (status < 0) throw uv_error("udp_send", status);
  });
}

void SscNetworkEngine::sendAdminCmd(string const &s)
{
  packet tx;
  tx.add_remainder_string(s);
  txPkt(tx);
}


bool SscNetworkEngine::setNetworkConfig(YogaContext const &ctx, AstAnnoCall *spec)
{
  if (!spec->options->getValueForKey("remoteHostname", config.remoteHostname, true)) {
    return ctx.logError("Bad remoteHostname");
  }
  config.remotePort = "10000";
  if (!spec->options->getValueForKey("remotePort", config.remotePort, true)) {
    return ctx.logError("Bad remotePort");
  }
  if (!spec->options->getValueForKey("localHostname", config.localHostname, false)) {
    return ctx.logError("Bad localHostname");
  }
  if (!spec->options->getValueForKey("localPort", config.localPort, false)) {
    return ctx.logError("Bad localPort");
  }
  if (!spec->options->getValueForKey("boardInstance", config.boardInstance, true)) {
    return ctx.logError("Bad boardInstance");
  }
  return true;
}
