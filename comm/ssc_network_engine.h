#pragma once
/*
  An SscNetworkEngine provides fast real-time communication to hardware devices.
  It uses a simple binary protocol over UDP.
  See the Real-Time Networking section in the README
*/
#include "./engine.h"
struct SscNetworkEngine;

#include "./ssc_io.h"
#include "common/packetbuf.h"
#include "common/uv_wrappers.h"
#include "../timeseq/timeseq.h"

struct AstAnnoCall;

struct SscNetworkConfig {
  string remoteHostname;
  string remotePort;
  string localHostname;
  string localPort;
  int boardInstance;
};

struct SscNetworkEngine : GenericEngine {

  SscNetworkEngine();
  SscNetworkEngine(SscNetworkConfig const &_config);
  ~SscNetworkEngine() override;
  SscNetworkEngine(SscNetworkEngine const &) = delete;
  SscNetworkEngine(SscNetworkEngine &&) = delete;
  SscNetworkEngine &operator=(SscNetworkEngine const &) = delete;
  SscNetworkEngine &operator=(SscNetworkEngine &&) = delete;

  void afterSetRunning() override;
  void afterSetStopped() override;

  void update(EngineUpdateContext &ctx) override;
  bool isNetwork() const override { return true; }
  size_t workOutstanding() const override { return isRunning() ? 1 : 0; }

  void setupSockets();
  virtual void setupHandlers();
  virtual void setupAccessors();

  virtual void rxPkt(packet &rx);
  virtual void rxEof();

  void txPkt(packet &tx);
  void sendAdminCmd(string const &s);

  bool setNetworkConfig(YogaContext const &ctx, AstAnnoCall *spec);

  SscNetworkConfig config;

  UvStream sock;

  sockaddr *localAddr{nullptr};
  string localAddrDesc;
  sockaddr *remoteAddr{nullptr};
  string remoteAddrDesc;

  double lastUpdateRequestTs{0.0};
  double lastLogTs{0.0};
  bool rxAnyFlag{false};

  std::function<void(double rxTime, packet &rx, bool &error)> rxHandlers[256];
  std::function<void(double rxTime, packet &rx, U64 pktTicks, bool &error)> updateHandlers[256];
  std::function<void(packet &rx, R ts, bool &error)> updateHandlersTime[256];
};
