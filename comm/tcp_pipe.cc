#include "./tcp_pipe.h"

TcpPipe::TcpPipe(string const &_host, string const &_port)
  : host(_host),
    port(_port)
{
}

TcpPipe::~TcpPipe()
{
  if (out_stream) {
    out_stream->read_stop();
    delete out_stream;
    out_stream = nullptr;
    in_stream = nullptr;
  }

}

void TcpPipe::afterSetWarming(R &delay)
{
  setupSocket();
  delay = max(delay, 0.1);
}

void TcpPipe::setupSocket()
{
  out_stream = in_stream = new UvStream();
  out_stream->tcp_init();
  out_stream->tcp_nodelay(true);
  if (verbose) console("tcp setup");
  uvGetAddrInfo(host, port, addrinfoForTcp(), [this, thisp = shared_from_this()](int status, struct addrinfo *result) {
    if (status < 0) throw uv_error("UvGetAddrInfo("s + host + ":" + port + ")", status);
    if (verbose) console("got addresses");

    assert(result->ai_family == PF_INET);
    auto remoteAddr = new sockaddr_in(*(sockaddr_in *)result->ai_addr);

    out_stream->tcp_connect(reinterpret_cast<sockaddr *>(remoteAddr), 
      [this, thisp = shared_from_this()](int rc) {
        if (rc < 0) {
          cerr << "Connect to " + host + ":" + port + ": " + uv_strerror(rc) + "\n";
          rxEof();
          return;
        }
        if (verbose) console("connected");
        setupStreams();
      });
    int timeoutRc = out_stream->set_tcp_timeout(5000);
    if (timeoutRc < 0) cerr << "Warning: " + host + ":" + port + ": set_tcp_timeout: " + uv_strerror(timeoutRc) + "\n";

  });
}
