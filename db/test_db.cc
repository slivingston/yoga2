#include "./yoga_db.h"
#include "./yoga_layout.h"
#include "../lib/scope_math.h"
#include "../test/test_utils.h"
#include "common/uv_wrappers.h"

TEST_CASE("Database API", "[db]") {

  string testFn = YogaLayout::instance().dataDir + "/test-traces.sqlite";
  unlink(testFn.c_str());
  auto db = make_shared<YogaDb>(testFn);
  if (0) db->setupTables();

  SECTION("trace name parsing") {
    YogaDbTraceInfoRow it;
    it.name = "wug_20190607_115424_dbb";
    REQUIRE(it.parseTraceName());
    CHECK(it.script == "wug"s);
    CHECK(it.date == "20190607_115424"s);
    CHECK(it.sortKey == "20190607_115424 wug "s);
  }

  SECTION("trace info write/readback from db") {
    YogaDbTraceInfoRow written("test_20190607_115424_dbb");
    db->addTraceInfo(written);

    YogaDbTraceInfoRow readback;
    REQUIRE(db->getTraceInfo(readback, written.name));
    CHECK(written.name == readback.name);
    CHECK(written.series == readback.series);
    CHECK(written.date == readback.date);
    CHECK(written.script == readback.script);
    CHECK(written.sortKey == readback.sortKey);
    CHECK(written.flags == readback.flags);
  }

  SECTION("trace info lastSeen write/readback from db") {
    YogaDbLastSeen abcnice;
    abcnice.name = "test_20190000_123456_abc";
    abcnice.part = "traceDir";
    abcnice.host = "nice";
    abcnice.dir = "/home/tlb/traces";
    db->addLastSeen(abcnice);

    YogaDbLastSeen abcnimes;
    abcnimes.name = "test_20190000_123456_abc";
    abcnimes.part = "traceDir";
    abcnimes.host = "nimes";
    abcnimes.dir = "/home/tlb/traces";
    db->addLastSeen(abcnimes);

    vector<YogaDbLastSeen> all;
    REQUIRE(db->getLastSeen(all, abcnice.name));
    REQUIRE(all.size() == (size_t)2);
  }

  SECTION("reading nonexistent lastSeen") {
    vector<YogaDbLastSeen> all;
    REQUIRE(db->getLastSeen(all, "nonexistent"));
    REQUIRE(all.size() == (size_t)0);
  }

  SECTION("Creating extract") {
    YogaDbExtractRow row{"script1", "foobar", "test_20190000_123456_abc", 0, 15.0};
    db->addExtract(row);
  
    vector<YogaDbExtractRow> all;
    all = db->getExtractsByScriptAndFunc("script1", "foobar");
    REQUIRE(all.size() == (size_t)1);
    CHECK(all[0].traceName == "test_20190000_123456_abc"s);
    CHECK(all[0].startTs == 0.0);
    CHECK(all[0].endTs == 15.0);
  }

}

