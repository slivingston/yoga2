#include "./yoga_db.h"
#include <sqlite3.h>
#include "../timeseq/trace.h"
#include "common/packetbuf_types.h"
#include "common/uv_wrappers.h"
#include <regex>
#include <dirent.h>
#include "common/asyncjoy.h"
#include "nlohmann-json/json.hpp"
#include "../jit/context.h"
#include "../comm/tcp_pipe.h"

string canonTraceName(string const &name)
{
  if (startsWith(name, "traces/")) return name.substr(7);
  return name;
}

YogaDb::YogaDb(string const &_fn)
  : fn(_fn)
{
  int accessRc = access(fn.c_str(), R_OK);

  int rc = sqlite3_open(fn.c_str(), &db);
  if (rc) {
    throw runtime_error("Can't open "s + fn + ": "s + sqlite3_errmsg(db));
    sqlite3_close(db);
  }
  if (accessRc < 0) {
    if (!setupTables()) {
      throw runtime_error("Can't setup tables in " + fn + ": "s + sqlite3_errmsg(db));
    }
  }
}

YogaDb &YogaDb::instance()
{
  static YogaDb *db = new YogaDb(YogaLayout::instance().dataDir + "/traces.sqlite");
  return *db;
}


struct YogaDbStmt {
  YogaDbStmt(sqlite3 *_db, string const &_cmd)
    :db(_db), cmd(_cmd)
  {
    auto rc = sqlite3_prepare_v2(db, cmd.c_str(), cmd.size()+1, &stmt, nullptr);
    if (rc != SQLITE_OK) throw runtime_error("sqlite3_prepare_v2 error "s + sqlite3_errmsg(db));
  }

  ~YogaDbStmt()
  {
    auto rc = sqlite3_finalize(stmt);
    if (rc != SQLITE_OK) {
      cerr << "sqlite3_finalize error " + repr(rc) + "\n";
    }
  }

  sqlite3 *db{nullptr};
  string cmd;
  sqlite3_stmt *stmt{nullptr};

  void getCol(string &col, int id)
  {
    auto t = sqlite3_column_type(stmt, id);
    if (t == SQLITE_BLOB) {
      auto size = sqlite3_column_bytes(stmt, id);
      if (size == 0) {
        col.clear();
        return;
      }
      auto *buf = sqlite3_column_blob(stmt, id);
      col.assign((char *)buf, size);
      return;
    }
    else if (t == SQLITE_TEXT) {
      auto size = sqlite3_column_bytes(stmt, id);
      if (size == 0) {
        col.clear();
        return;
      }
      auto *buf = sqlite3_column_text(stmt, id);
      col.assign((char *)buf, size);
      return;
    }
    else {
      throw runtime_error("getCol "s + to_string(id) + ": unhandled type=" + to_string(t));
    }
  }

  void getCol(int &col, int id)
  {
    col = sqlite3_column_int(stmt, id);
  }

  void getCol(double &col, int id)
  {
    col = sqlite3_column_double(stmt, id);
  }

  bool getResultRow(YogaDbTraceInfoRowNameFlags &r)
  {
    getCol(r.name, 0);
    getCol(r.flags, 1);
    return true;
  }

  bool getResultRow(YogaDbTraceInfoRow &r)
  {
    getCol(r.name, 0);
    getCol(r.series, 1);
    getCol(r.date, 2);
    getCol(r.script, 3);
    getCol(r.sortKey, 4);
    getCol(r.flags, 5);
    return true;
  }

  bool getResultRow(YogaDbVisInfoRow &r)
  {
    getCol(r.name, 0);
    getCol(r.visible, 1);
    return true;
  }

  bool getResultRow(YogaDbLastSeen &r)
  {
    getCol(r.name, 0);
    getCol(r.part, 1);
    getCol(r.host, 2);
    getCol(r.dir, 3);
    return true;
  }

  bool getResultRow(YogaDbLastTraceRow &r)
  {
    getCol(r.script, 0);
    getCol(r.name, 1);
    return true;
  }

  bool getResultRow(YogaDbExtractRow &r)
  {
    getCol(r.script, 0);
    getCol(r.funcName, 1);
    getCol(r.traceName, 2);
    getCol(r.startTs, 3);
    getCol(r.endTs, 4);
    return true;
  }


  void bind(int id, string const &value)
  {
    int rc = sqlite3_bind_text(stmt, id, value.c_str(), value.size(), SQLITE_STATIC);
    if (rc != SQLITE_OK) throw runtime_error("sqlite3_bind_text");
  }
  void bind(int id, int value)
  {
    int rc = sqlite3_bind_int(stmt, id, value);
    if (rc != SQLITE_OK) throw runtime_error("sqlite3_bind_int");
  }

  template<typename T>
  bool getAllResults(vector<T> &rows)
  {
    while (true) {
      int rc = sqlite3_step(stmt);
      if (rc == SQLITE_DONE) {
        break;
      }
      else if (rc == SQLITE_ROW) {
        T r;
        bool ok = getResultRow(r);
        if (!ok) return false;
        rows.push_back(r);
      }
      else {
        throw runtime_error("sqlite3_step: error code "s + to_string(rc));
      }
    }

    return true;
  }

  template<typename T>
  bool getOneResult(T &row)
  {
    int rc = sqlite3_step(stmt);
    if (rc == SQLITE_DONE) {
      return false;
    }
    else if (rc == SQLITE_ROW) {
      return getResultRow(row);
    }
    else {
      throw runtime_error("sqlite3_step: error code "s + to_string(rc));
    }
  }

  bool run()
  {
    int rc = sqlite3_step(stmt);
    if (rc == SQLITE_DONE) {
      return true;
    }
    else {
      throw runtime_error("sqlite3_step: error code "s + to_string(rc));
    }
  }

};

/*
  TraceFlags
*/

bool YogaDb::getMatchingTraceNames(vector<YogaDbTraceInfoRowNameFlags> &ret, string const &nameLike, int startIndex, int maxCount)
{
  YogaDbStmt stmt(db, "SELECT name, flags FROM traceInfo WHERE name LIKE $nameLike ORDER BY sortKey DESC LIMIT $maxCount OFFSET $startIndex;");
  stmt.bind(1, nameLike);
  stmt.bind(2, maxCount);
  stmt.bind(3, startIndex);

  ret.clear();
  return stmt.getAllResults(ret);
}

bool YogaDb::getLatestTraceName(string &ret, string const &nameLike)
{
  vector<YogaDbTraceInfoRowNameFlags> names;
  if (!getMatchingTraceNames(names, nameLike, 0, 1)) return false;
  if (names.size() < 1) return false;
  ret = names[0].name;
  return true;
}

bool YogaDb::updateTraceFlags(string const &name, string const &flags)
{
  YogaDbStmt stmt(db, "UPDATE traceInfo SET flags = $flags WHERE (name = $name);");
  stmt.bind(1, name);
  stmt.bind(2, flags);
  return stmt.run();
}

/*
  TraceInfo
*/

YogaDbTraceInfoRow::YogaDbTraceInfoRow()
{
}

YogaDbTraceInfoRow::YogaDbTraceInfoRow(string const &_name, string const &_flags)
  : name(_name), flags(_flags)
{
  parseTraceName();
}

ostream & operator <<(ostream &s, const YogaDbTraceInfoRow &a)
{
  s << 
    "TraceInfo(name=" << shellEscape(a.name) <<
    " series=" << shellEscape(a.series) <<
    " date=" << shellEscape(a.date) <<
    " script=" << shellEscape(a.script) <<
    " sortKey=" << shellEscape(a.sortKey) <<
    " flags=" << shellEscape(a.flags) <<
    ")";
  return s;
}

bool YogaDb::getTraceInfo(YogaDbTraceInfoRow &ret, string const &name)
{
  YogaDbStmt stmt(db, "SELECT name, series, date, script, sortKey, flags FROM traceInfo WHERE name = $name;");
  stmt.bind(1, name);
  return stmt.getOneResult(ret);
}

bool YogaDb::addTraceInfo(YogaDbTraceInfoRow const &it)
{
  YogaDbStmt stmt(db, "INSERT OR REPLACE INTO traceInfo VALUES($name, $series, $date, $script, $sortKey, $flags);");
  stmt.bind(1, it.name);
  stmt.bind(2, it.series);
  stmt.bind(3, it.date);
  stmt.bind(4, it.script);
  stmt.bind(5, it.sortKey);
  stmt.bind(6, it.flags);
  return stmt.run();
}

/*
  Extract
*/

ostream & operator <<(ostream &s, const YogaDbExtractRow &a)
{
  return s << 
    "Extract(script=" << shellEscape(a.script) <<
    " funcName=" << shellEscape(a.funcName) <<
    " traceName=" << shellEscape(a.traceName) <<
    " startTs=" << repr(a.startTs) <<
    " endTs=" << repr(a.endTs) <<
    ")";
}

bool YogaDb::addExtract(YogaDbExtractRow const &it)
{
  YogaDbStmt stmt(db, "INSERT OR REPLACE INTO extract VALUES($script, $funcName, $traceName, $startTs, $endTs);");
  stmt.bind(1, it.script);
  stmt.bind(2, it.funcName);
  stmt.bind(3, it.traceName);
  stmt.bind(4, it.startTs);
  stmt.bind(5, it.endTs);
  return stmt.run();
}

vector<YogaDbExtractRow> YogaDb::getExtractsByScriptAndFunc(string const &script, string const &funcName)
{
  YogaDbStmt stmt(db, "SELECT script, funcName, traceName, startTs, endTs FROM extract WHERE script = $script AND funcName = $funcName;");
  stmt.bind(1, script);
  stmt.bind(2, funcName);
  vector<YogaDbExtractRow> ret;
  stmt.getAllResults(ret);
  return ret;
}


/*
  VisInfo
*/

YogaDbVisInfoRow::YogaDbVisInfoRow()
{
}

YogaDbVisInfoRow::YogaDbVisInfoRow(string const &_name, string const &_visible)
  : name(_name), visible(_visible)
{
}

ostream & operator <<(ostream &s, const YogaDbVisInfoRow &a)
{
  return s <<
    "LastSeen(name=" << shellEscape(a.name) <<
    " visible=" << shellEscape(a.visible) <<
    ")";
}

bool YogaDb::getVisInfo(YogaDbVisInfoRow &ret, string const &name)
{
  YogaDbStmt stmt(db, "SELECT name, visible FROM visInfoByName WHERE name = $name;");
  stmt.bind(1, name);
  return stmt.getOneResult(ret);
}

bool YogaDb::addVisInfoByName(YogaDbVisInfoRow const &it)
{
  YogaDbStmt stmt(db, "INSERT OR REPLACE INTO visInfoByName VALUES($name, $visible);");
  stmt.bind(1, it.name);
  stmt.bind(2, it.visible);
  return stmt.run();
}

/*
  LastTrace
*/

YogaDbLastTraceRow::YogaDbLastTraceRow()
{
}

YogaDbLastTraceRow::YogaDbLastTraceRow(string const &_script, string const &_name)
  : script(_script), name(_name)
{
}

ostream & operator <<(ostream &s, const YogaDbLastTraceRow &a)
{
  return s <<
    "LastTrace(script=" << shellEscape(a.script) <<
    " name=" << shellEscape(a.name) <<
    ")";
}


bool YogaDb::getLastTraceByScript(YogaDbLastTraceRow &ret, string const &script)
{
  YogaDbStmt stmt(db, "SELECT script, name FROM lastTraceByScript WHERE script = $script;");
  stmt.bind(1, script);
  return stmt.getOneResult(ret);
}

bool YogaDb::addLastTraceByScript(YogaDbLastTraceRow const &it)
{
  YogaDbStmt stmt(db, "INSERT OR REPLACE INTO lastTraceByScript VALUES($script, $name);");
  stmt.bind(1, it.script);
  stmt.bind(2, it.name);
  return stmt.run();
}


/*
  LastSeen
*/

ostream & operator <<(ostream &s, const YogaDbLastSeen &a)
{
  return s <<
    "LastSeen(name=" << shellEscape(a.name) <<
    " part=" << shellEscape(a.part) <<
    " host=" << shellEscape(a.host) <<
    " dir=" << shellEscape(a.dir) <<
    ")";
}

bool YogaDb::getLastSeen(vector<YogaDbLastSeen> &ret, string const &name)
{
  YogaDbStmt stmt(db, "SELECT name, part, host, dir FROM lastSeen WHERE name = $name;");
  stmt.bind(1, name);
  return stmt.getAllResults(ret);
}


bool YogaDb::getLastSeen(vector<YogaDbLastSeen> &ret, string const &name, string const &part)
{
  YogaDbStmt stmt(db, "SELECT name, part, host, dir FROM lastSeen WHERE name = $name and part = $part;");
  stmt.bind(1, name);
  stmt.bind(2, part);
  return stmt.getAllResults(ret);
}

bool YogaDb::addLastSeen(YogaDbLastSeen const &it)
{
  YogaDbStmt stmt(db, "INSERT OR REPLACE INTO lastSeen VALUES($name, $part, $host, $dir);");
  stmt.bind(1, it.name);
  stmt.bind(2, it.part);
  stmt.bind(3, it.host);
  stmt.bind(4, it.dir);
  return stmt.run();
}


bool YogaDb::addLastSeen(vector<YogaDbLastSeen> const &its)
{
  for (auto &it : its) {
    if (!addLastSeen(it)) return false;
  }
  return true;
}

bool YogaDb::setupTables()
{
  if (!YogaDbStmt(db, R"(
      CREATE TABLE traceInfo (
          name TEXT NOT NULL,
          series,
          date,
          script,
          sortKey,
          flags,
          PRIMARY KEY(name));
      )").run()) return false;
  if (!YogaDbStmt(db, R"(
      CREATE TABLE visInfoByName (
          name TEXT NOT NULL, 
          visible BLOB, 
          PRIMARY KEY(name));
      )").run()) return false;
  if (!YogaDbStmt(db, R"(
      CREATE TABLE lastTraceByScript (
          script TEXT NOT NULL,
          name TEXT NOT NULL,
          PRIMARY KEY(script));
      )").run()) return false;
  if (!YogaDbStmt(db, R"(
      CREATE TABLE lastSeen (
          name TEXT NOT NULL,
          part,
          host,
          dir);
      )").run()) return false;
  if (!YogaDbStmt(db, R"(
      CREATE TABLE extract (
          script TEXT NOT NULL,
          funcName TEXT NOT NULL,
          traceName TEXT NOT NULL,
          startTs REAL,
          endTs REAL);
      )").run()) return false;
  if (!YogaDbStmt(db, R"(
      CREATE INDEX traceInfoByName ON traceInfo(name);
      )").run()) return false;
  if (!YogaDbStmt(db, R"(
      CREATE INDEX lastSeenByName ON lastSeen(name);
      )").run()) return false;
  return true;
}

vector<YogaDbTraceInfoRow> YogaDb::scanTracesDir()
{
  vector<YogaDbTraceInfoRow> ret;
  
  auto tracesDir = YogaLayout::instance().tracesDir;
  DIR *dir = opendir(tracesDir.c_str());
  if (!dir) {
    cerr << "scan("s + tracesDir + "): " + strerror(errno) + "\n";
    return ret;
  }
  while (1) {
    struct dirent *ent = readdir(dir);
    if (!ent) break;
    if (ent->d_name[0] == '.') continue;

    YogaDbTraceInfoRow r(ent->d_name, ""s);
    if (r.sortKey.size()) {
      ret.push_back(r);
    }
  }
  closedir(dir);
  return ret;
}

bool YogaDb::rebuildTracesTable()
{
  auto &layout = YogaLayout::instance();
  auto all = scanTracesDir();
  for (auto &it : all) {
    addTraceInfo(it);
    YogaDbLastSeen lastSeen;
    lastSeen.name = it.name;
    lastSeen.part = "traceDir";
    lastSeen.host = layout.localHostName;
    lastSeen.dir = layout.tracesDir;
    addLastSeen(lastSeen);
  }
  return true;
}

bool YogaDbTraceInfoRow::parseTraceName()
{
  std::smatch m;
  regex re1("(moves|c3d)_([a-z0-9_]+)");
  if (regex_match(name, m, re1)) {
    if (m.size() != 3) throw runtime_error("parseTraceName: bad match size");
    script = m[1].str();
    series = m[2].str();
    date = "";
    sortKey = date + " " + script + " " + series;
    return true;
  }

  regex re2("([a-zA-Z0-9_]+)_([0-9]+_[0-9]+)_([a-z]+)");
  if (regex_match(name, m, re2)) {
    if (m.size() != 4) throw runtime_error("parseTraceName: bad match size");
    
    script = m[1].str();
    series = "";
    date = m[2].str();
    string suffix = m[3].str();
    sortKey = date + " " + script + " " + series;
    return true;
  }
  return false;
}

// -------------


void YogaDb::ensureTraceLocal(
  string const &traceName, 
  std::function<void(string const &err)> const &cb,
  std::function<void(string const &status)> const &setStatus)
{
  workMutexes["local." + traceName].withMutex([this, traceName, cb, setStatus](std::function<void()> release) {
    auto &layout = YogaLayout::instance();

    vector<YogaDbLastSeen> lastSeenTraceDirs;
    if (!getLastSeen(lastSeenTraceDirs, traceName, "traceDir")) {
      release();
      return cb("db->getLastSeen(" + traceName + ", traceDir) failed");
    }

    vector<YogaDbLastSeen> lastSeenBlobs;
    if (!getLastSeen(lastSeenBlobs, traceName, "blob")) {
      release();
      return cb("db->getLastSeen(" + traceName + ", blob) failed");
    }

    YogaDbLastSeen closest;
    for (auto &lastSeen : lastSeenTraceDirs) {
      if (layout.isLocalhost(lastSeen.host)) {
        release();
        return cb("");
      }
      else {
        if (closest.host.empty()) closest = lastSeen;
      }
    }
    if (closest.host.empty()) {
      release();
      return cb("Trace " + traceName + " not found in lastSeen");
    }

    auto traceDir = layout.tracesDir + "/" + traceName;
    if (!access((traceDir + "/manifest.json").c_str(), F_OK)) {
      release();
      return cb("");
    }

    auto traceDirTmp = traceDir + ".fetch" + repr(getpid());

    if (mkdir(traceDirTmp.c_str(), 0777) < 0) {
      release();
      return cb("mkdir "s + traceDirTmp + ": " + strerror(errno));
    }

    auto remotes = layout.mkRpcPool("fetchd");

    auto aj = make_shared<AsyncJoy>([cb, release, remotes, traceDir, traceDirTmp](string const &err) {
      remotes->stopAll();
      if (!err.empty()) {
        release();
        cerr << "ensureTraceLocal: " + err + "\n";
        return cb(err);
      }
      else {
        cerr << "ensureTraceLocal: done\n";
        if (rename(traceDirTmp.c_str(), traceDir.c_str()) < 0) {
          return cb("rename to " + traceDir + ": " + strerror(errno));
        }
        release();
        return cb("");
      }
    });


    setStatus("Fetching\n" + remotes->progress());
    auto rem = remotes->get(closest.host);
    if (!rem) {
      aj->error("No connection to " + closest.host);
      return;
    }
    aj->start();
    rem->rpc("fetchTraceInfo", 
      [&traceName](packet &tx) {
        tx.add(traceName);
      },
      [this, aj, remotes, traceName, traceDirTmp, closest, setStatus](string const &err, packet &rx) {
        if (!err.empty()) {
          return aj->error(err);
        }

        string manifestJson;
        rx.get(manifestJson);

        json manifest = json::parse(manifestJson);
        for (auto tsInfo : manifest.value("timeseqInfos", json::array())) {
          if (!tsInfo.value("isEmpty", false)) {
            auto seqName = tsInfo.value("name", "");

            setStatus("Fetching\n" + remotes->progress());
            aj->start();
            fetchTraceSeq(remotes, closest.host, traceDirTmp, traceName, seqName, 0,
              [aj, remotes, setStatus](string const &err) {
                if (!err.empty()) {
                  return aj->error(err);
                }
                aj->end();
                setStatus("Fetching\n" + remotes->progress());
              });
          }
        }
        for (auto blobInfo : manifest.value("blobLocations", json::array())) {
          string blobFn = blobInfo.value("fn", "");
          string blobHost = blobInfo.value("host", "");
          if (blobFn.empty()) {
            return aj->error("Bad blobLocations entry");
          }
          U64 chunkId = 0;
          if (endsWith(blobFn, ".data") && blobFn.size() > 5+16) {
            auto chunkStr = blobFn.substr(blobFn.size()-16-5, 16);
            if (0) cerr << "Mapped " + blobFn + " to " + chunkStr + "\n";
            chunkId = strtoull(chunkStr.c_str(), nullptr, 16);
          }
          else {
            return aj->error("Bad blobLocations entry: "s + blobFn);
          }
          aj->start();
          setStatus("Fetching\n" + remotes->progress());
          fetchBlobFile(remotes, blobHost, fn, chunkId,
            [aj, remotes, setStatus](string const &err) {
              if (!err.empty()) {
                return aj->error(err);
              }
              setStatus("Fetching\n" + remotes->progress());
              aj->end();
            });
        }

        writeFile(traceDirTmp + "/manifest.json", manifestJson);
        aj->end();
      });
    aj->end();
  });
}


void YogaDb::fetchTraceSeq(shared_ptr<YogaRpcPool> pool,
    string const &host,
    string const &traceDirTmp,
    string const &traceName,
    string const &seqName,
    U64 ofs,
    std::function<void(string const &err)> cb) 
{
  auto rem = pool->get(host);
  if (!rem) {
    return cb("No connection to " + host);
  }
  rem->rpc("fetchTraceSeq",
    [&traceName, &seqName, ofs](packet &tx) {
      tx.add(traceName);
      tx.add(seqName);
      tx.add(ofs);
    },
    [pool, host, traceDirTmp, traceName, seqName, ofs, cb, this](string const &err, packet &rx) {
      if (!err.empty()) {
        cb(err);
        return;
      }
      auto fn = traceDirTmp + "/seq_" + seqName + ".bin.gz";
      int fd = open(fn.c_str(), O_WRONLY|(ofs ? 0 : (O_CREAT|O_TRUNC)), 0666);
      if (fd < 0) return cb(fn + ": " + strerror(errno));

      U64 fileSize=0;
      rx.get(fileSize);
      Blob tmpdata;
      rx.get(tmpdata);

      int nw = pwrite(fd, tmpdata.data(), tmpdata.size(), ofs);
      if (nw < 0) {
        cb(fn + ": " + strerror(errno));
        close(fd);
        return;
      }
      else if (nw != tmpdata.size()) {
        cb(fn + ": partial write " + repr(nw) + " / " + repr(tmpdata.size()));
        close(fd);
        return;
      }

      cerr << "Fetched seq " + fn + " " + repr(nw) + " / " + repr(fileSize) + "\n";
      close(fd);

      if (fileSize > ofs + tmpdata.size()) {
        fetchTraceSeq(pool, host, traceDirTmp, traceName, seqName, ofs + tmpdata.size(), cb);
      }
      else {
        cb("");
      }
    });
}

void YogaDb::fetchBlobFile(shared_ptr<YogaRpcPool> pool,
    string const &host,
    string const &fn,
    U64 chunkId,
    std::function<void(string const &err)> cb)
{
  auto rem = pool->get(host);
  rem->rpc("fetchBlobFile",
    [chunkId](packet &tx) {
      tx.add(chunkId);
    },
    [chunkId, cb](string const &err, packet &rx) {
      if (!err.empty()) {
        cb(err);
        return;
      }
      Blob tmpdata;
      rx.get(tmpdata);

      if (!saveBlobFile(chunkId, tmpdata)) {
        return cb("saving blob failed");
      }

      cerr << "Fetched blob " + repr_016x(chunkId) + " " + repr(tmpdata.size()) + "\n";

      return cb("");
    });
}




bool YogaDb::loadTrace(string const &traceName,
  shared_ptr<YogaCompilation> reg,
  std::function<void(string const &err, shared_ptr<Trace> const &trace, json const &manifest)> cb,
  std::function<void(string const &status)> const &setStatus)
{
  workMutexes["load." + traceName].withMutex([this, reg, traceName, cb, setStatus](std::function<void()> release) {
    setStatus("Checking");
    ensureTraceLocal(traceName, [cb, setStatus, traceName, reg, release](string const &err) {
      if (!err.empty()) {
        release();
        return cb(err, nullptr, json());
      }
      setStatus("Loading");
      uvUiActive = true;

      auto newTrace = make_shared<Trace>(reg);
      newTrace->load(traceName, [newTrace, cb, setStatus, traceName, release](string const &err, json const &manifest) {
        uvUiActive = true;
        if (err.size()) {
          cerr << "Loading "s + traceName + ": " + err + "\n";
          release();
          return cb(err, newTrace, json());
        }
        else {
          setStatus("");
          release();
          cb("", newTrace, manifest);
        }
      });
    }, setStatus);
  });
  return true;
}


// --




namespace packetio {
  string packet_get_typetag(YogaDbLastSeen const &x)
  {
    return "YogaDbLastSeen:1";
  }
  void packet_wr_value(packet &p, YogaDbLastSeen const &x)
  {
    packetio::packet_wr_value(p, x.name);
    packetio::packet_wr_value(p, x.part);
    packetio::packet_wr_value(p, x.host);
    packetio::packet_wr_value(p, x.dir);
  }
  void packet_rd_value(packet &p, YogaDbLastSeen &x)
  {
    packetio::packet_rd_value(p, x.name);
    packetio::packet_rd_value(p, x.part);
    packetio::packet_rd_value(p, x.host);
    packetio::packet_rd_value(p, x.dir);
  }
  std::function<void(packet &, YogaDbLastSeen &)> packet_rd_value_compat(YogaDbLastSeen const &x, string const &typetag)
  {
    if (typetag == "YogaDbLastSeen:1") {
      return static_cast<void(*)(packet &, YogaDbLastSeen &)>(packetio::packet_rd_value);
    }
    return nullptr;
  }
}

INSTANTIATE_PACKETIO(YogaDbLastSeen);
INSTANTIATE_PACKETIO(vector<YogaDbLastSeen>);


void YogaDb::addLocalLastSeen(vector<YogaDbLastSeen> &lastSeen, Trace *trace)
{
  {
    YogaDbLastSeen it;
    it.name = trace->rti->traceName;
    it.part = "traceDir";
    it.host = YogaLayout::instance().localHostName;
    it.dir = myRealpath(trace->rti->traceDir);
    lastSeen.push_back(it);
  }
  for (auto &chunkIt : trace->blobLocationsByChunkId) {
    YogaDbLastSeen it;
    it.name = trace->rti->traceName;
    it.part = "blob";
    it.host = chunkIt.second.first;
    it.dir = chunkIt.second.second;
    lastSeen.push_back(it);
  }
}

void YogaDb::addTrace(Trace *trace)
{
  vector<YogaDbLastSeen> lastSeen;
  addLocalLastSeen(lastSeen, trace);
  addTraceInfo(YogaDbTraceInfoRow(trace->rti->traceName));
  addLastSeen(lastSeen);
  for (auto &it : lastSeen) {
    cerr << repr(it) + "\n"; 
  }
}
