#include "./yoga_layout.h"
#include "../jit/jit_utils.h"
#include "../build.src/path_config.h"
#include "../comm/tcp_pipe.h"
#include "../comm/child_pipe.h"
#include <pwd.h>
#include "nlohmann-json/json.hpp"

int YogaLayout::ignoreUserConfig = 0;

ostream & operator <<(ostream &s, const YogaLayout &a)
{
  return s <<
    "YogaLayout(homeDir="s << shellEscape(a.homeDir) <<
    " runDir=" << shellEscape(a.runDir) <<
    " tracesDir=" << shellEscape(a.tracesDir) <<
    " yogaDir=" << shellEscape(a.yogaDir) <<
    " tlbcoreDir=" << shellEscape(a.tlbcoreDir) <<
    " config=" << a.config->dump() <<
    ")";
}

static json readJsonFile(string const &fn)
{
  int fd = open(fn.c_str(), O_RDONLY, 0);
  if (fd < 0) {
    if (errno == ENOENT) {
      return json::object();
    }
    throw runtime_error(fn + ": " + strerror(errno));
  }
  string jsonStr = readFile(fd, fn);
  close(fd);
  json ret;
  try {
    ret = json::parse(jsonStr);
  }
  catch (nlohmann::detail::parse_error err) {
    throw runtime_error("Error parsing " + fn);
  }

  return ret;
}


YogaLayout & YogaLayout::instance()
{
  static YogaLayout l;
  static std::once_flag init;

  call_once(init, []() {
    string failMsg;

    struct passwd *pw = getpwuid(getuid());
    if (!pw) throw runtime_error("getpwuid: "s + strerror(errno));

    {
      auto s = getenv("HOME");
      if (!s) {
        s = pw->pw_dir;
      }
      l.homeDir = s;
    }

    l.localUser = pw->pw_name;

    {
      char buf[1024];
      if (!getcwd(buf, sizeof(buf))) throw runtime_error("getcwd: "s + strerror(errno));
      l.runDir = myRealpath(buf);
    }

    // Somehow vscode puts us in the /bin dir, despite an explicit cwd in launch.json
    if (l.runDir.substr(l.runDir.size()-4) == "/bin") {
      l.runDir = l.runDir.substr(0, l.runDir.size()-4);
      chdir(l.runDir.c_str());
    }
    if (l.runDir.empty()) failMsg = "Can't find current directory"s;

    l.yogaDir = YOGA_DIR;


    l.tracesDir = myRealpath(l.yogaDir + "/traces");
    if (failMsg.empty() && l.tracesDir.empty()) failMsg = "No traces directory under "s + l.yogaDir;
    l.dataDir = myRealpath(l.yogaDir + "/data");
    if (failMsg.empty() && l.dataDir.empty()) failMsg = "No data directory under "s + l.yogaDir;

    l.yogaImagesDir = myRealpath(l.yogaDir + "/images");
    if (failMsg.empty() && l.yogaImagesDir.empty()) failMsg = "No images directory under "s + l.yogaDir;

    l.yogaBinDir = myRealpath(l.yogaDir + "/bin");
    if (failMsg.empty() && l.yogaBinDir.empty()) failMsg = "No bin directory under "s + l.yogaDir;

    l.tlbcoreDir = myRealpath(l.yogaDir + "/deps/tlbcore");
    if (failMsg.empty() && l.tlbcoreDir.empty()) failMsg = "No deps/tlbcore directory "s + l.yogaDir;
    l.tlbcoreImagesBase = myRealpath(l.tlbcoreDir + "/web/images");

    {
      char buf[1024];
      if (gethostname(buf, sizeof(buf)) < 0) throw runtime_error("gethostname: "s + strerror(errno));
      l.localHostName = buf;
    }

    l.includeMap["tlbcore"] = l.tlbcoreDir;
    l.includeMap["yoga"] = l.yogaDir;
    l.includeMap["run"] = l.runDir;

    if (!failMsg.empty()) {
      throw runtime_error(failMsg);
    }

  
    auto config = readJsonFile(l.yogaDir + "/config.json");
    if (!ignoreUserConfig) {
      auto userConfig = readJsonFile(l.homeDir + "/.yoga/config.json");
      config.merge_patch(userConfig);
    }
    l.config = make_shared<json>(config);

    if (0) cerr << "layout="s + repr(l) + "\n";

  });

  return l;
}

bool YogaLayout::isLocalhost(string const &host) const
{
  return (host.empty() || host == "localhost" || host == localHostName);
}

vector<string> YogaLayout::sshifyArgs(vector<string> execArgs, string const &sshHost) const
{
  if (sshHost.empty() || sshHost == "localhost" || sshHost == localHostName) {
    return execArgs;
  }

  string relDir = relativePath(homeDir, runDir);

  auto argsFixed = execArgs;

  for (auto &a : argsFixed) {
    if (startsWith(a, "/")) {
      a = relativePath(runDir, a);
    }
  }

  vector<string> ret;

  ret.push_back("ssh");
  ret.push_back("-o");
  ret.push_back("ForwardAgent=yes");
  ret.push_back(sshHost);
  ret.push_back("source /etc/profile ; source .profile ; cd " + shellEscape(relDir) + " && " + shellEscape(argsFixed));

  return ret;
}


tuple<string, bool> YogaLayout::requirePath(string const &requireArg, string const &hostFn) const
{
  auto fpc = firstPathComponent(requireArg);
  if (0) cerr << "getRequirePath: fpc=" + shellEscape(fpc) + "\n";
  if (fpc == "."s) {
    auto base = myDirname(myRealpath(hostFn));
    return make_tuple(myRealpath(base + requireArg.substr(1)), false);
  }
  else if (fpc == ".."s) {
    auto base = myDirname(myRealpath(hostFn));
    auto p2Munged = requireArg;
    while (firstPathComponent(p2Munged) == ".." && base != "/") {
      base = myDirname(base);
      p2Munged = p2Munged.substr(3);
    }
    return make_tuple(myRealpath(base + "/" + p2Munged), false);
  }
  else {
    auto dir = includeMap.find(fpc);
    if (dir != includeMap.end()) {
      auto tryPath = dir->second + requireArg.substr(fpc.size());
      if (0) cerr << "getRequirePath: mapped to " + shellEscape(tryPath) + "\n";
      return make_tuple(tryPath, true);
    }
  }
  return make_tuple(requireArg, false);
}

string YogaLayout::blobFilename(U64 chunkId) const
{
  // "zblob_" because blob files are usually big, and when rsyncing we want them to go after
  // "seq_" files
  // Should match trace_blobs.cc:parseChunkId/fmtChunkId
  char chunkIdHex[128];
  snprintf(chunkIdHex, sizeof(chunkIdHex), "zblob.%016llx.data", (unsigned long long)chunkId);
  return yogaDir + "/blobs/"s + chunkIdHex;
}


shared_ptr<YogaRpcPool> YogaLayout::mkRpcPool(string const &service) const
{
  return make_shared<YogaRpcPool>(*this, service);
}

shared_ptr<PacketNetworkEngine> YogaLayout::mkRpcConn(string const &service, string const &host, bool autoStart) const
{
  shared_ptr<PacketNetworkEngine> ret;

  if (host == "local" && service == "botd") return mkLocalBotdRpcConn(autoStart);

  auto services = (*config)["services"];
  if (!services.is_object()) {
    cerr << "Warning: no services entry in config\n";
    cerr << " (Add this in " + homeDir + "/.yoga/config.json)\n";
    return nullptr;
  }
  auto hostport = services.value(host + ":" + service, ""s);

  if (0) cerr << host + ":" + service + " => " + hostport + "\n";
  auto hpsplit = splitChar(hostport, ':');
  if (hpsplit.size() == 2) {
    ret = make_shared<TcpPipe>(hpsplit[0], hpsplit[1]);
  }
  else {
    cerr << 
      "Warning: no RPC ports for " + host + ":" + service + "\n" +
      "  (Add this in " + homeDir + "/.yoga/config.json)\n" +
      "  For example: {\n" +
      "    \"services\": {\n" +
      "      \"" + host + ":" + service + "\" : \"" + host + ".example.com:1234\"\n" +
      "    }\n" +
      "  }\n";

    return nullptr;
  }

  if (ret) {
    ret->label = service + "-" + host;
    if (autoStart) ret->start();
  }
  else {
    cerr << "Warning: no RPC service named " + service + "\n";
  }
  return ret;
}


shared_ptr<PacketNetworkEngine> YogaLayout::mkLocalBotdRpcConn(bool autoStart) const
{
  vector<string> execArgs = {
    "bin/yoga_botd"
  };
  vector<string> execEnv = {
  };
  auto ret = make_shared<ChildPipe>(execArgs, execEnv);

  if (ret) {
    ret->verbose += 2;
    ret->label = "local-botd";
    if (autoStart) ret->start();
  }
  else {
    cerr << "Warning: Can't start local botd\n";
  }
  return ret;
}


YogaRpcPool::YogaRpcPool(YogaLayout const &_layout, string _service)
  : layout(_layout),
    service(move(_service))
{
}

YogaRpcPool::~YogaRpcPool()
{
  stopAll();
}

void YogaRpcPool::stopAll()
{
  for (auto &it : byHost) {
    if (it.second) {
      it.second->stop();
      it.second->endDrain();
      it.second = nullptr;
    }
  }
}

shared_ptr<PacketNetworkEngine> YogaRpcPool::get_shared(string const &host)
{
  auto &ret = byHost[host];
  if (!ret) {
    ret = layout.mkRpcConn(service, host);
  }
  return ret;
}

PacketNetworkEngine *YogaRpcPool::get(string const &host)
{
  return get_shared(host).get();
}

string YogaRpcPool::progress()
{
  string ret;
  for (auto &it : byHost) {
    if (it.second) {
      if (!ret.empty()) ret += "\n";
      ret += it.second->progress();
    }
  }
  return ret;
}







bool isValidTraceName(string const &a)
{
  if (a.empty()) return false;
  if (a.front() == '.' || a.back() == '.') return false;
  for (auto &it : a) {
    if (!isalnum(it) && it != '_' && it != '-' && it != '.') return false;
  }
  return true;
}

bool isValidSeqName(string const &a)
{
  if (a.empty()) return false;
  if (a.front() == '.' || a.back() == '.') return false;
  for (auto &it : a) {
    if (!isalnum(it) && it != '_' && it != '-' && it != '.') return false;
  }
  return true;
}
