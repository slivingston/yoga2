
ifeq ($(UNAME_SYSTEM),Darwin)

FULLVERSION = 2.1.2
TARNAME=yogastudio-$(FULLVERSION)
TARBALL=$(TARNAME).tar
BINARYNAME=$(TARNAME)-$(UNAME_SYSTEM)-$(UNAME_MACHINE)
BINARYTAR=$(BINARYNAME).tar
PKG=$(TARNAME).pkg
MACOUT=out/mac

package : $(PKG)

$(PKG): build
	rm -rf $(MACOUT)
	mkdir -p $(MACOUT)/pkgs $(MACOUT)/dist $(MACOUT)/installer/productbuild/Resources
	cat deploy/mac/distribution.xml.tmpl | sed -E "s/\\{yogaversion\\}/$(FULLVERSION)/g" >$(MACOUT)/installer/productbuild/distribution.xml
	rsync -vR bin/yogastudio bin/yoga_botd bin/yoga_test $(shell git ls-files | grep '.yoga$$') $(MACOUT)/dist/yogastudio
	codesign -s "$(CODESIGN_IDENTITY)" $(MACOUT)/dist/yogastudio/bin/*
	install -c LICENSE $(MACOUT)/installer/productbuild/Resources/license.rtf
	#install -c images/osx_installer_logo.png $(MACOUT)/installer/productbuild/Resources
	pkgbuild --version $(FULLVERSION) \
		--identifier com.umbrellaresearch.yogastudio.pkg \
		--root $(MACOUT)/dist/yogastudio $(MACOUT)/pkgs/yogastudio-$(FULLVERSION).pkg
	productbuild --distribution $(MACOUT)/installer/productbuild/distribution.xml \
		--resources $(MACOUT)/installer/productbuild/Resources \
		--package-path $(MACOUT)/pkgs ./$(PKG)
	productsign --sign "$(CODESIGN_IDENTITY)" ./$(PKG) ./$(PKG)-signed
	mv $(PKG)-signed $(PKG)

endif
