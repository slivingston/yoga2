
ifeq ($(UNAME_HOST),orly)
install.sys ::
	sudo install -c deploy/charter.yoga.xinetd /etc/xinetd.d/yoga
	sudo killall -1 xinetd
endif

ifeq ($(UNAME_HOST),nimes)
install.sys ::
	sudo install -c deploy/charter.yoga.xinetd /etc/xinetd.d/yoga
	sudo killall -1 xinetd
endif

ifeq ($(UNAME_HOST),orly)
install.sys ::
	sudo install -c deploy/orly.odrive_server.sh /etc/init.d/odrive_server
	sudo update-rc.d odrive_server defaults
	sudo sh /etc/init.d/odrive_server restart
endif

deploy.dex42 : remote.deploy.dex42 build ## Push to dex42 machines
remote.deploy.dex42 : pushsrc.rome pushsrc.nice pushsrc.nimes
	ssh rome 'cd yoga2 && make -j`nproc` build pushbin.nice pushbin.nimes'

deploy.greppy1 : remote.deploy.greppy1 build ## Push to greppy1 machines
remote.deploy.greppy1 : pushsrc.rome pushsrc.orly pushsrc.nimes
	ssh rome 'cd yoga2 && make -j`nproc` build pushbin.orly pushbin.nimes'

deploypub.greppy1 : remote.deploypub.greppy1 build ## Push to yoga@greppy1 machines
remote.deploypub.greppy1 : pushsrc.yoga@rome pushsrc.yoga@orly pushsrc.yoga@nimes
	ssh yoga@rome 'cd yoga2 && make -j`nproc` build pushbin.orly pushbin.nimes'
