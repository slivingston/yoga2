#!/bin/sh
### BEGIN INIT INFO
# Provides:          odrive_server
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: ODrive server
# Description:       Manages ODrive USB device.
### END INIT INFO

set -e

cd /home/yoga/yoga2/drivers/greppy/odrive_server/

# Get lsb functions
. /lib/lsb/init-functions

case "$1" in
  start)
    log_begin_msg "Starting odrive_server..."
    python3 odrive_server.py >odrive_server.log 2>&1 &
    log_end_msg $?
    ;;
  stop)
    log_begin_msg "Stopping odrive_server..."
    echo WRITEME
    log_end_msg $?
    ;;
  restart)
    $0 stop
    sleep 1
    $0 start
    ;;
  *)
    log_success_msg "Usage: /etc/init.d/odrive_server {start|stop|restart}"
    exit 1
esac
