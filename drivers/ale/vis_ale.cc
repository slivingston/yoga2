#include "../studio/trace_render.h"
#include "../studio/panel_render.h"
#include "./ale_types.h"
#if defined(USE_ALE)
#include <ale/ale_interface.hpp>
#include <ale/common/ColourPalette.hpp>
using namespace ale;

struct ScopePanelRendererAleImage : ScopePanelRenderer {

  ScopePanelRendererAleImage(
    ScopeModel &_m, ScopeModelVisPanel &_tr)
    : ScopePanelRenderer(_m, _tr),
      seqPtr(tr.spec.refs[0])
  {
    assert(tr.spec.refs.size() == 1);
    palette.setPalette("standard", "NTSC");
  }

  YogaPtrAccessor seqPtr;
  bool glInited{false};
  TexCache texCache;
  ColourPalette palette;

  float preferredAspect() override
  {
    return 1.33f;
  }

  bool getImage(int &bestIndex, YogaValue const &v)
  {
    int verbose = 0;

    bestIndex = 0;
    if (!texCache.getIndexFor(v.buf, bestIndex)) {
      auto vf = reinterpret_cast<YogaAleImage *>(v.buf);

      auto rgb = reinterpret_cast<U8 *>(malloc(160*3*210));
      palette.applyPaletteRGB(rgb, &vf->pixels[0][0], 160*210);
      texCache.setPixelsRGB(bestIndex, 160, 210, rgb);
      free(rgb);
    } 
    else {
      if (verbose) cerr << "  found in cache at index "s + repr(bestIndex) + "\n";
    }

    return true;
  }

  void drawContents() override
  {
    setBoundingBox();
    if (!glInited) {
      glInited = true;
      texCache.alloc(4);
    }

    for (auto &alt : m.alts()) {
      if (alt.isSecondary) continue; // FIXME: but isSecondary is wrong for video traces.

      auto v = alt.getValue(tr.spec.refs[0], m.visTime);
      if (!v.isValid() || !v.nonZeroTs()) continue;

      int texi = 0;
      if (getImage(texi, v)) {
        GetWindowDrawList()->AddImage(
          (ImTextureID)(size_t)texCache.texIds[texi],
          curbb.Min, curbb.Max);
        
        if (IsItemHovered()) {
          auto &io = GetIO();
          auto x = clamp(int(160.0 * (io.MousePos.x - curbb.Min.x) / (curbb.Max.x - curbb.Min.x)), 0, 160);
          auto y = clamp(int(210.0 * (io.MousePos.y - curbb.Min.y) / (curbb.Max.y - curbb.Min.y)), 0, 210);
          auto vf = reinterpret_cast<YogaAleImage *>(v.buf);

          BeginTooltip();
          Text("x=%d y=%d pix=%d",
            x, y, (int)vf->pixels[y][x]);
          EndTooltip();


        }
      }
    }
  }
};



static ScopePanelRendererRegister regScopePanelRendererAleImage("AleImage", "screen",
  [](ScopeModel &m, ScopeModelVisPanel &tr)
  {
    tr.renderer = make_shared<ScopePanelRendererAleImage>(m, tr);
  }
);

#endif
