#pragma once
#include "common/std_headers.h"

#define USE_DEBUG_PACKETS
#define USE_PRINTBUF_DSP

// CPU

/*
  FCPU, FPBA and FPBB are allowed to be up to 66 MHz according to table 38.6 in the AT32UC3A datasheet.
 */
#define FOSC0 12000000
#define FCPU 66000000
#define FPBA 66000000
#define OSC0_STARTUP AVR32_PM_OSCCTRL0_STARTUP_8192_RCOSC

// SERCOM

#define N_SERCOMS 1

#undef SERCOM_USE_PKT

#define SERCOM_USART0 (AVR32_USART0)

// PA00 and PA01 on UC3A0512
#define SERCOM_USART0_RXD_PIN AVR32_USART0_RXD_0_0_PIN
#define SERCOM_USART0_RXD_FUNCTION AVR32_USART0_RXD_0_0_FUNCTION

#define SERCOM_USART0_TXD_PIN AVR32_USART0_TXD_0_0_PIN
#define SERCOM_USART0_TXD_FUNCTION AVR32_USART0_TXD_0_0_FUNCTION

#define SERCOM_USART0_IRQ AVR32_USART0_IRQ

// ETHCOM

// WDT
#undef SSC_USE_WDT

// ETHER

#if defined(__TARG_SSC_FOOT1__)
#define ETHER_NRST_PIN AVR32_PIN_PX38
#endif

/*
  For outputting the clock to see what the PLL is doing.
  This is PA07, same as ENC_NWR, so it's available on the lower right
  pin of the LS7266s.
*/
#define AVRHW_TEST_GCLK_ID 0
#define AVRHW_TEST_GCLK_PIN AVR32_PM_GCLK_0_0_PIN
#define AVRHW_TEST_GCLK_FUNCTION AVR32_PM_GCLK_0_0_FUNCTION

#if defined(__TARG_SSC_FOOT1__)
// ADC - a single ads8344
#define ADC_DI0_PIN AVR32_PIN_PX05
#define ADC_DO0_PIN AVR32_PIN_PX06
#define ADC_BUSY0_PIN AVR32_PIN_PX07
#define ADC_CLK_PIN AVR32_PIN_PX08
#define ADC_NCS_PIN AVR32_PIN_PX09
#define ADS8344_N_RESULTS 8
#endif
