#include "common/std_headers.h"

#include "./hwdefs.h"

#include "gpio.h"
#include "wdt.h"

#include "embedded/embedded_hostif.h"
#include "embedded/embedded_pktcom.h"
#include "embedded/embedded_timing.h"
#include "embedded/embedded_debug.h"

#include "embedded/avr32/avrhw.h"
#include "drivers/ssc/fw/conf_eth.h"
#include "embedded/avr32/sercom.h"
#include "embedded/avr32/ethcom.h"
#include "drivers/ssc/fw/ads8344.h"
#include "drivers/ssc/fw/hostif.h"
#include "drivers/ssc/fw/ssc_main.h"

#include "./ssc_foot.h"

ssc_foot_t foot0;
static rate_generator ssc_foot_polling_rg;

void ssc_foot_setup(void)
{
  rate_generator_enable(&ssc_foot_polling_rg, SECONDSTOTICKS(0.001), 3);

  debug_printf("ads8344...");
  ads8344_init(&ads83440);
  wdt_clear();
}

void ssc_poll_work(void)
{
  rate_generator_set_budget(&ssc_foot_polling_rg, 10);
  if (rate_generator_update(&ssc_foot_polling_rg)) {
    ads8344_poll(&ads83440);

    // For now, map entire range to [-1 .. +1]
    foot0.angle_sensors[0] = ads83440.cur.results[2] * DSP824(1.0 / 32768.0) - DSP824(1.0);
    foot0.angle_sensors[1] = ads83440.cur.results[1] * DSP824(1.0 / 32768.0) - DSP824(1.0);

    // Sensitivity of these Dytran 1051V4 units is 10 mV / lb.
    // Their 0..20v signal is converted to full scale 16 bits, so we have 0.136 N / lsb
    foot0.force_sensors[0] = ads83440.cur.results[4] * DSP1616(0.136) - foot0.force_sensor_tares[0];
    foot0.force_sensors[1] = ads83440.cur.results[5] * DSP1616(0.136) - foot0.force_sensor_tares[1];
    foot0.force_sensors[2] = ads83440.cur.results[6] * DSP1616(0.136) - foot0.force_sensor_tares[2];

    if (!foot0.force_sensor_tares[0]) {
      foot0.force_sensor_tares[0] = foot0.force_sensors[0];
      foot0.force_sensor_tares[1] = foot0.force_sensors[1];
      foot0.force_sensor_tares[2] = foot0.force_sensors[2];
    }

    foot0.update_ticks = get_ticks64();

    rate_generator_done(&ssc_foot_polling_rg);
  }
}

void ssc_poll_logging(void)
{
  ads8344_log_status(&ads83440, "ads83440");
}


void ssc_foot_emit_state(pkt_tx_buf *tx)
{
  pkt_tx_u64p(tx, &foot0.update_ticks);
  for (int i=0; i<N_SSC_FOOT_FORCE_SENSORS; i++) {
    pkt_tx_dsp1616p(tx, &foot0.force_sensors[i]);
  }
  for (int i=0; i<N_SSC_FOOT_ANGLE_SENSORS; i++) {
    pkt_tx_dsp824p(tx, &foot0.angle_sensors[i]);
  }
}

void host_fill_update(host_t *it, pkt_tx_buf *tx)
{
  if (foot0.update_ticks != it->last_foot0_update_ticks) {
    pkt_tx_u8(tx, 'f');
    ssc_foot_emit_state(tx);
    it->last_foot0_update_ticks = foot0.update_ticks;
  }
}

U8 host_handle_cmd(host_t *it, U8 cmd, pkt_rx_buf *rx)
{
  return 0;
}

void host_shutdown(host_t *it)
{
}

void host_init(host_t *it)
{
  /*
    Yes, I literally have the IP and MAC address of another board coded into the software for this board.
    In this case, foot_0 sends to leg_0, and foot_1 sends to leg_1.
    These last bytes of the IP and MAC are defined in ssc_makefile.
    I will regret this one day fo' shizizzle.
  */
  it->peer_leg_ep.ip4_addr[0] = 172;
  it->peer_leg_ep.ip4_addr[1] = 17;
  it->peer_leg_ep.ip4_addr[2] = 10;
  it->peer_leg_ep.udp_port = 10000;
  it->peer_leg_ep.mac_addr[0] = ETHERNET_CONF_ETHADDR0;
  it->peer_leg_ep.mac_addr[1] = ETHERNET_CONF_ETHADDR1;
  it->peer_leg_ep.mac_addr[2] = ETHERNET_CONF_ETHADDR2;
  it->peer_leg_ep.mac_addr[3] = ETHERNET_CONF_ETHADDR3;
  it->peer_leg_ep.mac_addr[4] = ETHERNET_CONF_ETHADDR4;
#if BOARD_INSTANCE == 0
  it->peer_leg_ep.ip4_addr[3] = 3; // SSC_NETID for foot_0
  it->peer_leg_ep.mac_addr[5] = 3;
#elif BOARD_INSTANCE == 1
  it->peer_leg_ep.ip4_addr[3] = 4; // SSC_NETID for foot_1
  it->peer_leg_ep.mac_addr[5] = 4;
#endif
}

void peer_send_update(host_t *it)
{
  if (it->peer_leg_ep.udp_port == 0) return;

  pkt_tx_buf *tx = ethcom_start_tx_udp(&ethcom0, &it->peer_leg_ep);
  if (!tx) {
    if (1) log_printf("peer_leg_send_update: no tx buf");
    return;
  }

  pkt_tx_u8(tx, 'p'); // peer info
  int orig_tx_len = tx->len;

  if (foot0.update_ticks != it->peer_leg_last_foot0_update_ticks) {
    pkt_tx_u8(tx, 'f');
    ssc_foot_emit_state(tx);
    it->last_foot0_update_ticks = foot0.update_ticks;
  }

  // If this starts failing, reduce something
  assert(tx->len < 1450);

  if (tx->len > orig_tx_len) {
    ethcom_tx_pkt(&ethcom0, tx);
    ethcom_poll(&ethcom0);
  } else {
    free_tx_buf(tx);
  }
}


int main(void)
{
  ssc_basic_setup();
  ssc_foot_setup();

  debug_printf("host...");
  host_init(&host0);

  ssc_poll();
}
