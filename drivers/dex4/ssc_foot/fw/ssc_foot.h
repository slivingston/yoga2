#pragma once
#include "common/std_headers.h"

#include "dspcore/dspcore.h"

#include "../ssc_foot_defs.h"

typedef struct ssc_foot_t {
  uint64_t update_ticks;
  dsp1616 force_sensors[N_SSC_FOOT_FORCE_SENSORS];
  dsp1616 force_sensor_tares[N_SSC_FOOT_FORCE_SENSORS];
  dsp824 angle_sensors[N_SSC_FOOT_ANGLE_SENSORS];
} ssc_foot_t;

extern ssc_foot_t foot0;

void ssc_foot_setup(void);
void ssc_foot_work(void);
void ssc_foot_log_status(void);

void ssc_foot_emit_state(pkt_tx_buf *tx);
