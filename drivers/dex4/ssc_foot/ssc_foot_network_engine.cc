#include "../../../comm/ssc_network_engine.h"

#include "./ssc_foot_io.h"

struct SscFootNetworkEngine : SscNetworkEngine {
  void setupHandlers() override;
  void setupAccessors() override;

  shared_ptr<YogaTimeseq> stateOut;
  unique_ptr<FootStateAccessorSuite> stateAs;
};

void SscFootNetworkEngine::setupAccessors()
{
  SscNetworkEngine::setupAccessors();
  stateAs = make_unique<FootStateAccessorSuite>(stateOut->type);
}
void SscFootNetworkEngine::setupHandlers()
{
  SscNetworkEngine::setupHandlers();
  updateHandlers[(U8)'f'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
    R ts;
    rxSscTimestamp(*this, rx, ts, rxTime, pktTicks);
    auto foot0 = stateOut->addNew(ts);
    stateAs->rxFoot(rx, foot0);
  };
}

static EngineRegister sscFootNetworkEngineReg("sscFootNetwork",
  {
    "Dex4FootState"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 1) {
      return ctx.logError("sscFootNetwork: expected 1 argument (footState)");
    }

    auto e = make_shared<SscFootNetworkEngine>();
    if (args.size() > 0) {
      e->stateOut = args[0];
      e->stateOut->needBackdateInitial = true;
      e->stateOut->isExtIn = true;
    }

    if (!e->setEngineConfig(ctx, spec)) return false;
    if (!e->setNetworkConfig(ctx, spec)) return false;

    trace->addEngine(engineName, e, true);

    return true;
  },
  true);

