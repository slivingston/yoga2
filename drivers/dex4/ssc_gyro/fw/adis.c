#include "common/std_headers.h"
#include "hwdefs.h"
#include "gpio.h"
#include "intc.h"
#include "spi.h"
#include "adis.h"
#include "embedded/embedded_debug.h"
#include "embedded/embedded_printbuf.h"
#include "embedded/embedded_timing.h"

#include "adis_registers.h"


/*
  Timings -- the ADIS16480 required 2 uS between consecutive transactions. We give it at least 4 uS below.

  When we get a non-newdata sample, we pause for 1/4 of the sample rate
*/
#define ADIS_TRANS_DLYBCT_NORMAL Min(255, (int)(((2.5e-6) * FPBA / 32.0) + 1.5))

// ----------------------------------------------------------------------

inline bool adis_engine_launch_spi_ready(volatile adis_engine_t *it)
{
  return it->spi_mode == SPI_MODE_IDLE;
}

inline void adis_engine_launch_spi_start(volatile adis_engine_t *it, enum spi_mode_t mode)
{
  it->spi_phase = 0;
  it->spi_mode = mode;
  // Generate a first interrupt
  it->spi->ier = AVR32_SPI_IMR_TDRE_MASK;
}


__attribute__((__noinline__))
static void adis_engine_handle_sync_interrupt(volatile adis_engine_t *it)
{
  gpio_clear_pin_interrupt_flag(it->sync_pin);
  it->sync_count ++;

  if (1) {
    it->sync_time = get_ticks64();
    if (adis_engine_launch_spi_ready(it)) {
      adis_engine_launch_spi_start(it, SPI_MODE_GETSAMPLE);
    }
  }
}

__attribute__((__noinline__))
static void adis_engine_handle_spi_interrupt(volatile adis_engine_t *it)
{
  U32 status = it->spi->sr;

  it->interrupt_profile[status&3] ++;

  if (status & AVR32_SPI_SR_RDRF_MASK) {
    U16 rxdata = it->spi->rdr;
    U16 txdata = 0;
    int next_spi_phase = it->spi_phase + 1;

    switch (it->spi_mode) {

    case SPI_MODE_IDLE:
      it->spi->idr = AVR32_SPI_IMR_TDRE_MASK;
      break;

    case SPI_MODE_GETSAMPLE:
      switch(it->spi_phase) {
      case 0:
        txdata = adis_wr_reg_cmd(ADIS_REGADDR_PAGE_ID, 0);
        it->cursample.seqno = it->sync_count;
        it->cursample.sample_ticks = it->sync_time;
        break;

      case 1:
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_X_GYRO_OUT);
        break;

      case 2:
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_X_GYRO_LOW);
        break;

      case 3:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Y_GYRO_OUT);
        break;

      case 4:
        it->cursample.gyro_x = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Y_GYRO_LOW);
        break;

      case 5:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Z_GYRO_OUT);
        break;

      case 6:
        it->cursample.gyro_y = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Z_GYRO_LOW);
        break;

      case 7:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_X_ACCL_OUT);
        break;

      case 8:
        it->cursample.gyro_z = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_X_ACCL_LOW);
        break;

      case 9:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Y_ACCL_OUT);
        break;

      case 10:
        it->cursample.accel_x = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Y_ACCL_LOW);
        break;

      case 11:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Z_ACCL_OUT);
        break;

      case 12:
        it->cursample.accel_y = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Z_ACCL_LOW);
        break;

      case 13:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_X_MAGN_OUT);
        break;

      case 14:
        it->cursample.accel_z = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Y_MAGN_OUT);
        break;

      case 15:
        it->cursample.mag_x = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Z_MAGN_OUT);
        break;

      case 16:
        it->cursample.mag_y = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_X_DELTANG_OUT);
        break;

      case 17:
        it->cursample.mag_z = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_X_DELTANG_LOW);
        break;

      case 18:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Y_DELTANG_OUT);
        break;

      case 19:
        it->cursample.deltang_x = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Y_DELTANG_LOW);
        break;

      case 20:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Z_DELTANG_OUT);
        break;

      case 21:
        it->cursample.deltang_y = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Z_DELTANG_LOW);
        break;

      case 22:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_X_DELTVEL_OUT);
        break;

      case 23:
        it->cursample.deltang_z = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_X_DELTVEL_LOW);
        break;

      case 24:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Y_DELTVEL_OUT);
        break;

      case 25:
        it->cursample.deltvel_x = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Y_DELTVEL_LOW);
        break;

      case 26:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Z_DELTVEL_OUT);
        break;

      case 27:
        it->cursample.deltvel_y = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_Z_DELTVEL_LOW);
        break;

      case 28:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_C11_OUT);
        break;

      case 29:
        it->cursample.deltvel_z = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_C12_OUT);
        break;

      case 30:
        it->cursample.orient_11 = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_C13_OUT);
        break;

      case 31:
        it->cursample.orient_12 = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_C21_OUT);
        break;

      case 32:
        it->cursample.orient_13 = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_C22_OUT);
        break;

      case 33:
        it->cursample.orient_21 = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_C23_OUT);
        break;

      case 34:
        it->cursample.orient_22 = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_C31_OUT);
        break;

      case 35:
        it->cursample.orient_23 = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_C32_OUT);
        break;

      case 36:
        it->cursample.orient_31 = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_C33_OUT);
        break;

      case 37:
        it->cursample.orient_32 = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_TEMP_OUT);
        break;

      case 38:
        it->cursample.orient_33 = (S16)rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_SYS_E_FLAG);
        break;

      case 39:
        it->cursample.temperature = rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_DIAG_STS);
        break;

      case 40:
        it->cursample.sys_e_flag = rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_ALM_STS);
        break;

      case 41:
        it->cursample.diag_sts = rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_BAROM_OUT);
        break;

      case 42:
        it->cursample.alm_sts = rxdata;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_BAROM_LOW);
        break;

      case 43:
        it->spi_accum = (U32)rxdata << 16;
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_PAGE_ID);
        break;

      case 44:
        it->cursample.barom = (S32)(it->spi_accum | (U32)rxdata);
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_PAGE_ID);

        if ((it->sample_q_head+1) % ADIS_SAMPLE_Q_SIZE == it->sample_q_tail) {
          // Discard oldest. Not sure if this is race-free.
          it->sample_q_tail = (it->sample_q_tail + 1) % ADIS_SAMPLE_Q_SIZE;
          it->sample_q_overruns++;
        }
        it->sample_q[it->sample_q_head] = it->cursample;
        it->sample_q_head = (it->sample_q_head+1) % ADIS_SAMPLE_Q_SIZE;

        it->spi_mode = SPI_MODE_DRAIN;
        break;
      }
      it->spi_phase = next_spi_phase;
      it->spi->tdr = txdata;
      break;

    case SPI_MODE_READREG16:
      switch (it->spi_phase) {
      case 0:
        txdata = adis_wr_reg_cmd(ADIS_REGADDR_PAGE_ID, (U8)((it->readreg_addr >> 8) & 0xff));
        break;

      case 1:
        txdata = adis_rd_reg_cmd(it->readreg_addr);
        break;

      case 2:
        txdata = adis_rd_reg_cmd(ADIS_REGADDR_PAGE_ID);
        break;

      case 3:
        txdata = adis_wr_reg_cmd(ADIS_REGADDR_PAGE_ID, 0);
        it->readreg_value = rxdata;
        it->spi_mode = SPI_MODE_DRAIN;
        break;
      }
      it->spi_phase = next_spi_phase;
      it->spi->tdr = txdata;
      break;

    case SPI_MODE_WRITEREG8:
      switch (it->spi_phase) {
      case 0:
        txdata = adis_wr_reg_cmd(ADIS_REGADDR_PAGE_ID, (U8)((it->writereg_addr >> 8) & 0xff));
        break;

      case 1:
        txdata = adis_wr_reg_cmd(it->writereg_addr, (it->writereg_value & 0xff));
        break;

      case 2:
        txdata = adis_wr_reg_cmd(ADIS_REGADDR_PAGE_ID, 0);
        it->spi_mode = SPI_MODE_DRAIN;
        break;

      }
      it->spi_phase = next_spi_phase;
      it->spi->tdr = txdata;
      break;

    case SPI_MODE_WRITEREG16:
      switch (it->spi_phase) {
      case 0:
        txdata = adis_wr_reg_cmd(ADIS_REGADDR_PAGE_ID, (U8)((it->writereg_addr >> 8) & 0xff));
        break;

      case 1:
        txdata = adis_wr_reg_cmd(it->writereg_addr, (it->writereg_value & 0xff));
        break;

      case 2:
        txdata = adis_wr_reg_cmd(it->writereg_addr + 1, (it->writereg_value >> 8) & 0xff);
        break;

      case 3:
        txdata = adis_wr_reg_cmd(ADIS_REGADDR_PAGE_ID, 0);
        it->spi_mode = SPI_MODE_DRAIN;
        break;
      }
      it->spi_phase = next_spi_phase;
      it->spi->tdr = txdata;
      break;

    case SPI_MODE_DRAIN:
      it->spi_phase = 0;
      it->spi_mode = SPI_MODE_IDLE; // main code can now kick us off again
      break;
    }

  }
  else if (status & AVR32_SPI_SR_TDRE_MASK) {
    it->spi->idr = AVR32_SPI_IMR_TDRE_MASK;

    // Start every transaction on page 0
    it->spi->tdr = adis_wr_reg_cmd(ADIS_REGADDR_PAGE_ID, 0);
  }
}


// ----------------------------------------------------------------------

// --

void adis_engine_wr_reg8(volatile adis_engine_t *it, U16 regaddr, U8 regval)
{
  int timeout = 0;
  while (!adis_engine_launch_spi_ready(it)) {
    if (++timeout > 1000000) {
      debug_printf("adis: timeout before wr_reg8");
      return;
    }
  }
  it->writereg_addr = regaddr;
  it->writereg_value = regval;
  adis_engine_launch_spi_start(it, SPI_MODE_WRITEREG8);
  while (!adis_engine_launch_spi_ready(it)) {
    if (++timeout > 1000000) {
      debug_printf("adis: timeout after wr_reg8");
      return;
    }
  }
}

void adis_engine_wr_reg16(volatile adis_engine_t *it, U16 regaddr, U16 regval)
{
  int timeout = 0;
  while (!adis_engine_launch_spi_ready(it)) {
    if (++timeout > 1000000) {
      debug_printf("adis: timeout before wr_reg16");
      return;
    }
  }
  it->writereg_addr = regaddr;
  it->writereg_value = regval;
  adis_engine_launch_spi_start(it, SPI_MODE_WRITEREG16);
  while (!adis_engine_launch_spi_ready(it)) {
    if (++timeout > 1000000) {
      debug_printf("adis: timeout after wr_reg16");
      return;
    }
  }

}

U16 adis_engine_rd_reg16(volatile adis_engine_t *it, U16 regaddr)
{
  int timeout = 0;
  while (!adis_engine_launch_spi_ready(it)) {
    if (++timeout > 1000000) {
      debug_printf("adis: timeout before rd_reg16");
      return 0;
    }
  }
  it->readreg_addr = regaddr;
  it->readreg_value = 0;
  adis_engine_launch_spi_start(it, SPI_MODE_READREG16);
  while (!adis_engine_launch_spi_ready(it)) {
    if (++timeout > 1000000) {
      debug_printf("adis: timeout after rd_reg16");
      return 0;
    }
  }

  return it->readreg_value;
}


// ----------------------------------------------------------------------


void adis_engine_setup_spi(volatile adis_engine_t *it)
{
  spi_options_t options = {
    .reg = it->spi_npcs,
    .baudrate = ADIS_BITRATE,
    .bits = 16,

    /*
      Tcs is 48.8 nS min, so the half-cycle delay between asserting CS and starting the
      clock is plenty.
    */
    .spck_delay = 0,  // becomes DLYBS

    .trans_delay = ADIS_TRANS_DLYBCT_NORMAL,

    .stay_act = 0,
    /*
      CPOL=1, CPHA=1: setup on leading(falling) edge, sample on trailing (rising) edge.
      I think that's what the ADIS spec sheet is trying to say.
    */
    .spi_mode = 0x03,
    .modfdis = 1, // disable mode fault, don't listen for other bus-masters
  };

  if (spi_initMaster(it->spi, &options) != SPI_OK) {
    debug_printf("spi_initMaster failed");
    return;
  }
  if (spi_selectionMode(it->spi, 0, 0, 30) != SPI_OK) {
    debug_printf("spi_selectionMode failed");
    return;
  }
  spi_enable(it->spi);
  if (spi_setupChipReg(it->spi, &options, FPBA) != SPI_OK) {
    debug_printf("spi_setupChipReg failed");
  }

  spi_selectChip(it->spi, it->spi_npcs);

}

void adis_engine_enable_gyro(volatile adis_engine_t *it)
{
  if (it->nrst_gpio_pin >= 0) {
    gpio_enable_gpio_pin(it->nrst_gpio_pin);
    gpio_clr_gpio_pin(it->nrst_gpio_pin);
    debug_printf("adis: reset gyro...");
    // "The RST line must be in a low state for at least 10 μs to assure a proper reset initiation and recovery."
    cpu_delay_ms(2, FCPU);
    gpio_set_gpio_pin(it->nrst_gpio_pin);
    // "Reset recovery time: 400 ± 160 mS"
    cpu_delay_ms(700, FCPU);
    debug_printf("adis: should be done reset...");
  }
  if (it->ncs_gpio_pin >= 0) {
    gpio_enable_gpio_pin(it->ncs_gpio_pin);
    gpio_clr_gpio_pin(it->ncs_gpio_pin);
  }

  if (1) cpu_delay_ms(100, FCPU);
}

void adis_engine_configure_gyro(volatile adis_engine_t *it)
{
  for (int i=0; i<10; i++) {
    adis_engine_rd_reg16(it, ADIS_REGADDR_PAGE_ID);
  }

  // Retry in case it hasn't woken up yet
  int read_prod_id_count = 0;
  U32 read_prod_id_t0 = get_ticks();
  while ((get_ticks() - read_prod_id_t0) < SECONDSTOTICKS(2.0)) {
    it->prod_id = adis_engine_rd_reg16(it, ADIS_REGADDR_PROD_ID);
    if (it->prod_id == 16480) break;
    read_prod_id_count ++;
  }

  {
    /*
      "The automatic reset process works best when the divergence comes from short-term, transient inertial conditions.
      Use this function only when predeployment validation testing can confirm that it performs well through all application
      conditions. If there is any sign of instability, keep this function off (EKF_CFG[12] = 0), monitor SYS_E_FLAG[13]
      to test for divergence in the EKF, and, after detecting divergence, use the manual EKF reset function in
      GLOB_CMD[15] (see Table 146) or the full software reset in GLB_CMD[7] to initiate a reset in the EKF. Note that
      this recovery process requires zero inertial motion and a magnetic environment free of interference to optimize
      postrecovery accuracy."
    */
    int auto_reset_enable = 0;
    /*
      "EKF_CNFG[9] (see Table 94) provides an on/off control bit for the gyroscope fade function, which is an internal
      adjustment of the gyroscope’s process covariance terms. This reduces the impact of gyroscope scale errors during
      transient events, where the gyroscope rates are quickly changing. The fade function effectively reduces the
      weighting of the gyroscope measurements, with respect to the accelerometers and magnetometers, during these
      transient events. The adjustment terminates when the rates return to zero."
    */
    int fade_enable = 0;
    /*
      "EKF_CNFG[8] (see Table 94) provides an on/off control bit for the adaptive part of the EKF function. The
      adaptive part of the EKF computes the measurement covariance terms (R), which enables real-time adjustments
      for vibration and magnetic field disturbances. See Table 80 through Table 83 for read access to the
      measurement covariance terms."
    */
    int adaptive_ekf_enable = 1;

    int use_matrix_format = 1;
    int use_body_frame = 0;
    /*
      EKF_CNFG[1] (see Table 94) provides an on/off control bit for the magnetometer disable function, which disables
      the magnetometer influence over angle calculations in the EKF.
    */
    int magnetometer_disable = 1;
    int gravity_removal = 0;
    U16 ekf_cnfg_wr =
      (auto_reset_enable << 12) |
      (fade_enable << 9) |
      (adaptive_ekf_enable << 8) |
      (use_matrix_format << 4) |
      (use_body_frame << 3) |
      (magnetometer_disable << 1) |
      (gravity_removal << 0);
    adis_engine_wr_reg16(it, ADIS_REGADDR_EKF_CNFG, ekf_cnfg_wr);
  }
  it->ekf_cnfg_rd = adis_engine_rd_reg16(it, ADIS_REGADDR_EKF_CNFG);

  {
    adis_engine_wr_reg16(it, ADIS_REGADDR_ACC_DISTB_THR, 128);
    adis_engine_wr_reg16(it, ADIS_REGADDR_MAG_DISTB_THR, 48);
  }

  adis_engine_wr_reg16(it, ADIS_REGADDR_DEC_RATE, 2); // Decimate by 3 (it uses DEC_RATE+1)

  // data ready enable, active high, on DIO2
  adis_engine_wr_reg16(it, ADIS_REGADDR_FNCTIO_CTRL, (1<<3) | (1<<2) | (1<<0));

  it->serial_num = adis_engine_rd_reg16(it, ADIS_REGADDR_SERIAL_NUM);
  it->firm_rev = adis_engine_rd_reg16(it, ADIS_REGADDR_FIRM_REV);
  it->firm_dm = adis_engine_rd_reg16(it, ADIS_REGADDR_FIRM_DM);
  it->firm_y = adis_engine_rd_reg16(it, ADIS_REGADDR_FIRM_Y);
  debug_printf("adis configured. prod_id=%d (%d tries) serial_num=%d firm=%04x date=%04x.%04x ekf_cnfg=%04x",
               it->prod_id, read_prod_id_count, it->serial_num, it->firm_rev, it->firm_y, it->firm_dm, it->ekf_cnfg_rd);
}


void adis_engine_setup(volatile adis_engine_t *it)
{
  debug_printf("configure adis...");
  it->serial_num = 0;
  it->prod_id = 0;
  it->spi_mode = SPI_MODE_IDLE;
  adis_engine_setup_spi(it);
  adis_engine_enable_gyro(it);

  it->spi->ier = AVR32_SPI_IMR_RDRF_MASK;

  adis_engine_configure_gyro(it);

}

void adis_engine_poll(adis_engine_t *it)
{
}

void adis_engine_log_status(adis_engine_t *it, const char *name)
{
  log_printf("%s:  prod_id=%d spi_mode=%d.%d"
             " interrupts=[%d %d %d %d]"
             " overruns=%d sync=%d deliveries=%d"
             " sys_e=%04x diag=%04x alm=%04x ekf_cnfg=%04x",
             name,
             it->prod_id, it->spi_mode, it->spi_phase,
             it->interrupt_profile[0], it->interrupt_profile[1], it->interrupt_profile[2], it->interrupt_profile[3],
             it->sample_q_overruns, it->sync_count, it->sample_q_deliveries,
             it->cursample.sys_e_flag,
             it->cursample.diag_sts,
             it->cursample.alm_sts,
             it->ekf_cnfg_rd);

  log_printf("%s:  gyro=%d %d %d"
             " accel=%d %d %d"
             //" deltang=%d %d %d "
             //" deltvel=%d %d %d "
             //" orient=%d %d %d / %d %d %d / %d %d %d "
             //" mag=%d %d %d "
             " temperature=%d",
             name,
             it->cursample.gyro_x, it->cursample.gyro_y, it->cursample.gyro_z,
             it->cursample.accel_x, it->cursample.accel_y, it->cursample.accel_z,
             //it->cursample.deltang_x, it->cursample.deltang_y, it->cursample.deltang_z,
             //it->cursample.deltvel_x, it->cursample.deltvel_y, it->cursample.deltvel_z,
             //it->cursample.orient_11, it->cursample.orient_12, it->cursample.orient_13,
             //it->cursample.orient_21, it->cursample.orient_22, it->cursample.orient_23,
             //it->cursample.orient_31, it->cursample.orient_32, it->cursample.orient_33,
             //it->cursample.mag_x, it->cursample.mag_y, it->cursample.mag_z,
             it->cursample.temperature);
}

// ----------------------------------------------------------------------

/*
  This should be the only section where we know how many adis_engine_ts there are and how
  they map to SPI ports.
*/

adis_engine_t adis0;

__attribute__((__interrupt__))
static void adis0_spi_isr()
{
  adis_engine_handle_spi_interrupt(&adis0);
}

__attribute__((__interrupt__))
static void adis0_sync_isr()
{
  adis_engine_handle_sync_interrupt(&adis0);
}

// ----------------------------------------------------------------------

int adis_get_sample(volatile adis_engine_t *it, adis_sample_t *sample)
{
  if (it->sample_q_tail == it->sample_q_head) return 0;
  *sample = it->sample_q[it->sample_q_tail];
  it->sample_q_tail = (it->sample_q_tail + 1) % ADIS_SAMPLE_Q_SIZE;
  it->sample_q_deliveries ++;
  return 1;
}

void adis_setup() {
  adis_engine_t *it = &adis0;
  it->spi =  &AVR32_SPI0;

  it->spi_npcs = ADIS_NPCS;
#ifdef ADIS_NRST_PIN
  it->nrst_gpio_pin = ADIS_NRST_PIN;
#else
  it->nrst_gpio_pin = -1;
#endif
#ifdef ADIS_NCS_PIN
  it->ncs_gpio_pin = ADIS_NCS_PIN;
#else
  it->ncs_gpio_pin = -1;
#endif
  it->sync_pin = ADIS_SYNC_PIN;

  gpio_enable_module_pin(ADIS_SPI_SCK_PIN,  ADIS_SPI_SCK_FUNCTION);
  gpio_enable_module_pin(ADIS_SPI_MISO_PIN,  ADIS_SPI_MISO_FUNCTION);
  gpio_enable_module_pin(ADIS_SPI_MOSI_PIN,  ADIS_SPI_MOSI_FUNCTION);
  gpio_enable_module_pin(ADIS_SPI_NPCS_PIN,  ADIS_SPI_NPCS_FUNCTION);

#if 1
  gpio_enable_pin_interrupt(ADIS_SYNC_PIN, GPIO_RISING_EDGE);
  INTC_register_interrupt((__int_handler)&adis0_sync_isr,
                          (AVR32_GPIO_IRQ_0 + (ADIS_SYNC_PIN/8)),
                          AVR32_INTC_INT2);
#endif

  Disable_global_interrupt();
  INTC_register_interrupt(&adis0_spi_isr, AVR32_SPI0_IRQ, AVR32_INTC_INT0);
  Enable_global_interrupt();

  adis_engine_setup(it);
}

// ----------------------------------------------------------------------

void pkt_tx_adis_sample(pkt_tx_buf *tx, adis_sample_t *s)
{
  pkt_tx_u64p(tx, &s->sample_ticks);
  pkt_tx_u32p(tx, &s->seqno);

  pkt_tx_s32p(tx, &s->gyro_x);
  pkt_tx_s32p(tx, &s->gyro_y);
  pkt_tx_s32p(tx, &s->gyro_z);

  pkt_tx_s32p(tx, &s->accel_x);
  pkt_tx_s32p(tx, &s->accel_y);
  pkt_tx_s32p(tx, &s->accel_z);

  pkt_tx_s32p(tx, &s->deltang_x);
  pkt_tx_s32p(tx, &s->deltang_y);
  pkt_tx_s32p(tx, &s->deltang_z);

  pkt_tx_s32p(tx, &s->deltvel_x);
  pkt_tx_s32p(tx, &s->deltvel_y);
  pkt_tx_s32p(tx, &s->deltvel_z);

  pkt_tx_s32p(tx, &s->barom);

  pkt_tx_s16p(tx, &s->orient_11);
  pkt_tx_s16p(tx, &s->orient_12);
  pkt_tx_s16p(tx, &s->orient_13);
  pkt_tx_s16p(tx, &s->orient_21);
  pkt_tx_s16p(tx, &s->orient_22);
  pkt_tx_s16p(tx, &s->orient_23);
  pkt_tx_s16p(tx, &s->orient_31);
  pkt_tx_s16p(tx, &s->orient_32);
  pkt_tx_s16p(tx, &s->orient_33);

  pkt_tx_s16p(tx, &s->mag_x);
  pkt_tx_s16p(tx, &s->mag_y);
  pkt_tx_s16p(tx, &s->mag_z);

  pkt_tx_s16p(tx, &s->temperature);

  pkt_tx_u16p(tx, &s->sys_e_flag);
  pkt_tx_u16p(tx, &s->diag_sts);
  pkt_tx_u16p(tx, &s->alm_sts);
}
