#pragma once
#include "common/std_headers.h"

#include "embedded/embedded_printbuf.h"
#include "dspcore/dspcore.h"

/*
  The datasheet says:

    Each register has two addresses (an upper address and a lower
    address), but either one can be used to access the entire 16 bits of
    data.

  But it's not quite the same: requesting the status register using its high byte address
  doesn't reset the error flags. Following some example code I had been using the high
  (odd) addresses but now I'm using the even ones.
 */

enum {
  // Data output registers from ADIS16480 datasheet revision C table 9

  ADIS_REGADDR_PAGE_ID = 0x0000,    // Page identifier N/A
  ADIS_REGADDR_SEQ_CNT = 0x0006,    // Sequence counter Table 68
  ADIS_REGADDR_SYS_E_FLAG = 0x0008, // Output, system error flags Table 59
  ADIS_REGADDR_DIAG_STS = 0x000a,   // Output, self-test error flags Table 60
  ADIS_REGADDR_ALM_STS = 0x000c,    // Output, alarm error flags Table 61
  ADIS_REGADDR_TEMP_OUT = 0x000e,   // Output, temperature Table 57

  ADIS_REGADDR_X_GYRO_LOW = 0x0010,    // Output, x-axis gyroscope, low word Table 14
  ADIS_REGADDR_X_GYRO_OUT = 0x0012,    // Output, x-axis gyroscope, high word Table 10
  ADIS_REGADDR_Y_GYRO_LOW = 0x0014,    // Output, y-axis gyroscope, low word Table 15
  ADIS_REGADDR_Y_GYRO_OUT = 0x0016,    // Output, y-axis gyroscope, high word Table 11
  ADIS_REGADDR_Z_GYRO_LOW = 0x0018,    // Output, z-axis gyroscope, low word Table 16
  ADIS_REGADDR_Z_GYRO_OUT = 0x001A,    // Output, z-axis gyroscope, high word Table 12
  ADIS_REGADDR_X_ACCL_LOW = 0x001C,    // Output, x-axis accelerometer, low word Table 21
  ADIS_REGADDR_X_ACCL_OUT = 0x001E,    // Output, x-axis accelerometer, high word Table 17
  ADIS_REGADDR_Y_ACCL_LOW = 0x0020,    // Output, y-axis accelerometer, low word Table 22
  ADIS_REGADDR_Y_ACCL_OUT = 0x0022,    // Output, y-axis accelerometer, high word Table 18
  ADIS_REGADDR_Z_ACCL_LOW = 0x0024,    // Output, z-axis accelerometer, low word Table 23
  ADIS_REGADDR_Z_ACCL_OUT = 0x0026,    // Output, z-axis accelerometer, high word Table 19
  ADIS_REGADDR_X_MAGN_OUT = 0x0028,    // Output, x-axis magnetometer, high word Table 38
  ADIS_REGADDR_Y_MAGN_OUT = 0x002A,    // Output, y-axis magnetometer, high word Table 39
  ADIS_REGADDR_Z_MAGN_OUT = 0x002C,    // Output, z-axis magnetometer, high word Table 40
  ADIS_REGADDR_BAROM_LOW = 0x002E,     // Output, barometer, low word Table 56
  ADIS_REGADDR_BAROM_OUT = 0x0030,     // Output, barometer, high word Table 54
  ADIS_REGADDR_X_DELTANG_LOW = 0x0040, // Output, x-axis delta angle, low word Table 28
  ADIS_REGADDR_X_DELTANG_OUT = 0x0042, // Output, x-axis delta angle, high word Table 24
  ADIS_REGADDR_Y_DELTANG_LOW = 0x0044, // Output, y-axis delta angle, low word Table 29
  ADIS_REGADDR_Y_DELTANG_OUT = 0x0046, // Output, y-axis delta angle, high word Table 25
  ADIS_REGADDR_Z_DELTANG_LOW = 0x0048, // Output, z-axis delta angle, low word Table 30
  ADIS_REGADDR_Z_DELTANG_OUT = 0x004A, // Output, z-axis delta angle, high word Table 26
  ADIS_REGADDR_X_DELTVEL_LOW = 0x004C, // Output, x-axis delta velocity, low word Table 35
  ADIS_REGADDR_X_DELTVEL_OUT = 0x004E, // Output, x-axis delta velocity, high word Table 31
  ADIS_REGADDR_Y_DELTVEL_LOW = 0x0050, // Output, y-axis delta velocity, low word Table 36
  ADIS_REGADDR_Y_DELTVEL_OUT = 0x0052, // Output, y-axis delta velocity, high word Table 32
  ADIS_REGADDR_Z_DELTVEL_LOW = 0x0054, // Output, z-axis delta velocity, low word Table 37
  ADIS_REGADDR_Z_DELTVEL_OUT = 0x0056, // Output, z-axis delta velocity, high word Table 33

  ADIS_REGADDR_C11_OUT = 0x0060, // rotation matrix, C11 Table 42
  ADIS_REGADDR_C12_OUT = 0x0062, // rotation matrix, C12 Table 43
  ADIS_REGADDR_C13_OUT = 0x0064, // rotation matrix, C13 Table 44
  ADIS_REGADDR_C21_OUT = 0x0066, // rotation matrix, C21 Table 45
  ADIS_REGADDR_C22_OUT = 0x0068, // C22 Table 46
  ADIS_REGADDR_C23_OUT = 0x006A, // rotation matrix, C23 Table 47
  ADIS_REGADDR_C31_OUT = 0x006C, // rotation matrix, C31 Table 48
  ADIS_REGADDR_C32_OUT = 0x006E, // rotation matrix, C32 Table 49
  ADIS_REGADDR_C33_OUT = 0x0070, // Rotation matrix, C33 Table 50

  ADIS_REGADDR_Q0_OUT = 0x0060,    // Quaternion, q0, Table 43
  ADIS_REGADDR_Q1_OUT = 0x0062,    // Quaternion, q1, Table 43
  ADIS_REGADDR_Q2_OUT = 0x0064,    // Quaternion, q2, Table 44
  ADIS_REGADDR_Q3_OUT = 0x0066,    // Quaternion, q3, Table 45
  ADIS_REGADDR_ROLL_OUT = 0x006A,  // Euler angle, roll axis, Table 47
  ADIS_REGADDR_PITCH_OUT = 0x006C, // Euler angle, pitch axis, Table 48
  ADIS_REGADDR_YAW_OUT = 0x006E,   // Euler angle, yaw axis, Table 49

  ADIS_REGADDR_TIME_YM_OUT = 0x007C, // Factory configuration date: year/month Table 158
  ADIS_REGADDR_PROD_ID = 0x007E,     // Output, product identification (16,480) Table 65

  ADIS_REGADDR_X_GYRO_SCALE = 0x0204,  // Calibration, scale, x-axis gyroscope Table 103
  ADIS_REGADDR_Y_GYRO_SCALE = 0x0206,  // Calibration, scale, y-axis gyroscope Table 104
  ADIS_REGADDR_Z_GYRO_SCALE = 0x0208,  // Calibration, scale, z-axis gyroscope Table 105
  ADIS_REGADDR_X_ACCL_SCALE = 0x020A,  // Calibration, scale, x-axis accelerometer Table 113
  ADIS_REGADDR_Y_ACCL_SCALE = 0x020C,  // Calibration, scale, y-axis accelerometer Table 114
  ADIS_REGADDR_Z_ACCL_SCALE = 0x020E,  // Calibration, scale, z-axis accelerometer Table 115
  ADIS_REGADDR_XG_BIAS_LOW = 0x0210,   // Calibration, offset, gyroscope, x-axis, low word Table 100
  ADIS_REGADDR_XG_BIAS_HIGH = 0x0212,  // Calibration, offset, gyroscope, x-axis, high word Table 97
  ADIS_REGADDR_YG_BIAS_LOW = 0x0214,   // Calibration, offset, gyroscope, y-axis, low word Table 101
  ADIS_REGADDR_YG_BIAS_HIGH = 0x0216,  // Calibration, offset, gyroscope, y-axis, high word Table 98
  ADIS_REGADDR_ZG_BIAS_LOW = 0x0218,   // Calibration, offset, gyroscope, z-axis, low word Table 102
  ADIS_REGADDR_ZG_BIAS_HIGH = 0x021A,  // Calibration, offset, gyroscope, z-axis, high word Table 99
  ADIS_REGADDR_XA_BIAS_LOW = 0x021C,   // Calibration, offset, accelerometer, x-axis, low word Table 110
  ADIS_REGADDR_XA_BIAS_HIGH = 0x021E,  // Calibration, offset, accelerometer, x-axis, high word Table 107
  ADIS_REGADDR_YA_BIAS_LOW = 0x0220,   // Calibration, offset, accelerometer, y-axis, low word Table 111
  ADIS_REGADDR_YA_BIAS_HIGH = 0x0222,  // Calibration, offset, accelerometer, y-axis, high word Table 108
  ADIS_REGADDR_ZA_BIAS_LOW = 0x0224,   // Calibration, offset, accelerometer, z-axis, low word Table 112
  ADIS_REGADDR_ZA_BIAS_HIGH = 0x0226,  // Calibration, offset, accelerometer, z-axis, high word Table 109
  ADIS_REGADDR_HARD_IRON_X = 0x0228,   // Calibration, hard iron, magnetometer, x-axis Table 116
  ADIS_REGADDR_HARD_IRON_Y = 0x022A,   // Calibration, hard iron, magnetometer, y-axis Table 117
  ADIS_REGADDR_HARD_IRON_Z = 0x022C,   // Calibration, hard iron, magnetometer, z-axis Table 118
  ADIS_REGADDR_SOFT_IRON_S11 = 0x022E, // Calibration, soft iron, magnetometer, S11 Table 120
  ADIS_REGADDR_SOFT_IRON_S12 = 0x0230, // Calibration, soft iron, magnetometer, S12 Table 121
  ADIS_REGADDR_SOFT_IRON_S13 = 0x0232, // Calibration, soft iron, magnetometer, S13 Table 122
  ADIS_REGADDR_SOFT_IRON_S21 = 0x0234, // Calibration, soft iron, magnetometer, S21 Table 123
  ADIS_REGADDR_SOFT_IRON_S22 = 0x0236, // Calibration, soft iron, magnetometer, S22 Table 124
  ADIS_REGADDR_SOFT_IRON_S23 = 0x0238, // Calibration, soft iron, magnetometer, S23 Table 125
  ADIS_REGADDR_SOFT_IRON_S31 = 0x023A, // Calibration, soft iron, magnetometer, S31 Table 126
  ADIS_REGADDR_SOFT_IRON_S32 = 0x023C, // Calibration, soft iron, magnetometer, S32 Table 127
  ADIS_REGADDR_SOFT_IRON_S33 = 0x023E, // Calibration, soft iron, magnetometer, S33 Table 128
  ADIS_REGADDR_BR_BIAS_LOW = 0x0240,   // Calibration, offset, barometer, low word Table 131
  ADIS_REGADDR_BR_BIAS_HIGH = 0x0242,  // Calibration, offset, barometer, high word Table 130
  ADIS_REGADDR_REFMTX_R11 = 0x0262,    // Reference transformation matrix, R11 Table 84
  ADIS_REGADDR_REFMTX_R12 = 0x0264,    // Reference transformation matrix, R12 Table 85
  ADIS_REGADDR_REFMTX_R13 = 0x0266,    // Reference transformation matrix, R13 Table 86
  ADIS_REGADDR_REFMTX_R21 = 0x0268,    // Reference transformation matrix, R21 Table 87
  ADIS_REGADDR_REFMTX_R22 = 0x026A,    // Reference transformation matrix, R22 Table 88
  ADIS_REGADDR_REFMTX_R23 = 0x026C,    // Reference transformation matrix, R23 Table 89
  ADIS_REGADDR_REFMTX_R31 = 0x026E,    // Reference transformation matrix, R31 Table 90
  ADIS_REGADDR_REFMTX_R32 = 0x0270,    // Reference transformation matrix, R32 Table 91
  ADIS_REGADDR_REFMTX_R33 = 0x0272,    // Reference transformation matrix, R33 Table 92
  ADIS_REGADDR_USER_SCR_1 = 0x0274,    // User Scratch Register 1 Table 152
  ADIS_REGADDR_USER_SCR_2 = 0x0276,    // User Scratch Register 2 Table 153
  ADIS_REGADDR_USER_SCR_3 = 0x0278,    // User Scratch Register 3 Table 154
  ADIS_REGADDR_USER_SCR_4 = 0x027A,    // User Scratch Register 4 Table 155
  ADIS_REGADDR_FLSHCNT_LOW = 0x027C,   // Diagnostic, flash memory count, low word Table 147
  ADIS_REGADDR_FLSHCNT_HIGH = 0x027E,  // Diagnostic, flash memory count, high word Table 148

  ADIS_REGADDR_GLOB_CMD = 0x0302,      // Control, global commands Table 146
  ADIS_REGADDR_FNCTIO_CTRL = 0x0306,   // 0x000D Control, I/O pins, functional definitions Table 149
  ADIS_REGADDR_GPIO_CTRL = 0x0308,     // 0x00X01 Control, I/O pins, general purpose Table 150
  ADIS_REGADDR_CONFIG = 0x030A,        // 0x00C0 Control, clock, and miscellaneous correction Table 106
  ADIS_REGADDR_DEC_RATE = 0x030C,      // 0x0000 Control, output sample rate decimation Table 67
  ADIS_REGADDR_SLP_CNT = 0x0310,       // Control, power-down/sleep mode Table 151
  ADIS_REGADDR_FILTR_BNK_0 = 0x0316,   // 0x0000 Filter selection Table 69
  ADIS_REGADDR_FILTR_BNK_1 = 0x0318,   // 0x0000 Filter selection Table 70
  ADIS_REGADDR_ALM_CNFG_0 = 0x0320,    // 0x0000 Alarm configuration Table 142
  ADIS_REGADDR_ALM_CNFG_1 = 0x0322,    // 0x0000 Alarm configuration Table 143
  ADIS_REGADDR_ALM_CNFG_2 = 0x0324,    // 0x0000 Alarm configuration Table 144
  ADIS_REGADDR_XG_ALM_MAGN = 0x0328,   // 0x0000 Alarm, x-axis gyroscope threshold setting Table 132
  ADIS_REGADDR_YG_ALM_MAGN = 0x032A,   // 0x0000 Alarm, y-axis gyroscope threshold setting Table 133
  ADIS_REGADDR_ZG_ALM_MAGN = 0x032C,   // 0x0000 Alarm, z-axis gyroscope threshold setting Table 134
  ADIS_REGADDR_XA_ALM_MAGN = 0x032E,   // 0x0000 Alarm, x-axis accelerometer threshold Table 135
  ADIS_REGADDR_YA_ALM_MAGN = 0x0330,   // 0x0000 Alarm, y-axis accelerometer threshold Table 136
  ADIS_REGADDR_ZA_ALM_MAGN = 0x0332,   // 0x0000 Alarm, z-axis accelerometer threshold Table 137
  ADIS_REGADDR_XM_ALM_MAGN = 0x0334,   // 0x0000 Alarm, x-axis magnetometer threshold Table 138
  ADIS_REGADDR_YM_ALM_MAGN = 0x0336,   // 0x0000 Alarm, y-axis magnetometer threshold Table 139
  ADIS_REGADDR_ZM_ALM_MAGN = 0x0338,   // 0x0000 Alarm, z-axis magnetometer threshold Table 140
  ADIS_REGADDR_BR_ALM_MAGN = 0x033A,   // 0x0000 Alarm, barometer threshold setting Table 141
  ADIS_REGADDR_EKF_CNFG = 0x0350,      // 0x0200 Extended Kalman filter configuration Table 94
  ADIS_REGADDR_DECLN_ANGL = 0x0354,    // 0x0000 Declination angle Table 93
  ADIS_REGADDR_ACC_DISTB_THR = 0x0356, // 0x0020 Accelerometer disturbance threshold Table 95
  ADIS_REGADDR_MAG_DISTB_THR = 0x0358, // 0x0030 Magnetometer disturbance threshold Table 96
  ADIS_REGADDR_QCVR_NOIS_LWR = 0x0360, // 0xC5AC Process covariance, gyroscope noise, lower word Table 77
  ADIS_REGADDR_QCVR_NOIS_UPR = 0x0362, // 0x3727 Process covariance, gyroscope noise, upper word Table 76
  ADIS_REGADDR_QCVR_RRW_LWR = 0x0364,  // 0xE6FF Process covariance, gyroscope RRW, lower word Table 79
  ADIS_REGADDR_QCVR_RRW_UPR = 0x0366,  // 0x2E5B Process covariance, gyroscope RRW, upper word Table 78
  ADIS_REGADDR_RCVR_ACC_LWR = 0x036C,  // 0x705F Measurement covariance, accelerometer, upper Table 81
  ADIS_REGADDR_RCVR_ACC_UPR = 0x036E,  // 0x3189 Measurement covariance, accelerometer, lower Table 80
  ADIS_REGADDR_RCVR_MAG_LWR = 0x0370,  // 0xCC77 Measurement covariance, magnetometer, upper Table 83
  ADIS_REGADDR_RCVR_MAG_UPR = 0x0372,  // 0x32AB Measurement covariance, magnetometer, lower Table 82
  ADIS_REGADDR_FIRM_REV = 0x0378,      // Firmware revision Table 62
  ADIS_REGADDR_FIRM_DM = 0x037A,       // Firmware programming date: day/month Table 63
  ADIS_REGADDR_FIRM_Y = 0x037C,        // Firmware programming date: year Table 64

  ADIS_REGADDR_SERIAL_NUM = 0x0420,  // Serial number Table 66
  ADIS_REGADDR_FIR_COEF_A0 = 0x0502, // FIR Filter Bank A, Coefficients 0 through 59 Table 71
  ADIS_REGADDR_FIR_COEF_A1 = 0x0602, // FIR Filter Bank A, Coefficients 60 through 119 Table 71
  ADIS_REGADDR_FIR_COEF_B0 = 0x0702, // FIR Filter Bank B, Coefficients 0 through 59 Table 72
  ADIS_REGADDR_FIR_COEF_B1 = 0x0802, // FIR Filter Bank B, Coefficients 60 through 119 Table 72
  ADIS_REGADDR_FIR_COEF_C0 = 0x0902, // FIR Filter Bank C, Coefficients 0 through 59 Table 73
  ADIS_REGADDR_FIR_COEF_C1 = 0x0A02, // FIR Filter Bank C, Coefficients 60 through 119 Table 73
  ADIS_REGADDR_FIR_COEF_D0 = 0x0B02, // FIR Filter Bank D, Coefficients 0 through 59 Table 74
  ADIS_REGADDR_FIR_COEF_D1 = 0x0C02, // FIR Filter Bank D, Coefficients 60 through 119 Table 74
};

enum {
  // This bit indicates a write command
  ADIS_REGVAL_CMD_READ = (0 << 15),
  ADIS_REGVAL_CMD_WRITE = (1 << 15),
};

// Command word to write a register
static inline U16 adis_wr_reg_cmd(U16 regaddr, U16 regval)
{
  return ADIS_REGVAL_CMD_WRITE | ((U16)(regaddr & 0xff) << 8) | (regval & 0xff);
}
// Command word to request a read of a register
static inline U16 adis_rd_reg_cmd(U16 regaddr)
{
  return ADIS_REGVAL_CMD_READ | ((U16)(regaddr & 0xff) << 8);
}

// Convert a register return value to fixed point, given the place value of the LSB
static inline dsp824 adis_adc14_to_dsp824(U16 adcval, dsp824 lsb_value)
{
  return ((S32)((S16)(adcval << 2) >> 2)) * lsb_value;
}
static inline dsp824 adis_adc12_to_dsp824(U16 adcval, dsp824 lsb_value)
{
  return ((S32)((U16)(adcval << 4) >> 4)) * lsb_value;
}

/*
  Convert supply to volts
  Convert gyro to radians/sec
  Convert accel to meters/sec/sec
  Convert temp to 100C units relative to 25C. So 0 = 25C, 0.5 = 75C 1.0 = 125C
  Convert aux_adc reading to volts

  These numbers are all adequately precise in 8.24.

*/
#define ADIS16480_SUPPLY_SCALE_LSB (2.42e-3)
#define ADIS16480_GYRO_SCALE_LSB (0.0200 * M_PI / 180.0) // ADIS16480 rev C, Table 10
#define ADIS16480_ACCEL_SCALE_LSB (0.800e−3 * 9.81)      // ADIS16480 rev C, Table 17
#define ADIS16480_BAROM_SCALE_LSB (40e-6)                // ADIS16480 rev C, Table 130
#define ADIS16480_TEMP_SCALE_LSB (0.00565 / 100.0)

void printbuf_format_adis_register_status(printbuf *out, int status, int width, int prec, int sign, int pad);
void printbuf_format_adis_regaddr(printbuf *out, int regaddr, int width, int prec, int sign, int pad);
