#include "common/std_headers.h"

#include "./hwdefs.h"

#include "gpio.h"
#include "wdt.h"

#include "embedded/avr32/macb.h"
#include "embedded/embedded_hostif.h"
#include "embedded/embedded_pktcom.h"
#include "embedded/embedded_timing.h"
#include "embedded/embedded_debug.h"

#include "./adis.h"

#include "embedded/avr32/avrhw.h"
#include "drivers/ssc/fw/beeper.h"
#include "drivers/ssc/fw/ads8344.h"
#include "drivers/ssc/fw/ssc_main.h"
#include "./ssc_gyro.h"
#include "drivers/ssc/fw/hostif.h"

static rate_generator ssc_gyro_polling_rg;

void ssc_gyro_setup(void)
{
  rate_generator_enable(&ssc_gyro_polling_rg, SECONDSTOTICKS(0.001), 3);

  debug_printf("adis...");
  adis_setup();
  wdt_clear();

  debug_printf("ads8344...");
  ads8344_init(&ads83440);
  wdt_clear();
}

void ssc_poll_work(void)
{
  adis_engine_poll(&adis0);

  rate_generator_set_budget(&ssc_gyro_polling_rg, 10);
  if (rate_generator_update(&ssc_gyro_polling_rg)) {
    ads8344_poll(&ads83440);
    rate_generator_done(&ssc_gyro_polling_rg);
  }
}

void ssc_poll_logging(void)
{
  ads8344_log_status(&ads83440, "ads83440");
  adis_engine_log_status(&adis0, "adis0");
}


void host_fill_update(host_t *it, pkt_tx_buf *tx)
{
  // Send up to 4 adis samples per packet. A sample is 92 bytes, so 4 is no problem in a UDP packet.
  for (int sampi=0; sampi<4; sampi++) {
    adis_sample_t adis_sample;
    if (adis_get_sample(&adis0, &adis_sample) <= 0) break;
    pkt_tx_u8(tx, 'a');
    pkt_tx_adis_sample(tx, &adis_sample);
  }
}

U8 host_handle_cmd(host_t *it, U8 cmd, pkt_rx_buf *rx)
{
  return 0;
}

void host_init(host_t *it)
{
}

void host_shutdown(host_t *it)
{
}

void peer_send_update(host_t *it)
{
}

int main(void)
{
  ssc_basic_setup();
  ssc_gyro_setup();

  debug_printf("host...");
  host_init(&host0);

  ssc_poll();
}
