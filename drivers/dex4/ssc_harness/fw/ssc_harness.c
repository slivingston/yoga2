#include "common/std_headers.h"

#include "./ssc_harness.h"

#include "./hwdefs.h"
#include "gpio.h"
#include "wdt.h"
#include "embedded/embedded_hostif.h"
#include "embedded/embedded_pktcom.h"
#include "embedded/embedded_timing.h"
#include "embedded/embedded_debug.h"
#include "dspcore/dspcore.h"
#include "drivers/ssc/fw/ls7266.h"
#include "drivers/ssc/fw/pressure.h"
#include "drivers/ssc/fw/dac.h"
#include "drivers/ssc/fw/binvalve.h"
#include "drivers/ssc/fw/clapper.h"
#include "drivers/ssc/fw/camtrig.h"
#include "drivers/ssc/fw/ads8344.h"
#include "drivers/ssc/fw/ssc_main.h"
#include "drivers/ssc/fw/conf_eth.h"
#include "embedded/avr32/ethcom.h"
#include "drivers/ssc/fw/hostif.h"

ssc_harness_t harness0;

static rate_generator ssc_harness_polling_rg;


void ssc_harness_setup(void)
{
  harness0.drop_mode = false;

  rate_generator_enable(&ssc_harness_polling_rg, SECONDSTOTICKS(0.001), 3);

  debug_printf("ls7266...");
  ls7266_init(&ls72660);
  wdt_clear();

  debug_printf("pressure...");
  pressure_init(&pressure0);
  wdt_clear();

  debug_printf("ads8344...");
  ads8344_init(&ads83440);
  wdt_clear();

  debug_printf("dac...");
  dac_init(&dac0);
  wdt_clear();

  debug_printf("clapper...");
  clapper_init(&clapper0);
  wdt_clear();

  debug_printf("binvalve...");
  binvalve_init(&binvalve0);
  wdt_clear();

  debug_printf("lift...");
  for (int i=0; i<N_SSC_HARNESS_LIFTS; i++) {
    ssc_lift_init(&harness0.lift_states[i]);
  }

  debug_printf("camtrig...");
  camtrig_init(&camtrig0);
}

static dsp824 conv_150pg_pressure(uint16_t raw)
{
  if (raw & (uint16_t)0xc000) {
    // error indication
    return DSP824(0.0);
  }
  /* We have the 150PG sensors, 150 psi gage.
     So 10% reading is 14.7 psi (at sea level) or 0.101 MPa
     and 90% reading is 164.7 psi or 1.136 MPa
     The reading is 14 bits, or 16384 counts.
     We want absolute MPa

     So the slope is:
       >>> (1.136-0.101) / (16384*0.8)
       7.896423339843748e-05
     Or as an 8.24 number:
       >>> (1.136-0.101) / (16384*0.8) * (1<<24)
       1324.7999999999997
     And the intercept is:
       >>> 0.101 - (16384*0.1) * 7.896423339843748e-05
       -0.028374999999999984
  */

  return raw * DSP824(7.896423339843748e-05) + DSP824(-0.028374999999999984);
}

static uint16_t conv_valve_dac(dsp824 value)
{
  /*
    Convert a valve command in (dsp824: -1 .. +1) to the DAC value required to get 0..10v.
    Due to a botch in the schematic in SSC_HARNESS1 (same PCB as SSC_LEG1), the DAC value to get 5v is 2178 instead of 2048.
    Divide by 8192 to convert 1<<24 to 1<<11
  */
  return dsplim(2178 - dsplim(value, DSP824(-1.0), DSP824(+1.0)) / 8192, 0, 4095);
}

void ssc_harness_core(void)
{
  harness0.update_ticks = get_ticks64();

  // Read sensors

  for (int i=0; i<N_SSC_HARNESS_LIFTS; i++) {
    harness0.lift_states[i].ext_pressure = conv_150pg_pressure(pressure0.values[i*2 + 0]);
    harness0.lift_states[i].ret_pressure = conv_150pg_pressure(pressure0.values[i*2 + 1]);
  }
  // Sensitivity of the Celesco PT5E-100-V62-FR-100-AB-TTL-M6 is 100 pulses/inch, and we have x4 and want to convert to meters
  harness0.lift_states[0].pos = ls72660.cur.encs[0] * DSP824(-0.0254 / 100.0 / 4.0);

  // Normal mode
  int64_t cmd_age = harness0.cmd_rx_ticks ? get_ticks64() - harness0.cmd_rx_ticks : SECONDSTOTICKS(30.0);

  if (harness0.drop_mode) {
      harness0.lift_cmds[0].base_valve = DSP824(0.0);
      harness0.lift_cmds[0].desired_force = DSP1616(+.0); // approx weight of robot in N
      harness0.lift_cmds[0].desired_pos = DSP824(-1.0); // meters up
      // area in mm^2, since pressure in MPA and force in N
      harness0.lift_cmds[0].ext_pressure_to_force = DSP1616(+50.8 * 50.8 * M_PI/4.0);
      harness0.lift_cmds[0].ret_pressure_to_force = DSP1616(-50.8 * 50.8 * M_PI/4.0);
      harness0.lift_cmds[0].force_feedback_coeff = DSP824(-0.002);
      harness0.lift_cmds[0].force_feedback_lim_lo = DSP824(-0.5);
      harness0.lift_cmds[0].force_feedback_lim_hi = DSP824(+0.5);
      harness0.lift_cmds[0].pos_feedback_coeff = DSP1616(-2000.0);
      harness0.lift_cmds[0].pos_feedback_lim_lo = DSP1616(-500.0);
      harness0.lift_cmds[0].pos_feedback_lim_hi = DSP1616(+500.0);
  }
  else {
    // normal mode
    if (cmd_age > (int64_t)SECONDSTOTICKS(0.2)) {
      harness0.lift_cmds[0].base_valve = DSP824(0.1);
      harness0.lift_cmds[0].desired_force = DSP1616(+800.0); // approx weight of robot in N
      harness0.lift_cmds[0].desired_pos = DSP824(0.0); // meters up
      // area in mm^2, since pressure in MPA and force in N
      harness0.lift_cmds[0].ext_pressure_to_force = DSP1616(+50.8 * 50.8 * M_PI/4.0);
      harness0.lift_cmds[0].ret_pressure_to_force = DSP1616(-50.8 * 50.8 * M_PI/4.0);
      harness0.lift_cmds[0].force_feedback_coeff = DSP824(-0.002);
      harness0.lift_cmds[0].force_feedback_lim_lo = DSP824(-0.3);
      harness0.lift_cmds[0].force_feedback_lim_hi = DSP824(+0.9);
      harness0.lift_cmds[0].pos_feedback_coeff = DSP1616(-3000.0);
      harness0.lift_cmds[0].pos_feedback_lim_lo = DSP1616(-500.0);
      harness0.lift_cmds[0].pos_feedback_lim_hi = DSP1616(+500.0);

      harness0.req_camtrig_mask = 0;
      for (int trigi=0; trigi<N_SSC_HARNESS_CAMTRIGS; trigi++) {
        harness0.last_camtrig_ticks[trigi] = 0;
      }
    }
  }

  // Update servos
  for (int i=0; i<N_SSC_HARNESS_LIFTS; i++) {
    ssc_lift_cmd_t *cmd = &harness0.lift_cmds[i];
    ssc_lift_state_t *st = &harness0.lift_states[i];

    ssc_lift_run(cmd, st);
  }


  for (int i=0; i < N_SSC_HARNESS_LIFTS; i++) {
    dac0.out_values[i] = conv_valve_dac(harness0.lift_states[i].valve);
  }
  dac0.vpwr_on = true;

}

void ssc_poll_work(void)
{
  rate_generator_set_budget(&ssc_harness_polling_rg, 10);
  if (rate_generator_update(&ssc_harness_polling_rg)) {
    ls7266_poll(&ls72660);
    pressure_poll(&pressure0);
    ads8344_poll(&ads83440);
    ssc_harness_core();

    dac_sync(&dac0);
    binvalve_sync(&binvalve0);
    rate_generator_done(&ssc_harness_polling_rg);
  }


  {
    uint32_t msecs = (int)(harness0.update_ticks / SECONDSTOTICKS(0.01));
    uint32_t digit = (msecs / 10u) % 10u;
    if (msecs % 10u == 0) {
      if (!clapper0.segments) {
        clapper_set_digit(&clapper0, digit);
        harness0.last_clapper_ticks = get_ticks64();
        harness0.last_clapper_digit = '0' + digit;
        clapper_sync(&clapper0);
      }
    } else {
      if (clapper0.segments) {
        clapper_set_off(&clapper0);
        clapper_sync(&clapper0);
      }
    }
  }

  for (int trigi=0; trigi<N_SSC_HARNESS_CAMTRIGS; trigi++) {
    uint8_t bitmask = ((uint8_t)1<<trigi);
    if ((harness0.req_camtrig_mask & bitmask) && harness0.last_camtrig_ticks[trigi] == 0) {
      harness0.last_camtrig_ticks[trigi] = harness0.send_camtrig_ticks[trigi] = get_ticks64();
      camtrig_trigger(&camtrig0, trigi, 1);
    }
    else if (get_ticks64() - harness0.last_camtrig_ticks[trigi] > SECONDSTOTICKS(0.1)) {
      camtrig_trigger(&camtrig0, trigi, 0);
    }
  }
}

void ssc_poll_logging(void)
{
  ls7266_log_status(&ls72660, "ls72660");
  pressure_log_status(&pressure0, "pressure0");
  ads8344_log_status(&ads83440, "ads83440");
  dac_log_status(&dac0, "dac0");
  clapper_log_status(&clapper0, "clapper0");
  binvalve_log_status(&binvalve0, "binvalve0");
}

void ssc_harness_handle_cmd_V(pkt_rx_buf *rx)
{
  harness0.drop_mode = false;
  for (int i=0; i<N_SSC_HARNESS_LIFTS; i++) {
    pkt_rx_dsp824p(rx, &harness0.lift_cmds[i].base_valve);
    pkt_rx_dsp1616p(rx, &harness0.lift_cmds[i].desired_force);
    pkt_rx_dsp824p(rx, &harness0.lift_cmds[i].desired_pos);
    pkt_rx_dsp1616p(rx, &harness0.lift_cmds[i].ext_pressure_to_force);
    pkt_rx_dsp1616p(rx, &harness0.lift_cmds[i].ret_pressure_to_force);
    pkt_rx_dsp824p(rx, &harness0.lift_cmds[i].force_feedback_coeff);
    pkt_rx_dsp824p(rx, &harness0.lift_cmds[i].force_feedback_lim_lo);
    pkt_rx_dsp824p(rx, &harness0.lift_cmds[i].force_feedback_lim_hi);
    pkt_rx_dsp1616p(rx, &harness0.lift_cmds[i].pos_feedback_coeff);
    pkt_rx_dsp1616p(rx, &harness0.lift_cmds[i].pos_feedback_lim_lo);
    pkt_rx_dsp1616p(rx, &harness0.lift_cmds[i].pos_feedback_lim_hi);
  }
  pkt_rx_u8p(rx, &harness0.req_camtrig_mask);

  harness0.cmd_rx_ticks = get_ticks64();
}

void ssc_harness_handle_cmd_M(pkt_rx_buf *rx, pkt_tx_buf *tx)
{
  U8 subtype = pkt_rx_u8(rx);
  if (subtype == 'R') {
    for (int i=0; i<LS7266_NCOUNTERS; i++) {
      ls7266_zero_counter(&ls72660, i);
    }
    pkt_tx_str(tx, "Zeroed encoders");
  }
  else if (subtype == 'D') {
    harness0.drop_mode = true;
  }
  else if (subtype == '0') {
    harness0.drop_mode = false;
  }
  else {
    pkt_tx_str(tx, "Unknown message");
  }
}


void ssc_harness_emit_state(pkt_tx_buf *tx)
{
  pkt_tx_u64p(tx, &harness0.update_ticks);
  for (int i=0; i<N_SSC_HARNESS_LIFTS; i++) {
    pkt_tx_dsp824p(tx, &harness0.lift_states[i].ext_pressure);
    pkt_tx_dsp824p(tx, &harness0.lift_states[i].ret_pressure);
    pkt_tx_dsp824p(tx, &harness0.lift_states[i].pos);
    pkt_tx_dsp1616p(tx, &harness0.lift_states[i].force);
    pkt_tx_dsp1616p(tx, &harness0.lift_states[i].pos_feedback);
    pkt_tx_dsp824p(tx, &harness0.lift_states[i].force_feedback);
    pkt_tx_dsp824p(tx, &harness0.lift_states[i].valve);
  }
}

void host_fill_update(host_t *it, pkt_tx_buf *tx)
{

  if (harness0.update_ticks != it->last_harness0_update_ticks) {
    pkt_tx_u8(tx, 'h');
    ssc_harness_emit_state(tx);
    it->last_harness0_update_ticks = harness0.update_ticks;
  }

  if (harness0.last_clapper_ticks) {
    pkt_tx_u8(tx, 'c');
    pkt_tx_u64p(tx, &harness0.last_clapper_ticks);
    pkt_tx_u8p(tx, &harness0.last_clapper_digit);
    harness0.last_clapper_digit = 0;
    harness0.last_clapper_ticks = 0;
  }

  for (uint8_t trigi=0; trigi<N_SSC_HARNESS_CAMTRIGS; trigi++) {
    if (harness0.send_camtrig_ticks[trigi] != 0) {
      pkt_tx_u8(tx, 't');
      pkt_tx_u64p(tx, &harness0.send_camtrig_ticks[trigi]);
      pkt_tx_u8(tx, trigi);
      harness0.send_camtrig_ticks[trigi] = 0;
    }
  }
}

U8 host_handle_cmd(host_t *it, U8 cmd, pkt_rx_buf *rx)
{
  if (cmd == 'V') {
    ssc_harness_handle_cmd_V(rx);
    return 1;
  }

  else if (cmd == 'M') {
    pkt_tx_buf *tx = ethcom_start_tx_udp(&ethcom0, &rx->src);
    if (!tx) {
      log_printf("reply to M: no tx buf");
      return 0;
    }
    pkt_tx_u8(tx, 'm');
    int orig_len = tx->len;

    ssc_harness_handle_cmd_M(rx, tx);

    if (tx->len > orig_len) {
      pkt_tx_u8(tx, '\n');
      ethcom_tx_pkt(&ethcom0, tx);
    }
    else {
      free_tx_buf(tx);
    }
    return 1;
  }

  return 0;
}

void host_init(host_t *it)
{
}

void host_shutdown(host_t *it)
{
  dac_shutdown(&dac0);
}

void peer_send_update(host_t *it)
{
}

int main(void)
{
  ssc_basic_setup();
  ssc_harness_setup();

  debug_printf("host...");
  host_init(&host0);

  ssc_poll();
}
