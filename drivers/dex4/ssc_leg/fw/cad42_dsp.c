#include "./ssc_leg.h"
#include "./hwdefs.h"
#include "build.src/cylVol_dsp.h"

#define REVPRESSURE(ROD_DIA, CYL_DIA) \
  DSP824(-(1.0 - ((ROD_DIA)*(ROD_DIA)/((CYL_DIA)*(CYL_DIA)))))

void calcTorques_lht(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = lhtVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 2.5));
}

void calcTorques_lhf(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = lhfVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 2.5));
}

void calcTorques_lhs(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = lhsVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 2.0));
}

void calcTorques_lki(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = lkiVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 1.5));
}

void calcTorques_lko(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = lkoVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 1.5));
}

void calcTorques_lai(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = laiVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.5, 1.5));
}

void calcTorques_lao(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = laoVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.5, 1.5));
}





void calcTorques_rht(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = rhtVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 2.5));
}

void calcTorques_rhf(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = rhfVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 2.5));
}

void calcTorques_rhs(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = rhsVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 2.0));
}

void calcTorques_rki(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = rkiVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 1.5));
}

void calcTorques_rko(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = rkoVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.625, 1.5));
}

void calcTorques_rai(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = raiVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.5, 1.5));
}

void calcTorques_rao(ssc_pneuservo_state_t *it)
{
  dsp1616 t1 = raoVolDeriv_dsp824_dsp1616(it->pos);
  it->ext_pressure_to_torque = t1;
  it->ret_pressure_to_torque = mul_dsp1616_dsp824_dsp1616(t1, REVPRESSURE(0.5, 1.5));
}

