#pragma once


void calcTorques_lht(ssc_pneuservo_state_t *it);
void calcTorques_lhf(ssc_pneuservo_state_t *it);
void calcTorques_lhs(ssc_pneuservo_state_t *it);
void calcTorques_lki(ssc_pneuservo_state_t *it);
void calcTorques_lko(ssc_pneuservo_state_t *it);
void calcTorques_lai(ssc_pneuservo_state_t *it);
void calcTorques_lao(ssc_pneuservo_state_t *it);


void calcTorques_rht(ssc_pneuservo_state_t *it);
void calcTorques_rhf(ssc_pneuservo_state_t *it);
void calcTorques_rhs(ssc_pneuservo_state_t *it);
void calcTorques_rki(ssc_pneuservo_state_t *it);
void calcTorques_rko(ssc_pneuservo_state_t *it);
void calcTorques_rai(ssc_pneuservo_state_t *it);
void calcTorques_rao(ssc_pneuservo_state_t *it);
