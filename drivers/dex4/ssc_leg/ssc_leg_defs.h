#pragma once

#define N_SSC_LEG_VALVES 8
#define N_SSC_LEG_DACS (N_SSC_LEG_VALVES)

#define N_SSC_LEG_PRESSURE 16

#define N_SSC_LEG_ENCODERS 6

#define N_SSC_LEG_POTS 4
