#include "../../../comm/ssc_network_engine.h"
#include "./ssc_leg_io.h"

struct SscLegNetworkEngine : SscNetworkEngine {
  void setupHandlers() override;
  void setupAccessors() override;
  void update(EngineUpdateContext &ctx) override;

  R lastCmdTs{0.0};

  shared_ptr<YogaTimeseq> stateOut;
  unique_ptr<LegStateAccessorSuite> stateAs;
  shared_ptr<YogaTimeseq> cmdIn;
  unique_ptr<LegCmdAccessorSuite> cmdAs;
};

void SscLegNetworkEngine::setupAccessors()
{
  SscNetworkEngine::setupAccessors();
  stateAs = make_unique<LegStateAccessorSuite>(stateOut->type);
  cmdAs = make_unique<LegCmdAccessorSuite>(cmdIn->type);
}

void SscLegNetworkEngine::setupHandlers()
{
  SscNetworkEngine::setupHandlers();
  updateHandlers[(U8)'l'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
    R ts;
    rxSscTimestamp(*this, rx, ts, rxTime, pktTicks);
    auto state0 = stateOut->addNew(ts);

    if (config.boardInstance == 0) {
      stateAs->rxState0(rx, state0);
    }
    else if (config.boardInstance == 1) {
      stateAs->rxState1(rx, state0);
    }
    else {
      throw runtime_error("Bad boardInstance "s + repr(config.boardInstance));
    }
  };
#if 0 // WRITEME
  updateHandlers[(U8)'d'] = [this](double rxTime, packet &rx, U64 pktTicks, bool &error) {
    SscRawDump rawDump;
    rxSscRawDump(*this, rx, rawDump, rxTime, pktTicks);
    if (rawDumpOut) {
      rawDumpOut->add(fromExternalTime(rawDump.ts), rawDump);
    }
  };
#endif
}

void SscLegNetworkEngine::update(EngineUpdateContext &ctx)
{
  if (cmdIn) {
    auto out0 = cmdIn->getLast();
    /*
      Limit to 500 Hz, because I thought I saw queue overflows when I didn't limit it at all.
      But 1000 might work too, haven't thoroughly tested.
    */
    if (out0.ts > lastCmdTs + 0.0019) {
      packet tx(2048);
      tx.add((U8)'V');
      bool valveOpen = false;
      //cmdAs->valveOpen.rd(out0, valveOpen);
      tx.add(valveOpen ? (U8)'O' : (U8)'.');
      if (config.boardInstance == 0) {
        cmdAs->txCmd0(tx, out0);
      }
      else if (config.boardInstance == 1) {
        cmdAs->txCmd1(tx, out0);
      }
      else {
        throw runtime_error("Bad boardInstance "s + repr(config.boardInstance));
      }
      txPkt(tx);
      lastCmdTs = out0.ts;
    }
  }
  SscNetworkEngine::update(ctx);
}


static EngineRegister sscLegNetworkEngineReg(
  "sscLegNetwork",
  {
    "Dex4LegCmd",
    "Dex4LegState"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 2) {
      return ctx.logError("sscLegNetwork: expected 2 arguments (cmd, state)");
    }

    auto e = make_shared<SscLegNetworkEngine>();
    if (args.size() > 0) {
      e->cmdIn = args[0];
      e->cmdIn->isExtOut = true;
    }
    if (args.size() > 1) {
      e->stateOut = args[1];
      e->stateOut->needBackdateInitial = true;
      e->stateOut->isExtIn = true;
    }

    if (!e->setEngineConfig(ctx, spec)) return false;
    if (!e->setNetworkConfig(ctx, spec)) return false;
    trace->addEngine(engineName, e, true);

    return true;
  }
);

