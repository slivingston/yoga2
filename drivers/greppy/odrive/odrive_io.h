#include "../../../comm/ssc_io.h"


struct ODriveStateAccessorSuite {
  ODriveStateAccessorSuite(YogaType *t)
    : active(t, "active"),
      lVelActual(t, "lVelActual"),
      rVelActual(t, "rVelActual"),
      lTorque(t, "lTorque"),
      rTorque(t, "rTorque"),
      lPos(t, "lPos"),
      rPos(t, "rPos")
  {
  }

  YogaValueAccessor<R> active;
  YogaValueAccessor<R> lVelActual;
  YogaValueAccessor<R> rVelActual;
  YogaValueAccessor<R> lTorque;
  YogaValueAccessor<R> rTorque;
  YogaValueAccessor<R> lPos;
  YogaValueAccessor<R> rPos;

  void rxState(packet &rx, YogaValue const &yv)
  {
    rxDouble(rx, active, yv);
    rxDouble(rx, lVelActual, yv);
    rxDouble(rx, rVelActual, yv);
    rxDouble(rx, lTorque, yv);
    rxDouble(rx, rTorque, yv);
    rxDouble(rx, lPos, yv);
    rxDouble(rx, rPos, yv);
  }
};


struct ODriveCmdAccessorSuite {
  ODriveCmdAccessorSuite(YogaType *t)
    : lVel(t, "lVel"),
      rVel(t, "rVel")
  {
  }

  YogaValueAccessor<R> lVel;
  YogaValueAccessor<R> rVel;

  void txCmd(packet &tx, YogaValue const &yv)
  {
    txDouble(tx, lVel, yv);
    txDouble(tx, rVel, yv);
  }

};


