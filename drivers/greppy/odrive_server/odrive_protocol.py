import sys, ujson, socket, math, time, struct, os
import odrive, odrive.enums
import cProfile as profile

odrive0 = odrive.find_any("usb")
print("Found odrive serial", odrive0.serial_number, file=sys.stderr, flush=True)

for iter in range(0):
    print("START WRITES", file=sys.stderr, flush=True)
    odrive0.axis0.controller.vel_setpoint = 0
    #odrive0.axis1.controller.vel_setpoint = 0

    print("START READS", file=sys.stderr, flush=True)
    counts_per_sec_left = odrive0.axis0.encoder.vel_estimate
    #counts_per_sec_right = odrive0.axis1.encoder.vel_estimate


def reprw():
    for iter in range(1000):
        odrive0.axis0.controller.vel_setpoint = 0
        counts_per_sec_left = odrive0.axis0.encoder.vel_estimate

profile.run('reprw()')

#
# fd 6: a pipe
# fd 8: timerfd_create
# fd 9: /dev/bus/usb/001/004

# Writes look like this:
"""
30617      0.000462 ioctl(9, USBDEVFS_SUBMITURB, 0x9f7020) = 0
30623      0.000029 <... ppoll resumed> ) = 1 ([{fd=9, revents=POLLOUT}], left {59, 999196654})
30623      0.000047 ioctl(9, USBDEVFS_REAPURBNDELAY, 0x7f7ee73e80) = 0
30623      0.000073 ioctl(9, USBDEVFS_REAPURBNDELAY, 0x7f7ee73e80) = 0
30623      0.000071 timerfd_settime(8, 0, {it_interval={0, 0}, it_value={0, 0}}, NULL) = 0
30623      0.000075 ioctl(9, USBDEVFS_REAPURBNDELAY, 0x7f7ee73e80) = -1 EAGAIN (Resource temporarily unavailable)
30623      0.000020 timerfd_settime(8, TFD_TIMER_ABSTIME, {it_interval={0, 0}, it_value={322109, 659232000}}, NULL) = 0
30623      0.000079 ioctl(9, USBDEVFS_SUBMITURB, 0x7f70002730) = 0
30623      0.000084 ppoll([{fd=6, events=POLLIN}, {fd=8, events=POLLIN}, {fd=9, events=POLLOUT}], 3, {60, 0}, NULL, 0 <unfinished ...>
"""

# Reads look like this:
"""
30617      0.000406 ioctl(9, USBDEVFS_SUBMITURB, 0x9f7020) = 0
30623      0.000028 <... ppoll resumed> ) = 1 ([{fd=9, revents=POLLOUT}], left {59, 999292332})
30623      0.000047 ioctl(9, USBDEVFS_REAPURBNDELAY, 0x7f7ee73e80) = 0
30623      0.000071 ioctl(9, USBDEVFS_REAPURBNDELAY, 0x7f7ee73e80) = 0
30623      0.000072 timerfd_settime(8, 0, {it_interval={0, 0}, it_value={0, 0}}, NULL) = 0
30623      0.000074 ioctl(9, USBDEVFS_REAPURBNDELAY, 0x7f7ee73e80) = -1 EAGAIN (Resource temporarily unavailable)
30623      0.000019 timerfd_settime(8, TFD_TIMER_ABSTIME, {it_interval={0, 0}, it_value={322109, 661046000}}, NULL <unfinished ...>
30623      0.000019 <... timerfd_settime resumed> ) = 0
30623      0.000035 ioctl(9, USBDEVFS_SUBMITURB, 0x7f70002730) = 0
30623      0.000085 ppoll([{fd=6, events=POLLIN}, {fd=8, events=POLLIN}, {fd=9, events=POLLOUT}], 3, {60, 0}, NULL, 0 <unfinished ...>
"""
