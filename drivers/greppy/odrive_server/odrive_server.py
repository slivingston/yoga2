#
# ODrive docs at https://docs.odriverobotics.com/interfaces
#
# You may have to set your python path to link to the odrive tools directory,
# like `export PYTHONPATH=$PYTHONPATH:/home/nvidia/Projects/ODrive/tools`
#     * To send a new velocity: {"vel_left": N} and/or {"vel_right": N}
#     * To read status: {"read": true}
#     * Ctrl-C to quit.
#     * Prints the host and port in json on bootup, after the ODrive has been found
#     * Example output: {"current_right_actual": -0.6115353107452393,
#                        "torque_right_Nm": -0.3164803698854147,
#                        "vel_left_actual": 5.59063825518775,
#                        "torque_left_Nm": 0.6981195698261279,
#                        "current_left_actual": 1.3489770889282227, "status": "ok",
#                        "vel_right_actual": -3.1341456562336045}

import sys, ujson, socket, math, time, struct

ODRIVE0 = None
QUIT = False
ERROR_JSON = b'"status":"error"'
OK_DICT = {"status":"ok"}

ODRIVE0_HULL_SENSOR_COUNTS_PER_POLE_PAIR = 6
ODRIVE0_MOTOR_LEFT_COUNTS_PER_REV = None # Read from config on odrive
ODRIVE0_MOTOR_RIGHT_COUNTS_PER_REV = None # Read from config on odrive
ODRIVE0_MOTOR_LEFT_KV = 15.98
ODRIVE0_MOTOR_RIGHT_KV = ODRIVE0_MOTOR_LEFT_KV

RADIANS_PER_REV = 2.0 * math.pi
# Source: https://en.wikipedia.org/wiki/Motor_constants#Motor_Torque_constant
AMPS_KV_NM_CONVERSION = (3.0/2.0) / (math.sqrt(3) * (1.0/60.0) * 2.0*math.pi)


#### ODrive functions ##########################################################


def init_odrive():
    global ODRIVE0_HULL_SENSOR_COUNTS_PER_POLE_PAIR
    global ODRIVE0_MOTOR_LEFT_COUNTS_PER_REV
    global ODRIVE0_MOTOR_RIGHT_COUNTS_PER_REV
    print("[DEBUG] finding an odrive...", file=sys.stderr, flush=True)
    odrive0 = odrive.find_any("usb")
    print("[DEBUG] found odrive with serial number", odrive0.serial_number, file=sys.stderr, flush=True)
    ODRIVE0_MOTOR_LEFT_COUNTS_PER_REV = odrive0.axis0.motor.config.pole_pairs \
        * ODRIVE0_HULL_SENSOR_COUNTS_PER_POLE_PAIR
    ODRIVE0_MOTOR_RIGHT_COUNTS_PER_REV = odrive0.axis1.motor.config.pole_pairs \
        * ODRIVE0_HULL_SENSOR_COUNTS_PER_POLE_PAIR
    print("[DEBUG] CPR=", ODRIVE0_MOTOR_LEFT_COUNTS_PER_REV, ODRIVE0_MOTOR_RIGHT_COUNTS_PER_REV, file=sys.stderr, flush=True)
    return odrive0

def set_odrive_vel(target_rads_per_sec_left, target_rads_per_sec_right):
    global ODRIVE0_MOTOR_LEFT_COUNTS_PER_REV
    global ODRIVE0_MOTOR_RIGHT_COUNTS_PER_REV
    if target_rads_per_sec_left is not None:
        target_revs_per_sec_left = target_rads_per_sec_left / RADIANS_PER_REV
        target_counts_per_sec_left = target_revs_per_sec_left * \
            ODRIVE0_MOTOR_LEFT_COUNTS_PER_REV
        ODRIVE0.axis0.controller.vel_setpoint = target_counts_per_sec_left
    if target_rads_per_sec_right is not None:
        target_revs_per_sec_right = target_rads_per_sec_right / RADIANS_PER_REV
        target_counts_per_sec_right = target_revs_per_sec_right * \
          ODRIVE0_MOTOR_RIGHT_COUNTS_PER_REV
        ODRIVE0.axis1.controller.vel_setpoint = target_counts_per_sec_right

# FIXME TODO values are not translated to rads/sec correctly
def read_odrive_rads_per_sec():
    global ODRIVE0_MOTOR_LEFT_COUNTS_PER_REV
    global ODRIVE0_MOTOR_RIGHT_COUNTS_PER_REV
    counts_per_sec_left = ODRIVE0.axis0.encoder.vel_estimate
    counts_per_sec_right = ODRIVE0.axis1.encoder.vel_estimate
    rads_per_sec_left = 0
    if counts_per_sec_left != 0:
        revs_per_sec_left = counts_per_sec_left / ODRIVE0_MOTOR_LEFT_COUNTS_PER_REV
        rads_per_sec_left = RADIANS_PER_REV * revs_per_sec_left
    rads_per_sec_right = 0
    if counts_per_sec_right != 0:
      revs_per_sec_right = counts_per_sec_right / ODRIVE0_MOTOR_RIGHT_COUNTS_PER_REV
      rads_per_sec_right = RADIANS_PER_REV * revs_per_sec_right
    return {"lVelActual": rads_per_sec_left, "rVelActual": rads_per_sec_right}

def read_odrive_amps_and_Nm():
    global AMPS_KV_NM_CONVERSION
    global ODRIVE0_MOTOR_LEFT_KV
    global ODRIVE0_MOTOR_RIGHT_KV
    current_left = ODRIVE0.axis0.motor.current_control.Iq_setpoint
    current_right = ODRIVE0.axis1.motor.current_control.Iq_setpoint
    torque_left = AMPS_KV_NM_CONVERSION * current_left / ODRIVE0_MOTOR_LEFT_KV
    torque_right = AMPS_KV_NM_CONVERSION * current_right / ODRIVE0_MOTOR_RIGHT_KV
    return {
        "lCurrent": current_left,
        "rCurrent": current_right,
        "lTorque": torque_left,
        "rTorque": torque_right
    }

def read_odrive_pos():
    pos_left = ODRIVE0.axis0.encoder.pos_estimate * (RADIANS_PER_REV / ODRIVE0_MOTOR_LEFT_COUNTS_PER_REV)
    pos_right = ODRIVE0.axis1.encoder.pos_estimate * (RADIANS_PER_REV / ODRIVE0_MOTOR_RIGHT_COUNTS_PER_REV)
    return {
        "lPos": pos_left,
        "rPos": pos_right
    }


def idle_odrive():
    print("odrive to idle", file=sys.stderr, flush=True)
    ODRIVE0.axis0.requested_state = odrive.enums.AXIS_STATE_IDLE
    ODRIVE0.axis1.requested_state = odrive.enums.AXIS_STATE_IDLE

def activate_odrive():
    print("odrive to active", file=sys.stderr, flush=True)
    ODRIVE0.axis0.requested_state = odrive.enums.AXIS_STATE_CLOSED_LOOP_CONTROL
    ODRIVE0.axis1.requested_state = odrive.enums.AXIS_STATE_CLOSED_LOOP_CONTROL
    odrive.utils.dump_errors(ODRIVE0, True)

#### Server functions ##########################################################

class ODriveSscServer:

    def __init__(self, host, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((host, port))
        self.sock.settimeout(0.5)
        self.alive = True
        self.last_update_ts = 0.0
        self.client = None
        self.status = {}
        self.vphase = 0

    def run(self):
        while self.alive:
            try:
                data, addr = self.sock.recvfrom(8192)
            except socket.timeout:
                if 0: print('Timeout', file=sys.stderr, flush=True)
                if self.client is not None:
                    print('Client from', addr, 'gone', file=sys.stderr, flush=True)
                    idle_odrive()
                    self.client = None
                continue
            if self.client is None:
                self.client = addr
                print('Client from', addr, 'active', file=sys.stderr, flush=True)
                activate_odrive()
            elif addr != self.client:
                print('Competition between', addr, 'and', self.client, file=sys.stderr, flush=True)
                self.client = addr
            if 0: print('Got', data, addr, file=sys.stderr, flush=True)
            self.rx_data(data)

    def tx_data(self, data):
        self.sock.sendto(data, self.client)

    def rx_data(self, data):
        datai = 0
        while datai < len(data):
            cmd = data[datai]
            datai += 1
            if cmd == ord(b'U'):
                self.last_update_ts = time.time()
            elif cmd == ord(b'Q'):
                if (datai+4 < len(data) and
                    data[datai+0] == ord(b'3') and data[datai+1] == ord(b'b') and
                    data[datai+2] == ord(b'0') and data[datai+3] == ord(b'0') and
                    data[datai+4] == ord(b't')):
                    self.sock.close()
                    self.alive = False
                    break
            elif cmd == ord(b'V'):
                (self.tx_seq, lVel, rVel) = struct.unpack('<Idd', data[datai : datai+20])
                datai += 20
                self.handle_V(lVel, rVel)
                break
            else:
                print('Unknown command', cmd, file=sys.stderr, flush=True)
                break

    def handle_V(self, lVel, rVel):
        self.vphase += 1
        set_odrive_vel(lVel, rVel)

        self.status.update(read_odrive_rads_per_sec())
        if self.vphase == 1 or self.vphase%2 == 0:
            self.status.update(read_odrive_amps_and_Nm())
        if self.vphase%2 == 1:
            self.status.update(read_odrive_pos())
        self.status['active'] = 1.0
        self.tx_data(b'v' + struct.pack('<Iddddddd',
            self.tx_seq,
            self.status['active'],
            self.status['lVelActual'], self.status['rVelActual'],
            self.status['lTorque'], self.status['rTorque'],
            self.status['lPos'], self.status['rPos']))


def odrive_speed_test():
    loop_iters = 1000
    for epoch in range(20):
        t0 = time.time()
        maxwr = 0
        maxrd = 0
        for i in range(loop_iters):
            maxwr_t0 = time.time()
            set_odrive_vel(0, 0)
            maxwr = max(maxwr, time.time() - maxwr_t0)
        t1 = time.time()
        print(loop_iters*2, "writes of vel_setpoint", (t1-t0)/(loop_iters*2)*1000, "mS each", "max", maxwr/2*1000, file=sys.stderr)
        for i in range(loop_iters):
            maxrd_t0 = time.time()
            read_odrive_rads_per_sec()
            read_odrive_amps_and_Nm()
            read_odrive_pos()
            maxrd = max(maxrd, time.time() - maxrd_t0)
        t2 = time.time()
        print(loop_iters*6, "reads of vel_estimate, pos_estimate, and Iq_setpoint", (t2-t1)/(loop_iters*6)*1000, "mS each", "max", maxrd/6*1000, file=sys.stderr)
        print("Loop iteration time (2 writes, 6 reads): ",(t2-t1)/(loop_iters)*1000, "mS per loop", file=sys.stderr)
        print("Loop frequency: ",1000/(t2-t1), "Hz", file=sys.stderr)
        print("", file=sys.stderr, flush=True)



#### Main ######################################################################


def main():
    import argparse
    import socket
    import time, signal
    #sys.stderr = open('/tmp/odrive_server.err', 'w', buffering=1);

    parser = argparse.ArgumentParser(description=
        "A threaded server interface to an odrive."
    )
    parser.add_argument(
        '-f', '--fake', dest='fake_odrive', action='store_true',
        help="If enabled, fake the odrive connection"
    )
    parser.add_argument(
        '--speed-test', dest='speed_test', action='store_true',
        help="test speed of odrive api"
    )
    parser.set_defaults(fake=False)
    args = parser.parse_args()

    global odrive
    if args.fake_odrive:
        print("[INFO] using fake odrive", file=sys.stderr, flush=True)
        import fauxdrive as odrive
    else:
        import odrive
        import odrive.enums
        import odrive.utils



    global ODRIVE0
    init_t0 = time.time()
    ODRIVE0 = init_odrive()
    if ODRIVE0 is None:
        print("[ERROR] Could not find an odrive device", file=sys.stderr, flush=True)
        exit(1)

    if args.speed_test:
        print("Initializing odrive", (time.time() - init_t0)*1000, "mS")
        odrive_speed_test()

    else:
        server = ODriveSscServer('localhost', 20027)

        server.run()
    if not args.fake_odrive:
        idle_odrive()
        time.sleep(1)

if __name__ == '__main__':
    main()
