#include "../../comm/engine.h"
#include "../../timeseq/trace_blobs.h"
#include "../video/video_types.h"
#if defined(USE_REALSENSE)
#include <librealsense2/rs.hpp>
#endif

struct RealsenseEngine : GenericEngine {
  void update(EngineUpdateContext &ctx) override;
  void beforeSetRunning() override;
  void afterSetStopped() override;

  glm::dmat4 baseOrientation;

#if defined(USE_REALSENSE)
  rs2::pipeline dev;
  rs2::frame_queue depthq;
  rs2::frame_queue colorq;
#endif
  U64 curChunkId{0};

  shared_ptr<YogaTimeseq> colorOut;
  shared_ptr<YogaTimeseq> depthOut;
};


void RealsenseEngine::update(EngineUpdateContext &ctx)
{
#if defined(USE_REALSENSE)
  rs2::frameset fs;
  if (dev.poll_for_frames(&fs)) {

    rotateBlobChunkId(curChunkId);

    // WRITEME: these blobs are large, we should write them on a worker thread.

    auto depth = fs.get_depth_frame();

    YogaVideoFrame depthFrame;
    depthFrame.format = YVFF_DEPTH16;
    depthFrame.width = depth.get_width(); // FIXME
    depthFrame.height = depth.get_height();
    depthFrame.orientation = baseOrientation;
    depthFrame.blob = mkBlob(curChunkId, (U8 const *)depth.get_data(), depth.get_data_size());
    depthFrame.priority = 0;

    U8 *depthFrameYoga = depthOut->type->mkInstance(depthFrame);
    depthOut->add(ctx.now - rti->realTimeOffset, depthFrameYoga);

    auto color = fs.get_color_frame();

    YogaVideoFrame colorFrame;
    colorFrame.format = YVFF_RGB;
    colorFrame.width = color.get_width(); // FIXME
    colorFrame.height = color.get_height();
    colorFrame.orientation = baseOrientation;
    colorFrame.blob = mkBlob(curChunkId, (U8 const *)color.get_data(), color.get_data_size());
    colorFrame.priority = 0;

    U8 *colorFrameYoga = colorOut->type->mkInstance(colorFrame);
    colorOut->add(ctx.now - rti->realTimeOffset, colorFrameYoga);
    ctx.updateCnt ++;
  }
#endif
}

void RealsenseEngine::beforeSetRunning()
{
  curChunkId = mkBlobChunkId();

#if defined(USE_REALSENSE)
  rs2::config cfg;
  cfg.enable_stream(RS2_STREAM_DEPTH); // Enable default depth
  cfg.enable_stream(RS2_STREAM_COLOR, RS2_FORMAT_RGBA8);
  auto profile = dev.start(cfg);
#endif
}

void RealsenseEngine::afterSetStopped()
{
#if defined(USE_REALSENSE)
  dev.stop();
#endif
}


static EngineRegister realsenseEngineReg(
  "realsense", 
  {
    "VideoFrame",
    "VideoFrame"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 2) {
      return ctx.logError("RealsenseEngine: expected 2 arguments (color, depth)");
    }

    auto e = make_shared<RealsenseEngine>();
    if (args.size() > 0) {
      e->colorOut = args[0];
      e->colorOut->needBackdateInitial = true;
      e->colorOut->isExtIn = true;
    }
    if (args.size() > 1) {
      e->depthOut = args[1];
      e->depthOut->needBackdateInitial = true;
      e->depthOut->isExtIn = true;
    }

    if (!e->setEngineConfig(ctx, spec)) return false;

    e->baseOrientation = glm::dmat4(1.0);
    if (!spec->options->getValueForKey("baseOrientation", e->baseOrientation)) {
      return ctx.logError("Bad baseOrientation");
    }

    trace->addEngine(engineName, e, true);

    return true;
  }
);
