#include "common/std_headers.h"
#include "hwdefs.h"
#include "embedded/embedded_timing.h"
#include "embedded/embedded_debug.h"
#include "./ads8344.h"

ads8344_t ads83440;


void ads8344_init(ads8344_t *it)
{
  it->boned = false;

  fastgpio_init_0(&it->adc_clk, ADC_CLK_PIN);
  fastgpio_init_1(&it->adc_ncs, ADC_NCS_PIN);
  fastgpio_init_0(&it->adc_di0, ADC_DI0_PIN);
  fastgpio_init(&it->adc_busy0, ADC_BUSY0_PIN);
  fastgpio_init(&it->adc_do0, ADC_DO0_PIN);
}


/*
  See data sheet, Figure 6, "Internal Clock Mode Timing"
  The actual conversion step should take 16 internal clock cycles at 2.4 MHz, or 6.66 uS.
  Make sure our acquire time is at least 1.7 uS (it's the last 4 clocks of the command word)
  The number of nops below was tuned by oscilloscope :-(.
*/

void ads8344_poll(ads8344_t *it)
{
  if (it->boned) return;
  for (int chani=0; chani < ADS8344_N_RESULTS; chani++) {
    Disable_global_interrupt();
    // The addressing is borked. See data sheet table 1.
    // SGL=1, PD1=1, PD0=0 (non-differential, internal clock)
    uint8_t cmd = 0x86 | ((chani&1) << 6) | ((chani&6) << 3);

    fastgpio_set_0(&it->adc_ncs);
    for (int i=0; i < 8; i++) {
      fastgpio_set(&it->adc_di0, cmd & (0x80 >> i));
      asm("nop; nop; nop; nop");
      fastgpio_set_1(&it->adc_clk);
      asm("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop");
      fastgpio_set_0(&it->adc_clk);
    }
    fastgpio_set_0(&it->adc_di0);

    Enable_global_interrupt();

    int waitcount = 0;
    while (!fastgpio_get_pin_value(&it->adc_busy0)) {
      if (++waitcount > 10000) {
        it->boned = true;
        log_printf("ads8344: timeout waiting for busy pin");
        return;
      }
    }

    Disable_global_interrupt();

    fastgpio_set_1(&it->adc_clk);
    asm("nop; nop; nop; nop; nop; nop; nop; nop");
    fastgpio_set_0(&it->adc_clk);
    asm("nop; nop; nop; nop; nop; nop; nop; nop");

    uint16_t result = 0;
    for (int biti=0; biti<16; biti++) {
      // Dout changes on the negative edge of clock
      fastgpio_set_1(&it->adc_clk);
      if (fastgpio_get_pin_value(&it->adc_do0)) {
        result = (result << 1) | 1;
      } else {
        result = (result << 1) | 0;
      }
      fastgpio_set_0(&it->adc_clk);
      asm("nop; nop; nop; nop; nop; nop; nop; nop");
    }
    it->cur.results[chani] = result;

    fastgpio_set_1(&it->adc_ncs);

    Enable_global_interrupt();
  }
  it->cur.sample_ticks = get_ticks64();
}

void ads8344_log_status(ads8344_t *it, char *name)
{
#if ADS8344_N_RESULTS==4
  log_printf("%s:  %sresults=[%04x %04x %04x %04x]",
             name,
             it->boned ? "(boned) " : "",
             it->cur.results[0],
             it->cur.results[1],
             it->cur.results[2],
             it->cur.results[3]);
#elif ADS8344_N_RESULTS==8
  log_printf("%s:  %sresults=[%04x %04x %04x %04x %04x %04x %04x %04x]",
             name,
             it->boned ? "(boned) " : "",
             it->cur.results[0],
             it->cur.results[1],
             it->cur.results[2],
             it->cur.results[3],
             it->cur.results[4],
             it->cur.results[5],
             it->cur.results[6],
             it->cur.results[7]);
#endif
}


void pkt_tx_ads8344_sample(pkt_tx_buf *tx, ads8344_sample_t *s)
{
  pkt_tx_u64p(tx, &s->sample_ticks);
  for (int i=0; i<ADS8344_N_RESULTS; i++) {
    pkt_tx_u16p(tx, &s->results[i]);
  }
}
