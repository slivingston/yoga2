#pragma once
#include "hwdefs.h"
#include "../../../embedded/avr32/fastgpio.h"
#include "../../../embedded/embedded_pktcom.h"

typedef struct ads8344_sample_t {
  U64 sample_ticks;
  uint16_t results[ADS8344_N_RESULTS];
} ads8344_sample_t;

typedef struct ads8344_t {
  bool boned;

  ads8344_sample_t cur;

  fastgpio_t adc_clk;
  fastgpio_t adc_ncs;
  fastgpio_t adc_di0;
  fastgpio_t adc_busy0;
  fastgpio_t adc_do0;
} ads8344_t;

extern ads8344_t ads83440;

/* Public */
void ads8344_poll(ads8344_t *it);
void ads8344_init(ads8344_t *it);
void ads8344_accept_message(ads8344_t *it, pkt_rx_buf *rx, pkt_tx_buf *tx);
void ads8344_log_status(ads8344_t *it, char *name);

void pkt_tx_ads8344_sample(pkt_tx_buf *tx, ads8344_sample_t *s);
