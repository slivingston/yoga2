#include "common/std_headers.h"
#include "hwdefs.h"
#include "gpio.h"
#include "./beeper.h"
#include "embedded/embedded_timing.h"
#include "embedded/embedded_debug.h"

beeper_t beeper0;

void beeper_set(beeper_t *it, U16 mask)
{
  it->beeper_mask = mask;
  beeper_poll(it);
}

void led_set(beeper_t *it, U16 rmask, U16 gmask, U16 bmask)
{
  it->rled_mask = rmask;
  it->gled_mask = gmask;
  it->bled_mask = bmask;
  beeper_poll(it);
}

void beeper_poll(beeper_t *it)
{
  U16 mscount = (U16)0x8000 >> ((get_ticks() >> 22) & 0x0f);

  if (mscount & it->beeper_mask) {
    gpio_clr_gpio_pin(CHEST_BEEPER);
  } else {
    gpio_set_gpio_pin(CHEST_BEEPER);
  }

  // LEDs are from +3v3 to the pin, so low output turns them on
  if (mscount & it->rled_mask) {
    gpio_clr_gpio_pin(CHEST_LEDC0);
  } else {
    gpio_set_gpio_pin(CHEST_LEDC0);
  }
  if (mscount & it->gled_mask) {
    gpio_clr_gpio_pin(CHEST_LEDC1);
  } else {
    gpio_set_gpio_pin(CHEST_LEDC1);
  }
  if (mscount & it->bled_mask) {
    gpio_clr_gpio_pin(CHEST_LEDC2);
  } else {
    gpio_set_gpio_pin(CHEST_LEDC2);
  }

}

void beeper_log_status(beeper_t *it, char *name)
{
}

void beeper_setup()
{
  gpio_enable_gpio_pin(CHEST_BEEPER);
  gpio_clr_gpio_pin(CHEST_BEEPER);


  gpio_enable_gpio_pin(CHEST_LEDC0);
  gpio_set_gpio_pin(CHEST_LEDC0);
  gpio_enable_gpio_pin(CHEST_LEDC1);
  gpio_set_gpio_pin(CHEST_LEDC1);
  gpio_enable_gpio_pin(CHEST_LEDC2);
  gpio_set_gpio_pin(CHEST_LEDC2);

}
