#pragma once

typedef struct binvalve_t {
  bool out_values[N_SSC_BINVALVES];
} binvalve_t;

extern binvalve_t binvalve0;

void binvalve_sync(binvalve_t *it);
void binvalve_init(binvalve_t *it);
void binvalve_set_digit(binvalve_t *it, int digit);
void binvalve_set_off(binvalve_t *it);
void binvalve_shutdown(binvalve_t *it);
void binvalve_log_status(binvalve_t *it, const char *name);
