#include "common/std_headers.h"
#include "hwdefs.h"
#include "gpio.h"
#include "embedded/embedded_debug.h"
#include "embedded/embedded_printbuf.h"
#include "embedded/embedded_timing.h"
#include "./camtrig.h"

camtrig_t camtrig0;

/*
  These are active low, because of the optocoupler circuitry. High impedance (on reset) is also inactive.
*/
void camtrig_init(camtrig_t *it)
{
  camtrig_clear(it);
  gpio_set_gpio_pin(CAMTRIG0_PIN);
  gpio_set_gpio_pin(CAMTRIG1_PIN);
  gpio_set_gpio_pin(CAMTRIG2_PIN);
  gpio_set_gpio_pin(CAMTRIG3_PIN);
}

void camtrig_trigger(camtrig_t *it, uint8_t trigi, uint8_t active)
{
  if (trigi == 0) {
    if (active) {
      gpio_clr_gpio_pin(CAMTRIG0_PIN);
    } else {
      gpio_set_gpio_pin(CAMTRIG0_PIN);
    }
  }
  else if (trigi == 1) {
    if (active) {
      gpio_clr_gpio_pin(CAMTRIG1_PIN);
    } else {
      gpio_set_gpio_pin(CAMTRIG1_PIN);
    }
  }
  else if (trigi == 2) {
    if (active) {
      gpio_clr_gpio_pin(CAMTRIG2_PIN);
    } else {
      gpio_set_gpio_pin(CAMTRIG2_PIN);
    }
  }
  else if (trigi == 3) {
    if (active) {
      gpio_clr_gpio_pin(CAMTRIG3_PIN);
    } else {
      gpio_set_gpio_pin(CAMTRIG3_PIN);
    }
  }
}

void camtrig_clear(camtrig_t *it)
{
  gpio_set_gpio_pin(CAMTRIG0_PIN);
  gpio_set_gpio_pin(CAMTRIG1_PIN);
  gpio_set_gpio_pin(CAMTRIG2_PIN);
  gpio_set_gpio_pin(CAMTRIG3_PIN);
}

void camtrig_shutdown(camtrig_t *it)
{
  camtrig_clear(it);
}
