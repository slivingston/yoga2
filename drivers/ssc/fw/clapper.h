#pragma once

typedef struct clapper_t {
  uint8_t segments;
} clapper_t;

extern clapper_t clapper0;

void clapper_sync(clapper_t *it);
void clapper_init(clapper_t *it);
void clapper_set_digit(clapper_t *it, int digit);
void clapper_set_off(clapper_t *it);
void clapper_shutdown(clapper_t *it);
void clapper_log_status(clapper_t *it, const char *name);
