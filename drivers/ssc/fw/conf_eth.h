#pragma once

/*! Number of receive buffers */
#define ETHERNET_CONF_NB_RX_BUFFERS 40

/*! USE_RMII_INTERFACE must be defined as 1 to use an RMII interface, or 0
to use an MII interface. */
#define ETHERNET_CONF_USE_RMII_INTERFACE 0

/*! Number of Transmit buffers */
#define ETHERNET_CONF_NB_TX_BUFFERS 10

/*! Size of each Transmit buffer. */
#define ETHERNET_CONF_TX_BUFFER_SIZE 512

/*! Clock definition. I believe it uses the HSB clock (= CPU clock), and not a peripheral bus clock */
#define ETHERNET_CONF_SYSTEM_CLOCK FCPU

/*! Phy Address (set through strap options) */
#define ETHERNET_CONF_PHY_ADDR 0x01

#if defined(__TARG_EVK1100__)
/*! Phy Identifier (On EVK1100, this is a DP83848) */
#define ETHERNET_CONF_PHY_ID 0x20005C90
#elif defined(__TARG_SSC_GYRO1__) || defined(__TARG_SSC_LEG1__) || defined(__TARG_SSC_FOOT1__) \
    || defined(__TARG_SSC_HARNESS1__)
/* TLK100 */
#define ETHERNET_CONF_PHY_ID 0x40005200
#else
#error "No such target"
#endif

/*! Use Auto Negotiation to get speed and duplex */
#define ETHERNET_CONF_AN_ENABLE 1

/*! Use auto cross capability. Used by default. */
#define ETHERNET_CONF_AUTO_CROSS_ENABLE 1

/*! set to 1 if Phy status changes handle an interrupt */
#define ETHERNET_CONF_USE_PHY_IT 0

#define MACB_INTERRUPT_PIN AVR32_PIN_PA24
