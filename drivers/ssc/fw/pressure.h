#pragma once

typedef struct pressure_sense_t {
  uint16_t values[N_SSC_PRESSURE];
} pressure_sense_t;

extern pressure_sense_t pressure0;

void pressure_init(pressure_sense_t *it);
void pressure_poll(pressure_sense_t *it);

void pressure_log_status(pressure_sense_t *it, const char *name);
