#pragma once

#include "dspcore/smootherdsp.h"

typedef struct ssc_lift_cmd_t {
  dsp824 base_valve;
  dsp1616 desired_force;                                // N
  dsp824 desired_pos;                                   // m
  dsp1616 ext_pressure_to_force, ret_pressure_to_force; // N/MPa, or mm^2
  dsp824 force_feedback_coeff;                          // valve / N
  dsp824 force_feedback_lim_lo, force_feedback_lim_hi;  // valve
  dsp1616 pos_feedback_coeff;                           // N / m
  dsp1616 pos_feedback_lim_lo, pos_feedback_lim_hi;     // N
} ssc_lift_cmd_t;

typedef struct ssc_lift_state_t {
  dsp824 ext_pressure, ret_pressure; // MPa
  dsp824 pos;                        // m
  dsp1616 force;                     // N
  dsp1616 pos_feedback;              // N / m
  dsp824 force_feedback;             // valve / N
  dsp824 valve_prefilter;            // valve
  smoother2_state_dsp824 valve_filter_state;
  smoother2_coeffs_dsp824 valve_filter_coeffs;
  dsp824 valve;
} ssc_lift_state_t;

void ssc_lift_init(ssc_lift_state_t *st);
void ssc_lift_run(ssc_lift_cmd_t *cmd, ssc_lift_state_t *st);
