#include "common/std_headers.h"

#include "hwdefs.h"

#include "gpio.h"
#include "wdt.h"

#include "embedded/avr32/macb.h"
#include "embedded/avr32/avrhw.h"
#include "embedded/embedded_hostif.h"
#include "embedded/embedded_pktcom.h"
#include "embedded/embedded_timing.h"
#include "embedded/embedded_debug.h"
#include "embedded/avr32/sercom.h"
#include "embedded/avr32/ethcom.h"
#include "./ssc_main.h"
#include "./hostif.h"

rate_generator logging_rg;

void ssc_basic_setup(void)
{
  wdt_disable();

#if defined(AVR32_GPIO_LOCAL_ADDRESS)
  gpio_local_init();
#endif

#if !defined(TARG_BOOTLOADED)
  avrhw_setup();
#endif

#if defined(SSC_USE_WDT)
  debug_printf("wdt...");
  wdt_clear();
  wdt_enable(60000000); //60 second time out (actually will be some multiple of the watchdog timer prescale greater than 60 unless 60 is longer than the max value, in which case it will be the max value)
#endif

  wdt_clear();

  pkt_setup();
  wdt_clear();

  sercom_setup();
  if (host_sercom) sercom_tx_str(host_sercom, "\n\nssc 0.4\n");

  if (0) avrhw_debug_dump();
  if (0) test_dspcore();
  if (0) test_vec3dsp();
  wdt_clear();

  avrhw_report_rcause();
  wdt_clear();

  debug_printf("ethcom...");
  ethcom_init(&ethcom0);
  ethcom_start_syslog(&ethcom0, &syslog_ep);
  rate_generator_enable(&logging_rg, SECONDSTOTICKS(1.0), 3);

  wdt_clear();
}

void ssc_poll(void)
{
  wdt_clear();
  debug_printf("work...");
  while (1) {
    wdt_clear();

    ssc_poll_work();

    ethcom_poll(&ethcom0);
    host_fastpoll(&host0);

    rate_generator_set_budget(&logging_rg, 10);
    if (rate_generator_update(&logging_rg)) {
      ssc_poll_logging();
      ethcom_log_status(&ethcom0, "ethcom0");
      if (host_sercom) sercom_report_errors(host_sercom);
      log_flush();
      rate_generator_done(&logging_rg);
    }
  }
}

void panic_str(char const *message)
{
  cpu_delay_ms(100, FCPU); // let serial console drain

  Disable_global_interrupt();

  while (1) {
    if (host_sercom) {
      sercom_panic_write(host_sercom, message);
    }
    cpu_delay_ms(1000, FCPU);
  }
}
