#pragma once

void ssc_basic_setup(void);
void panic_str(char const *message);
void ssc_poll(void);

// Called by ssc_poll -- you should implement these
void ssc_poll_work(void);
void ssc_poll_logging(void);
