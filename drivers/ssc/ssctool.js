#!/usr/bin/env node
'use strict';
console._stdout = process.stderr;  // eslint-disable-line no-console
const _ = require('lodash');
const path = require('path');
const async = require('async');
const assert = require('assert');
const dns = require('dns');
const os = require('os');
const dgram = require('dgram');

exports.pokeSsc = pokeSsc;
exports.lookupBoardIp = lookupBoardIp;

function pokeSsc(boardName, msg, cb) {
  if (_.isString(msg)) {
    msg = Buffer.from(msg, 'utf8');
  }
  lookupBoardIp(boardName, (err, boardIp) => {
    if (err) return cb(err);
    let s = dgram.createSocket('udp4');
    s.bind(10002, null, (err) => {
      if (err) {
        s.close();
        return cb(err);
      }

      s.send(msg, 10000, boardIp, (err) => {
        if (err) {
          s.close();
          return cb(err);
        }
        s.close();
        return cb(null);
      });
    });
  });
}


function lookupBoardIp(boardName, cb) {
  dns.lookup(boardName, {family: 4}, (err, ipaddr, family) => {
    if (err) {
      if (err.code === 'ENOTFOUND') {
        return cb(new Error(`Board name ${boardName} not found. Add it to /etc/hosts.conf?`));
      }
      return cb(err);
    }
    if (family !== 4) {
      return cb(new Error(`Expected IPv4 address, got ${family} (${ipaddr})`));
    }
    return cb(null, ipaddr);
  });
}

function splitIp(ip) {
  assert.ok(_.isString(ip));
  let ret = _.map(ip.split('.'), (a) => parseInt(a, 10));
  if (ret.length !== 4) {
    throw new Error(`Expected IPv4 address, got ${ip}`);
  }
  return ret;
}

function splitEth(eth) {
  assert.ok(_.isString(eth));
  let ret = _.map(eth.split(':'), (a) => parseInt(a, 16));
  if (ret.length !== 6) {
    throw new Error(`Expected MAC address, got ${eth}`);
  }
  return ret;
}


function fmtEthByte(b) {
  let ret = '0' + b.toString(16);
  return ret.substring(ret.length-2);
}


function cppIpDefs(a, name) {
  let aSplit = splitIp(a);
  let defs = {};
  defs[name] = `"${a}"`;
  _.each(_.range(4), (i) => {
    defs[`${name}${i}`] = aSplit[i].toString(10);
  });
  return defs;
}

function cppEthDefs(a, name) {
  let aSplit = splitEth(a);
  let defs = {};
  defs[name] = `"${a}"`;
  _.each(_.range(6), (i) => {
    defs[`${name}${i}`] = '0x' + fmtEthByte(aSplit[i]);
  });
  return defs;
}


function main(argv) {
  let boardName = null;
  let verbose = 0;
  let doCppArgs = false;
  let doBoardIp = false;

  let msgs = [];

  for (let argi=2; argi < argv.length; argi++) {
    let arg = argv[argi];
    if (0) {
    }
    else if (arg === '-v') {
      verbose += 1;
    }
    else if (arg === '--cppargs') {
      doCppArgs = true;
    }
    else if (arg === '--boardip') {
      doBoardIp = true;
    }
    else if (arg === '--reboot') {
      msgs.push('Q3b00t');
    }
    else if (arg === '--mode') {
      msgs.push('M' + argv[++argi]);
    }
    else if (arg.startsWith('-')) {
      throw new Error(`Unknown arg ${arg}`);
    }
    else {
      boardName = arg;
    }
  }

  if (!_.isString(boardName)) {
    throw new Error(`Board name must be given`);
  }

  if (msgs.length) {
    async.each(msgs, (msg, cb1) => {
      pokeSsc(boardName, msg, cb1);
    }, (err) => {
      if (err) throw err;
    });
    return;
  }


  dns.lookup(os.hostname(), {family: 4, all: true}, (err, localIpAddrs) => {
    if (err) throw err;
    if (0) console.log('localIpaddrs', localIpAddrs);

    lookupBoardIp(boardName, (err, boardIp) => {
      if (err) throw err;
      let boardIpSplit = splitIp(boardIp);

      if (doBoardIp) {
        process.stdout.write(boardIp + '\n');
      }

      let bootIp = '172.17.10.1';
      if (0) {
        let netPrefix = `${boardIpSplit[0]}.${boardIpSplit[1]}`;
        bootIp = _.map(_.filter(localIpAddrs, (x) => x.family === 4 && x.address.startsWith(netPrefix)), (x) => x.address)[0];
        if (!bootIp) {
          throw new Error(`Couldn't find IP address of local machine starting with ${netPrefix} to use as bootloader`);
        }
      }
      if (verbose) console.log('Set bootip', bootIp);

      if (doCppArgs) {
        let syslogIp = bootIp;

        let boardMac = `02:69:11:04:${fmtEthByte(boardIpSplit[2])}:${fmtEthByte(boardIpSplit[3])}`;

        let defs = {};
        _.assign(defs, cppIpDefs(boardIp, 'ETHERNET_CONF_IPADDR'));
        _.assign(defs, cppEthDefs(boardMac, 'ETHERNET_CONF_ETHADDR'));
        _.assign(defs, cppIpDefs(bootIp, 'ETHERNET_CONF_BOOTIP'));
        _.assign(defs, cppIpDefs(syslogIp, 'ETHERNET_CONF_SYSLOGIP'));

        if (verbose) console.log(defs);

        let cppArgs = _.map(defs, (v, k) => {
          return `-D${k}=${v}`;
        });

        process.stdout.write(cppArgs.join(' ') + '\n');
      }

    });
  });

}

if (require.main === module) {
  main(process.argv);
}
