#include "./ui.h"
#include "./ui_types.h"
#include "imgui.h"
#include "../../jit/value.h"
#include "../../db/yoga_layout.h"
#include "../../lib/core_types.h"
#include "../jit/compilation.h"
#include <thread>
#include <SDL.h>
using namespace ImGui;

#ifdef USE_SPNAV
#include "spnav.h"
#endif

#ifdef USE_3DX
#include "/Library/Frameworks/3DconnexionClient.framework/Headers/ConnexionClientAPI.h"
#endif

YogaValue yogaKbdState;
YogaValue yogaPuckState;


// Must match YogaKeyboard struct
array<tuple<SDL_Scancode, char const *>, 36> yogaKeyboardScanCodes {{
  {SDL_SCANCODE_A, "A"},
  {SDL_SCANCODE_B, "B"},
  {SDL_SCANCODE_C, "C"},
  {SDL_SCANCODE_D, "D"},
  {SDL_SCANCODE_E, "E"},
  {SDL_SCANCODE_F, "F"},
  {SDL_SCANCODE_G, "G"},
  {SDL_SCANCODE_H, "H"},
  {SDL_SCANCODE_I, "I"},
  {SDL_SCANCODE_J, "J"},
  {SDL_SCANCODE_K, "K"},
  {SDL_SCANCODE_L, "L"},
  {SDL_SCANCODE_M, "M"},
  {SDL_SCANCODE_N, "N"},
  {SDL_SCANCODE_O, "O"},
  {SDL_SCANCODE_P, "P"},
  {SDL_SCANCODE_Q, "Q"},
  {SDL_SCANCODE_R, "R"},
  {SDL_SCANCODE_S, "S"},
  {SDL_SCANCODE_T, "T"},
  {SDL_SCANCODE_U, "U"},
  {SDL_SCANCODE_V, "V"},
  {SDL_SCANCODE_W, "W"},
  {SDL_SCANCODE_X, "X"},
  {SDL_SCANCODE_Y, "Y"},
  {SDL_SCANCODE_Z, "Z"},
  {SDL_SCANCODE_1, "1"},
  {SDL_SCANCODE_2, "2"},
  {SDL_SCANCODE_3, "3"},
  {SDL_SCANCODE_4, "4"},
  {SDL_SCANCODE_5, "5"},
  {SDL_SCANCODE_6, "6"},
  {SDL_SCANCODE_7, "7"},
  {SDL_SCANCODE_8, "8"},
  {SDL_SCANCODE_9, "9"},
  {SDL_SCANCODE_0, "0"}
}};


void initYogaKbdState(YogaContext const &ctx)
{
  yogaKbdState = YogaValue(ctx.reg, ctx.getTypeOrThrow("YogaKeyboard"), ctx.reg->mem);
}

void syncYogaKbdState()
{
  auto &io = GetIO();
  auto cppKbdState = reinterpret_cast<YogaKeyboard *>(yogaKbdState.buf);
  bool changed = false;

  for (size_t i = 0; i < yogaKeyboardScanCodes.size(); i++) {
    auto [scanCode, keyName] = yogaKeyboardScanCodes[i];
    auto down = io.KeysDown[scanCode];
    auto downDuration = io.KeysDownDuration[scanCode];
    auto prevDownDuration = io.KeysDownDurationPrev[scanCode];
    
    if (downDuration == 0.0f) {
      if (!io.KeyCtrl && !io.KeyShift && !io.KeyAlt) {
        if (cppKbdState->down[i] < 0.5) {
          cppKbdState->down[i] = 1.0;
          changed = true;
        }
      }
    }
    else if (prevDownDuration >= 0.0 && !down) {
      if (cppKbdState->down[i] >= 0.5) {
        cppKbdState->down[i] = 0.0;
        changed = true;
      }
    }
  }
  if (changed) {
    yogaKbdState.ts = realtime();
  }
}




#ifdef USE_3DX

static uint16_t cxClientId;

static double scale520(int16_t const &raw)
{
  return (double)raw / 520.0;
}

static void handleCxMessage(unsigned int productId, unsigned int messageType, void *messageArgument)
{
  const int verbose = 0;
  if (verbose) cerr << "handle_3dx(" + repr(productId) + ", " + repr(messageType) + ")\n";
  if (messageType == kConnexionMsgDeviceState) {
    auto s = reinterpret_cast<ConnexionDeviceState *>(messageArgument);
    if (s->client == cxClientId && yogaPuckState.isValid()) {
      auto cppPuckState = reinterpret_cast<YogaPuck *>(yogaPuckState.buf);
      switch (s->command) {
        case kConnexionCmdHandleAxis: {
          cppPuckState->active = 1.0;
          cppPuckState->force[0] = +scale520(s->axis[0]);
          cppPuckState->force[1] = -scale520(s->axis[1]);
          cppPuckState->force[2] = -scale520(s->axis[2]);
          cppPuckState->torque[0] = +scale520(s->axis[3]);
          cppPuckState->torque[1] = -scale520(s->axis[4]);
          cppPuckState->torque[2] = -scale520(s->axis[5]);
          if (verbose) cerr << "  axis " + repr(yogaPuckState) + ")\n";
          break;
        }
        case kConnexionCmdHandleButtons: {
          for (int i=0; i < 8; i++) {
            cppPuckState->buttons[i] = (s->buttons >> i) & 1;
          }
          if (verbose) cerr << "  buttons " + repr(yogaPuckState) + ")\n";
          break;
        }
      }
      yogaPuckState.ts = realtime();
    }
  }
}

static void handleCxAdded(unsigned int unused)
{
  int32_t result;
  ConnexionClientControl(cxClientId, kConnexionCtlGetDeviceID, 0, &result);
  int16_t vendorId = result >> 16;
  int16_t productId = result & 0xffff;

  cerr << "handleAdded(unused=" + repr(unused) + ", vendorId=" + repr(vendorId) + ", productId=" + repr(productId) + ")\n";
}

static void handleCxRemoved(unsigned int productId)
{
  cerr << "handleRemoved(" + repr(productId) + ")\n";
}

#endif

#ifdef USE_SPNAV
std::thread spnavThread;

static double scale520(int const &raw)
{
  return (double)raw / 520.0;
}

static void spnavMain()
{
  if (0) cerr << "spnav starting\n";
  while (true) {
    spnav_event ev;
    int rc = spnav_wait_event(&ev);
    if (rc == 0) {
      if (0) cerr << "spnav error\n";
      break;
    }

    bool changed=false;

    if (yogaPuckState.isValid()) {
      auto cppPuckState = reinterpret_cast<YogaPuck *>(yogaPuckState.buf);

      if (ev.type == SPNAV_EVENT_MOTION) {
        cppPuckState->active = 1.0;
        cppPuckState->force[0] = +scale520(ev.motion.x);
        cppPuckState->force[1] = -scale520(ev.motion.y);
        cppPuckState->force[2] = -scale520(ev.motion.z);
        cppPuckState->torque[0] = +scale520(ev.motion.rx);
        cppPuckState->torque[1] = -scale520(ev.motion.ry);
        cppPuckState->torque[2] = -scale520(ev.motion.rz);
        changed = true;
      }
      else if (ev.type == SPNAV_EVENT_BUTTON) {
        if (ev.button.bnum >= 0 && ev.button.bnum < 8) {
          if (ev.button.press) {
            cppPuckState->buttons[ev.button.bnum] = 1.0;
            changed = true;
          }
          else {
            cppPuckState->buttons[ev.button.bnum] = 0.0;
            changed = true;
          }
        }
      }
      if (changed) {
        yogaPuckState.ts = realtime();
      }

    }
  }
  if (0) cerr << "spnav stopping\n";
}
#endif



void initYogaPuckState(YogaContext const &ctx)
{
  yogaPuckState = YogaValue(ctx.reg, ctx.getTypeOrThrow("YogaPuck"), ctx.reg->mem);

#ifdef USE_3DX
  if (!cxClientId) {
    SetConnexionHandlers(handleCxMessage, handleCxAdded, handleCxRemoved, true);

    cxClientId = RegisterConnexionClient(
        kConnexionClientWildcard,
        const_cast<uint8_t *>(reinterpret_cast<uint8_t const *>("\013democontrol")),
        kConnexionClientModeTakeOver,
        kConnexionMaskAll);
    SetConnexionClientButtonMask(cxClientId, kConnexionMaskAllButtons);
  }
#endif
#ifdef USE_SPNAV
  if (!spnavThread.joinable()) {
    if (spnav_open() < 0) {
      if (1) cerr << "Can't open spnav\n";
    }
    else {
      spnavThread = std::thread(&spnavMain);
      atexit(endYogaPuckState);
    }
  }
#endif
}

void endYogaPuckState()
{
#ifdef USE_SPNAV
  if (spnavThread.joinable()) {
    if (0) cerr << "Closing spnav\n";
    // This seems like the only way of getting the reading thread to notice that
    // the socket has been closed and exit nicely.
    shutdown(spnav_fd(), SHUT_RDWR);
    spnavThread.join();
  }
#endif
}

