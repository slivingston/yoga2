#pragma once
#include "../../jit/jit_utils.h"
#include "../../geom/geom.h"

// These must match the yoga types in ./ui_types.cc

struct YogaKeyboard {
  union {
    R down[26+10]; // A-Z, 1-9, 0
    struct {
      R a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z;
      R n1, n2, n3, n4, n5, n6, n7, n8, n9, n0;
    };
  };
};

struct YogaPuck {
  R active;
  R force[3];
  R torque[3];
  R buttons[8];
};

struct YogaRenderCircle {
  R order;
  glm::dvec2 pos;
  R radius;
  U32 color;
  R thickness;
};

struct YogaRenderCircleFilled {
  R order;
  glm::dvec2 pos;
  R radius;
  U32 color;
};

struct YogaRenderLine {
  R order;
  glm::dvec2 p1, p2;
  U32 color;
  R thickness;
};

struct YogaRenderArcArrow {
  R order;
  glm::dvec2 pos;
  R radius;
  R arcFrom, arcTo;
  U32 color;
  R thickness;
};

struct YogaRenderSolid {
  R order;
  char const *name;
  glm::dmat4 pos; 
  U32 color;
};
