#pragma once
#include "./universal_network_engine.h"

#include "./universal_network_utils.h"
#include "./universal_types.h"

struct UniversalRobotiqGripper : enable_shared_from_this<UniversalRobotiqGripper> {

  const size_t UPTO_NL = 1000010;

  UniversalRobotiqGripper(shared_ptr<UniversalNetworkEngine> _engine, string _hostname, string _port)
      : engine(_engine), hostname(_hostname), port(_port), sock()
  {
    auto ctx = engine->mkCtx("controlPort");
    robotiqGripStatusAs = make_unique<RobotiqGripStatusAccessorSuite>(ctx.getTypeOrThrow("RobotiqGripStatus"));
    robotiqGripCmdAs = make_unique<RobotiqGripCmdAccessorSuite>(ctx.getTypeOrThrow("RobotiqGripCmd"));
  }

  void start()
  {
    /*
      We keep this object alive until all the callbacks return, by capturing thisp in the lambda.
      Because we also have a shared_ptr to the engine, that stays alive too.
    */

    uvGetAddrInfo(hostname, port, addrinfoForTcp(), [this, thisp = shared_from_this()](int status, struct addrinfo *result) {
      if (status < 0) throw uv_error("Gripper UvGetAddrInfo("s + hostname + ":"s + port + ")"s, status);

      assert(result->ai_family == PF_INET);
      auto remoteAddr_in = new sockaddr_in(*(sockaddr_in *)result->ai_addr);
      remoteAddr = reinterpret_cast<sockaddr *>(remoteAddr_in);
      remoteAddrDesc = sockaddr_desc(remoteAddr);

      sock.tcp_init();
      sock.tcp_nodelay(true);
      sock.tcp_connect(remoteAddr, [this, thisp = shared_from_this()](int rc) {
        if (rc < 0) {
          if (rc == UV_EAI_CANCELED) return;
          engine->console("grp > socket couldn't connect: "s + uv_strerror(rc));
          return;
        }
        if (engine->verbose >= 0) engine->console("grp > socket connected to "s + remoteAddrDesc);
        localAddrDesc = "local";

        commStart();

        sock.read_start([this, thisp = shared_from_this()](ssize_t nr, const uv_buf_t *buf) {
          if (nr == UV_EOF) {
            rxEof();
            sock.read_stop();
          }
          else if (nr < 0) {
            throw uv_error("grp > "s + remoteAddrDesc + (": read tcp"), nr);
          }
          else {
            //R rxTime = realtime();
            sock.tcp_quickack(true);
            if (!rxAnyFlag) {
              rxAnyFlag = true;
              if (engine->verbose >= 1)
                engine->console("grp > receiving ("s + localAddrDesc + " <=> "s + remoteAddrDesc + ")"s);
            }

            extendRxBuf(rxBuf, nr, buf);
            while (!ackQueue.empty() && !rxBuf.empty()) {
              auto expectAck = ackQueue.front();
              if (engine->verbose >= 5) engine->console("grp | expect "s + repr(expectAck.first));
              if (expectAck.first == UPTO_NL) {
                auto eolIt = find(rxBuf.begin(), rxBuf.end(), (u_char)10);
                if (engine->verbose >= 5)
                  engine->console(
                      "grp | grab "s + repr(eolIt - rxBuf.begin()) + " of "s + repr(rxBuf.size()));
                if (eolIt == rxBuf.end()) break;
                string rx = string(reinterpret_cast<char *>(rxBuf.data()), eolIt - rxBuf.begin());
                rxBuf.erase(rxBuf.begin(), eolIt + 1);
                if (engine->verbose >= 4) engine->console("grp > raw \""s + asPrintableString(rx) + "\"");
                expectAck.second(rx);
                ackQueue.pop_front();
              }
              else {
                if (rxBuf.size() < expectAck.first) break;
                string rx = string(reinterpret_cast<char *>(rxBuf.data()), expectAck.first);
                if (engine->verbose >= 4) engine->console("grp > raw \""s + asPrintableString(rx) + "\"");
                rxBuf.erase(rxBuf.begin(), rxBuf.begin() + expectAck.first);
                expectAck.second(rx);
                ackQueue.pop_front();
              }
            }
          }
        });
      });
    });
  }

  void close()
  {
    if (sock.is_active()) {
      sock.read_stop();
    }
  }

  void commStart()
  {
    commState = "resetting";
    txCmd("GET VER\n", UPTO_NL, [this, thisp = shared_from_this()](string const &reply) {
      if (engine->verbose >= 2) engine->console("grp < \""s + reply + "\""s);
      if (reply == "VER DCU-2.0.0"s) {
        remoteActivate([this, thisp = shared_from_this()](string const &err) {
          if (!err.empty()) {
            engine->console("grp > remoteActivate: "s + err);
            commState = "error";
            return;
          }
          commState = "running";
        });
      }
      else {
        if (engine->verbose >= 1) engine->console("grp < GET VER: \""s + reply + "\" (WRITEME: retry? accept?)"s);
        commState = "error";
      }
    });
  }

  void rxEof() { commState = "eof"; }

  void txCmd(string const &tx, size_t ackSize, std::function<void(string const &rx)> ackHandler)
  {
    if (ackSize != 0) {
      ackQueue.emplace_back(ackSize, ackHandler);
    }
    if (engine->verbose >= 2) engine->console("grp < cmd \""s + asPrintableString(tx) + "\""s);
    if (tx.empty() || tx.back() != '\n') {
      throw runtime_error("txCmd arg should end in newline"s);
    }
    sock.write((char const *)tx.data(), tx.size(), [this](int status) {
      if (status == UV_EAI_CANCELED) {
        engine->console("grp > write cancelled"s);
        return;
      }
      if (status < 0) throw uv_error("grp txPkt"s, status);
    });
  }

  void remoteSet(string const &regName, int v, std::function<void(string const &err)> cb = nullptr)
  {
    txCmd("SET "s + regName + " "s + repr(v) + "\n"s, 3, [this, regName, v, cb](string const &reply) {
      if (reply == "ack") {
        if (engine->verbose >= 3) engine->console("grp Set "s + regName + "="s + repr(v) + ": ack");
        if (cb) cb("");
      }
      else {
        if (engine->verbose >= 1)
          engine->console("grp Set "s + regName + "="s + repr(v) + ": \""s + reply + "\" (WRITEME: retry?)"s);
        if (cb) cb("Expected ack, got \""s + reply + "\""s);
      }
    });
  }

  void remoteIsConnected(int sid, std::function<void(string const &, bool)> cb)
  {
    txCmd("SET SID "s + repr(sid) + "\n"s, 3, [this, cb](string const &reply) {
      (void)this;
      if (reply == "ack") {
        cb("", true);
      }
      else {
        cb("", false);
      }
    });
  }

  void remoteGetSid(std::function<void(string const &err)> cb)
  {
    txCmd("GET SID\n"s, UPTO_NL, [this, cb](string const &reply) {
      if (reply.substr(0, 4) == "SID ") {
        auto sidListJson = json::parse(reply.substr(4));
        sidList.clear();
        for (auto &it : sidListJson) {
          sidList.push_back((int)it);
        }
        cb(""s);
      }
      else {
        cb("Expected SID, got \""s + reply + "\""s);
      }
    });
  }

  void remoteReset(std::function<void(string const &err)> cb)
  {
    remoteSet("ACT", 0);
    remoteSet("ATR", 0, cb);
  }

  void remoteStop(std::function<void(string const &err)> cb) { remoteSet("GTO", 0, cb); }

  void remoteAutoReleaseOpen(std::function<void(string const &err)> cb)
  {
    remoteSet("ARD", 0);
    remoteSet("ACT", 1);
    remoteSet("ATR", 0, [this, cb](string const &err) {
      if (!err.empty()) return cb(err);
      // pause?
      remoteSet("ATR", 1, [cb](string const &err) { return cb(err); });
    });
  }

  void remoteActivate(std::function<void(string const &err)> cb)
  {
    remoteGetSid([this, cb](string const &err) {
      if (!err.empty()) return cb(err);
      if (sidList.empty()) return cb("no grippers. sidList=\""s + repr(sidList) + "\""s);
      remoteSet("ACT", 1, [this, cb](string const &err) {
        if (!err.empty()) return cb(err);
        waitForStatusActivated(cb);
      });
    });
  };

  void waitForStatusActivated(std::function<void(string const &err)> cb)
  {
    txCmd("GET STA\n"s, UPTO_NL, [this, cb](string const &reply) {
      if (reply == "STA ?") {
        return cb("Expected STA 0|1, got \""s + reply + "\""s);
      }
      else if (reply == "STA 3") {
        return cb(""s);
      }
      else {
        if (engine->verbose >= 2) engine->console("Waiting for STA=3, got \""s + reply + "\"");
        waitForStatusActivated(cb);
      }
    });
  }

  void remoteResetAndActivate(std::function<void(string const &err)> cb)
  {
    remoteReset([this, cb](string const &err) {
      (void)this;
      if (!err.empty()) return cb(err);
      remoteActivate([this, cb](string const &err) {
        (void)this;
        if (!err.empty()) return cb(err);
      });
    });
  }

  void remoteSetGrip(int pos, int speed, int force, std::function<void(string const &err)> cb = nullptr)
  {
    if (engine->verbose >= 2) {
      engine->console(
          "grp < setGrip: pos="s + repr(pos) + " speed="s + repr(speed) + " force="s + repr(force));
    }
    txCmd(
        "SET POS "s + repr(pos) + " SPE "s + repr(speed) + " FOR "s + repr(force) + "\n"s,
        3,
        [this, cb](string const &reply) {
          (void)this;
          if (reply == "ack") {
            if (cb) cb(""s);
          }
          else {
            if (engine->verbose >= 1) engine->console("grp > SetGrip: \""s + reply + "\" (WRITEME: retry?)"s);
            if (cb) cb("Expected ack, got \""s + reply + "\""s);
          }
        });
  }

  void remoteIsObjectDetected(std::function<void(string const &err, int detected)> cb)
  {
    txCmd("GET OBJ\n"s, 1, [cb](string const &reply) {
      if (isdigit(reply[0])) {
        return cb(""s, atoi(reply.c_str()));
      }
      else {
        return cb("Expected digit, got \""s + reply + "\""s, 0);
      }
    });
  }

  void remoteGetGrip(std::function<void(string const &err, int obj, int flt, int sta, int pre)> cb)
  {
    struct Results {
      int obj{0};
      int flt{0};
      int sta{0};
      int pre{0};
      int todo{0};
      std::function<void(string const &err, int obj, int flt, int sta, int pre)> cb{nullptr};
      string err;
      shared_ptr<UniversalRobotiqGripper> thisp;

      void checkdone()
      {
        todo--;
        if (todo == 0) {
          if (thisp->engine->verbose >= 1) {
            thisp->engine->console(
                "grp > obj="s + repr(obj) + " flt="s + repr(flt) + " sta="s + repr(sta) + " pre="s
                + repr(pre));
          }
          cb(err, obj, flt, sta, pre);
        }
      };
    };
    auto r = make_shared<Results>();
    r->cb = cb;
    r->todo = 4;
    r->thisp = shared_from_this();

    txCmd("GET OBJ\n"s, UPTO_NL, [this, r](string const &reply) {
      (void)this;
      if (reply.substr(0, 4) == "OBJ ") {
        r->obj = atoi(reply.substr(4).c_str());
      }
      else {
        r->err = "Expected OBJ <d>, got \""s + reply + "\""s;
      }
      r->checkdone();
    });

    txCmd("GET FLT\n"s, UPTO_NL, [this, r](string const &reply) {
      (void)this;
      if (reply.substr(0, 4) == "FLT ") {
        r->flt = atoi(reply.substr(4).c_str());
      }
      else {
        r->err = "Expected FLT <d>, got \""s + reply + "\""s;
      }
      r->checkdone();
    });

    txCmd("GET STA\n"s, UPTO_NL, [this, r](string const &reply) {
      (void)this;
      if (reply.substr(0, 4) == "STA ") {
        r->sta = atoi(reply.substr(4).c_str());
      }
      else {
        r->err = "Expected STA <d>, got \""s + reply + "\""s;
      }
      r->checkdone();
    });

    txCmd("GET PRE\n"s, UPTO_NL, [this, r](string const &reply) {
      (void)this;
      if (reply.substr(0, 4) == "PRE ") {
        r->pre = atoi(reply.substr(4).c_str());
      }
      else {
        r->err = "Expected PRE <d>, got \""s + reply + "\""s;
      }
      r->checkdone();
    });
  }

  void remoteIter(YogaValue const &cmd)
  {
    iterActive++;

    remoteSetGrip(
        min(255, max(0, int(robotiqGripCmdAs->pos.rd(cmd) * 255.0))),
        min(255, max(0, int(robotiqGripCmdAs->speed.rd(cmd) * 255.0))),
        min(255, max(0, int(robotiqGripCmdAs->force.rd(cmd) * 255.0))),
        [this, thisp = shared_from_this()](string const &err) {
          if (!err.empty()) {
            engine->console("grp > error: \""s + err + "\" (WRITEME: retry?)"s);
            commState = "failed";
          }
          remoteGetGrip([this, thisp = shared_from_this()](string const &err, int obj, int flt, int sta, int pre) {
            if (!err.empty()) {
              engine->console("grp > error: \""s + err + "\" (WRITEME: retry?)"s);
              commState = "failed";
            }
            R rxTime = realtime();
            YogaValue grip(engine->rti->reg.get(), robotiqGripStatusAs->t, rxTime, engine->rti->reg->mem);
            robotiqGripStatusAs->obj.wr(grip, (R)obj);
            robotiqGripStatusAs->flt.wr(grip, (int)flt);
            robotiqGripStatusAs->sta.wr(grip, (R)sta);
            robotiqGripStatusAs->pre.wr(grip, (R)pre/255.0);
            iterActive--;
            onRxGripStatus(grip);
          });
        });
  }

  shared_ptr<UniversalNetworkEngine> engine;
  string hostname, port;
  string commState;
  int iterActive{0};

  sockaddr *localAddr{nullptr};
  string localAddrDesc;
  sockaddr *remoteAddr{nullptr};
  string remoteAddrDesc;

  vector<u_char> rxBuf;
  bool rxAnyFlag{false};

  vector<int> sidList;
  deque<pair<size_t, std::function<void(string const &rx)>>> ackQueue;

  UvStream sock;

  unique_ptr<RobotiqGripStatusAccessorSuite> robotiqGripStatusAs;
  unique_ptr<RobotiqGripCmdAccessorSuite> robotiqGripCmdAs;

  std::function<void(YogaValue const &grip)> onRxGripStatus;
};
