#pragma once
#include "./universal_network_engine.h"

#include "./universal_network_utils.h"
#include "./universal_types.h"

struct UniversalNetworkControlPortActive : enable_shared_from_this<UniversalNetworkControlPortActive> {

  UniversalNetworkControlPortActive(shared_ptr<UniversalNetworkEngine> _engine, string _hostname, string _port)
      : engine(_engine), hostname(_hostname), port(_port), sock()
  {
    auto ctx = engine->mkCtx("controlPort");
    universalRobotKinematicsAs = make_unique<UniversalRobotKinematicsAccessorSuite>(ctx.getTypeOrThrow("UniversalRobotKinematics"));
    universalRobotModeDataAs = make_unique<UniversalRobotModeDataAccessorSuite>(ctx.getTypeOrThrow("UniversalRobotModeData"));
    universalRobotJointDataAs = make_unique<UniversalRobotJointDataAccessorSuite>(ctx.getTypeOrThrow("UniversalRobotJointData"));
  }

  void start()
  {
    uvGetAddrInfo(hostname, port, addrinfoForTcp(), [this, thisp = shared_from_this()](int status, struct addrinfo *result) {
      if (status < 0) throw uv_error("UvGetAddrInfo("s + hostname + ":" + port + ")", status);

      assert(result->ai_family == PF_INET);
      auto remoteAddr_in = new sockaddr_in(*(sockaddr_in *)result->ai_addr);
      remoteAddr = reinterpret_cast<sockaddr *>(remoteAddr_in);
      remoteAddrDesc = sockaddr_desc(remoteAddr);

      sock.tcp_init();
      sock.tcp_nodelay(true);
      sock.tcp_connect(remoteAddr, [this, thisp = shared_from_this()](int rc) {
        if (rc < 0) {
          if (rc == UV_EAI_CANCELED) return;
          engine->console("ctl > socket couldn't connect: "s + uv_strerror(rc));
          return;
        }
        if (engine->verbose >= 0) engine->console("ctl > socket connected to "s + remoteAddrDesc);
        localAddrDesc = "local"s;

        commStart();

        sock.read_start([this, thisp = shared_from_this()](ssize_t nr, const uv_buf_t *buf) {
          if (nr == UV_EOF) {
            rxEof();
            sock.read_stop();
          }
          else if (nr < 0) {
            throw uv_error("ctl > socket("s + remoteAddrDesc + "): read tcp"s, nr);
          }
          else {
            double rxTime = realtime();
            if (!rxAnyFlag) {
              rxAnyFlag = true;
              if (engine->verbose >= 1)
                engine->console("ctl > Receiving ("s + localAddrDesc + " <=> "s + remoteAddrDesc + ")"s);
            }
            if (engine->verbose >= 5) engine->console("ctl > raw "s + repr(nr) + " bytes"s);
            extendRxBuf(rxBuf, nr, buf);
            forEachControlPacket(rxBuf, [this, rxTime](packet &rx) { rxPkt(rx, rxTime); });
          }
        });
      });
    });
  }

  void close()
  {
    if (sock.is_active()) {
      sock.read_stop();
    }
  }

  void rxEof()
  {
    engine->console("ctl > EOF"s);
    commState = "EOF"s;
  }

  void rxPkt(packet &rx, double rxTime)
  {
    auto pktType = rx.get_be_uint8();
    if (pktType == 0) {
      if (engine->verbose >= 2) engine->console("ctl > robot_mode_data"s);
    }
    else if (pktType == 1) {
      if (engine->verbose >= 2) engine->console("ctl > joint_data"s);
    }
    else if (pktType == 20) {
      auto timestamp = rx.get_be_uint64();
      (void)timestamp;
      auto source = rx.get_be_uint8();
      auto robotMessageType = rx.get_be_uint8();
      if (robotMessageType == 3) {
        string projectName = rx.get_len8_string();
        auto majorVersion = rx.get_be_uint8();
        auto minorVersion = rx.get_be_uint8();
        auto bugfixVersion = rx.get_be_uint32();
        auto buildNumber = rx.get_be_uint32();
        auto buildDate = rx.get_remainder_string();
        if (engine->verbose >= 1)
          engine->console(
              "ctl > "s + universalMessageSourceName(source) + " version: projectName="s + projectName + " "s
              + repr(int(majorVersion)) + "."s + repr(int(minorVersion)) + "."s
              + repr(int(bugfixVersion)) + "."s + repr(int(buildNumber)) + " on "s + buildDate);
        if (commState == "wait.version") {
          if (mainScript.size()) {
            txScript(mainScript);
          }
          commState = "running";
        }
      }
      else if (robotMessageType == 6) {
        if (engine->verbose >= 3)
          engine->console("ctl > "s + universalMessageSourceName(source) + " comm: pkt="s + rx.dump_hex());

        auto code = rx.get_be_uint32();
        auto arg = rx.get_be_uint32();
        auto level = rx.get_be_uint32();
        auto dataType = rx.get_be_uint32();
        auto text = rx.get_remainder_string();
        rxCommCount++;
        if (engine->verbose >= 1)
          engine->console(
              "ctl > "s + universalMessageSourceName(source).c_str() + " comm #"s + repr(rxCommCount) + " code="s
              + repr(int(code)) + " arg="s + repr(int(arg)) + " level="s + universalReportLevelName(level)
              + " dataType="s + repr(int(dataType)) + " text="s + asPrintableString(text));
      }
      else if (robotMessageType == 10) {
        auto scriptLineNumber = rx.get_be_uint32();
        auto scriptColNumber = rx.get_be_uint32();
        auto text = rx.get_remainder_string();
        if (engine->verbose >= 1)
          engine->console(
              "ctl > "s + universalMessageSourceName(source) + " runtime_exception at "s + repr(scriptLineNumber)
              + ":"s + repr(scriptColNumber) + " \""s + asPrintableString(text) + "\""s);
      }
      else if (robotMessageType == 1) {
        auto id = rx.get_be_uint32();
        auto text = rx.get_remainder_string();
        if (engine->verbose >= 1)
          engine->console(
              "ctl > "s + universalMessageSourceName(source) + " label "s + repr(int(id)) + ": "s
              + asPrintableString(text));
      }
      else if (robotMessageType == 0) {
        auto text = rx.get_remainder_string();
        if (engine->verbose >= 1)
          engine->console("ctl > "s + universalMessageSourceName(source) + " message: "s + asPrintableString(text));
      }
      else if (robotMessageType == 7) {
        auto code = rx.get_be_uint32();
        auto arg = rx.get_be_uint32();
        auto title = rx.get_len8_string();
        auto text = rx.get_remainder_string();
        if (engine->verbose >= 1)
          engine->console(
              "ctl > "s + universalMessageSourceName(source) + " KeyMessage: code="s + repr(code) + " arg="s
              + repr(arg) + " \""s + title + "\" \""s + text + "\""s);
      }
      else {
        if (engine->verbose >= 2)
          engine->console(
              "ctl > "s + universalMessageSourceName(source) + " text message pktType="s + repr((int)pktType)
              + " robotMessageType="s + repr((int)robotMessageType) + " pkt="s + rx.dump_hex());
      }
    }
    else if (pktType == 5) {
      if (rx.remaining() == 4) {
        if (engine->verbose >= 4) engine->console("ctl > kinematics_info <empty>"s);
      }
      else {
        YogaValue kin(engine->rti->reg.get(), universalRobotKinematicsAs->t, rxTime, engine->rti->reg->mem);
        universalRobotKinematicsAs->rx(rx, kin);
        if (engine->verbose >= 2) {
          engine->console("ctl > kinematics_info "s + repr(kin));
        }
        if (onRxKinematics) onRxKinematics(kin);
      }
    }
    else if (pktType == 25) {
      auto timestamp = rx.get_be_uint64();
      (void)timestamp;
      auto robotMessageType = rx.get_be_uint8();
      if (robotMessageType == 2) { // variable_update
        if (engine->verbose >= 2) engine->console("ctl > variable_update (WRITEME)"s);
      }
      else if (robotMessageType == 0) { // global_variables_setup
        auto startIndex = rx.get_be_uint16();
        string allVars = rx.get_remainder_string();
        if (engine->verbose >= 2)
          engine->console(
              "ctl > global variables setup "s + repr(startIndex) + " "s + asPrintableString(allVars));
        auto allVarsVec = splitNewlines(allVars);
        globalVarNames.resize(max(globalVarNames.size(), startIndex + allVarsVec.size()));
        for (int i = 0; i < allVarsVec.size(); i++) {
          globalVarNames[i + startIndex] = allVarsVec[i];
        }
      }
      else if (robotMessageType == 1) { // global_variables_update
        int startIndex = (int)rx.get_be_uint16();
        json globalVars;
        while (rx.remaining() > 0) {
          json varDump = parseVariableDump(rx);
          string varName
              = startIndex < globalVarNames.size() ? globalVarNames[startIndex] : "global"s + repr(startIndex);
          globalVars[varName] = varDump;
          if (rx.remaining() > 0) {
            auto nl = rx.get_be_uint8(); // newline terminator, despite otherwise binary format
            (void)nl;
            startIndex++;
          }
        }
        if (engine->verbose >= 4) engine->console("ctl > global var update pkt="s + rx.dump_hex());
        if (engine->verbose >= 3) engine->console("ctl > global var update "s + repr(globalVars));
        if (onRxGlobalVarDump) onRxGlobalVarDump(globalVars, rxTime);
      }
      else {
        if (engine->verbose >= 2)
          engine->console(
              "ctl > pktType="s + repr(int(pktType)) + " robotMessageType="s + repr(int(robotMessageType))
              + " (WRITEME)");
      }
    }
    else if (pktType == 16) { // RobotState. Supposedly only on secondary IF, but I see it on primary
      while (rx.remaining() > 0) {
        auto subPackageLen = rx.get_be_uint32();
        if (rx.remaining() < subPackageLen - 4) {
          throw packet_rd_overrun_err(subPackageLen - 4 - rx.remaining());
        }
        auto subPackageType = rx.get_be_uint8();
        if (subPackageType == 0) { // mode data
          auto timestamp = rx.get_be_uint64();
          (void)timestamp;

          YogaValue value(engine->rti->reg.get(), universalRobotModeDataAs->t, rxTime, engine->rti->reg->mem);
          universalRobotModeDataAs->rx(rx, value);
          if (engine->verbose >= 3) engine->console("ctl > "s + repr(value));
          if (onRobotStateModeData) onRobotStateModeData(value);
        }
        else if (subPackageType == 1) { // joint data
          YogaValue value(engine->rti->reg.get(), universalRobotJointDataAs->t, rxTime, engine->rti->reg->mem);
          universalRobotJointDataAs->rx(rx, value);
          if (onRobotStateJointData) onRobotStateJointData(value);
          if (engine->verbose >= 3) engine->console("ctl > "s + repr(value));
        }
        else {
          if (engine->verbose >= 3)
            engine->console(
                "ctl > robot state packageType="s + repr((int)subPackageType) + " len="s
                + repr(subPackageLen));
          rx.get_skip(subPackageLen - 5);
        }
      }
    }
    else if (pktType == 23 || pktType == 24) {
      if (engine->verbose >= 3)
        engine->console("ctl > message type "s + repr((int)pktType) + " ignoring pkt="s + rx.dump_hex());
    }
    else {
      if (engine->verbose >= 2)
        engine->console("ctl > unknown message "s + repr(int(pktType)) + " pkt="s + rx.dump_hex());
    }
  }

  void commStart() { commState = "wait.version"; }

  void txScript(string const &script)
  {
    txScriptCount++;
    if (engine->verbose >= 1) {
      auto nlpos = script.find('\n');
      engine->console("ctl < script #"s + repr(txScriptCount) + " \""s + script.substr(0, nlpos) + "...\""s);
    }
    sock.write(script, [this](int status) {
      if (status == UV_EAI_CANCELED) {
        engine->console("ctl > write cancelled"s);
        return;
      }
      if (status < 0) throw uv_error("txScript", status);
    });
  }

  shared_ptr<UniversalNetworkEngine> engine;
  string hostname, port;
  string commState;

  string mainScript;

  sockaddr *localAddr{nullptr};
  string localAddrDesc;
  sockaddr *remoteAddr{nullptr};
  string remoteAddrDesc;

  vector<u_char> rxBuf;
  bool rxAnyFlag{false};

  UvStream sock;

  int rxCommCount{0};
  int txScriptCount{0};

  vector<string> globalVarNames;

  unique_ptr<UniversalRobotKinematicsAccessorSuite> universalRobotKinematicsAs;
  unique_ptr<UniversalRobotModeDataAccessorSuite> universalRobotModeDataAs;
  unique_ptr<UniversalRobotJointDataAccessorSuite> universalRobotJointDataAs;

  std::function<void(packet &rx, double rxTime)> onRxStatus;
  std::function<void(YogaValue const &kin)> onRxKinematics;
  std::function<void(json const &globalVars, double rxTime)> onRxGlobalVarDump;
  std::function<void(YogaValue const &value)> onRobotStateModeData;
  std::function<void(YogaValue const &value)> onRobotStateJointData;
};
