#include "./universal_network_engine.h"

#include "./robotiq_gripper.h"
#include "./universal_network_control.h"
#include "./universal_network_monitor.h"
#include "./universal_network_script.h"
#include "common/uv_wrappers.h"

UniversalNetworkEngine::UniversalNetworkEngine()
{
}

UniversalNetworkEngine::UniversalNetworkEngine(UniversalNetworkConfig const &_config) 
  : config(_config)
{
}

UniversalNetworkEngine::~UniversalNetworkEngine()
{
}


void UniversalNetworkEngine::afterSetStopped()
{
  if (monitorConn) {
    monitorConn->close();
    monitorConn = nullptr;
  }
  if (controlConn) {
    controlConn->close();
    controlConn = nullptr;
  }
  for (auto &portIt : scriptPorts) {
    portIt->close();
  }
  scriptPorts.clear();
}


bool UniversalNetworkEngine::setNetworkConfig(YogaContext const &ctx, AstAnnoCall *spec)
{
  if (!spec->options->getValueForKey("robotHostname", config.robotHostname)) {
    return ctx.logError("Bad robotHostname");
  }
  if (!spec->options->getValueForKey("controlScript", config.controlScript)) {
    return ctx.logError("Bad controlScript");
  }
  return true;
}


void UniversalNetworkEngine::update(EngineUpdateContext &ctx) {}

UniversalNetworkScriptDataPortListener *UniversalNetworkEngine::addScriptPort(string port)
{
  auto ret = make_shared<UniversalNetworkScriptDataPortListener>(
      static_pointer_cast<UniversalNetworkEngine>(shared_from_this()), port);
  scriptPorts.push_back(ret);
  if (verbose >= 1) console("Listening on port "s + port);
  return ret.get();
}

UniversalNetworkMonitorPortActive *UniversalNetworkEngine::addMonitor(string hostname, string port)
{
  monitorConn = make_shared<UniversalNetworkMonitorPortActive>(
      static_pointer_cast<UniversalNetworkEngine>(shared_from_this()), hostname, port);
  return monitorConn.get();
}

UniversalNetworkControlPortActive *UniversalNetworkEngine::addControl(string hostname, string port)
{
  controlConn = make_shared<UniversalNetworkControlPortActive>(
      static_pointer_cast<UniversalNetworkEngine>(shared_from_this()), hostname, port);
  return controlConn.get();
}

UniversalRobotiqGripper *UniversalNetworkEngine::addGripper(string hostname, string port)
{
  gripperConn = make_shared<UniversalRobotiqGripper>(
      static_pointer_cast<UniversalNetworkEngine>(shared_from_this()), hostname, port);
  return gripperConn.get();
}
