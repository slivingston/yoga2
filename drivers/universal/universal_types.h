#pragma once
#include "../../jit/runtime.h"

struct UniversalRobotKinematicsAccessorSuite {
  UniversalRobotKinematicsAccessorSuite(YogaType *_t)
    : t(_t),
      checksums_(t, "checksums"),
      dhtheta_(t, "dhtheta"),
      dha_(t, "dha"),
      dhd_(t, "dhd"),
      dhalpha_(t, "dhalpha"),
      calibrationStatus_(t, "calibrationStatus")
  {
  }

  YogaType *t;
  YogaValueAccessor<R *> checksums_;
  YogaValueAccessor<R *> dhtheta_;
  YogaValueAccessor<R *> dha_;
  YogaValueAccessor<R *> dhd_;
  YogaValueAccessor<R *> dhalpha_;
  YogaValueAccessor<S32> calibrationStatus_;


  void rx(packet &rx, YogaValue const &yv)
  {
    assert(yv.type == t);

    R checksums[6];
    for (int i = 0; i < 6; i++) checksums[i] = rx.get_be_uint32();
    checksums_.wr(yv, checksums);

    R dhtheta[6];
    for (int i = 0; i < 6; i++) dhtheta[i] = rx.get_be_double();
    dhtheta_.wr(yv, dhtheta);

    R dha[6];
    for (int i = 0; i < 6; i++) dha[i] = rx.get_be_double();
    dha_.wr(yv, dha);

    R dhd[6];
    for (int i = 0; i < 6; i++) dhd[i] = rx.get_be_double();
    dhd_.wr(yv, dhd);

    R dhalpha[6];
    for (int i = 0; i < 6; i++) dhalpha[i] = rx.get_be_double();
    dhalpha_.wr(yv, dhalpha);

    S32 calibrationStatus = rx.get_be_uint8();
    calibrationStatus_.wr(yv, calibrationStatus);
  }
};

struct UniversalRobotJointDataAccessorSuite {
  UniversalRobotJointDataAccessorSuite(YogaType *_t)
    : t(_t),
      q_actual(t, "q_actual"),
      q_target(t, "q_target"),
      qd_actual(t, "qd_actual"),
      i_actual(t, "i_actual"),
      v_actual(t, "v_actual"),
      temperature(t, "temperature"),
      joint_mode(t, "joint_mode")
  {
  }

  YogaType *t;
  YogaValueAccessor<R *> q_actual;
  YogaValueAccessor<R *> q_target;
  YogaValueAccessor<R *> qd_actual;
  YogaValueAccessor<R *> i_actual;
  YogaValueAccessor<R *> v_actual;
  YogaValueAccessor<R *> temperature;
  YogaValueAccessor<int *> joint_mode;

  void rx(packet &rx, YogaValue const &yv)
  {
    array<R, 6> _q_actual;
    array<R, 6> _q_target;
    array<R, 6> _qd_actual;
    array<R, 6> _i_actual;
    array<R, 6> _v_actual;
    array<R, 6> _temperature;
    array<int, 6> _joint_mode;

    for (int i=0; i<6; i++) {
      _q_actual[i] = rx.get_be_double();
      _q_target[i] = rx.get_be_double();
      _qd_actual[i] = rx.get_be_double();
      _i_actual[i] = rx.get_be_float();
      _v_actual[i] = rx.get_be_float();
      _temperature[i] = rx.get_be_float();
      rx.get_skip(4);
      _joint_mode[i] = rx.get_be_uint8();
    }

    q_actual.wr(yv, _q_actual.data());
    q_target.wr(yv, _q_target.data());
    qd_actual.wr(yv, _qd_actual.data());
    i_actual.wr(yv, _i_actual.data());
    v_actual.wr(yv, _v_actual.data());
    temperature.wr(yv, _temperature.data());
    joint_mode.wr(yv, _joint_mode.data());
  }
};


struct UniversalRobotModeDataAccessorSuite {
  UniversalRobotModeDataAccessorSuite(YogaType *_t)
    : t(_t),
      physicalRobotConnected_(t, "physicalRobotConnected"),
      realRobotEnabled_(t, "realRobotEnabled"),
      robotPowerOn_(t, "robotPowerOn"),
      emergencyStopped_(t, "emergencyStopped"),
      protectiveStopped_(t, "protectiveStopped"),
      programRunning_(t, "programRunning"),
      programPaused_(t, "programPaused"),
      robotMode_(t, "robotMode"),
      controlMode_(t, "controlMode"),
      targetSpeedFraction_(t, "targetSpeedFraction"),
      speedScaling_(t, "speedScaling"),
      targetSpeedFractionLimit_(t, "targetSpeedFractionLimit")
  {
  }

  YogaType *t;
  YogaValueAccessor<bool> physicalRobotConnected_;
  YogaValueAccessor<bool> realRobotEnabled_;
  YogaValueAccessor<bool> robotPowerOn_;
  YogaValueAccessor<bool> emergencyStopped_;
  YogaValueAccessor<bool> protectiveStopped_;
  YogaValueAccessor<bool> programRunning_;
  YogaValueAccessor<bool> programPaused_;
  YogaValueAccessor<int> robotMode_;
  YogaValueAccessor<int> controlMode_;
  YogaValueAccessor<R> targetSpeedFraction_;
  YogaValueAccessor<R> speedScaling_;
  YogaValueAccessor<R> targetSpeedFractionLimit_;

  void rx(packet &rx, YogaValue const &yv)
  {
    physicalRobotConnected_.wr(yv, !!rx.get_be_uint8());
    realRobotEnabled_.wr(yv, !!rx.get_be_uint8());
    robotPowerOn_.wr(yv, !!rx.get_be_uint8());
    emergencyStopped_.wr(yv, !!rx.get_be_uint8());
    protectiveStopped_.wr(yv, !!rx.get_be_uint8());
    programRunning_.wr(yv, !!rx.get_be_uint8());
    programPaused_.wr(yv, !!rx.get_be_uint8());
    robotMode_.wr(yv, (int)rx.get_be_uint8());
    controlMode_.wr(yv, (int)rx.get_be_uint8());
    targetSpeedFraction_.wr(yv, rx.get_be_double());
    speedScaling_.wr(yv, rx.get_be_double());
    targetSpeedFractionLimit_.wr(yv, rx.get_be_double());
    rx.get_be_uint8(); // dummy
  }
};

struct RobotiqGripStatusAccessorSuite {
  RobotiqGripStatusAccessorSuite(YogaType *_t)
    : t(_t),
      obj(t, "obj"),
      flt(t, "flt"),
      sta(t, "sta"),
      pre(t, "pre")
  {
  }

  YogaType *t;
  YogaValueAccessor<R> obj;
  YogaValueAccessor<int> flt;
  YogaValueAccessor<R> sta;
  YogaValueAccessor<R> pre;
};


struct RobotiqGripCmdAccessorSuite {
  RobotiqGripCmdAccessorSuite(YogaType *_t)
    : t(_t),
      pos(t, "pos"),
      speed(t, "speed"),
      force(t, "force")
  {
  }

  YogaType *t;
  YogaValueAccessor<R> pos;
  YogaValueAccessor<R> speed;
  YogaValueAccessor<R> force;


};

struct UniversalRobotStatusAccessorSuite {
  UniversalRobotStatusAccessorSuite(YogaType *_t)
    : t(_t)
  {
  }

  YogaType *t;
};

struct BaristaArmObsAccessorSuite {
  BaristaArmObsAccessorSuite(YogaType *_t)
    : t(_t),
      target_q(t, "target_q"),
      target_qd(t, "target_qd"),
      target_current(t, "target_current"),
      target_moment(t, "target_moment"),
      actual_q(t, "actual_q"),
      actual_qd(t, "actual_qd"),
      actual_current(t, "actual_current"),
      actual_TCP_pose(t, "actual_TCP_pose"),
      actual_TCP_speed(t, "actual_TCP_speed"),
      actual_TCP_force(t, "actual_TCP_force"),
      joint_mode(t, "joint_mode"),
      safety_mode(t, "safety_mode")
  {
  }

  YogaType *t;
  YogaValueAccessor<R *> target_q;
  YogaValueAccessor<R *> target_qd;
  YogaValueAccessor<R *> target_current;
  YogaValueAccessor<R *> target_moment;
  YogaValueAccessor<R *> actual_q;
  YogaValueAccessor<R *> actual_qd;
  YogaValueAccessor<R *> actual_current;
  YogaValueAccessor<R *> actual_TCP_pose;
  YogaValueAccessor<R *> actual_TCP_speed;
  YogaValueAccessor<R *> actual_TCP_force;
  YogaValueAccessor<S32 *> joint_mode;
  YogaValueAccessor<S32> safety_mode;

  void rx(packet &rx, YogaValue &yv)
  {
    
    R tmp[6];
    S32 tmpi[6];
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    target_q.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    target_qd.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    target_current.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    target_moment.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    actual_q.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    actual_qd.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    actual_current.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    actual_TCP_pose.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    actual_TCP_speed.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmp[i] = rx.get_be_double();
    actual_TCP_force.wr(yv, tmp);
    for (int i = 0; i < 6; i++) tmpi[i] = rx.get_be_uint32();
    joint_mode.wr(yv, tmpi);

    safety_mode.wr(yv, rx.get_be_uint32());
  }
  
};

struct BaristaCmdAccessorSuite {
  BaristaCmdAccessorSuite(YogaType *_t)
    :t(_t),
     q_target(t, "q_target"),
     qd_target(t, "qd_target"),
     servogain(t, "servogain"),
     lookahead(t, "lookahead")
  {
  }

  YogaType *t;
  YogaValueAccessor<R> q_target;
  YogaValueAccessor<R> qd_target;
  YogaValueAccessor<R> servogain;
  YogaValueAccessor<R> lookahead;

  void tx(packet &tx, YogaValue &yv)
  {
    R tmp[6];
    q_target.rd(yv, tmp[0]);
    for (int i = 0; i < 6; i++) tx.add_be_double(tmp[i]);
    qd_target.rd(yv, tmp[0]);
    for (int i = 0; i < 6; i++) tx.add_be_double(tmp[i]);

    R t1;
    servogain.rd(yv, t1);
    tx.add_be_double(t1);
    lookahead.rd(yv, t1);
    tx.add_be_double(t1);
    tx.add_be_uint32(1);
  }
  
};
