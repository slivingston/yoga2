'use strict';

const assert = require('assert');
const _ = require('lodash');
const util = require('util');
const path = require('path');
const yb = require('../binary');

function indent(code, prefix)
{
  return _.map(code.split('\n'), (l) => prefix+l).join('\n');
}

function stdThreadProgram(o)
{
  return `def servoloop():
  set_gravity([0.0, 0.0, 9.82])
  set_tcp(p[0.0,0.0,0.0,0.0,0.0,0.0])
  set_payload(0.0)
  set_tool_communication(False, 115200, 0, 1, 1.5, 3.5)
  set_tool_output_mode(0)
  set_tool_digital_output_mode(0, 1)
  set_tool_digital_output_mode(1, 1)
  set_safety_mode_transition_hardness(1)
  set_standard_analog_input_domain(0, 1)
  set_standard_analog_input_domain(1, 1)
  set_tool_analog_input_domain(0, 1)
  set_tool_analog_input_domain(1, 1)
  set_analog_outputdomain(0, 0)
  set_analog_outputdomain(1, 0)
  set_tool_voltage(0)
  set_input_actions_to_default()
  global setp = get_actual_joint_positions()
  global origjp = get_actual_joint_positions()
  rtde_set_watchdog("input_int_register_0", 2.0, "pause")
  thread Thread_1():
    while (True):
      global tmp = [0,0,0,0,0,0]
      tmp[0] = read_input_float_register(0)
      tmp[1] = read_input_float_register(1)
      tmp[2] = read_input_float_register(2)
      tmp[3] = read_input_float_register(3)
      tmp[4] = read_input_float_register(4)
      tmp[5] = read_input_float_register(5)
      servogain = read_input_float_register(6)
      lookahead = read_input_float_register(7)
      if (tmp != [0,0,0,0,0,0]):
        global setp = tmp
      end
      servoj(setp, 0, 0, 0.002, lookahead, servogain)
    end
  end
  threadId_Thread_1 = run Thread_1()
  while (True):
    sleep(1.0)
  end
end
`;
}
