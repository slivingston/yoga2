#include "./camera_client.h"

#include "../../timeseq/timeseq.h"
#include "common/uv_wrappers.h"

/*
  Upgrade Axis p1365 firmware to version 8.40. Version 7.x sucks.
*/

struct GenericParser;

struct AxisCameraClient : CameraClient {
  AxisCameraClient()
    : CameraClient(),
      httpClientSock()
  {
  }

  ~AxisCameraClient()
  {
    pleaseStop();
  }

  AxisCameraClient(AxisCameraClient const &other) = delete;
  AxisCameraClient(AxisCameraClient &&other) = delete;
  AxisCameraClient &operator=(const AxisCameraClient &other) = delete;
  AxisCameraClient &operator=(AxisCameraClient &&other) = delete;

  bool isRunning() override { return running; }
  void pleaseStop() override;

  void startHttpClient();
  void httpClientRead(ssize_t nr, const uv_buf_t *buf);

  bool running{false};
  UvStream httpClientSock;
  string httpClientStatus;
  GenericParser *parser{nullptr};
};

shared_ptr<CameraClient>
CameraClient::mkAxisCameraClient(RemoteCameraConfig const &config, VideoTraceSpec const &spec)
{
  auto c = make_shared<AxisCameraClient>();
  c->cameraConfig = config;
  c->traceSpec = spec;
  c->startHttpClient();

  return static_pointer_cast<CameraClient>(c);
}

struct GenericParser {
  GenericParser(AxisCameraClient *_parent) : parent(_parent) {}

  virtual void run(char *buf, int nr) = 0;
  virtual bool isStateError() { return false; }
  AxisCameraClient *parent;
};

string canonHeader(string const &s)
{
  string ret;
  for (auto c : s) {
    if (c >= 'A' && c <= 'Z') {
      ret.push_back(c + ('a' - 'A'));
    }
    else {
      ret.push_back(c);
    }
  }
  return ret;
}

struct MultipartParser : GenericParser {
  MultipartParser(AxisCameraClient *_parent, string const &_separator)
      : GenericParser(_parent),
        separator("\r\n--"s + _separator + "\r\n"s),
        frameno(0),
        sepaccum("\r\n"s),
        partBytesLeft(0),
        partTotalBytes(0),
        partData(nullptr),
        partDataPos(0),
        partStartTs(0.0),
        state(WAIT_BOUNDARY)
  {
  }

  void run(char *buf, int nr) override
  {
    for (int bufi = 0; bufi < nr; bufi++) {
      int c = buf[bufi];

      switch (state) {
        case WAIT_BOUNDARY:
          sepaccum.push_back(c);
          if (sepaccum.size() >= separator.size()
              && string(sepaccum.begin() + (sepaccum.size() - separator.size()), sepaccum.end()) == separator) {
            if (sepaccum.size() > separator.size() + 10) {
              cerr << "scanned "s + repr(sepaccum.size()) + " characters for separator\n";
            }
            state = HDRLINE;
            sepaccum.clear();
            headers.clear();
          }
          break;

        case HDRLINE:
          if (c == '\n') {
            if (curHeader.empty()) {
              startPart();
            }
            else {
              setHdrLine(curHeader);
              curHeader.resize(0);
            }
          }
          else if (c == '\r') {
          }
          else {
            curHeader.push_back(c);
          }
          break;

        case PART_BODY:
          int todo = min(partBytesLeft, nr - bufi);
          memcpy(partData + partDataPos, buf + bufi, todo);

          partDataPos += todo;
          partBytesLeft -= todo;
          bufi += todo - 1;

          if (partBytesLeft == 0) endPart();
          break;
      }
    }
  }

  void startPart()
  {

    string cl_str = headers["content-length"s];
    int cl = atoi(cl_str.c_str());
    state = PART_BODY;
    partTotalBytes = partBytesLeft = cl;
    partDataPos = 0;
    partData = new char[partTotalBytes];
    partStartTs = realtime();
    if (parent->verbose) cerr << "Start part len="s + repr(cl) + "\n";
  }

  void endPart()
  {

    double frameTs = partStartTs - 0.030; // ex-recto

    YogaVideoFrame frame;
    frame.format = YVFF_JPEG;
    frame.width = 640.0; // FIXME
    frame.height = 480.0;
    frame.orientation = parent->cameraConfig.baseOrientation;
    frame.blob = mkBlob(parent->curChunkId, (U8 *)partData, (U64)partTotalBytes);
    frame.priority = 0;
    delete[] partData;
    partData = nullptr;

    if (parent) {
      if (parent->cameraConfig.captureStillsInterval > 0.0
          && frameTs - parent->lastStillTs > parent->cameraConfig.captureStillsInterval) {
        parent->lastStillTs = frameTs;
        frame.priority = 1;
      }
      parent->videoQueue.emplace_back(frameTs, frame);
    }

    state = WAIT_BOUNDARY;
    frameno++;
  }

  void setHdrLine(string const &s)
  {
    if (parent->verbose) cerr << "hdr line: " + s + "\n";

    char *stmp = strdup(s.c_str());
    char *p = stmp;
    char *key = strsep(&p, ":");
    while (*p == ' ') p++;
    char *value = strsep(&p, "");

    headers[canonHeader(string(key))] = string(value);
    free(stmp);
  }

  string separator;
  int frameno;

  string curHeader;
  string sepaccum;
  map<string, string> headers;
  int partBytesLeft;
  int partTotalBytes;

  char *partData;
  int partDataPos;
  double partStartTs;

  enum {
    WAIT_BOUNDARY,
    HDRLINE,
    PART_BODY,
  } state;
};

struct ResponseParser : GenericParser {
  ResponseParser(AxisCameraClient *_parent) : GenericParser(_parent), state(RESPLINE), clrem(0), outParser(nullptr) {}

  void run(char *buf, int nr) override
  {
    for (int bufi = 0; bufi < nr; bufi++) {
      int c = buf[bufi];

      switch (state) {

        case RESPLINE:
          if (c == '\n') {
            if (parent->verbose >= 1) cerr << "resp line: "s + curHeader + "\n";
            curHeader.resize(0);
            state = HDRLINE;
          }
          else if (c == '\r') {
          }
          else {
            curHeader.push_back(c);
          }
          break;

        case HDRLINE:
          if (c == '\n') {
            if (curHeader.empty()) {
              startBody();
            }
            else {
              setHdrLine(curHeader);
              curHeader.resize(0);
            }
          }
          else if (c == '\r') {
          }
          else {
            curHeader.push_back(c);
          }
          break;

        case BODY:
          outParser->run(&buf[bufi], nr - bufi);
          bufi += nr - bufi - 1;
          break;

        case DUMPING:
          cerr << "BODY: "s + string(&buf[bufi], nr - bufi) + "\n";
          bufi += nr - bufi - 1;
          state = ERROR;

        case ERROR: break;
      }
    }
  }

  void startBody()
  {
    state = BODY;

    string ct = headers["content-type"s];
    string lookfor("multipart/x-mixed-replace; boundary=");
    string boundary;
    if (ct.size() > lookfor.size() && ct.compare(0, lookfor.size(), lookfor) == 0) {
      boundary = string(ct.begin() + lookfor.size(), ct.end());
      if (parent->verbose >= 1) {
        cerr << "Found boundary=" + shellEscape(boundary) + " in " + shellEscape(ct) + "\n";
      }
      outParser = new MultipartParser(parent, boundary);
    }
    else {
      cerr << "AxisCameraClient Can't handle content-type " + ct + "\n";
      for (auto it : headers) {
        cerr << "Header: "s + it.first + ": " + it.second + "\n";
      }
      state = DUMPING;
    }
  }

  void setHdrLine(string const &s)
  {
    if (parent->verbose >= 1) cerr << "hdr line: " + s + "\n";

    char *stmp = strdup(s.c_str());
    char *p = stmp;
    char *key = strsep(&p, ":");
    char *value = nullptr;
    if (!key || !p) goto fail;
    while (*p == ' ') p++;
    value = strsep(&p, "");
    if (!value) goto fail;
    headers[canonHeader(string(key))] = string(value);
  fail:
    free(stmp);
  }

  bool isStateError() override { return state == ERROR; }

  enum { RESPLINE, HDRLINE, BODY, DUMPING, ERROR } state;
  int clrem;

  string curHeader;
  map<string, string> headers;

  GenericParser *outParser;
};

// ----------------------------------------------------------------------

void AxisCameraClient::pleaseStop()
{
  CameraClient::pleaseStop();
  if (running) {
    httpClientSock.read_stop();
    httpClientSock.close();
    running = false;
  }
}

void AxisCameraClient::startHttpClient()
{
  if (isRunning()) return;
  parser = new ResponseParser(this);

  cerr << "Start AxisCameraClient(" + cameraConfig.cameraHost + ")\n";

  running = true;
  uvGetAddrInfo(cameraConfig.cameraHost, "80"s, addrinfoForTcp(), [this](int status, struct addrinfo *res) {
    if (status < 0) {
      cerr << "Resolve hostname "s + cameraConfig.cameraHost + ": " + uv_strerror(status) + "\n";
      running = false;
      return;
    }

    httpClientSock.tcp_init();
    httpClientSock.tcp_connect(res->ai_addr, [this](int status) {
      if (status < 0) {
        cerr << "Connect to "s + cameraConfig.cameraHost + ": " + uv_strerror(status) + "\n";
        running = false;
        return;
      }

      httpClientSock.read_start(
          [](size_t suggested_size, uv_buf_t *buf) {
            buf->base = static_cast<char *>(malloc(suggested_size));
            buf->len = suggested_size;
          },
          [this](ssize_t nr, const uv_buf_t *buf) { httpClientRead(nr, buf); });

      string httpCmd = "GET "s + cameraConfig.url + " HTTP/1.1\r\n"s;
      httpCmd += "Accept: */*\r\n"s;
      if (!cameraConfig.authHeader.empty()) {
        httpCmd += "Authorization: "s + cameraConfig.authHeader + "\r\n"s;
      }
      httpCmd += "Host: "s + cameraConfig.cameraHost + "\r\n"s;
      httpCmd += "\r\n"s;

      if (verbose) cerr << cameraConfig.cameraHost + " < " + httpCmd;
      httpClientSock.write(httpCmd, [this](int status) {
        if (status < 0) {
          cerr << "Write to "s + cameraConfig.cameraHost + ": " + uv_strerror(status) + "\n";
          running = false;
          return;
        }
      });
    });
  });
}

void AxisCameraClient::httpClientRead(ssize_t nr, const uv_buf_t *buf)
{
  if (nr < 0) {
    // socket was closed while we were reading
    cerr << cameraConfig.cameraHost + ": " + uv_strerror(nr) + "\n";
  }
  else if (nr == 0) {
    cerr << cameraConfig.cameraHost + ": EOF\n";
    pleaseStop();
  }
  else {
    parser->run(buf->base, nr);
    if (parser->isStateError()) {
      pleaseStop();
    }
  }
}
