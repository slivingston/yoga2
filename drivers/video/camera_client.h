#pragma once
#include "common/std_headers.h"
#include "common/uv_wrappers.h"
#include "./video_types.h"

#include <atomic>


struct CameraClient {
  CameraClient() 
    : asyncQueue()
  {
    curChunkId = mkBlobChunkId();
    if (0) recordUntil = realtime() + 1.0;
  }
  virtual ~CameraClient() {}

  CameraClient(CameraClient const &other) = delete;
  CameraClient(CameraClient &&other) = delete;
  CameraClient &operator=(const CameraClient &other) = delete;
  CameraClient &operator=(CameraClient &&other) = delete;

  virtual void pleaseStop() { recordUntil = 0.0; };
  virtual bool isRunning() = 0;

  RemoteCameraConfig cameraConfig;
  VideoTraceSpec traceSpec;
  double recordUntil{0.0};
  U64 curChunkId{0};
  int verbose{0};

  double lastStillTs{0.0};
  deque<pair<R, YogaVideoFrame>> videoQueue;
  deque<pair<R, YogaAudioFrame>> audioQueue;

  UvAsyncQueue asyncQueue;

  static shared_ptr<CameraClient>
  mkAxisCameraClient(RemoteCameraConfig const &config, VideoTraceSpec const &spec);
  static shared_ptr<CameraClient>
  mkPylonCameraClient(RemoteCameraConfig const &config, VideoTraceSpec const &spec);
  static shared_ptr<CameraClient>
  mkAlsaAudioClient(RemoteCameraConfig const &config, VideoTraceSpec const &spec);

  static void enumeratePylonCameras();
};
