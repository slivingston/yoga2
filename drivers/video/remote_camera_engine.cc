#include "./video_types.h"
#include "common/packetbuf_types.h"
#include "../../geom/geom.h"
#include "../../../comm/child_pipe.h"
#include "../../../comm/tcp_pipe.h"
#include <glm/gtx/string_cast.hpp>
#include "nlohmann-json/json.hpp"

struct RemoteCameraEngine : GenericEngine {
  RemoteCameraEngine(string const &_ctlHost, shared_ptr<PacketNetworkEngine> _pkt)
    : ctlHost(_ctlHost),
      pkt(_pkt)
  {
  }

  string ctlHost;
  shared_ptr<PacketNetworkEngine> pkt;
  shared_ptr<YogaTimeseq> videoSeq;
  shared_ptr<YogaTimeseq> audioSeq;

  RemoteCameraConfig config;

  R delta{0.0};
  bool deltaSet{false};
  R lastSentTs{0.0};
  U64 fastIngestCount{0}, slowIngestCount{0};

  void addToManifest(json &manifest) override;

  bool setCameraConfig(YogaContext const &ctx, AstAnnoCall *spec);
  void afterSetDraining() override;
  bool needsDrain() const override { return true; }
  VideoTraceSpec getTraceSpec();
  int ingestCameraData(packet &rx);

  void update(EngineUpdateContext &ctx) override;
};

void RemoteCameraEngine::addToManifest(json &manifest)
{
  manifest["videoHosts"s].push_back(ctlHost);
}

void RemoteCameraEngine::afterSetDraining()
{
  pkt->rpc("record",
    [this](packet &tx) {
        auto traceSpec = getTraceSpec();
        tx.add(0.0); // recordFor=0
        tx.add(traceSpec);
        tx.add(config);
    },
    [this, thisp=shared_from_this()](string const &err, packet &rx) {
      if (!err.empty()) {
        console(err);
      }
      else {
        R reqTxTime;
        rx.get(reqTxTime);
        ingestCameraData(rx);
      }
      endDrain();
      pkt->endDrain();
      pkt = nullptr;
    }
  );
}

VideoTraceSpec RemoteCameraEngine::getTraceSpec()
{
  VideoTraceSpec traceSpec;
  traceSpec.traceName = rti->traceName;
  if (videoSeq) {
    traceSpec.seqName = videoSeq->seqName;
  }
  else if (audioSeq) {
    traceSpec.seqName = audioSeq->seqName;
  }
  return traceSpec;
}



void RemoteCameraEngine::update(EngineUpdateContext &ctx)
{
  if (isRunning() && ctx.now - lastSentTs > 0.05 && workOutstanding() < 1 && (videoSeq || audioSeq)) {
    lastSentTs = ctx.now;

    R txTime = ctx.now;

    pkt->rpc( // See RemoteCameraDaemon::rpc_record
      "record",
      [this](packet &tx) {

        auto traceSpec = getTraceSpec();
        double recordFor = 2.0;
        tx.add(recordFor);
        tx.add(traceSpec);
        tx.add(config);
      },
      [this, thisp=shared_from_this(), txTime](string const &err, packet &rx) {
        double rxTime = realtime();
        if (err.size()) {
          console("record error: "s + err);
          return;
        }

        R remoteTime;
        rx.get(remoteTime);

        /*
          Estimate time skew between remote and local. The first few rpcs are usually slow, so wait
          until we get a RTT less than 2 mS before committing.
        */
        double minDelta = remoteTime - rxTime;
        double maxDelta = remoteTime - txTime;
        if (!deltaSet) {
          delta = (minDelta + maxDelta) * 0.5;
          if (maxDelta - minDelta < 0.005) {
            if (verbose >= 2) console(
              "txTime=" + repr_0_3f(txTime) + 
              " rxTime=" + repr_0_3f(rxTime) + 
              " remoteTime=" + repr_0_3f(remoteTime));
            if (verbose >= 1) console(
              "timing minDelta="s + repr_0_6f(minDelta) + 
              " maxDelta="s + repr_0_6f(maxDelta) +
              " averaged delta="s + repr_0_6f(delta));
            deltaSet = true;
          }
        }
        if (delta < minDelta) {
          delta = minDelta;
          if (verbose >= 1) console(
              "timing minDelta="s + repr_0_6f(minDelta) + 
              " maxDelta="s + repr_0_6f(maxDelta) +
              " increased delta="s + repr_0_6f(delta));
        }
        else if (delta > maxDelta) {
          delta = maxDelta;
          if (verbose >= 1) console(
              "timing minDelta="s + repr_0_6f(minDelta) + 
              " maxDelta="s + repr_0_6f(maxDelta) +
              " decreased delta="s + repr_0_6f(delta));
        }
        fastIngestCount += ingestCameraData(rx);

        requestUpdate();
      }
    );
  }
}

int RemoteCameraEngine::ingestCameraData(packet &rx)
{
  int doneCount = 0;
  while (1) {
    char token = rx.get_char();
    if (token == 'V') {
      R ts{0.0};
      YogaVideoFrame f;
      rx.get(ts);
      if (pkt->rx_version == "yoga 1.0/camerad 1.0") {
        auto getter = rx.get_compat_func(f, "VideoFrame:1");
        getter(rx, f);
      }
      else {
        rx.get_compat(f);
      }
      console("version=" + pkt->rx_version + 
        " f.priority=" + repr(f.priority) +
        " f.format=" + repr(f.format) +
        " f.width=" + repr(f.width) +
        " f.height=" + repr(f.height));
      if (videoSeq) {
        U8 *p = videoSeq->type->mkInstance(f);
        auto relts = fromExternalTime(ts - delta);
        if (verbose >= 2) {
          console("video frame "s + repr_0_6f(relts) + " (" + repr_0_6f(ts) + "-" + repr_0_6f(delta) + ")");
        }
        videoSeq->add(relts, p);
      }
      doneCount++;
    }
    else if (token == 'A') {
      R ts{0.0};
      YogaAudioFrame f;
      rx.get(ts);
      if (pkt->rx_version == "yoga 1.0/camerad 1.0") {
        auto getter = rx.get_compat_func(f, "AudioFrame:1");
        getter(rx, f);
      }
      else {
        rx.get_compat(f);
      }
      if (audioSeq) {
        U8 *p = audioSeq->type->mkInstance(f);
        audioSeq->add(fromExternalTime(ts - delta), p);
      }
      doneCount++;
    }
    else if (token == 'L') {
      U64 chunkId;
      string host, fn;
      rx.get(chunkId);
      rx.get(host);
      rx.get(fn);

      if (auto ltrace = trace.lock()) {
        ltrace->blobLocationsByChunkId[chunkId] = make_pair(host, fn);
      }
    }
    else if (token == '.') {
      break;
    }
    else {
      throw runtime_error("Bad token " + to_string(int(token)));
    }
  }
  return doneCount;
}


bool RemoteCameraEngine::setCameraConfig(YogaContext const &ctx, AstAnnoCall *spec)
{
  if (!spec->options->getValueForKey("ctlHost", config.ctlHost)) {
    return ctx.logError("Bad ctlHost");
  }
  if (!spec->options->getValueForKey("cameraHost", config.cameraHost)) {
    return ctx.logError("Bad cameraHost");
  }
  if (!spec->options->getValueForKey("url", config.url)) {
    return ctx.logError("Bad url");
  }
  if (!spec->options->getValueForKey("authHeader", config.authHeader)) {
    return ctx.logError("Bad authHeader");
  }
  if (!spec->options->getValueForKey("usbId", config.usbId)) {
    return ctx.logError("Bad usbId");
  }
  if (!spec->options->getValueForKey("devSerial", config.devSerial)) {
    return ctx.logError("Bad devSerial");
  }
  if (!spec->options->getValueForKey("captureStillsInterval", config.captureStillsInterval)) {
    return ctx.logError("Bad captureStillsInterval");
  }
  config.baseOrientation = glm::dmat4(1.0);
  if (!spec->options->getValueForKey("baseOrientation", config.baseOrientation)) {
    return ctx.logError("Bad baseOrientation");
  }
  if (!spec->options->getValueForKey("camSettings", config.camSettings)) {
    return ctx.logError("Bad camSettings");
  }

  cerr << "Configured camera "s + repr(config) + "\n";

  return true;
}

static EngineRegister axisVideoReg("axisVideo",
  {
    "VideoFrame"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 1) {
      return ctx.logError("axisVideo: expected 1 argument (video)");
    }

    string ctlHost;
    if (!spec->options->getValueForKey("ctlHost", ctlHost)) {
      return ctx.logError("Bad ctlHost");
    }
    auto pkt = YogaLayout::instance().mkRpcConn("camerad", ctlHost, false);
    if (!pkt) return ctx.logError("No connection to " + ctlHost + ":camerad");

    auto e = make_shared<RemoteCameraEngine>(ctlHost, pkt);
    if (args.size() > 0) {
      e->videoSeq = args[0];
      e->videoSeq->isExtIn = true;
    }

    e->config.camType = "axis";
    if (!e->setEngineConfig(ctx, spec)) return false;
    if (!e->setCameraConfig(ctx, spec)) return false;
    pkt->verbose = max(0, e->verbose - 1);

    trace->addEngine(engineName, e, true);
    trace->addEngine(engineName + "_pkt", pkt, true);      

    return true;
  }
);


static EngineRegister pylonVideoReg(
  "pylonVideo", 
  {
    "VideoFrame"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 1) {
      return ctx.logError("pylonVideo: expected 1 argument (video)");
    }

    string ctlHost;
    if (!spec->options->getValueForKey("ctlHost", ctlHost)) {
      return ctx.logError("Bad ctlHost");
    }
    auto pkt = YogaLayout::instance().mkRpcConn("camerad", ctlHost, false);
    if (!pkt) return ctx.logError("No connection to " + ctlHost + ":camerad");

    auto e = make_shared<RemoteCameraEngine>(ctlHost, pkt);
    if (args.size() > 0) {
      e->videoSeq = args[0];
      e->videoSeq->isExtIn = true;
    }

    e->config.camType = "pylon";
    if (!e->setEngineConfig(ctx, spec)) return false;
    if (!e->setCameraConfig(ctx, spec)) return false;
    pkt->verbose = max(0, e->verbose - 1);

    trace->addEngine(engineName, e, true);      
    trace->addEngine(engineName + "_pkt", pkt, true);      

    return true;
  }
);



static EngineRegister alsaAudioReg(
  "alsaAudio",
  {
    "AudioFrame"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 1) {
      return ctx.logError("alsaAudio: expected 1 argument (audio)");
    }

    string ctlHost;
    if (!spec->options->getValueForKey("ctlHost", ctlHost)) {
      return ctx.logError("Bad ctlHost");
    }
    auto pkt = YogaLayout::instance().mkRpcConn("camerad", ctlHost, false);
    if (!pkt) return ctx.logError("No connection to " + ctlHost + ":camerad");

    auto e = make_shared<RemoteCameraEngine>(ctlHost, pkt);
    if (args.size() > 0) {
      e->audioSeq = args[0];
      e->audioSeq->isExtIn = true;
    }

    e->config.camType = "alsa";
    if (!e->setEngineConfig(ctx, spec)) return false;
    if (!e->setCameraConfig(ctx, spec)) return false;
    pkt->verbose = max(0, e->verbose - 1);

    trace->addEngine(engineName, e, true);      
    trace->addEngine(engineName + "_pkt", pkt, true);      

    return true;
  }
);


ostream & operator <<(ostream &s, const RemoteCameraConfig &a)
{
  return s <<
    "RemoteCameraConfig("s <<
      "ctlHost=" << shellEscape(a.ctlHost) << " " <<
      "cameraHost=" << shellEscape(a.cameraHost) << " " <<
      "url=" << shellEscape(a.url) << " " <<
      "authHeader=" << shellEscape(a.authHeader) << " " <<
      "usbId=" << shellEscape(a.usbId) << " " <<
      "devSerial=" << shellEscape(a.devSerial) << " " <<
      "captureStillsInterval=" << a.captureStillsInterval << " " <<
      "camSettings=" << a.camSettings << " " <<
      "baseOrientation=" << to_string(a.baseOrientation) <<
      ")";
      
}


ostream & operator <<(ostream &s, const VideoTraceSpec &a)
{
  return s <<
    "VideoTraceSpec("s <<
      "seqName=" << shellEscape(a.seqName) << " " <<
      "traceName=" << shellEscape(a.traceName) << " " <<
      ")";
      
}
