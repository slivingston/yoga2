#include "./video_types.h"
#include "common/packetbuf_types.h"
#include "../../../jit/compilation.h"
#include "../../../jit/memory.h"
#include <glm/gtc/type_ptr.hpp>

namespace packetio {
void packet_wr_value(packet &p, RemoteCameraConfig const &x)
{
  packet_wr_value(p, x.camType);
  packet_wr_value(p, x.ctlHost);
  packet_wr_value(p, x.cameraHost);
  packet_wr_value(p, x.url);
  packet_wr_value(p, x.authHeader);
  packet_wr_value(p, x.usbId);
  packet_wr_value(p, x.devSerial);
  packet_wr_value(p, x.captureStillsInterval);
  packet_wr_value(p, x.camSettings);
  packet_wr_value(p, x.baseOrientation);
}
string packet_get_typetag(RemoteCameraConfig const & /* x */)
{
  return "RemoteCameraConfig:1";
}
void packet_rd_value(packet &p, RemoteCameraConfig &x)
{
  packet_rd_value(p, x.camType);
  packet_rd_value(p, x.ctlHost);
  packet_rd_value(p, x.cameraHost);
  packet_rd_value(p, x.url);
  packet_rd_value(p, x.authHeader);
  packet_rd_value(p, x.usbId);
  packet_rd_value(p, x.devSerial);
  packet_rd_value(p, x.captureStillsInterval);
  packet_rd_value(p, x.camSettings);
  packet_rd_value(p, x.baseOrientation);
}

std::function<void(packet &, RemoteCameraConfig &)> packet_rd_value_compat(RemoteCameraConfig const &x, string const &typetag)
{
  return static_cast<void (*)(packet &, RemoteCameraConfig &)>(packet_rd_value);
}
}



namespace packetio {
void packet_wr_value(packet &p, VideoTraceSpec const &x)
{
  packet_wr_value(p, x.seqName);
  packet_wr_value(p, x.traceName);
}
string packet_get_typetag(VideoTraceSpec const & /* x */)
{
  return "VideoTraceSpec:1";
}
void packet_rd_value(packet &p, VideoTraceSpec &x)
{
  packet_rd_value(p, x.seqName);
  packet_rd_value(p, x.traceName);
}

std::function<void(packet &, VideoTraceSpec &)> packet_rd_value_compat(VideoTraceSpec const &x, string const &typetag)
{
  return static_cast<void (*)(packet &, VideoTraceSpec &)>(packet_rd_value);
}
}


namespace packetio {
void packet_wr_value(packet &p, YogaVideoFrame const &x)
{
  packet_wr_value(p, x.priority);
  packet_wr_value(p, x.format);
  packet_wr_value(p, x.width);
  packet_wr_value(p, x.height);
  packet_wr_value(p, x.orientation);
  packet_wr_value(p, x.blob);
}
string packet_get_typetag(YogaVideoFrame const & /* x */)
{
  return "VideoFrame:2";
}
void packet_rd_value(packet &p, YogaVideoFrame &x)
{
  packet_rd_value(p, x.priority);
  packet_rd_value(p, x.format);
  packet_rd_value(p, x.width);
  packet_rd_value(p, x.height);
  packet_rd_value(p, x.orientation);
  packet_rd_value(p, x.blob);
}

std::function<void(packet &, YogaVideoFrame &)> packet_rd_value_compat(YogaVideoFrame const &x, string const &typetag)
{
  return static_cast<void (*)(packet &, YogaVideoFrame &)>(packet_rd_value);
}
}

static YogaCodeRegister registerVideoFrame("video_types.cc", R"(
  struct BlobRef {
    U64 chunkId;
    U64 partOfs;
    U64 partSize;
  };

  struct VideoFrame {
    R priority;
    R format;
    R width;
    R height;
    R[4,4] orientation;
    BlobRef blob;
  };

  struct AudioFrame {
    R priority;
    R format;
    R sampleRate;
    R sampleCount;
    BlobRef blob;
  };

)");


namespace packetio {
void packet_wr_value(packet &p, YogaAudioFrame const &x)
{
  packet_wr_value(p, x.priority);
  packet_wr_value(p, x.format);
  packet_wr_value(p, x.sampleRate);
  packet_wr_value(p, x.sampleCount);
  packet_wr_value(p, x.blob);
}
string packet_get_typetag(YogaAudioFrame const & /* x */)
{
  return "AudioFrame:2";
}
void packet_rd_value(packet &p, YogaAudioFrame &x)
{
  packet_rd_value(p, x.priority);
  packet_rd_value(p, x.format);
  packet_rd_value(p, x.sampleRate);
  packet_rd_value(p, x.sampleCount);
  packet_rd_value(p, x.blob);
}

std::function<void(packet &, YogaAudioFrame &)> packet_rd_value_compat(YogaAudioFrame const &x, string const &typetag)
{
  return static_cast<void (*)(packet &, YogaAudioFrame &)>(packet_rd_value);
}
}



namespace packetio {
void packet_wr_value(packet &p, glm::dmat4 const &x)
{
  for (int i=0; i<16; i++) {
    packet_wr_value(p, glm::value_ptr(x)[i]);
  }
}

void packet_rd_value(packet &p, glm::dmat4 &x)
{
  for (int i=0; i<16; i++) {
    packet_rd_value(p, glm::value_ptr(x)[i]);
  }
}

}
