#include <atomic>
#include "./pylon_includes.h"

#include "../../../comm/parent_pipe.h"
#include "./camera_client.h"

#include "common/packetbuf_types.h"
#include <getopt.h>

/*
  {"method":"record","id":42,"params":[{"__type":"RemoteCameraRecordReq","txTime":1501021115.482347,"recordFor":2,"cameraConfig":{"__type":"RemoteCameraConfig","camType":"","hostname":"","url":"","authHeader":"","usbId":"","devSerial":"22322717","captureStillsInterval":0},"traceSpec":{"__type":"VideoTraceSpec","traceDir":"/tmp/pylontest_2017-07-25_15-18-35_ehr","seqName":"frontVid","traceName":"pylontest_2017-07-25_15-18-35_ehr"}}]}
*/

// ------------------

struct RemoteCameraDaemon : ParentPipe {
  RemoteCameraDaemon();

  void startTest();
  void setupHandlers() override;

  void rxEof() override;

  void rpc_record(packet &rx, PacketApiCb cb);
  void rpc_fetchBlob(packet &rx, PacketApiCb cb);
  void rpc_fetchBlobFile(packet &rx, PacketApiCb cb);
  void rpc_fetchTraceInfo(packet &rx, PacketApiCb cb);
  void rpc_fetchTraceSeq(packet &rx, PacketApiCb cb);

  map<string, shared_ptr<CameraClient>> clients;
};

static string calcClientName(RemoteCameraConfig const &config, VideoTraceSpec const &spec)
{
  return (
      config.camType + ";"s + 
      config.devSerial + ";"s +
      config.cameraHost + ";"s +
      spec.seqName);
}

RemoteCameraDaemon::RemoteCameraDaemon() {}

void RemoteCameraDaemon::setupHandlers()
{
  addApiMethod(
    "record",
    [this](packet &rx, PacketApiCb cb) {
    rpc_record(rx, cb);
    });

  addApiMethod(
    "fetchBlobFile",
    [this](packet &rx, PacketApiCb cb) {
      rpc_fetchBlobFile(rx, cb);
    });

  addApiMethod(
    "fetchTraceInfo",
    [this](packet &rx, PacketApiCb cb) {
      rpc_fetchTraceInfo(rx, cb);
    });

  addApiMethod(
    "fetchTraceSeq",
    [this](packet &rx, PacketApiCb cb) {
      rpc_fetchTraceSeq(rx, cb);
    });

  addApiMethod(
    "fetchBlob",
    [this](packet &rx, PacketApiCb cb) {
      rpc_fetchBlob(rx, cb);
    }
  );
}

void RemoteCameraDaemon::rxEof()
{
  cerr << "RemoteCameraDaemon: got EOF. Closing clients, expect uv loop to exit.\n";
  vector<string> delList;
  for (auto it : clients) {
    if (it.second) {
      it.second->pleaseStop();
      delList.push_back(it.first);
    }
  }

  while (true) {
    bool waitmore = false;
    for (auto const &delName : delList) {
      if (clients[delName]) {
        if (clients[delName]->isRunning()) {
          waitmore = true;
        }
      }
    }
    if (!waitmore) break;
    usleep(10000);
  }
  clients.clear();
}

template<typename T>
void txFrames(packet &tx, deque<pair<R, T>> &q, char token, int limit)
{
  set<U64> chunkIds;
  while (!q.empty() && limit > 0) {
    auto x = q.front();
    tx.add(token);
    tx.add(x.first);
    tx.add_checked(x.second);
    chunkIds.insert(x.second.blob.chunkId);
    q.pop_front();
    limit --;
  }
  for (auto &chunkId : chunkIds) {
    tx.add('L');
    tx.add(chunkId);
    tx.add(YogaLayout::instance().localHostName);
    tx.add(YogaLayout::instance().blobFilename(chunkId));
  }
}

void RemoteCameraDaemon::rpc_record(packet &rx, PacketApiCb cb)
{
  VideoTraceSpec traceSpec;
  RemoteCameraConfig cameraConfig;

  R recordFor{0.0};
  rx.get(recordFor);

  rx.get(traceSpec);
  rx.get(cameraConfig);

  string clientName = calcClientName(cameraConfig, traceSpec);
  auto &client = clients[clientName];

  
  if (recordFor > 0.0) {
    if (!client) {
      cerr << "Create client " + clientName + "\n";
      if (cameraConfig.camType == "pylon") {
        client = CameraClient::mkPylonCameraClient(cameraConfig, traceSpec);
      }
      else if (cameraConfig.camType == "axis") {
        client = CameraClient::mkAxisCameraClient(cameraConfig, traceSpec);
      }
      else if (cameraConfig.camType == "alsa") {
        client = CameraClient::mkAlsaAudioClient(cameraConfig, traceSpec);
      }
      else {
        return cb.err("badCamType");
      }
    }
    client->recordUntil = realtime() + recordFor;
    rotateBlobChunkId(client->curChunkId);
  }
  else if (client) {
    cerr << "recordFor=0, stopping client\n";
    client->pleaseStop();
  }
  else {
    cb.err("No such client");
    return;
  }

  cb.ok(
    [client](packet &tx) {
      tx.add(realtime());
      txFrames(tx, client->videoQueue, 'V', 100);
      txFrames(tx, client->audioQueue, 'A', 100);
      tx.add('.');
    }
  );
}

void RemoteCameraDaemon::rpc_fetchBlobFile(packet &rx, PacketApiCb cb)
{
  U64 chunkId=0;
  rx.get(chunkId);
  Blob data;
  if (!loadBlobFile(chunkId, data)) {
    return cb.err("Loading blobfile failed");
  }
  cb.ok([&data](packet &tx) {
    tx.add(data);
  });
}


void RemoteCameraDaemon::rpc_fetchBlob(packet &rx, PacketApiCb cb)
{
  BlobRef blobRef;
  rx.get(blobRef);
  Blob data;
  if (!loadBlob(blobRef, data)) {
    return cb.err("Loading blob failed");
  }
  cb.ok([&data](packet &tx) {
    tx.add(data);
  });
}

static bool isValidName(string const &a)
{
  if (a.empty()) return false;
  for (auto &it : a) {
    if (!isalnum(it) && it != '_' && it != '-' && it != '.') return false;
  }
  return true;
}



void RemoteCameraDaemon::rpc_fetchTraceInfo(packet &rx, PacketApiCb cb)
{
  string traceName;
  rx.get(traceName);
  if (!isValidName(traceName)) return cb.err("Bad traceName");

  auto fn = YogaLayout::instance().tracesDir + "/"s + traceName + "/manifest.json";


  int fd = open(fn.c_str(), O_RDONLY, 0);
  if (fd < 0) {
    return cb.err("File " + fn + ": " + strerror(errno));
  }

  struct stat st;
  if (fstat(fd, &st) < 0) {
    close(fd);
    return cb.err("File " + fn + ": " + strerror(errno));
  }
  auto size = st.st_size;

  string tmpdata;
  tmpdata.resize(size);

  auto nr = pread(fd, tmpdata.data(), size, 0);

  if (nr < 0) {
    return cb.err("Loading " + fn + ": " + strerror(errno));
  }
  else if (nr == 0) {
    return cb.err("Loading " + fn + ": empty");
  }
  else if (nr < size) {
    return cb.err("Loading " + fn + ": read " + to_string(nr) + "/" + to_string(size));
  }
  
  close(fd);
  cb.ok([&tmpdata](packet &tx) {
    tx.add(tmpdata);
  });
}


void RemoteCameraDaemon::rpc_fetchTraceSeq(packet &rx, PacketApiCb cb)
{
  string traceName;
  rx.get(traceName);
  string seqName;
  rx.get(seqName);
  U64 ofs = 0;
  rx.get(ofs);
  if (!isValidName(traceName)) return cb.err("Bad traceName");
  if (!isValidName(seqName)) return cb.err("Bad seqName");

  auto fn = YogaLayout::instance().tracesDir + "/"s + traceName + "/seq_" + seqName + ".bin.gz";
  
  int fd = open(fn.c_str(), O_RDONLY, 0);
  if (fd < 0) {
    return cb.err("File " + fn + ": " + strerror(errno));
  }

  struct stat st;
  if (fstat(fd, &st) < 0) {
    close(fd);
    return cb.err("File " + fn + ": " + strerror(errno));
  }
  auto size = min((off_t)(10*1024*1024), off_t(st.st_size - ofs));

  Blob tmpdata(size);

  auto nr = pread(fd, tmpdata.data(), size, ofs);

  if (nr < 0) {
    return cb.err("Loading " + fn + ": " + strerror(errno));
  }
  else if (nr == 0) {
    return cb.err("Loading " + fn + ": empty");
  }
  else if (nr < size) {
    return cb.err("Loading " + fn + ": read " + to_string(nr) + "/" + to_string(size));
  }
  
  close(fd);
  cb.ok([&tmpdata, &st](packet &tx) {
    tx.add((U64)st.st_size);
    tx.add(tmpdata);
  });
}


void usage()
{
  cerr << "usage: yoga_camerad [-d] [-C dir]\n";
}

int main(int argc, char * const * argv)
{
  apr_initialize();
  atexit(apr_terminate);
#ifdef USE_PYLON
  Pylon::PylonAutoInitTerm autoInitTerm; // PylonInitialize() will be called now
#endif

  static struct option longopts[] = {
    {nullptr, 0, nullptr, 0}
  };
  bool fromInetd=false;

  int ch;
  while ((ch = getopt_long(argc, argv, "C:d", longopts, nullptr)) != -1) {
    switch (ch) {
      case 'C':
        if (chdir(optarg) < 0) {
          cerr << string(optarg) + ": "s + strerror(errno);
          return 1;
        }
        break;

      case 'd':
        fromInetd = true;
        close(2);
        open("yoga_camerad.log", O_WRONLY|O_CREAT|O_APPEND, 0666);
        break;

      default:
        usage();
        return 2;
    }
  }

  argc -= optind;
  argv += optind;


  auto d = make_shared<RemoteCameraDaemon>();
  d->engineName = "rcd0";
  d->tx_version += "/camerad 1.1";
  d->start();
  if (0) CameraClient::enumeratePylonCameras();
  uvRunMainThread();
  d->stop();
  cerr << "yoga_camerad: main exiting\n";
  return 0;
}
