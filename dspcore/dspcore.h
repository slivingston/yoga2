#pragma once
#include "common/std_headers.h"

/*
  A simple fixed point arithmetic library.
  Mainly intended for avr32, but should work on Unix too.
*/

#if !defined(__AVR__) && !defined(__AVR32__)
#define DSPCORE_HAS_FP
#endif

#ifdef __cplusplus

/*
  In C, the dsp types are simple scalars of 16, 32, or 64 bits. In C++ we wrap them in a
  struct and define restrictive type conversion operators on them to enforce correctness.
  The goal is that the C++ code should behave exactly like the C code, but enforce type checks.
*/
template <int BITS>
struct underlying_dsp_rep {
};
template <>
struct underlying_dsp_rep<64> {
  typedef S64 raw_t;
};
template <>
struct underlying_dsp_rep<32> {
  typedef S32 raw_t;
};
template <>
struct underlying_dsp_rep<16> {
  typedef S16 raw_t;
};

template <int QA, int QB>
struct generic_boxed_dsp_type {
  typedef typename underlying_dsp_rep<QA + QB>::raw_t raw_t;

  raw_t raw;

  generic_boxed_dsp_type(raw_t value) : raw(value) {}

  generic_boxed_dsp_type() : raw(0) {}

  /*
    Behaves the same as typecasting in C: ie, do nothing
   */
  operator raw_t() { return raw; }

  /*
    Conversion operators from floating point in C++
  */
  operator double() { return as_double(); }

  double as_double() { return (double)raw * (1.0 / get_scale()); }

  static double get_scale() { return (double)(1LL << QB); }
};

/*
  Adding and subtracting should behave the same as in C
 */
template <int QA, int QB>
generic_boxed_dsp_type<QA, QB> operator+(generic_boxed_dsp_type<QA, QB> x, generic_boxed_dsp_type<QA, QB> y)
{
  return generic_boxed_dsp_type<QA, QB>(
      (typename generic_boxed_dsp_type<QA, QB>::raw_t)x + (typename generic_boxed_dsp_type<QA, QB>::raw_t)y);
}
template <int QA, int QB>
generic_boxed_dsp_type<QA, QB> operator-(generic_boxed_dsp_type<QA, QB> x, generic_boxed_dsp_type<QA, QB> y)
{
  return generic_boxed_dsp_type<QA, QB>(
      (typename generic_boxed_dsp_type<QA, QB>::raw_t)x - (typename generic_boxed_dsp_type<QA, QB>::raw_t)y);
}
template <int QA, int QB>
bool operator==(generic_boxed_dsp_type<QA, QB> x, generic_boxed_dsp_type<QA, QB> y)
{
  return ((typename generic_boxed_dsp_type<QA, QB>::raw_t)x == (typename generic_boxed_dsp_type<QA, QB>::raw_t)y);
}
template <int QA, int QB>
bool operator>(generic_boxed_dsp_type<QA, QB> x, generic_boxed_dsp_type<QA, QB> y)
{
  return ((typename generic_boxed_dsp_type<QA, QB>::raw_t)x > (typename generic_boxed_dsp_type<QA, QB>::raw_t)y);
}
template <int QA, int QB>
bool operator>=(generic_boxed_dsp_type<QA, QB> x, generic_boxed_dsp_type<QA, QB> y)
{
  return ((typename generic_boxed_dsp_type<QA, QB>::raw_t)x >= (typename generic_boxed_dsp_type<QA, QB>::raw_t)y);
}
template <int QA, int QB>
bool operator<(generic_boxed_dsp_type<QA, QB> x, generic_boxed_dsp_type<QA, QB> y)
{
  return ((typename generic_boxed_dsp_type<QA, QB>::raw_t)x < (typename generic_boxed_dsp_type<QA, QB>::raw_t)y);
}
template <int QA, int QB>
bool operator<=(generic_boxed_dsp_type<QA, QB> x, generic_boxed_dsp_type<QA, QB> y)
{
  return ((typename generic_boxed_dsp_type<QA, QB>::raw_t)x <= (typename generic_boxed_dsp_type<QA, QB>::raw_t)y);
}
template <int QA, int QB>
bool operator!=(generic_boxed_dsp_type<QA, QB> x, generic_boxed_dsp_type<QA, QB> y)
{
  return ((typename generic_boxed_dsp_type<QA, QB>::raw_t)x != (typename generic_boxed_dsp_type<QA, QB>::raw_t)y);
}

#ifdef notyet
template <int QA, int QB>
json to_json(generic_boxed_dsp_type<QA, QB> it)
{
  return to_json(it.as_double());
}
#endif

template <int QA, int QB>
ostream &operator<<(ostream &s, generic_boxed_dsp_type<QA, QB> x)
{
  s << x.as_double() << "D";
  return s;
}

template <int QA, int QB>
void marshall_to_stream(ostream &s, generic_boxed_dsp_type<QA, QB> x)
{
  marshall_to_stream(s, x.raw);
}
template <int QA, int QB>
void unmarshall_from_stream(istream &s, generic_boxed_dsp_type<QA, QB> &x)
{
  unmarshall_from_stream(s, x.raw);
}

template <int QA, int QB>
bool isnan(generic_boxed_dsp_type<QA, QB> x)
{
  return false;
}
template <int QA, int QB>
bool isallnan(generic_boxed_dsp_type<QA, QB> x)
{
  return false;
}
template <int QA, int QB>
bool isanynan(generic_boxed_dsp_type<QA, QB> x)
{
  return false;
}

typedef generic_boxed_dsp_type<8, 8> dsp88;
typedef generic_boxed_dsp_type<4, 12> dsp412;

typedef generic_boxed_dsp_type<16, 16> dsp1616;
typedef generic_boxed_dsp_type<8, 24> dsp824;
typedef generic_boxed_dsp_type<2, 30> dsp230;

typedef generic_boxed_dsp_type<40, 24> dsp4024;
typedef generic_boxed_dsp_type<32, 32> dsp3232;
typedef generic_boxed_dsp_type<24, 40> dsp2440;
typedef generic_boxed_dsp_type<18, 46> dsp1846;
typedef generic_boxed_dsp_type<16, 48> dsp1648;
typedef generic_boxed_dsp_type<10, 54> dsp1054;
typedef generic_boxed_dsp_type<4, 60> dsp460;

#else

typedef S16 dsp88;
typedef S16 dsp412;

typedef S32 dsp1616;
typedef S32 dsp824;
typedef S32 dsp230;

typedef S32 dsp32denorm;

typedef S64 dsp4024;
typedef S64 dsp3232;
typedef S64 dsp2440;
typedef S64 dsp1846;
typedef S64 dsp1648;
typedef S64 dsp1054;
typedef S64 dsp460;

#endif

/*
  Generate numbers. The argument should be floating point, like DSP824(0.755) or DSP824(M_PI)
 */

#define DSP88(X) ((dsp88)(S16)((float)(X) * (float)((S16)1 << 8)))
#define DSP412(X) ((dsp412)(S16)((float)(X) * (float)((S16)1 << 12)))

#define DSP1616(X) ((dsp1616)(S32)((float)(X) * (float)((S32)1 << 16)))
#define DSP824(X) ((dsp824)(S32)((float)(X) * (float)((S32)1 << 24)))
#define DSP230(X) ((dsp230)(S32)((float)(X) * (float)((S32)1 << 30)))
#define DSP1648(X) ((dsp1648)(S64)((double)(X) * (double)((S64)1 << 48)))

#if defined(__cplusplus)
extern "C" {
#endif

U32 convrnd_generator_u32();
U64 convrnd_generator_u64();

#if 0
dsp230 mul_dsp230_dsp230_dsp230(dsp230 a, dsp230 b);
dsp824 mul_dsp824_dsp824_dsp824(dsp824 a, dsp824 b);

dsp1616 mul_dsp230_dsp1616_dsp1616(dsp230 a, dsp1616 b);
dsp824 mul_dsp230_dsp824_dsp824(dsp230 a, dsp824 b);
dsp230 mul_dsp230_dsp1616_dsp230(dsp230 a, dsp1616 b);
dsp230 conv_dsp824_dsp230(dsp824 a);
dsp824 conv_dsp230_dsp824(dsp230 a);

dsp824 mulsat_dsp824_dsp824_dsp824(dsp824 a, dsp824 b);
dsp230 mulsat_dsp230_dsp230_dsp230(dsp230 a, dsp230 b);
#endif

#include "build.src/dsp_math_ops.h"

/*
  Basic operators. They're all named as follows:
     <operation>_< arg1 >_< arg2 >_<result>
  so for example mul_dsp824_dsp230_dsp824 multiplies an 8.24 by a 2.30 and returns an 8.24.
  Saturating multiplies are guaranteed not to overflow
*/

dsp824 inverse_dsp824_dsp824(dsp824 a);
dsp1616 inverse_dsp824_dsp1616(dsp824 a);
dsp1616 inverse_dsp1616_dsp1616(dsp1616 a);

dsp1616 div_dsp1616_dsp1616_dsp1616(dsp1616 a, dsp1616 b);
dsp824 div_dsp824_dsp824_dsp824(dsp824 a, dsp824 b);
dsp230 div_dsp824_dsp824_dsp230(dsp824 a, dsp824 b);
dsp230 div_dsp230_dsp824_dsp230(dsp230 a, dsp824 b);

dsp1616 div_dsp824_dsp824_dsp1616(dsp824 a, dsp824 b);
dsp1616 div_int_dsp824_dsp1616(S32 a, dsp824 b);

#ifdef DSPCORE_HAS_FP
dsp412 conv_float_dsp412(float x);
dsp412 conv_double_dsp412(double x);

dsp230 conv_float_dsp230(float x);
dsp230 conv_double_dsp230(double x);
dsp824 conv_float_dsp824(float x);
dsp824 conv_double_dsp824(double x);
dsp1616 conv_float_dsp1616(float x);
dsp1616 conv_double_dsp1616(double x);

dsp4024 conv_float_dsp4024(float x);
dsp4024 conv_double_dsp4024(double x);
dsp3232 conv_float_dsp3232(float x);
dsp3232 conv_double_dsp3232(double x);
dsp2440 conv_float_dsp2440(float x);
dsp2440 conv_double_dsp2440(double x);
dsp1846 conv_float_dsp1846(float x);
dsp1846 conv_double_dsp1846(double x);
dsp1648 conv_float_dsp1648(float x);
dsp1648 conv_double_dsp1648(double x);
dsp1054 conv_float_dsp1054(float x);
dsp1054 conv_double_dsp1054(double x);
dsp460 conv_float_dsp460(float x);
dsp460 conv_double_dsp460(double x);

float conv_dsp412_float(dsp412 x);
double conv_dsp412_double(dsp412 x);

float conv_dsp230_float(dsp230 x);
double conv_dsp230_double(dsp230 x);
float conv_dsp824_float(dsp824 x);
double conv_dsp824_double(dsp824 x);
float conv_dsp1616_float(dsp1616 x);
double conv_dsp1616_double(dsp1616 x);

float conv_dsp4024_float(dsp4024 x);
double conv_dsp4024_double(dsp4024 x);
float conv_dsp3232_float(dsp3232 x);
double conv_dsp3232_double(dsp3232 x);
float conv_dsp2440_float(dsp2440 x);
double conv_dsp2440_double(dsp2440 x);
float conv_dsp1846_float(dsp1846 x);
double conv_dsp1846_double(dsp1846 x);
float conv_dsp1648_float(dsp1648 x);
double conv_dsp1648_double(dsp1648 x);
float conv_dsp1054_float(dsp1054 x);
double conv_dsp1054_double(dsp1054 x);
float conv_dsp460_float(dsp460 x);
double conv_dsp460_double(dsp460 x);

#endif

/*
  Additive stuff is all simple and doesn't really depend on the representation
*/
static inline S32 dspmax(S32 a, S32 b)
{
  if (a >= b) {
    return a;
  }
  else {
    return b;
  }
}

static inline S32 dspmin(S32 a, S32 b)
{
  if (a <= b) {
    return a;
  }
  else {
    return b;
  }
}

static inline S32 dsplim(S32 x, S32 lo, S32 hi)
{
  if (x > hi) return hi;
  if (x < lo) return lo;
  return x;
}

/*
  Other functions, see dspcore.c
*/
dsp230 invsqrtsat_dsp230_dsp230(dsp230 x);
void sincos_smallangle_dsp230_dsp230_dsp230(dsp230 x, dsp230 *sin_r, dsp230 *cos_r);
void sincos_dsp230_dsp230_dsp230(dsp230 theta, dsp230 *sin_r, dsp230 *cos_r);
void sincos_dsp824_dsp230_dsp230(dsp824 theta, dsp230 *sin_r, dsp230 *cos_r);
void sincos_dsp824_dsp824_dsp824(dsp824 theta, dsp824 *sin_r, dsp824 *cos_r);
dsp230 atan_dsp230_dsp230(dsp230 q);
dsp824 atan2_dsp824_dsp824_dsp824(dsp824 y, dsp824 x);

void test_dspcore();

#if defined(__cplusplus)
} // extern "C"
#endif
