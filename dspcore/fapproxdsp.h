#pragma once

#include "./dspcore.h"

typedef struct fapprox_poly_0_3dsp824 {
  dsp824 c0, c1, c2, c3;
} fapprox_poly_0_3dsp824;

dsp824 fapprox_poly_0_3dsp824_calc(fapprox_poly_0_3dsp824 *it, dsp824 value);
dsp824 fapprox_poly_0_3dsp824_calc_deriv(fapprox_poly_0_3dsp824 *it, dsp824 value);
