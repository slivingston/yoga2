'use strict';
const _ = require('lodash');
const fs = require('fs');

function mkBiquad(w0, q)
{
  let cosw0 = Math.cos(w0);
  let sinw0 = Math.sin(w0);
  let alpha = sinw0 / (2 * q);

  let a0 = 1 + alpha;

  let as = [(1 + alpha) / a0,
            (-2 * cosw0) / a0,
            (1 - alpha) / a0];
  let bs = [((1 - cosw0) / 2) / a0,
            (1 - cosw0) / a0,
            ((1 - cosw0) / 2) / a0];
  return {a: as, b: bs};
}

function emitSmoother2(f, coeffs) {
  function fmtDsp(v) {
    return 'DSP824(' + v.toString() + ') /* ' + Math.round(v*(1<<24)).toString() + ' */ ';
  }
  f('c->b0 = ' + fmtDsp(coeffs.b[2]) + ';');
  f('c->b1 = ' + fmtDsp(coeffs.b[1]) + ';');
  f('c->b2 = ' + fmtDsp(coeffs.b[0]) + ';');
  f('c->a0 = ' + fmtDsp(coeffs.a[2]) + ';');
  f('c->a1 = ' + fmtDsp(coeffs.a[1]) + ';');
}

function emitButtFilters(f, tc) {
  let coeffs = mkBiquad(1.0/tc, Math.sqrt(2)/2);
  f('static inline void smoother2_dsp824_setup_butt_' + tc.toString().replace(/\./, '_') + '(smoother2_coeffs_dsp824 *c)');
  f('{');
  emitSmoother2(f, coeffs);
  f('}');
}

function emitButtFiltersAll(f) {
  _.each([0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19,
          0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
          1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,
          2,2.5,3,4,5,6,7,8,9,
          10,11,12,13,14,15,16,17,18,19,
          20,25,30,40,50,60,70,80,90,
          100,110,120,130,140,150,160,170,180,190,
          200], (tc) => {
            emitButtFilters(f, tc);
          });
}

function main() {
  let lines = [];
  let f = (l) => {
    lines.push(l);
  };
  emitButtFiltersAll(f);
  fs.writeFileSync('build.src/smoother_coeffs.h', lines.join('\n'));
}

if (require.main === module) {
  main();
}
