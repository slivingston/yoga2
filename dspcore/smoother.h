#pragma once
#include "common/std_headers.h"

struct Smoother2Coeffs {
  R b0;
  R b1;
  R b2;
  R a0;
  R a1;
};

struct Smoother2State {
  R x0;
  R x1;
  R x2;
  R y0;
  R y1;
  R y2;
};

double smoother2Run(Smoother2Coeffs &c, Smoother2State &s, double v);
void smoother2Clear(Smoother2State &s, double v);
double smoother2Deriv(Smoother2State &s);
Smoother2Coeffs smoother2SetupBiquad(double w0, double q);
