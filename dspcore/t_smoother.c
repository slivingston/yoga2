#include "./dspcore.h"
#include "./smootherdsp.h"

double DSP824F(dsp824 x) { return (1.0/16777216.0) * (double)x; }


void printBiquad(double w0, double q)
{
  double cosw0 = cos(w0);
  double sinw0 = sin(w0);

  double alpha = sinw0 / (2.0 * q);
  eprintf("w0=%0.9f cosw0=%0.9f sinw0=%0.9f, alpha=%0.9f ***\n",
          w0, cosw0, sinw0, alpha);

  double a0 = 1 + alpha;

  double a1 = (-2 * cosw0) / a0;
  double a2 = (1 - alpha) / a0;
  double b0 = ((1 - cosw0) / 2) / a0;
  double b1 = (1 - cosw0) / a0;
  double b2 = ((1 - cosw0) / 2) / a0;

  eprintf("w0=%0.9f b=[%+0.9f %+0.9f %+0.9f] a=[%+0.9f %+0.9f %+0.9f] ***\n",
          w0,
          b0, b1, b2,
          a2, a1, 1.0);
}


int main()
{
  smoother2_coeffs_dsp824 coeffs;
  for (double w0=0.01; w0<1; w0 *= 1.1) {

    printBiquad(w0, 0.7071067811865476);
    smoother2_dsp824_setup_biquad(&coeffs, DSP824(w0), DSP824(0.7071067811865476));
    eprintf("w0=%0.9f b=[%+0.9f %+0.9f %+0.9f] a=[%+0.9f %+0.9f %+0.9f]\n",
            w0,
            DSP824F(coeffs.b0), DSP824F(coeffs.b1), DSP824F(coeffs.b2),
            DSP824F(coeffs.a0), DSP824F(coeffs.a1), 1.0);
    eprintf("\n");
  }

}
