#include "common/std_headers.h"
#include "./vec3dsp.h"
#include "../embedded/embedded_timing.h"
#include "../embedded/embedded_debug.h"


mat3dsp230 const mat3dsp230_identity = {
  DSP230(1.0), 0, 0,
  0, DSP230(1.0), 0,
  0, 0, DSP230(1.0),
};

vec3dsp824 const vec3dsp824_zero = { 0, 0, 0};

ea3dsp824 const ea3dsp824_zero = { 0, 0, 0};
ea3dsp230 const ea3dsp230_zero = { 0, 0, 0};

// ----------------------------------------------------------------------

/*
  Generate rotations around principal axes. Works just like anycore/vec3 implementation
 */

mat3dsp230 rotation_xaxis_dsp230_mat3dsp230(dsp230 theta)
{
  dsp230 sa, ca;
  sincos_dsp230_dsp230_dsp230(theta, &sa, &ca);

  mat3dsp230 ret;
  ret.xx = DSP230(1.0);       ret.xy = 0;                   ret.xz = 0;
  ret.yx = 0;                 ret.yy = +ca;                 ret.yz = -sa;
  ret.zx = 0;                 ret.zy = +sa;                 ret.zz = +ca;

  return ret;
}

mat3dsp230 rotation_xaxis_dsp824_mat3dsp230(dsp824 theta)
{
  dsp230 sa, ca;
  sincos_dsp824_dsp230_dsp230(theta, &sa, &ca);

  mat3dsp230 ret;
  ret.xx = DSP230(1.0);       ret.xy = 0;                   ret.xz = 0;
  ret.yx = 0;                 ret.yy = +ca;                 ret.yz = -sa;
  ret.zx = 0;                 ret.zy = +sa;                 ret.zz = +ca;

  return ret;
}

mat3dsp230 rotation_yaxis_dsp230_mat3dsp230(dsp230 theta)
{
  dsp230 sa, ca;
  sincos_dsp230_dsp230_dsp230(theta, &sa, &ca);

  mat3dsp230 ret;
  ret.xx = +ca;                 ret.xy =  0;                ret.xz = +sa;
  ret.yx = 0;                   ret.yy = DSP230(1.0);       ret.yz = 0;
  ret.zx = -sa;                 ret.zy =   0;               ret.zz = +ca;

  return ret;
}

mat3dsp230 rotation_yaxis_dsp824_mat3dsp230(dsp824 theta)
{
  dsp230 sa, ca;
  sincos_dsp824_dsp230_dsp230(theta, &sa, &ca);

  mat3dsp230 ret;
  ret.xx = +ca;                 ret.xy =  0;                ret.xz = +sa;
  ret.yx = 0;                   ret.yy = DSP230(1.0);       ret.yz = 0;
  ret.zx = -sa;                 ret.zy =   0;               ret.zz = +ca;

  return ret;
}

mat3dsp230 rotation_zaxis_dsp230_mat3dsp230(dsp230 theta)
{
  dsp230 sa, ca;
  sincos_dsp230_dsp230_dsp230(theta, &sa, &ca);

  mat3dsp230 ret;
  ret.xx = +ca;                 ret.xy = -sa;                 ret.xz = 0;
  ret.yx = +sa;                 ret.yy = +ca;                 ret.yz = 0;
  ret.zx = 0;                   ret.zy =   0;                 ret.zz = DSP230(1.0);

  return ret;
}

mat3dsp230 rotation_zaxis_dsp824_mat3dsp230(dsp824 theta)
{
  dsp230 sa, ca;
  sincos_dsp824_dsp230_dsp230(theta, &sa, &ca);

  mat3dsp230 ret;
  ret.xx = +ca;                 ret.xy = -sa;                 ret.xz = 0;
  ret.yx = +sa;                 ret.yy = +ca;                 ret.yz = 0;
  ret.zx = 0;                   ret.zy =   0;                 ret.zz = DSP230(1.0);

  return ret;
}

// ----------------------------------------------------------------------

vec3dsp824 rotation_velocity_vec3dsp824_ea3dsp824_vec3dsp824(vec3dsp824 pos, ea3dsp824 rate)
{
  vec3dsp824 ret = {
    mul_dsp824_dsp824_dsp824(pos.z, rate.roll)  - mul_dsp824_dsp824_dsp824(pos.y, rate.yaw),
    mul_dsp824_dsp824_dsp824(pos.x, rate.yaw)   - mul_dsp824_dsp824_dsp824(pos.z, rate.pitch),
    mul_dsp824_dsp824_dsp824(pos.y, rate.pitch) - mul_dsp824_dsp824_dsp824(pos.x, rate.roll),
  };
  return ret;
}

vec3dsp824 pitch_velocity_vec3dsp824_dsp824_vec3dsp824(vec3dsp824 pos, dsp824 pitch_rate)
{
  vec3dsp824 ret = {
    0,
    - mul_dsp824_dsp824_dsp824(pos.z, pitch_rate),
    mul_dsp824_dsp824_dsp824(pos.y, pitch_rate)
  };
  return ret;
}

vec3dsp824 roll_velocity_vec3dsp824_dsp824_vec3dsp824(vec3dsp824 pos, dsp824 roll_rate)
{
  vec3dsp824 ret = {
    mul_dsp824_dsp824_dsp824(pos.z, roll_rate),
    0,
    - mul_dsp824_dsp824_dsp824(pos.x, roll_rate),
  };
  return ret;
}

vec3dsp824 yaw_velocity_vec3dsp824_dsp824_vec3dsp824(vec3dsp824 pos, dsp824 yaw_rate)
{
  vec3dsp824 ret = {
    - mul_dsp824_dsp824_dsp824(pos.y, yaw_rate),
    mul_dsp824_dsp824_dsp824(pos.x, yaw_rate),
    0,
  };
  return ret;
}

// ----------------------------------------------------------------------

mat3dsp230 transposed_mat3dsp230_mat3dsp230(mat3dsp230 u)
{
  mat3dsp230 ret;
  ret.xx = u.xx;      ret.xy = u.yx;      ret.xz = u.zx;
  ret.yx = u.xy;      ret.yy = u.yy;      ret.yz = u.zy;
  ret.zx = u.xz;      ret.zy = u.yz;      ret.zz = u.zz;
  return ret;
}

// ----------------------------------------------------------------------

// mat3 * mat3
mat3dsp230 matmul_mat3dsp230_mat3dsp230_mat3dsp230(mat3dsp230 u, mat3dsp230 v)
{
  mat3dsp230 ret;

  ret.xx = mul_dsp230_dsp230_dsp230(u.xx,v.xx) + mul_dsp230_dsp230_dsp230(u.xy,v.yx) + mul_dsp230_dsp230_dsp230(u.xz,v.zx);
  ret.xy = mul_dsp230_dsp230_dsp230(u.xx,v.xy) + mul_dsp230_dsp230_dsp230(u.xy,v.yy) + mul_dsp230_dsp230_dsp230(u.xz,v.zy);
  ret.xz = mul_dsp230_dsp230_dsp230(u.xx,v.xz) + mul_dsp230_dsp230_dsp230(u.xy,v.yz) + mul_dsp230_dsp230_dsp230(u.xz,v.zz);
  ret.yx = mul_dsp230_dsp230_dsp230(u.yx,v.xx) + mul_dsp230_dsp230_dsp230(u.yy,v.yx) + mul_dsp230_dsp230_dsp230(u.yz,v.zx);
  ret.yy = mul_dsp230_dsp230_dsp230(u.yx,v.xy) + mul_dsp230_dsp230_dsp230(u.yy,v.yy) + mul_dsp230_dsp230_dsp230(u.yz,v.zy);
  ret.yz = mul_dsp230_dsp230_dsp230(u.yx,v.xz) + mul_dsp230_dsp230_dsp230(u.yy,v.yz) + mul_dsp230_dsp230_dsp230(u.yz,v.zz);
  ret.zx = mul_dsp230_dsp230_dsp230(u.zx,v.xx) + mul_dsp230_dsp230_dsp230(u.zy,v.yx) + mul_dsp230_dsp230_dsp230(u.zz,v.zx);
  ret.zy = mul_dsp230_dsp230_dsp230(u.zx,v.xy) + mul_dsp230_dsp230_dsp230(u.zy,v.yy) + mul_dsp230_dsp230_dsp230(u.zz,v.zy);
  ret.zz = mul_dsp230_dsp230_dsp230(u.zx,v.xz) + mul_dsp230_dsp230_dsp230(u.zy,v.yz) + mul_dsp230_dsp230_dsp230(u.zz,v.zz);

  return ret;
}

// mat3 * vec3
vec3dsp230 matmul_mat3dsp230_vec3dsp230_vec3dsp230(mat3dsp230 m, vec3dsp230 v)
{
  vec3dsp230 ret;

  ret.x = mul_dsp230_dsp230_dsp230(m.xx, v.x) + mul_dsp230_dsp230_dsp230(m.xy, v.y) + mul_dsp230_dsp230_dsp230(m.xz, v.z);
  ret.y = mul_dsp230_dsp230_dsp230(m.yx, v.x) + mul_dsp230_dsp230_dsp230(m.yy, v.y) + mul_dsp230_dsp230_dsp230(m.yz, v.z);
  ret.z = mul_dsp230_dsp230_dsp230(m.zx, v.x) + mul_dsp230_dsp230_dsp230(m.zy, v.y) + mul_dsp230_dsp230_dsp230(m.zz, v.z);

  return ret;
}
vec3dsp824 matmul_mat3dsp230_vec3dsp824_vec3dsp824(mat3dsp230 m, vec3dsp824 v)
{
  vec3dsp824 ret;

  ret.x = mul_dsp230_dsp824_dsp824(m.xx, v.x) + mul_dsp230_dsp824_dsp824(m.xy, v.y) + mul_dsp230_dsp824_dsp824(m.xz, v.z);
  ret.y = mul_dsp230_dsp824_dsp824(m.yx, v.x) + mul_dsp230_dsp824_dsp824(m.yy, v.y) + mul_dsp230_dsp824_dsp824(m.yz, v.z);
  ret.z = mul_dsp230_dsp824_dsp824(m.zx, v.x) + mul_dsp230_dsp824_dsp824(m.zy, v.y) + mul_dsp230_dsp824_dsp824(m.zz, v.z);

  return ret;
}


/*
  Generate a rotation matrix by first rotating around z, then x, then y.
  Works just like anycore/vec3 ea3::to_mat
 */
mat3dsp230 eatomat_dsp230_dsp230_dsp230_mat3dsp230(dsp230 rotx, dsp230 roty, dsp230 rotz)
{
  return matmul_mat3dsp230_mat3dsp230_mat3dsp230(rotation_zaxis_dsp230_mat3dsp230(rotz),
                                                 matmul_mat3dsp230_mat3dsp230_mat3dsp230(
                                                                                         rotation_xaxis_dsp230_mat3dsp230(rotx),
                                                                                         rotation_yaxis_dsp230_mat3dsp230(roty)));
}

// ----------------------------------------------------------------------

vec3dsp824 add_vec3dsp824_vec3dsp824_vec3dsp824(vec3dsp824 u, vec3dsp824 v)
{
  vec3dsp824 ret;

  ret.x = u.x + v.x;
  ret.y = u.y + v.y;
  ret.z = u.z + v.z;

  return ret;
}

// ----------------------------------------------------------------------

vec3dsp230 cross_vec3dsp230_vec3dsp230_vec3dsp230(vec3dsp230 u, vec3dsp230 v)
{
  vec3dsp230 ret = {
    mul_dsp230_dsp230_dsp230(u.y, v.z) - mul_dsp230_dsp230_dsp230(u.z, v.y),
    mul_dsp230_dsp230_dsp230(u.z, v.x) - mul_dsp230_dsp230_dsp230(u.x, v.z),
    mul_dsp230_dsp230_dsp230(u.x, v.y) - mul_dsp230_dsp230_dsp230(u.y, v.x),
  };
  return ret;
}

dsp230 normsq_vec3dsp230_dsp230(vec3dsp230 u)
{
  return (mul_dsp230_dsp230_dsp230(u.x, u.x) +
          mul_dsp230_dsp230_dsp230(u.y, u.y) +
          mul_dsp230_dsp230_dsp230(u.z, u.z));
}

#ifdef NOTYET
dsp230 norm_vec3dsp230_dsp230(vec3dsp230 u)
{
  return sqrt_dsp230_dsp230(normsq_vec3dsp230_dsp230(u));
}
#endif

vec3dsp230 normalized_vec3dsp230_vec3dsp230(vec3dsp230 u)
{
  dsp230 n = normsq_vec3dsp230_dsp230(u);
  dsp230 factor = invsqrtsat_dsp230_dsp230(n);

  vec3dsp230 ret = {
    mul_dsp230_dsp230_dsp230(u.x, factor),
    mul_dsp230_dsp230_dsp230(u.y, factor),
    mul_dsp230_dsp230_dsp230(u.z, factor),
  };
  return ret;
}

mat3dsp230 orthonormalized_mat3dsp230_mat3dsp230(mat3dsp230 u)
{
  vec3dsp230 vy = {u.xy, u.yy, u.zy};
  vec3dsp230 vz = {u.xz, u.yz, u.zz};

  vec3dsp230 vxfix = normalized_vec3dsp230_vec3dsp230(cross_vec3dsp230_vec3dsp230_vec3dsp230(vy, vz));
  vec3dsp230 vyfix = normalized_vec3dsp230_vec3dsp230(cross_vec3dsp230_vec3dsp230_vec3dsp230(vz, vxfix));
  vec3dsp230 vzfix = normalized_vec3dsp230_vec3dsp230(cross_vec3dsp230_vec3dsp230_vec3dsp230(vxfix, vyfix));

  mat3dsp230 ret = {
    vxfix.x,   vyfix.x,     vzfix.x,
    vxfix.y,   vyfix.y,     vzfix.y,
    vxfix.z,   vyfix.z,     vzfix.z,
  };

  return ret;
}


// ======================================================================

void test_vec3dsp()
{

  for (dsp230 x=DSP230(+0.6); x<DSP230(+1.4); x += DSP230(0.125)) {
    debug_printf("1/sqrt(%{dsp230}} = %{dsp230}", x, invsqrtsat_dsp230_dsp230(x));
  }


  for (dsp230 theta=DSP230(-1.625); theta<DSP230(+1.625); theta += DSP230(0.125)) {
    dsp230 sa, ca;
    sincos_dsp230_dsp230_dsp230(theta, &sa, &ca);
    debug_printf("theta=%{dsp230} cos=%{dsp230} sin=%{dsp230}", theta, ca, sa);
  }

  {
    mat3dsp230 rot = rotation_xaxis_dsp230_mat3dsp230(DSP230(0.25));
    debug_printf("rotx(0.25) = %+8.6{mat3dsp230}", &rot);
  }

  {
    mat3dsp230 rot = eatomat_dsp230_dsp230_dsp230_mat3dsp230(DSP230(0.25), DSP230(0.25), DSP230(0.25));
    debug_printf("rot(0.25, 0.25, 0.25) = %+8.6{mat3dsp230}", &rot);
    rot = orthonormalized_mat3dsp230_mat3dsp230(rot);
    debug_printf("orthonormalized = %+8.6{mat3dsp230}", &rot);
  }

  {
    dsp230 sa, ca;
    U32 t0 = get_ticks();
    sincos_smallangle_dsp230_dsp230_dsp230(DSP230(0.25), &sa, &ca);
    U32 t1 = get_ticks();

    debug_printf("Single sincos_smallangle took %d cycles", t1-t0);
    debug_printf("Got %{dsp230}, %{dsp230}", sa, ca);
  }
  {
    dsp230 sa, ca;
    U32 t0 = get_ticks();
    sincos_dsp230_dsp230_dsp230(DSP230(0.25), &sa, &ca);
    U32 t1 = get_ticks();

    debug_printf("Single sincos took %d cycles", t1-t0);
    debug_printf("Got %{dsp230}, %{dsp230}", sa, ca);
  }

  {
    U32 t0 = get_ticks();
    mat3dsp230 rot = rotation_xaxis_dsp230_mat3dsp230(DSP230(0.25));
    U32 t1 = get_ticks();

    debug_printf("Single X rotation took %d cycles", t1-t0);
    debug_printf("Got %{mat3dsp230}", &rot);
  }
  {
    mat3dsp230 rot1 = rotation_xaxis_dsp230_mat3dsp230(DSP230(0.25));
    mat3dsp230 rot2 = rotation_xaxis_dsp230_mat3dsp230(DSP230(0.25));

    U32 t0 = get_ticks();
    mat3dsp230 rot = matmul_mat3dsp230_mat3dsp230_mat3dsp230(rot1, rot2);
    U32 t1 = get_ticks();

    debug_printf("Single matmul took %d cycles", t1-t0);
    debug_printf("Got %{mat3dsp230}", &rot);
  }


  {
    U32 t0 = get_ticks();
    mat3dsp230 rot = mat3dsp230_identity;
    for (int i=0; i<1000; i++) {
      rot = matmul_mat3dsp230_mat3dsp230_mat3dsp230(rot,
                                                    eatomat_dsp230_dsp230_dsp230_mat3dsp230(DSP230(1.0/256.0), DSP230(1.0/256.0), DSP230(1.0/256.0)));
    }
    U32 t1 = get_ticks();
    debug_printf("1000 full xyz rotations took %d cycles", t1-t0);
    debug_printf("Got %{mat3dsp230}", &rot);
    rot = orthonormalized_mat3dsp230_mat3dsp230(rot);
    debug_printf("orthonormalized = %{mat3dsp230}", &rot);
  }

}
