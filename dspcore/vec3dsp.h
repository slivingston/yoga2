#pragma once

#include "./dspcore.h"

typedef struct vec2dsp824 {
  dsp824 x, y;
} vec2dsp824;

typedef struct vec3dsp824 {
  dsp824 x, y, z;
} vec3dsp824;
typedef struct vec3dsp230 {
  dsp230 x, y, z;
} vec3dsp230;

typedef struct vec4dsp824 {
  dsp824 x, y, z, a;
} vec4dsp824;

typedef struct ea3dsp824 {
  dsp824 pitch, roll, yaw;
} ea3dsp824;
typedef struct ea3dsp230 {
  dsp230 pitch, roll, yaw;
} ea3dsp230;

typedef struct mat2dsp824 {
  dsp824 xx, xy, yx, yy;
} mat2dsp824;

typedef struct mat3dsp824 {
  dsp824 xx, xy, xz, yx, yy, yz, zx, zy, zz;
} mat3dsp824;
typedef struct mat3dsp230 {
  dsp230 xx, xy, xz, yx, yy, yz, zx, zy, zz;
} mat3dsp230;

extern mat3dsp230 const mat3dsp230_identity;
extern vec3dsp824 const vec3dsp824_zero;
extern ea3dsp824 const ea3dsp824_zero;
extern ea3dsp230 const ea3dsp230_zero;

mat3dsp230
mat3dsp230_cons(dsp230 xx, dsp230 xy, dsp230 xz, dsp230 yx, dsp230 yy, dsp230 yz, dsp230 zx, dsp230 zy, dsp230 zz);

mat3dsp230 rotation_xaxis_dsp230_mat3dsp230(dsp230 angle);
mat3dsp230 rotation_xaxis_dsp824_mat3dsp230(dsp824 theta);
mat3dsp230 rotation_yaxis_dsp230_mat3dsp230(dsp230 angle);
mat3dsp230 rotation_yaxis_dsp824_mat3dsp230(dsp824 theta);
mat3dsp230 rotation_zaxis_dsp230_mat3dsp230(dsp230 angle);
mat3dsp230 rotation_zaxis_dsp824_mat3dsp230(dsp824 theta);

vec3dsp824 rotation_velocity_vec3dsp824_ea3dsp824_vec3dsp824(vec3dsp824 pos, ea3dsp824 rate);
vec3dsp824 pitch_velocity_vec3dsp824_dsp824_vec3dsp824(vec3dsp824 pos, dsp824 pitch_rate);
vec3dsp824 roll_velocity_vec3dsp824_dsp824_vec3dsp824(vec3dsp824 pos, dsp824 roll_rate);
vec3dsp824 yaw_velocity_vec3dsp824_dsp824_vec3dsp824(vec3dsp824 pos, dsp824 yaw_rate);

mat3dsp230 matmul_mat3dsp230_mat3dsp230_mat3dsp230(mat3dsp230 u, mat3dsp230 v);
vec3dsp230 matmul_mat3dsp230_vec3dsp230_vec3dsp230(mat3dsp230 m, vec3dsp230 v);
vec3dsp824 matmul_mat3dsp230_vec3dsp824_vec3dsp824(mat3dsp230 m, vec3dsp824 v);
mat3dsp230 eatomat_dsp230_dsp230_dsp230_mat3dsp230(dsp230 rotx, dsp230 roty, dsp230 rotz);

vec3dsp824 add_vec3dsp824_vec3dsp824_vec3dsp824(vec3dsp824 u, vec3dsp824 v);

mat3dsp230 transposed_mat3dsp230_mat3dsp230(mat3dsp230 u);
mat3dsp230 orthonormalized_mat3dsp230_mat3dsp230(mat3dsp230 u);

void test_vec3dsp();
