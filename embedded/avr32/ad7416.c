#include "common/std_headers.h"
#include "hwdefs.h"
#include "embedded/embedded_debug.h"
#include "embedded/embedded_timing.h"
#include "yogstudio/embedded/avr32/ad7416.h"

#define AD7416_REGNO_TEMPERATURE 0
#define AD7416_REGNO_CONFIG 1
#define AD7416_REGNO_THYST 2
#define AD7416_REGNO_TOTI 3
#define AD7416_REGNO_ADC 4
#define AD7416_REGNO_CONFIG2 5

// Units are quarter degrees starting around 0, in the 10 high bits of a 16-bit register
U16 ad7416_celsius_to_regval(dsp824 temperature)
{
  return (U16)(S16)(temperature >> 16);
}
dsp824 ad7416_regval_to_celsius(U16 regval)
{
  return (dsp824)((S32)regval << 16);
}

// ----------------------------------------------------------------------

void ad7416_start_transaction(volatile twibus_transaction_state *st, int chipno, int regaddr)
{
  // The ad7416 7-bit addresses start with 1001, plus 3 bits of address
  int device_addr = 0x48 | (chipno&7);
  st->regaddr = regaddr;
  st->regaddrlen = 1;
  twibus_transaction_start(st, device_addr);
}

void ad7416_start_transaction_wr_reg8(volatile twibus_transaction_state *st, int chipno, int regaddr, U8 regval)
{
  st->tx_buf[0] = regval;
  st->tx_len = 1;
  st->rx_len = 0;
  ad7416_start_transaction(st, chipno, regaddr);
}

void ad7416_start_transaction_wr_reg16(volatile twibus_transaction_state *st, int chipno, int regaddr, U16 regval)
{
  st->tx_buf[0] = regval>>8;
  st->tx_buf[1] = regval>>0;
  st->tx_len = 2;
  st->rx_len = 0;
  ad7416_start_transaction(st, chipno, regaddr);
}

void ad7416_start_transaction_rd_reg8(volatile twibus_transaction_state *st, int chipno, int regaddr)
{
  st->tx_len = 0;
  st->rx_len = 1;
  ad7416_start_transaction(st, chipno, regaddr);
}

U8 ad7416_collect_transaction_rd_reg8(volatile twibus_transaction_state *st)
{
  return st->rx_buf[0];
}

void ad7416_start_transaction_rd_reg16(volatile twibus_transaction_state *st, int chipno, U8 regaddr)
{
  st->tx_len = 0;
  st->rx_len = 2;
  ad7416_start_transaction(st, chipno, regaddr);
}

U16 ad7416_collect_transaction_rd_reg16(volatile twibus_transaction_state *st)
{
  return ((U16)st->rx_buf[0] << 8) | ((U16)st->rx_buf[1] << 0);
}

int ad7416_engine_run(ad7416_engine *it, volatile twibus_transaction_state *st)
{
  int tmode = st->mode;
  int tmode_idle = (tmode == TWIBUS_MODE_IDLE ||
                    tmode == TWIBUS_MODE_RX_COMPLETE ||
                    tmode == TWIBUS_MODE_TX_COMPLETE);

  switch (it->state) {
  case 0:
    if (tmode_idle) {
      if (0) debug_printf("ad7416.%d: start write config", it->chipno);
      ad7416_start_transaction_wr_reg8(st, it->chipno, AD7416_REGNO_CONFIG, 0x00); // write configuration register to 0
      it->state = 1;
      return 1;
    }
    return 0;

  case 1:
    if (tmode_idle) {
      // Set 10 degrees hysteresis
      if (0) debug_printf("ad7416.%d: start write hyst", it->chipno);
      ad7416_start_transaction_wr_reg16(st, it->chipno, AD7416_REGNO_THYST, ad7416_celsius_to_regval(DSP1616(+10.0)));
      it->state = 2;
      return 1;
    }
    return 0;

  case 2:
    if (tmode_idle) {
      // Set +80C start point.
      if (0) debug_printf("ad7416.%d: start write toti", it->chipno);
      ad7416_start_transaction_wr_reg16(st, it->chipno, AD7416_REGNO_TOTI, ad7416_celsius_to_regval(DSP1616(+80.0)));
      it->state = 3;
      return 1;
    }
    return 0;

  case 3:
    if (tmode_idle) {
      if (0) debug_printf("ad7416.%d: start read temperature", it->chipno);
      ad7416_start_transaction_rd_reg16(st, it->chipno, AD7416_REGNO_TEMPERATURE);
      it->state = 4;
    }
    return 0;

  case 4:
    if (tmode == TWIBUS_MODE_RX_COMPLETE) {
      it->temperature = ad7416_regval_to_celsius(ad7416_collect_transaction_rd_reg16(st));
      if (0) debug_printf("ad7416.%d: got temperature %{dsp824}", it->chipno, it->temperature);
      it->state = 3;
      return 1;
    }
    else if (tmode_idle) {
      if (0) log_printf("ad7416 transaction failed (chipno=%d, tmode=%d)\n", it->chipno, tmode);
      it->state = 3;
      return 1;
    }
    return 0;

  default:
    it->state = 0;
    return 1;
  }
}

void ad7416_engine_create(ad7416_engine *it, twibus *bus, int chipno)
{
  it->super.run_func = (int (*)(twibus_engine *, volatile twibus_transaction_state *st)) ad7416_engine_run;
  it->chipno = chipno;
  it->state = 0;
  it->temperature = DSP824(-99.0); // implausible
  twibus_add_engine(bus, &it->super);
}
