#include "common/std_headers.h"
#include "hwdefs.h"
#include "embedded/embedded_debug.h"
#include "./avrhw.h"
#include "INTC/intc.h"
#include "FLASHC/flashc.h"
#include "WDT/wdt.h"
#include "PM/pm.h"


void avrhw_setup()
{
  volatile avr32_pm_t *pm = &AVR32_PM;

  /*
    The CPU comes out of reset running on the internal 115 kHz RC oscillator
  */

#if 0

  pm_freq_param_t cparam;
  cparam.osc0_startup = OSC0_STARTUP;
  cparam.cpu_f = FCPU;
  cparam.osc0_f = FOSC0;
  cparam.pba_f = FPBA;

  pm_configure_clocks(&cparam);

#elif (FOSC0==12000000) && (FCPU==48000000)

  /*
    Use CLK0 and PLL0 for the CPU, PBA and PBB
    Multiply 12 MHz crystal by 8 to 196 MHz, then divide by 2 for 48.
    Set low-frequency (<160 MHz) mode
    set dividers:
       PBA = PBB = CLK / 2
       CPU = HSB = CLK / 1
    Flash memory with one wait state (needed above 33 MHz)
  */
  pm_enable_osc0_crystal(pm, FOSC);
  pm_enable_clk0(pm, OSC0_STARTUP);
  pm_pll_setup(pm, 0, 7, 1, 0, 16);  // PM, pll, mul, div, osc, lockcount
  pm_pll_set_option(pm, 0, 0, 1, 0);  // PM, pll, pll_freq, pll_div2, pll_wbwdisable
  pm_pll_enable(pm, 0);
  pm_wait_for_pll0_locked(pm);
#if FPBA == FCPU/2
  pm_cksel(pm, 1, 0, 1, 0, 0, 0); // PM, PBAdiven, PBAdiv, PBBdiven, PBBdiv, CPUdiven, CPUdiv
#elif FPBA == FCPU
  pm_cksel(pm, 1, 0, 1, 0, 0, 0); // PM, PBAdiven, PBAdiv, PBBdiven, PBBdiv, CPUdiven, CPUdiv
#else
#error "Unsupported FPBA"
#endif
  flashc_set_wait_state(1);
  pm_switch_to_clock(pm, AVR32_PM_MCCTRL_MCSEL_PLL0);

#elif (FOSC0==12000000) && (FCPU==66000000)

  /*
    Use CLK0 and PLL0 for the CPU, PBA & PBB
    Multiply 12 MHz crystal by 11 to 132 MHz and divide by 2
    Set low-frequency (<160 MHz) mode
    set dividers:
       CPU = HSB = CLK
       PBA = PBB = CLK / {1,2,4}
    Flash memory with one wait state (needed above 33 MHz)
  */

  pm_enable_osc0_crystal(pm, FOSC0);
  pm_enable_clk0(pm, OSC0_STARTUP);
  pm_pll_setup(pm, 0,  10, 1, 0, 16); // PM, pll, mul, div, osc, lockcount
  pm_pll_set_option(pm, 0, 1, 1, 0); // PM, pll, pll_freq, pll_div2, pll_wbwdisable
  pm_pll_enable(pm, 0);
  pm_wait_for_pll0_locked(pm);
  flashc_set_wait_state(1);
#if (FPBA==FCPU/4)
  pm_cksel(pm, 1, 1, 1, 1, 0, 0);  // PM, PBAdiven, PBAdiv, PBBdiven, PBBdiv, CPUdiven, CPUdiv
#elif (FPBA==FCPU/2)
  pm_cksel(pm, 1, 0, 1, 0, 0, 0);  // PM, PBAdiven, PBAdiv, PBBdiven, PBBdiv, CPUdiven, CPUdiv
#elif (FPBA==FCPU)
  pm_cksel(pm, 0, 0, 0, 0, 0, 0);  // PM, PBAdiven, PBAdiv, PBBdiven, PBBdiv, CPUdiven, CPUdiv
#else
#error "Unsupported FPBA"
#endif
    pm_switch_to_clock(pm, AVR32_PM_MCCTRL_MCSEL_PLL0);

#elif (FOSC0==12000000) && (FCPU==60000000)

  /*
    Use CLK0 and PLL0 for the CPU, PBA & PBB
    Multiply 12 MHz crystal by 11 to 132 MHz and divide by 2
    Set low-frequency (<160 MHz) mode
    set dividers:
       CPU = HSB = CLK
       PBA = PBB = CLK / {1,2,4}
    Flash memory with one wait state (needed above 33 MHz)
  */

  pm_enable_osc0_crystal(pm, FOSC0);
  pm_enable_clk0(pm, OSC0_STARTUP);
  pm_pll_setup(pm, 0,  9, 1, 0, 16); // PM, pll, mul, div, osc, lockcount
  pm_pll_set_option(pm, 0, 1, 1, 0); // PM, pll, pll_freq, pll_div2, pll_wbwdisable
  pm_pll_enable(pm, 0);
  pm_wait_for_pll0_locked(pm);
  flashc_set_wait_state(1);
#if (FPBA==FCPU/4)
  pm_cksel(pm, 1, 1, 1, 1, 0, 0);  // PM, PBAdiven, PBAdiv, PBBdiven, PBBdiv, CPUdiven, CPUdiv
#elif (FPBA==FCPU/2)
  pm_cksel(pm, 1, 0, 1, 0, 0, 0);  // PM, PBAdiven, PBAdiv, PBBdiven, PBBdiv, CPUdiven, CPUdiv
#elif (FPBA==FCPU)
  pm_cksel(pm, 0, 0, 0, 0, 0, 0);  // PM, PBAdiven, PBAdiv, PBBdiven, PBBdiv, CPUdiven, CPUdiv
#else
#error "Unsupported FPBA"
#endif
    pm_switch_to_clock(pm, AVR32_PM_MCCTRL_MCSEL_PLL0);

#elif (FCPU==115000) && (FPBA==115000)

  /*
    Stay with RC oscillator
   */

#else

#  error "Unsupported clock setup for main CPU, PBA, PBB"

#endif

#if (FOSC0==12000000)

  /*
    Use CLK0 and PLL1 for USB
    Multiply 12 MHz crystal by 8 for 96 MHz and divide by 2
    Set low-frequency (80-160 MHz) mode
    Set the GCLK to use PLL1 with no divider
  */
  pm_pll_setup(pm, 1, 7, 1, 0, 16); // PM, pll, mul, div, osc, lockcount
  pm_pll_set_option(pm, 1, 1, 1, 0);  // PM, pll, pll_freq, div2, wbwdisable
  pm_pll_enable(pm, 1);
  pm_wait_for_pll1_locked(pm);
  pm_gc_setup(pm, AVR32_PM_GCLK_USBB, 1, 1, 0, 0); // PM, GC, osc_or_pll, pll_osc, diven, div
  pm_gc_enable(pm, AVR32_PM_GCLK_USBB);

#else

#  error "Unsupported clock setup for USB GCLK"

#endif

  /*
    Enable the interrupt controller. No interrupts should be set up yet.
  */

  Disable_global_interrupt();
  INTC_init_interrupts();
  Enable_global_interrupt();

}


void avrhw_revert_clocks(void)
{
  volatile avr32_pm_t *const pm = &AVR32_PM;

  pm_gc_disable(pm, AVR32_PM_GCLK_USBB);
  pm_gc_setup(pm, AVR32_PM_GCLK_USBB, 0, 0, 0, 0);

  pm_switch_to_clock(pm, AVR32_PM_MCCTRL_MCSEL_SLOW);

  flashc_set_wait_state(0);
}



int get_force_isp(void)
{
  return flashc_read_gp_fuse_bit(31);
}
void set_force_isp(int flag)
{
  flashc_set_gp_fuse_bit(31, flag);
}

void avrhw_reset()
{
  set_force_isp(1);

  if (0) {
    avrhw_revert_clocks();
    // Jump to ISP bootloader
    void (*foo)() = (void (*)())0x80000000;
    (*foo)();
  }
  else if (1) {
    U64 real_us_to = wdt_enable(500000);
    debug_printf("Set watchdog -- kablooey in %u uS", (U32)real_us_to);

    cpu_delay_ms(250, FCPU);

    Disable_global_interrupt();
    while (1) {}
  }
  else {
    wdt_reset_mcu();     // It sets the watchdog to a few microseconds, so it never gets a chance to start running again
  }

}


// ======================================================================


void avrhw_report_rcause()
{
  volatile avr32_pm_t *pm = &AVR32_PM;

  if (pm->RCAUSE.por) log_printf("Reset cause was power on");
  if (pm->RCAUSE.bod) log_printf("Reset cause was brownout");
  if (pm->RCAUSE.ext) log_printf("Reset cause was external");
  if (pm->RCAUSE.wdt) log_printf("Reset cause was watchdog");
  if (pm->RCAUSE.cpuerr) log_printf("Reset cause was cpu error");
  if (pm->RCAUSE.ocdrst) log_printf("Reset cause was OCD");
  if (pm->RCAUSE.jtaghard) log_printf("Reset cause was jtag hard");
  else if (pm->RCAUSE.jtag) log_printf("Reset cause was jtag");
}


void avrhw_debug_dump()
{
  volatile avr32_pm_t *pm = pm;

  debug_printf("FOSC0=%d FCPU=%d FPBA=%d", FOSC0, FCPU, FPBA);
  debug_printf("pm:  mcctrl=%x cksel=%x pll0=%x pll1=%x poscsr=%x",
               pm->mcctrl, pm->cksel, pm->pll[0], pm->pll[1], pm->poscsr);

}
