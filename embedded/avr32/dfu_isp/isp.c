/* This source file is part of the ATMEL AVR32-SoftwareFramework-1.3.0-AT32UC3A Release */

/*This file is prepared for Doxygen automatic documentation generation.*/
/*! \file ******************************************************************
 *
 * \brief Main file of the USB DFU ISP.
 *
 * - Compiler:           IAR EWAVR32 and GNU GCC for AVR32
 * - Supported devices:  All AVR32 devices with a USB module can be used.
 * - AppNote:
 *
 * \author               Atmel Corporation: http://www.atmel.com \n
 *                       Support and FAQ: http://support.atmel.no/
 *
 ***************************************************************************/

/* Copyright (C) 2006-2008, Atmel Corporation All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. The name of ATMEL may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
 * SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


//_____  I N C L U D E S ___________________________________________________

#include "preprocessor.h"
#include "compiler.h"
#include "pm.h"
#include "flashc.h"
#include "conf_usb.h"
#include "usb_drv.h"
#include "usb_task.h"
#if USB_DEVICE_FEATURE == ENABLED
#include "usb_dfu.h"
#endif
#include "conf_isp.h"
#include "isp.h"


#define AVR32_WDT_CLR                                      0x00000004
#define AVR32_WDT_CTRL                                     0x00000000
#define AVR32_WDT_CTRL_EN                                           0
#define AVR32_WDT_CTRL_EN_MASK                             0x00000001
#define AVR32_WDT_CTRL_EN_OFFSET                                    0
#define AVR32_WDT_CTRL_EN_SIZE                                      1
#define AVR32_WDT_CTRL_KEY                                         24
#define AVR32_WDT_CTRL_KEY_MASK                            0xff000000
#define AVR32_WDT_CTRL_KEY_OFFSET                                  24
#define AVR32_WDT_CTRL_KEY_SIZE                                     8
#define AVR32_WDT_CTRL_PSEL                                         8
#define AVR32_WDT_CTRL_PSEL_MASK                           0x00001f00
#define AVR32_WDT_CTRL_PSEL_OFFSET                                  8
#define AVR32_WDT_CTRL_PSEL_SIZE                                    5
#define AVR32_WDT_EN                                                0
#define AVR32_WDT_EN_MASK                                  0x00000001
#define AVR32_WDT_EN_OFFSET                                         0
#define AVR32_WDT_EN_SIZE                                           1
#define AVR32_WDT_KEY                                              24
#define AVR32_WDT_KEY_MASK                                 0xff000000
#define AVR32_WDT_KEY_OFFSET                                       24
#define AVR32_WDT_KEY_SIZE                                          8
#define AVR32_WDT_PSEL                                              8
#define AVR32_WDT_PSEL_MASK                                0x00001f00
#define AVR32_WDT_PSEL_OFFSET                                       8
#define AVR32_WDT_PSEL_SIZE                                         5
#define AVR32_WDT_KEY_VALUE                                0x00000055
#define WDT_PSEL_VALUE 19 // log2(T * Frcosc) - 1
#define FRCOSC 115200

Bool usb_host_on = 0;
void host_on(void)  {usb_host_on = 1;}
void host_off(void) {usb_host_on = 0;}
void vbus_on_action(void) { sys_clk_gen_start(); usb_host_on = 1; }



static void wdt_set_ctrl(unsigned long ctrl) {
  AVR32_WDT.ctrl = ctrl | (AVR32_WDT_KEY_VALUE << AVR32_WDT_CTRL_KEY_OFFSET);
  AVR32_WDT.ctrl = ctrl | ((~AVR32_WDT_KEY_VALUE << AVR32_WDT_CTRL_KEY_OFFSET) & AVR32_WDT_CTRL_KEY_MASK);
}


unsigned long long wdt_enable(unsigned long long us_timeout_period)
{
#define MIN_US_TIMEOUT_PERIOD   (((1ULL <<  1                             ) * 1000000 + FRCOSC / 2) / FRCOSC)
#define MAX_US_TIMEOUT_PERIOD   (((1ULL << (1 << AVR32_WDT_CTRL_PSEL_SIZE)) * 1000000 + FRCOSC / 2) / FRCOSC)

  // Set the CTRL.EN bit and translate the us timeout to fit in CTRL.PSEL using
  // the formula Twdt = 2pow(PSEL+1) / fRCosc
  wdt_set_ctrl(AVR32_WDT_CTRL_EN_MASK |
               ((32 - clz(((((Min(Max(us_timeout_period, MIN_US_TIMEOUT_PERIOD), MAX_US_TIMEOUT_PERIOD) *
                              FRCOSC + 500000) / 1000000) << 1) - 1) >> 1) - 1) <<
                AVR32_WDT_CTRL_PSEL_OFFSET));

  // Return the actual wdt period in us.
  return wdt_get_us_timeout_period();
}


void wdt_disable(void) {
  wdt_set_ctrl(AVR32_WDT.ctrl & ~AVR32_WDT_CTRL_EN_MASK);
}

void wdt_clear(void) {
  AVR32_WDT.clr = 0;
}

static S8 osc_freq_id = -1;


void wait_10_ms(void)
{
  Set_system_register(AVR32_COUNT, 0);
  while ((U32)Get_system_register(AVR32_COUNT) < (FRCOSC * 10 + 999) / 1000);
}


/*!
 *  Start the generation of system clocks with USB autobaud
 */
void sys_clk_gen_start(void)
{
  wdt_disable(); // Disable while messing with clocks, seems to trigger it
  
  #define OSC_FREQ_COUNT  1

  #define MAX_OSC_FREQ    12000000

  static const U8 OSC_PLL_MUL[OSC_FREQ_COUNT] =
  {
    //48000000 /  8000000 - 1,
    48000000 / 12000000 - 1,
    //48000000 / 16000000 - 1
  };

  volatile avr32_pm_t *const pm = &AVR32_PM;

  Bool sav_glob_int_en;

  if ((sav_glob_int_en = Is_global_interrupt_enabled())) Disable_global_interrupt();

  if (osc_freq_id < 0)
  {
    // Start the oscillator
    pm_enable_osc_crystal(pm, MAX_OSC_FREQ);
    pm_enable_clk_no_wait(pm, ATPASTE3(AVR32_PM_OSCCTRL, ISP_OSC, _STARTUP_16384_RCOSC));

    for (osc_freq_id = 0; !Is_usb_sof(); osc_freq_id++)
    {
      if (osc_freq_id >= OSC_FREQ_COUNT) osc_freq_id = 0;

      Usb_freeze_clock();

      // Set PLL0 VCO @ 96 MHz
      pm_pll_setup(pm, 0,                         // pll
                       OSC_PLL_MUL[osc_freq_id],  // mul
                       0,                         // div
                       ISP_OSC,                   // osc
                       63);                       // lockcount

      // Set PLL0 @ 48 MHz
      pm_pll_set_option(pm, 0,  // pll
                            1,  // pll_freq
                            1,  // pll_div2
                            0); // pll_wbwdisable

      // Enable PLL0
      pm_pll_enable(pm, 0);

      // Wait for PLL0 locked with a 10-ms time-out
      wait_10_ms();
      if (!(pm->poscsr & AVR32_PM_POSCSR_LOCK0_MASK)) continue;

      // Setup USB GCLK
      pm_gc_setup(pm, AVR32_PM_GCLK_USBB, // gc
                      1,                  // osc_or_pll: use Osc (if 0) or PLL (if 1)
                      0,                  // pll_osc: select Osc0/PLL0 or Osc1/PLL1
                      0,                  // diven
                      0);                 // div

      // Enable USB GCLK
      pm_gc_enable(pm, AVR32_PM_GCLK_USBB);

      Usb_unfreeze_clock();

      wait_10_ms();
      wait_10_ms();
      wait_10_ms();
    }

    // Use 1 flash wait state
    flashc_set_wait_state(1);

    // Switch the main clock to PLL0
    pm_switch_to_clock(pm, AVR32_PM_MCCTRL_MCSEL_PLL0);

    // fPBA: 12 MHz
    // fPBB: 12 MHz
    // fHSB: 12 MHz
    pm_cksel(pm, 1,   // pbadiv
                 1,   // pbasel
                 1,   // pbbdiv
                 1,   // pbbsel
                 1,   // hsbdiv
                 1);  // hsbsel

    // Use 0 flash wait state
    flashc_set_wait_state(0);

    Usb_ack_sof();
  }

  if (sav_glob_int_en) Enable_global_interrupt();

  wdt_enable();
}


/*!
 *  Stop the generation of system clocks and switch to RCOsc
 */
void sys_clk_gen_stop(void)
{
  wdt_disable(); // Disable while messing with clocks, seems to trigger it

  volatile avr32_pm_t *const pm = &AVR32_PM;

  pm_gc_disable(pm, AVR32_PM_GCLK_USBB);
  pm_gc_setup(pm, AVR32_PM_GCLK_USBB, 0, 0, 0, 0);
  flashc_set_wait_state(1);
  pm_cksel(pm, 0, 0, 0, 0, 0, 0);
  pm_switch_to_clock(pm, AVR32_PM_MCCTRL_MCSEL_SLOW);
  flashc_set_wait_state(0);
  pm_pll_disable(pm, 0);
  pm_pll_set_option(pm, 0, 0, 0, 0);
  pm_pll_setup(pm, 0, 0, 0, 0, 0);
  pm_enable_clk_no_wait(pm, ATPASTE3(AVR32_PM_OSCCTRL, ISP_OSC, _STARTUP_0_RCOSC));
  pm_disable_clk(pm);
  pm_enable_osc_ext_clock(pm);

  wdt_enable();
}

#if defined(__TARG_QBEXPRESSION2__)
#define SCREEN_EN   AVR32_PIN_PA27

void screen_pwr_init(void) {
  gpio_enable_gpio_pin(SCREEN_EN);
  gpio_clr_gpio_pin(SCREEN_EN);
}

void screen_pwr_task(void) {
  static unsigned int usb_off_count;
  if (usb_host_on) {
    gpio_set_gpio_pin(SCREEN_EN); // Turn on the screen if USB is running
    usb_off_count = 0;
  }
  else {
    if (usb_off_count > 4000000) gpio_clr_gpio_pin(SCREEN_EN); // Wait a long time then turn of screen if USB is still off
    else usb_off_count++;
  }
}
#endif


int main(void)
{
  wdt_disable();
  wait_10_ms();
  wdt_enable(10000000); //enable watchdog for 10 second timeout
  usb_task_init();
#if USB_DEVICE_FEATURE == ENABLED
  usb_dfu_init();
#endif

  while (TRUE)
  {
    usb_task();
#if defined(__TARG_QBEXPRESSION2__)
    screen_pwr_task();
#endif
  }
}
