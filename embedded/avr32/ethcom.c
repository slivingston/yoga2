#include "common/std_headers.h"
#include "hwdefs.h"
#include "gpio.h"
#include "./ethcom.h"
#include "../embedded_timing.h"
#include "../embedded_debug.h"
#include "./macb.h"
/*
  https://en.wikipedia.org/wiki/Ethernet_frame
  https://en.wikipedia.org/wiki/Address_Resolution_Protocol
  https://en.wikipedia.org/wiki/IPv4
  https://en.wikipedia.org/wiki/Private_network
  https://en.wikipedia.org/wiki/User_Datagram_Protocol
  https://en.wikipedia.org/wiki/MAC_address

*/


/*
  Because the AVR32UC3A doesn't support unaligned data access, we generally access 16- and 32-bit fields in packets by combining 2 or 4 bytes.
  (The architectural manual says implementations vary, but the AT32UC3A manual section 9.2.5 says our hardware doesn't support it.)
  It's a pity, because in fact we are probably always in 16-bit alignment given the way we allocate packets.
  That also means we don't depend on byte order anywhere here.
*/

ethcom ethcom0;

static uint8_t zero_mac[6] = {0,0,0,0,0,0};

void ethcom_init(ethcom *it)
{
  it->verbose = 0;

#if defined(ETHER_NRST_PIN)
  gpio_enable_gpio_pin(ETHER_NRST_PIN);
  gpio_set_gpio_pin(ETHER_NRST_PIN);
#endif

  it->local.ip4_addr[0] = ETHERNET_CONF_IPADDR0;
  it->local.ip4_addr[1] = ETHERNET_CONF_IPADDR1;
  it->local.ip4_addr[2] = ETHERNET_CONF_IPADDR2;
  it->local.ip4_addr[3] = ETHERNET_CONF_IPADDR3;
  it->local.udp_port = 10000;
  it->local.mac_addr[0] = ETHERNET_CONF_ETHADDR0;
  it->local.mac_addr[1] = ETHERNET_CONF_ETHADDR1;
  it->local.mac_addr[2] = ETHERNET_CONF_ETHADDR2;
  it->local.mac_addr[3] = ETHERNET_CONF_ETHADDR3;
  it->local.mac_addr[4] = ETHERNET_CONF_ETHADDR4;
  it->local.mac_addr[5] = ETHERNET_CONF_ETHADDR5;

  it->bcast.ip4_addr[0] = 172;
  it->bcast.ip4_addr[1] = 31;
  it->bcast.ip4_addr[2] = 255;
  it->bcast.ip4_addr[3] = 255;
  it->bcast.udp_port = 0xffff;
  it->bcast.mac_addr[0] = 0xff;
  it->bcast.mac_addr[1] = 0xff;
  it->bcast.mac_addr[2] = 0xff;
  it->bcast.mac_addr[3] = 0xff;
  it->bcast.mac_addr[4] = 0xff;
  it->bcast.mac_addr[5] = 0xff;

  it->ip4_id_ctr = 0x1234; // could be more random

  // Enable MACB pins on output
  gpio_enable_module_pin(AVR32_MACB_MDC_0_PIN,    AVR32_MACB_MDC_0_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_MDIO_0_PIN,   AVR32_MACB_MDIO_0_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_RXD_0_PIN,    AVR32_MACB_RXD_0_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_TXD_0_PIN,    AVR32_MACB_TXD_0_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_RXD_1_PIN,    AVR32_MACB_RXD_1_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_TXD_1_PIN,    AVR32_MACB_TXD_1_FUNCTION);
#if !ETHERNET_CONF_USE_RMII_INTERFACE
  gpio_enable_module_pin(AVR32_MACB_RXD_2_PIN,    AVR32_MACB_RXD_2_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_TXD_2_PIN,    AVR32_MACB_TXD_2_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_RXD_3_PIN,    AVR32_MACB_RXD_3_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_TXD_3_PIN,    AVR32_MACB_TXD_3_FUNCTION);
#endif
  gpio_enable_module_pin(AVR32_MACB_TX_EN_0_PIN,  AVR32_MACB_TX_EN_0_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_RX_ER_0_PIN,  AVR32_MACB_RX_ER_0_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_RX_DV_0_PIN,  AVR32_MACB_RX_DV_0_FUNCTION);
  gpio_enable_module_pin(AVR32_MACB_TX_CLK_0_PIN, AVR32_MACB_TX_CLK_0_FUNCTION);

  if (!xMACBInit(&AVR32_MACB)) {
    panic_printf("ethcom: macb init failed");
  }
  debug_printf("MACB inited");

}

void ethcom_shutdown(ethcom *it)
{
}


pkt_tx_buf *ethcom_start_tx_ether(ethcom *it, uint8_t *dst_mac, uint16_t ethertype)
{
  pkt_tx_buf *tx = alloc_tx_buf();
  if (!tx) {
    it->stats.tx_no_buffers++;
    return NULL;
  }

  pkt_tx_mem(tx, dst_mac, 6);
  pkt_tx_mem(tx, it->local.mac_addr, 6);
  pkt_tx_netu16(tx, ethertype);
  return tx;
}

pkt_tx_buf *ethcom_start_tx_ip4(ethcom *it, pkt_endpoint *ep)
{
  if (memcmp(ep->mac_addr, zero_mac, 6) == 0) {
    it->stats.tx_no_route++;
    return NULL;
  }

  pkt_tx_buf *tx = ethcom_start_tx_ether(it, ep->mac_addr, 0x0800);
  if (!tx) return NULL;

  tx->ip4_hdr_pos = tx->len;
  pkt_tx_u8(tx, 0x45);// Version and IHL
  pkt_tx_u8(tx, 0x00); // DSCP, ECN
  pkt_tx_netu16(tx, 0); // ip4 length, fill in later
  pkt_tx_netu16(tx, it->ip4_id_ctr++); // ID
  pkt_tx_netu16(tx, 0); // Flags, fragment

  pkt_tx_u8(tx, 5); // TTL
  pkt_tx_u8(tx, 0x11); // Protocol, UDP=0x11

  pkt_tx_netu16(tx, 0); // Header checksum, fill in later

  pkt_tx_mem(tx, it->local.ip4_addr, 4);
  pkt_tx_mem(tx, ep->ip4_addr, 4);

  return tx;
}

pkt_tx_buf *ethcom_start_tx_udp(ethcom *it, pkt_endpoint *ep)
{
  pkt_tx_buf *tx = ethcom_start_tx_ip4(it, ep);
  if (!tx) return NULL;

  tx->udp_hdr_pos = tx->len;
  pkt_tx_netu16(tx, it->local.udp_port);
  pkt_tx_netu16(tx, ep->udp_port);
  pkt_tx_netu16(tx, 0); // UDP length, fill in later
  pkt_tx_netu16(tx, 0); // UDP checksum, fill in later

  it->stats.tx_down++;
  return tx;
}

uint16_t rfc768_checksum(U8 *p, unsigned int len)
{
  /*
    See https://tools.ietf.org/html/rfc768
    And https://tools.ietf.org/html/rfc1071

    This loop is 8 instructions per 2 bytes, not too bad.
    I could get more clever by getting to an 16-bit alignment
    and using 16-bit instructions.
    80002ed8:	11 8b       	ld.ub	r11,r8[0x0]
    80002eda:	11 9a       	ld.ub	r10,r8[0x1]
    80002edc:	20 29       	sub	r9,2
    80002ede:	f5 eb 10 8a 	or	r10,r10,r11<<0x8
    80002ee2:	2f e8       	sub	r8,-2
    80002ee4:	14 0c       	add	r12,r10
    80002ee6:	58 19       	cp.w	r9,1
    80002ee8:	fe 9b ff f8 	brhi	80002ed8 <rfc768_checksum+0x1c>

  */
  assert(len < 65536);
  uint32_t accum = 0;
  while (len >= 2) {
    accum += ((uint16_t)p[0] << 8) | ((uint16_t)p[1] << 0);
    p += 2;
    len -= 2;
  }
  if (len > 0) {
    accum += ((uint16_t)p[0] << 8);
  }
  while (accum >= 0x10000) {
    accum = (accum >> 16) + ((accum >> 0) & 0xffff);
  }
  return accum;
}

void ethcom_finish_pkt(ethcom *it, pkt_tx_buf *tx)
{
  if (tx->ip4_hdr_pos >= 0) {
    uint16_t ip4_len = tx->len - tx->ip4_hdr_pos;
    tx->buf[tx->ip4_hdr_pos + 2] = (uint8_t)(ip4_len >> 8);
    tx->buf[tx->ip4_hdr_pos + 3] = (uint8_t)(ip4_len >> 0);

    int ip4_hdr_len = (tx->buf[tx->ip4_hdr_pos] & 0x0f) * 4;
    uint16_t ip4_cksum = (uint16_t)0xffff - rfc768_checksum(&tx->buf[tx->ip4_hdr_pos], ip4_hdr_len);
    tx->buf[tx->ip4_hdr_pos + 10] = (uint8_t)(ip4_cksum >> 8);
    tx->buf[tx->ip4_hdr_pos + 11] = (uint8_t)(ip4_cksum >> 0);
  }

  if (tx->icmp_hdr_pos >= 0) {
    int icmp_len = tx->len - tx->icmp_hdr_pos;
    uint16_t icmp_cksum = (uint16_t)0xffff - rfc768_checksum(&tx->buf[tx->icmp_hdr_pos], icmp_len);
    tx->buf[tx->icmp_hdr_pos + 2] = (uint8_t)(icmp_cksum >> 8);
    tx->buf[tx->icmp_hdr_pos + 3] = (uint8_t)(icmp_cksum >> 0);
  }

  if (tx->udp_hdr_pos >= 0) {
    uint16_t udp_len = tx->len - tx->udp_hdr_pos;
    tx->buf[tx->udp_hdr_pos + 4] = (uint8_t)(udp_len >> 8);
    tx->buf[tx->udp_hdr_pos + 5] = (uint8_t)(udp_len >> 0);

    uint16_t udp_cksum = 0;
    /*
      in udp4 the checksum is optional, 0 means omitted
      It has to be created over a pseudo-header including part of the ip4 header with
    */
    tx->buf[tx->udp_hdr_pos + 6] = (uint8_t)(udp_cksum >> 8);
    tx->buf[tx->udp_hdr_pos + 7] = (uint8_t)(udp_cksum >> 0);
  }
}


void ethcom_send_arp(ethcom *it, pkt_endpoint *ep)
{
  for (int i=0; i<it->n_pending_arps; i++) {
    if (it->pending_arps[i] == ep) return;
  }
  if (it->n_pending_arps >= ETHCOM_N_ARPS) {
    log_printf("ethcom: arp table overflow");
    return;
  }
  it->pending_arps[it->n_pending_arps] = ep;
  it->n_pending_arps ++;

  it->last_arp_tick = get_ticks();
  ethcom_resend_arp(it, ep);
}

void ethcom_resend_arp(ethcom *it, pkt_endpoint *ep)
{
  pkt_tx_buf *tx = ethcom_start_tx_ether(it, it->bcast.mac_addr, 0x0806);
  if (!tx) goto fail;

  pkt_tx_netu16(tx, 0x0001);
  pkt_tx_netu16(tx, 0x0800);
  pkt_tx_u8(tx, 6);
  pkt_tx_u8(tx, 4);
  pkt_tx_netu16(tx, 0x0001); // OPER = 1, ARP request

  // I am the sender, so include my canonical info
  pkt_tx_mem(tx, it->local.mac_addr, 6);
  pkt_tx_mem(tx, it->local.ip4_addr, 4);
  pkt_tx_mem(tx, zero_mac, 6);
  pkt_tx_mem(tx, ep->ip4_addr, 4);

  ethcom_tx_pkt(it, tx);

  if (it->verbose >= 2) log_printf("ethcom: Sending ARP for %d.%d.%d.%d",
                                     ep->ip4_addr[0], ep->ip4_addr[1], ep->ip4_addr[2], ep->ip4_addr[3]);
  return;

 fail:
  if (it->verbose >= 1) log_printf("ethcom: Sending ARP failed");
}

void ethcom_handle_arp_reply(ethcom *it, uint8_t *spa, uint8_t *sha)
{
  for (int i=0; i<it->n_pending_arps; i++) {
    if (it->pending_arps[i] && memcmp(spa, it->pending_arps[i]->ip4_addr, 4) == 0) {
      if (it->verbose >= 1) log_printf("ethcom: ARP successful for %d.%d.%d.%d: %02x:%02x:%02x:%02x:%02x:%02x",
                                       spa[0], spa[1], spa[2], spa[3],
                                       sha[0], sha[1], sha[2], sha[3], sha[4], sha[5]);
      memcpy(it->pending_arps[i]->mac_addr, sha, 6);
    }
  }
}

void ethcom_start_syslog(ethcom *it, pkt_endpoint *ep)
{
  ep->ip4_addr[0] = ETHERNET_CONF_SYSLOGIP0;
  ep->ip4_addr[1] = ETHERNET_CONF_SYSLOGIP1;
  ep->ip4_addr[2] = ETHERNET_CONF_SYSLOGIP2;
  ep->ip4_addr[3] = ETHERNET_CONF_SYSLOGIP3;
  ep->udp_port = 514;
  ethcom_send_arp(it, ep);
}

static void ethcom_handle_rx(ethcom *it, pkt_rx_buf *rx)
{
  it->stats.rx++;
  it->stats.rx_largest = max(it->stats.rx_largest, rx->len);
  if (rx->rdpos + 12 <= rx->len) {
    uint8_t mac_dst[6], mac_src[6];
    pkt_rx_mem(rx, mac_dst, 6);
    pkt_rx_mem(rx, mac_src, 6);
    memcpy(rx->src.mac_addr, mac_src, 6);
    uint16_t ethertype = pkt_rx_netu16(rx);
    // Do I need to skip a 802.1Q header on some networks?

    if (it->verbose >= 2) log_printf("ethcom: packet dst=%02x:%02x:%02x:%02x:%02x:%02x src=%02x:%02x:%02x:%02x:%02x:%02x ethertype=%04x rem=%d",
				     mac_dst[0], mac_dst[1], mac_dst[2], mac_dst[3], mac_dst[4], mac_dst[5],
				     mac_src[0], mac_src[1], mac_src[2], mac_src[3], mac_src[4], mac_src[5],
				     ethertype,
				     rx->len - rx->rdpos);

    if (ethertype == 0x0806 && rx->rdpos + 28 <= rx->len) { // ARP
      uint16_t htype = pkt_rx_netu16(rx);
      uint16_t ptype = pkt_rx_netu16(rx);
      uint8_t hlen = pkt_rx_u8(rx);
      uint8_t plen = pkt_rx_u8(rx);
      uint16_t oper = pkt_rx_netu16(rx);
      if (it->verbose >= 2) log_printf("ethcom: ARP htype=%04x ptype=%04x hlen=%02x plen=%02x oper=%04x",
				       htype,
				       ptype,
				       hlen, plen,
				       oper);

      if (oper == 0x0001) { // ARP request
        uint8_t sha[6]; // sender hw address
        pkt_rx_mem(rx, sha, 6);
        uint8_t spa[4]; // sender prototol address
        pkt_rx_mem(rx, spa, 4);
        uint8_t tha[6]; // target hardware address
        pkt_rx_mem(rx, tha, 6);
        uint8_t tpa[4]; // target protocol address
        pkt_rx_mem(rx, tpa, 4);

        bool for_local = memcmp(tpa, it->local.ip4_addr, 4) == 0;
        if (it->verbose >= (for_local ? 1 : 2)) log_printf("ethcom: ARP request sender=%02x:%02x:%02x:%02x:%02x:%02x (%d.%d.%d.%d) target=%d.%d.%d.%d %s",
							   sha[0], sha[1], sha[2], sha[3], sha[4], sha[5],
							   spa[0], spa[1], spa[2], spa[3],
							   tpa[0], tpa[1], tpa[2], tpa[3],
							   for_local ? "for us" : "not for us");

        if (for_local) {
          pkt_tx_buf *tx = ethcom_start_tx_ether(it, mac_src, 0x0806);
          if (!tx) goto fail;
          pkt_tx_netu16(tx, htype);
          pkt_tx_netu16(tx, ptype);
          pkt_tx_u8(tx, hlen);
          pkt_tx_u8(tx, plen);
          pkt_tx_netu16(tx, 0x0002); // ARP reply

          // I am the sender, so include my canonical info
          pkt_tx_mem(tx, it->local.mac_addr, 6);
          pkt_tx_mem(tx, it->local.ip4_addr, 4);
          pkt_tx_mem(tx, sha, 6);
          pkt_tx_mem(tx, spa, 4);
          ethcom_tx_pkt(it, tx);

          if (it->verbose >= 1) log_printf("ethcom: Replying to ARP with %02x:%02x:%02x:%02x:%02x:%02x (%d.%d.%d.%d)",
					   it->local.mac_addr[0], it->local.mac_addr[1], it->local.mac_addr[2],
					   it->local.mac_addr[3], it->local.mac_addr[4], it->local.mac_addr[5],
					   it->local.ip4_addr[0], it->local.ip4_addr[1], it->local.ip4_addr[2], it->local.ip4_addr[3]);
        }
        else {
          if (it->verbose >= 2) log_printf("ethcom: ARP request not for us (tpa=%d.%d.%d.%d, we are %d.%d.%d.%d)",
					   tpa[0], tpa[1], tpa[2], tpa[3],
					   it->local.ip4_addr[0], it->local.ip4_addr[1], it->local.ip4_addr[2], it->local.ip4_addr[3]);
        }

      }
      else if (oper == 0x0002) { // ARP reply

        uint8_t sha[6]; // sender hw address
        pkt_rx_mem(rx, sha, 6);
        uint8_t spa[4]; // sender prototol address
        pkt_rx_mem(rx, spa, 4);
        uint8_t tha[6]; // target hardware address
        pkt_rx_mem(rx, tha, 6);
        uint8_t tpa[4]; // target protocol address
        pkt_rx_mem(rx, tpa, 4);

        if (it->verbose >= 1) log_printf("ethcom: ARP reply sender=%02x:%02x:%02x:%02x:%02x:%02x (%d.%d.%d.%d) target=%02x:%02x:%02x:%02x:%02x:%02x (%d.%d.%d.%d)",
					 sha[0], sha[1], sha[2], sha[3], sha[4], sha[5],
					 spa[0], spa[1], spa[2], spa[3],
					 tha[0], tha[1], tha[2], tha[3], tha[4], tha[5],
					 tpa[0], tpa[1], tpa[2], tpa[3]);

        ethcom_handle_arp_reply(it, spa, sha);
      }
      else {
        if (it->verbose >= 1) log_printf("ethcom: Unknown ARP packet htype=%04x ptype=%04x hlen=%02x plen=%02x oper=%04x",
					 htype,
					 ptype,
					 hlen, plen,
					 oper);
      }
    }
    else if (ethertype == 0x0800 && rx->rdpos + 20 <= rx->len) { // IP4 over Ethernet
      uint8_t ip4_version_ihl = pkt_rx_u8(rx);
      uint8_t ip4_version = (ip4_version_ihl >> 4) & 0x0f;
      uint8_t ip4_ihl = (ip4_version_ihl >> 0) & 0x0f;
      if (ip4_version == 0x4) {
        uint8_t ip4_dsp_ecn = pkt_rx_u8(rx);
        uint16_t ip4_len = pkt_rx_netu16(rx);
        uint16_t ip4_id = pkt_rx_netu16(rx);
        uint16_t ip4_flags_fragofs = pkt_rx_netu16(rx);
        uint8_t ip4_ttl = pkt_rx_u8(rx);
        uint8_t ip4_proto = pkt_rx_u8(rx);
        uint16_t ip4_hdr_cksum = pkt_rx_netu16(rx);
        if (ip4_ihl < 5) {
          it->stats.rx_bad_ip4++;
          goto fail;
        }
        if (ip4_ihl > 5) {
          rx->rdpos += (ip4_ihl-5) * 4; // skip option headers
          if (rx->rdpos > rx->len) {
            it->stats.rx_bad_ip4++;
            goto fail;
          }
        }

        uint8_t ip4_src_addr[4];
        pkt_rx_mem(rx, ip4_src_addr, 4);
	      memcpy(rx->src.ip4_addr, ip4_src_addr, 4);
        uint8_t ip4_dst_addr[4];
        pkt_rx_mem(rx, ip4_dst_addr, 4);

        if (it->verbose >= 2) log_printf("ethcom: Received ip4 %d.%d.%d.%d -> %d.%d.%d.%d ihl=%01x dsp_ecn=%02x len=%04x id=%04x flags_fragofs=%04x ttl=%02x proto=%02x hdr_cksum=%04x",
					 ip4_src_addr[0], ip4_src_addr[1], ip4_src_addr[2], ip4_src_addr[3],
					 ip4_dst_addr[0], ip4_dst_addr[1], ip4_dst_addr[2], ip4_dst_addr[3],
					 ip4_ihl, ip4_dsp_ecn,
					 ip4_len, ip4_id, ip4_flags_fragofs, ip4_ttl, ip4_proto, ip4_hdr_cksum);

        if (ip4_proto == 0x01) { // ICMP
          uint8_t icmp_type = pkt_rx_u8(rx);
          uint8_t icmp_code = pkt_rx_u8(rx);
          uint16_t icmp_cksum = pkt_rx_netu16(rx);
          if (icmp_type == 0x08) { // echo request
            uint16_t icmp_ident = pkt_rx_netu16(rx);
            uint16_t icmp_seq = pkt_rx_netu16(rx);

            // return to sender mac, possibly incorrect
            pkt_tx_buf *tx = ethcom_start_tx_ether(it, mac_src, 0x0800);
            if (!tx) goto fail;
            if (it->verbose >= 2) log_printf("ethcom: Replying to ping echo request seq=%d", icmp_seq);

            // IP4 header
            tx->ip4_hdr_pos = tx->len;
            pkt_tx_u8(tx, 0x45); // ip4, no options
            pkt_tx_u8(tx, 0x00); // DSP, ECN
            pkt_tx_netu16(tx, ip4_len);
            pkt_tx_netu16(tx, it->ip4_id_ctr++);
            pkt_tx_netu16(tx, 0); // Flags, fragmentofs

            pkt_tx_u8(tx, 5); // TTL
            pkt_tx_u8(tx, 0x01); // Protocol, ICMP

            pkt_tx_netu16(tx, 0); // cksum, filled in later

            pkt_tx_mem(tx, it->local.ip4_addr, 4);
            pkt_tx_mem(tx, ip4_src_addr, 4); // return to sender

            tx->icmp_hdr_pos = tx->len;
            pkt_tx_u8(tx, 0); // ICMP type echo reply
            pkt_tx_u8(tx, 0); // ICMP code
            pkt_tx_netu16(tx, 0); // cksum, filled in later
            pkt_tx_netu16(tx, icmp_ident);
            pkt_tx_netu16(tx, icmp_seq);

            // Copy rest of data
            pkt_tx_mem(tx, &rx->buf[rx->rdpos], pkt_rx_remaining(rx));

            ethcom_tx_pkt(it, tx);
          }
          else {
            if (it->verbose >= 2) log_printf("ethcom: Received unknown icmp %d.%d.%d.%d -> %d.%d.%d.%d type=%02x code=%02x cksum=%04x",
					     ip4_src_addr[0], ip4_src_addr[1], ip4_src_addr[2], ip4_src_addr[3],
					     ip4_dst_addr[0], ip4_dst_addr[1], ip4_dst_addr[2], ip4_dst_addr[3],
					     icmp_type, icmp_code, icmp_cksum);
          }
        }
        else if (ip4_proto == 0x11) { // UDP
          uint16_t udp_src_port = pkt_rx_netu16(rx);
          rx->src.udp_port = udp_src_port;
          uint16_t udp_dst_port = pkt_rx_netu16(rx);
          uint16_t udp_len = pkt_rx_netu16(rx);
          uint16_t udp_cksum = pkt_rx_netu16(rx);

          if (it->verbose >= 1) log_printf("ethcom: Received udp4 %d.%d.%d.%d:%d -> %d.%d.%d.%d:%d udp_len=%d pkt_payload=%d cksum=%04x",
					   ip4_src_addr[0], ip4_src_addr[1], ip4_src_addr[2], ip4_src_addr[3], udp_src_port,
					   ip4_dst_addr[0], ip4_dst_addr[1], ip4_dst_addr[2], ip4_dst_addr[3], udp_dst_port,
					   udp_len,
					   rx->len - rx->rdpos,
					   udp_cksum);
          if (rx->len - rx->rdpos < udp_len - 8) {
            if (it->verbose >= 1) log_printf("ethcom: udp4 packet truncated (%d , %d)", rx->len - rx->rdpos, udp_len);
            it->stats.rx_bad_udp++;
            goto fail;
          }
          rx->len = rx->rdpos + udp_len - 8;

          if (memcmp(ip4_dst_addr, it->local.ip4_addr, 4) == 0 && udp_dst_port == it->local.udp_port) {
            if ((it->rx_q_head + 1) % ETHCOM_RX_Q_SIZE == it->rx_q_tail) {
              it->stats.rx_q_overflow++;
              goto fail;
            }
            if (it->verbose >= 2) log_printf("ethcom: queuing packet len=%d", pkt_rx_remaining(rx));
            it->rx_q[it->rx_q_head] = rx;
            it->rx_q_head = (it->rx_q_head + 1) % ETHCOM_RX_Q_SIZE;
            return; // and don't free packet
          }
          else {
            if (it->verbose >= 2) log_printf("ethcom: Discarding UDP packet for %d.%d.%d.%d:%d",
					     ip4_dst_addr[0], ip4_dst_addr[1], ip4_dst_addr[2], ip4_dst_addr[3],
					     udp_dst_port);
          }

        }
        else {
          if (it->verbose >= 1) log_printf("ethcom: Received unknown ip4 proto %d.%d.%d.%d -> %d.%d.%d.%d proto=%02x dsp_ecp=%02x len=%04x id=%04x flags_fragofs=%04x ttl=%02x hdr_cksum=%04x",
					   ip4_src_addr[0], ip4_src_addr[1], ip4_src_addr[2], ip4_src_addr[3],
					   ip4_dst_addr[0], ip4_dst_addr[1], ip4_dst_addr[2], ip4_dst_addr[3],
					   ip4_proto,
					   ip4_dsp_ecn, ip4_len, ip4_id, ip4_flags_fragofs, ip4_ttl, ip4_hdr_cksum);
          it->stats.rx_bad_ip4++;
        }
      }
      else {
        it->stats.rx_bad_ip4++;
        if (it->verbose >= 1) log_printf("ethcom: unknown ip protocol %01x", ip4_version);
      }
    }
    else {
      it->stats.rx_bad_ether++;
      if (it->verbose >= 1) log_printf("ethcom: unknown ethertype %02x", ethertype);
    }
  }
 fail:
  free_rx_buf(rx);
}


static void ethcom_do_rx(ethcom *it)
{
#if 0
  static uint32_t last_check_tick;
  uint32_t cur_tick = get_ticks();
  if (cur_tick - last_check_tick > SECONDSTOTICKS(0.25)) {
    vMACBDumpRxDescriptors();
    last_check_tick = cur_tick;
  }
#endif

  // Scan MACB queue, get size of pending full packet
  uint32_t len = ulMACBInputLength();
  if (len != 0) {

    pkt_rx_buf *rx = alloc_rx_buf();
    if (!rx) {
      it->stats.rx_no_buffers++;
      return;
    }
    if (len > rx->alloc_size) {
      it->stats.rx_too_long++;
      free_rx_buf(rx);
      vMACBFlushCurrentPacket(len);
      return;
    }

    // Let the driver know we are going to read a new packet.
    vMACBRead(NULL, 0, 0);
    // Read enough bytes to fill this buf.
    vMACBRead(rx->buf, len, len);
    rx->len = len;

    ethcom_handle_rx(it, rx);
  }

}

void ethcom_do_tx(ethcom *it)
{
  while (it->tx_q_head != it->tx_q_tail) {

    pkt_tx_buf *tx = it->tx_q[it->tx_q_tail];
    if (!bMACBCanSend(&AVR32_MACB, tx->len)) return;
    it->tx_q_tail = (it->tx_q_tail + 1) % ETHCOM_TX_Q_SIZE;
    it->stats.tx++;
    it->stats.tx_largest = max(it->stats.tx_largest, tx->len);

    // Argh, this is blocking. Will have to rewrite
    vMACBSend(&AVR32_MACB, tx->buf, tx->len, 1);
    free_tx_buf(tx);
  }
}

/*
  Put a packet in the transmit queue.
*/
void ethcom_tx_pkt(ethcom *it, pkt_tx_buf *tx)
{
  if (tx->overrun) {
    log_printf("ethcom: tx pkt overran by %d (len=%d)", tx->overrun, tx->len);
    free_tx_buf(tx);
    return;
  }
  // Should I be more conservative here?
  if (tx->len >= 1500) {
    log_printf("ethcom: tx pkt too long: %d", tx->len);
    free_tx_buf(tx);
    return;
  }

  ethcom_finish_pkt(it, tx);

  if ((it->tx_q_head + 1) % ETHCOM_TX_Q_SIZE == it->tx_q_tail) {
    it->stats.tx_q_overflow++;
    free_tx_buf(tx);
    return;
  }

  it->tx_q[it->tx_q_head] = tx;
  it->tx_q_head = (it->tx_q_head + 1) % ETHCOM_TX_Q_SIZE;
}

/*
  Return a complete packet out of the receive queue.
*/
pkt_rx_buf *ethcom_rx_pkt(ethcom *it)
{
  if (it->rx_q_tail == it->rx_q_head) return 0;

  pkt_rx_buf *rx = it->rx_q[it->rx_q_tail];
  it->rx_q[it->rx_q_tail] = NULL;
  it->rx_q_tail = (it->rx_q_tail + 1) % ETHCOM_RX_Q_SIZE;
  it->stats.rx_up++;
  return rx;
}

/*
  Call this frequently to do all the work of transferring packets in & out of the ethernet
  hardware into our local queues.
*/
void ethcom_poll(ethcom *it)
{
  ethcom_do_rx(it);
  ethcom_do_tx(it);

  U32 now = get_ticks();
  if (now - it->last_arp_tick > SECONDSTOTICKS(1.0)) {
    it->last_arp_tick = now;
    if (it->n_pending_arps > 0) {
      ethcom_resend_arp(it, it->pending_arps[it->arp_resend_ctr]);
      it->arp_resend_ctr = (it->arp_resend_ctr + 1) % it->n_pending_arps;
    }
  }
}

void ethcom_drain(ethcom *it)
{
  U32 t0 = get_ticks();
  while ((get_ticks() - t0) < SECONDSTOTICKS(0.1)) {
    ethcom_poll(it);
  }
}


void ethcom_log_status(ethcom *it, char const *name)
{
  if (it->verbose >= 1) {
    log_printf("%s:  rx=%d q=%d largest=%d q_overflow=%d no_buffers=%d too_long=%d bad_ether=%d bad_ip4=%d bad_udp=%d bad_icmp=%d up=%d",
               name,
               it->stats.rx,
               (it->rx_q_head-it->rx_q_tail + ETHCOM_RX_Q_SIZE) % ETHCOM_RX_Q_SIZE,
               it->stats.rx_largest,
               it->stats.rx_q_overflow, it->stats.rx_no_buffers, it->stats.rx_too_long, it->stats.rx_bad_ether,
               it->stats.rx_bad_ip4, it->stats.rx_bad_udp, it->stats.rx_bad_icmp, it->stats.rx_up);

    log_printf("%s:  tx=%d q=%d largest=%d q_overflow=%d no_buffers=%d no_route=%d down=%d",
               name,
               it->stats.tx,
               (it->tx_q_head-it->tx_q_tail + ETHCOM_TX_Q_SIZE) % ETHCOM_TX_Q_SIZE,
               it->stats.tx_largest,
               it->stats.tx_q_overflow, it->stats.tx_no_buffers, it->stats.tx_no_route, it->stats.tx_down);
    it->stats.rx_largest = 0;
    it->stats.tx_largest = 0;
  }
}
