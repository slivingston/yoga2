#pragma once
#include "../embedded_pktcom.h"
#include "../embedded_timing.h"

#define ETHCOM_RX_Q_SIZE 32
#define ETHCOM_TX_Q_SIZE 32
#define ETHCOM_N_ARPS 4
typedef struct ethcom {

  int rx_q_head, rx_q_tail;
  int tx_q_head, tx_q_tail;

  pkt_rx_buf *rx_q[ETHCOM_RX_Q_SIZE];
  pkt_tx_buf *tx_q[ETHCOM_TX_Q_SIZE];

  struct {
    int rx;
    int rx_no_buffers;
    int rx_q_overflow;
    int rx_too_long;
    int rx_bad_ether;
    int rx_bad_ip4;
    int rx_bad_udp;
    int rx_bad_icmp;
    int rx_up;
    int rx_largest;

    int tx;
    int tx_no_buffers;
    int tx_q_overflow;
    int tx_no_route;
    int tx_down;
    int tx_largest;
  } stats;

  int verbose;

  pkt_endpoint local;
  pkt_endpoint bcast;
  uint16_t ip4_id_ctr;

  pkt_endpoint *pending_arps[ETHCOM_N_ARPS];
  int n_pending_arps;
  int arp_resend_ctr;
  U32 last_arp_tick;
} ethcom;

void ethcom_init(ethcom *it);
void ethcom_shutdown(ethcom *it);
void ethcom_poll(ethcom *it);
void ethcom_drain(ethcom *it);
void ethcom_log_status(ethcom *it, char const *name);
void ethcom_send_arp(ethcom *it, pkt_endpoint *ep);
void ethcom_resend_arp(ethcom *it, pkt_endpoint *ep);
void ethcom_start_syslog(ethcom *it, pkt_endpoint *ep);

pkt_tx_buf *ethcom_start_tx_ip4(ethcom *it, pkt_endpoint *ep);
pkt_tx_buf *ethcom_start_tx_udp(ethcom *it, pkt_endpoint *ep);

void ethcom_tx_pkt(ethcom *it, pkt_tx_buf *tx);
pkt_rx_buf *ethcom_rx_pkt(ethcom *it);

extern ethcom ethcom0;
