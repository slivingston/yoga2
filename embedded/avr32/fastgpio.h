#include "gpio.h"

typedef struct fastgpio_t {
  volatile avr32_gpio_port_t *port;
  uint32_t mask;
} fastgpio_t;

static inline void fastgpio_init_0(fastgpio_t *it, int gpio)
{
  it->port = &AVR32_GPIO.port[gpio >> 5];
  it->mask = 1 << (gpio & 0x1f);
  gpio_enable_gpio_pin(gpio);
  gpio_clr_gpio_pin(gpio);
}

static inline void fastgpio_init_1(fastgpio_t *it, int gpio)
{
  it->port = &AVR32_GPIO.port[gpio >> 5];
  it->mask = 1 << (gpio & 0x1f);
  gpio_enable_gpio_pin(gpio);
  gpio_set_gpio_pin(gpio);
}

static inline void fastgpio_init(fastgpio_t *it, int gpio)
{
  it->port = &AVR32_GPIO.port[gpio >> 5];
  it->mask = 1 << (gpio & 0x1f);
  gpio_enable_gpio_pin(gpio);
}

static inline void fastgpio_set_1(fastgpio_t *it)
{
  it->port->ovrs = it->mask;
}

static inline void fastgpio_set_0(fastgpio_t *it)
{
  it->port->ovrc = it->mask;
}

static inline void fastgpio_set(fastgpio_t *it, bool value)
{
  if (value) {
    it->port->ovrs = it->mask;
  }
  else {
    it->port->ovrc = it->mask;
  }
}

static inline bool fastgpio_get_pin_value(fastgpio_t *it)
{
  return (it->port->pvr & it->mask) ? true : false;
}
