#include "common/std_headers.h"

U32 get_jtag_id()
{
  return __builtin_mfdr(AVR32_DID);
}

void identify_hardware_report()
{
  U32 jtag_id = read_jtag_id();
  debug_printf("jtag_id=%08x RN=%d PN=%d MID=%d", jtag_id,
               (jtag_id>>28)&(1<<4-1),
               (jtag_id>>12)&(1<<16-1),
               (jtag_id>>1)&(1<<11-1))
}
