#include "common/std_headers.h"
#include "hwdefs.h"
#include "intc.h"
#include "usart.h"
#include "gpio.h"
#include "./sercom.h"
#include "../embedded_pktcom.h"
#include "../embedded_debug.h"
#include "../embedded_printbuf.h"
#include "../embedded_utils.h"

#define TX_BUF_SIZE 1024
#define RX_BUF_SIZE 256

typedef struct sercom_errors {
  int rx_overrun;
  int rx_hw_overrun;
  int rx_frame;
  int rx_parity;
#if defined(SERCOM_USE_PKT)
  int rx_pktshort;
  int rx_pktlong;
  int rx_pktcrc;
  int rx_pkttz;
  int rx_pktterm;
#endif
  int tx_busywait;
} sercom_errors;

struct sercom {

  volatile avr32_usart_t *usart;

  volatile U32 tx_head;
  volatile U32 tx_tail;

  volatile U32 rx_head;
  volatile U32 rx_tail;

  usart_options_t usart_options;

  sercom_errors errors;

  U8 tx_buf[TX_BUF_SIZE];
  U8 rx_buf[RX_BUF_SIZE];

};

sercom sercoms[N_SERCOMS];


#if N_SERCOMS>=1
__attribute__((__noinline__))
static void sercom_isr(volatile sercom *it)
{
  U32 csr = it->usart->csr;
  if (csr & AVR32_USART_CSR_RXRDY_MASK) {

    U32 next_head = (it->rx_head + 1) % RX_BUF_SIZE;
    U8 rx_char = it->usart->rhr;

    if (next_head == it->rx_tail) {
      it->errors.rx_overrun++;
    }
    else {
      it->rx_buf[it->rx_head] = rx_char;
      it->rx_head = next_head;
    }
  }
  if (csr & AVR32_USART_CSR_TXRDY_MASK) {
    if (it->tx_tail == it->tx_head) {
      it->usart->idr = AVR32_USART_CSR_TXRDY_MASK;
    } else {
      U8 tx_char = it->tx_buf[it->tx_tail];
      it->tx_tail = (it->tx_tail + 1) % TX_BUF_SIZE;
      it->usart->thr = tx_char;
    }
  }
  Bool need_rststa = 0;
  if (csr & AVR32_USART_CSR_OVRE_MASK) {
    it->errors.rx_hw_overrun++;
    need_rststa = 1;
  }
  if (csr & AVR32_USART_CSR_FRAME_MASK) {
    it->errors.rx_frame++;
    need_rststa = 1;
  }
  if (csr & AVR32_USART_CSR_PARE_MASK) {
    it->errors.rx_parity++;
    need_rststa = 1;
  }
  if (csr & AVR32_USART_CSR_PARE_MASK) {
    it->errors.rx_parity++;
    need_rststa = 1;
  }

  if (need_rststa) {
    it->usart->cr = AVR32_USART_CR_RSTSTA_MASK;
  }

}

#if N_SERCOMS>=1
__attribute__((__interrupt__))
static void usart0_isr( void )
{
  sercom_isr(&sercoms[0]);
}
#endif
#endif
#if N_SERCOMS>=2
__attribute__((__interrupt__))
static void usart1_isr( void )
{
  sercom_isr(&sercoms[1]);
}
#endif
#if N_SERCOMS>=3
__attribute__((__interrupt__))
static void usart2_isr( void )
{
  sercom_isr(&sercoms[2]);
}
#endif

void sercom_panic_write(volatile sercom *it, char const *s)
{
  // Kill interrupts
  it->usart->idr = AVR32_USART_CSR_TXRDY_MASK | AVR32_USART_CSR_RXRDY_MASK;

  while (*s) {
    usart_putchar(it->usart, *s);
    s++;
  }
  usart_putchar(it->usart, '\r');
  usart_putchar(it->usart, '\n');
}

void sercom_tx_u8(volatile sercom *it, U8 data)
{
  U32 next_head = (it->tx_head + 1) % TX_BUF_SIZE;

  // busy loop, waiting for space in buffer
  if (next_head == it->tx_tail) {
    it->errors.tx_busywait++;
    while (next_head == it->tx_tail) {}
  }

  it->tx_buf[it->tx_head] = data;
  it->tx_head = next_head;

  it->usart->ier = AVR32_USART_CSR_TXRDY_MASK;
}

void sercom_tx_needspace(volatile sercom *it)
{
  if (sercom_tx_qlen(it) > TX_BUF_SIZE-8) {
    it->errors.tx_busywait++;
    while (sercom_tx_qlen(it) > TX_BUF_SIZE-16) {}
  }
}

Bool sercom_tx_empty(volatile sercom *it)
{
  return it->tx_head == it->tx_tail;
}

int sercom_tx_qlen(volatile sercom *it)
{
  return (it->tx_head + TX_BUF_SIZE - it->tx_tail) % TX_BUF_SIZE;
}

int sercom_tx_qspace(volatile sercom *it)
{
  return TX_BUF_SIZE - (it->tx_head + TX_BUF_SIZE - it->tx_tail) % TX_BUF_SIZE;
}

void sercom_tx_drain(volatile sercom *it)
{
  while (!sercom_tx_empty(it)) {
  }
}

// ----------------------------------------------------------------------


void sercom_tx_str(volatile sercom *it, char const *s)
{
  while (*s) {
    if (*s == '\n') {
      sercom_tx_u8(it, '\r');
    }
    sercom_tx_u8(it, *s);
    s++;
  }
}

void sercom_tx_u8_hex(volatile sercom *it, U8 x)
{
  sercom_tx_u8(it, hex((x>>4)&0xf));
  sercom_tx_u8(it, hex((x>>0)&0xf));
}

void sercom_tx_u16_hex(volatile sercom *it, U16 x)
{
  sercom_tx_u8(it, hex((x>>12)&0xf));
  sercom_tx_u8(it, hex((x>>8)&0xf));
  sercom_tx_u8(it, hex((x>>4)&0xf));
  sercom_tx_u8(it, hex((x>>0)&0xf));
}

void sercom_tx_u32_hex(volatile sercom *it, U32 x)
{
  sercom_tx_u16_hex(it, x>>16);
  sercom_tx_u16_hex(it, x);
}

void sercom_tx_udec(volatile sercom *it, U32 x)
{
  Bool didsome=0;
  for (U32 placevalue=1000000000; placevalue; placevalue /= 10) {
    U32 digit = (x/placevalue)%10;
    if (digit) {
      didsome = 1;
    }
    if (didsome) {
      sercom_tx_u8(it, '0' + digit);
    }
  }
}

void sercom_tx_dec(volatile sercom *it, S32 x)
{
  if (x < 0) {
    sercom_tx_u8(it, '-');
    sercom_tx_udec(it, (U32)-x);
  } else {
    sercom_tx_udec(it, (U32)x);
  }
}

void sercom_tx_s8(volatile sercom *it, S8 x)
{
  sercom_tx_u8(it, (U8)x);
}

void sercom_tx_u16(volatile sercom *it, U16 x)
{
  sercom_tx_u8(it, x&0xff);
  sercom_tx_u8(it, x>>8);
}

void sercom_tx_s16(volatile sercom *it, S16 x)
{
  sercom_tx_u8(it, x&0xff);
  sercom_tx_u8(it, x>>8);
}

void sercom_tx_u32(volatile sercom *it, U32 x)
{
  sercom_tx_u16(it, x>>16);
  sercom_tx_u16(it, x);
}

// ----------------------------------------------------------------------


U8 sercom_rx_u8(volatile sercom *it)
{
  while (it->rx_head == it->rx_tail) {}

  U8 ret = it->rx_buf[it->rx_tail];
  it->rx_tail = (it->rx_tail + 1) % RX_BUF_SIZE;

  return ret;
}

U16 sercom_rx_u16(volatile sercom *it)
{
  U8 c1 = sercom_rx_u8(it);
  U8 c2 = sercom_rx_u8(it);
  return ((U16)c2)<<8 | (U16)c1;
}

U8 sercom_rx_peek(volatile sercom *it)
{
  while (it->rx_head == it->rx_tail) {}
  return it->rx_buf[it->rx_tail];
}

U8 sercom_rx_ahead(volatile sercom *it, int offset)
{
  while (sercom_rx_qlen(it) <= offset) {}
  return it->rx_buf[(it->rx_tail + offset) % RX_BUF_SIZE];
}

void sercom_rx_consume(volatile sercom *it, int len)
{
  it->rx_tail = (it->rx_tail + len) % RX_BUF_SIZE;
}

Bool sercom_rx_empty(volatile sercom *it)
{
  return it->rx_head == it->rx_tail;
}

int sercom_rx_qlen(volatile sercom *it)
{
  return (it->rx_head + RX_BUF_SIZE - it->rx_tail) % RX_BUF_SIZE;
}



// ----------------------------------------------------------------------

void sercom_set_comm(volatile sercom *it)
{
  /*
    There's a bug in usart_init_rs232 where it calculates 8/8 for the fractional part,
    which gets truncated to 0. The AVR32 folks at Atmel acknowledged it, so it'll probably be fixed
    in 1.3.1 or so.
    For now, diddle the baud rate in this special case.
   */
  if (it->usart_options.baudrate == 115200 && FPBA==16500000) {
    it->usart_options.baudrate = 114000;
  }
  // Have to cast volatile out of usart_options_t to avoid warning
  if (usart_init_rs232(it->usart, (usart_options_t *)&it->usart_options, FPBA) != USART_SUCCESS) {
    panic_printf("usart_init_rs232 failed");
  }
}

void sercom_init(int usarti, int baudrate)
{
  if (usarti<0 || usarti >= N_SERCOMS) {
    log_printf("Attempt to create sercom %d, but only %d configured", usarti, N_SERCOMS);
    return;
  }

  sercom *it = &sercoms[usarti];

  it->usart_options.baudrate = baudrate;
  it->usart_options.charlength   = 8;
  it->usart_options.paritytype   = USART_NO_PARITY;
  it->usart_options.stopbits     = USART_1_STOPBIT;
  it->usart_options.channelmode  = USART_NORMAL_CHMODE;

  switch(usarti) {
#if N_SERCOMS>=1
  case 0:
    it->usart = &SERCOM_USART0;
    gpio_enable_module_pin(SERCOM_USART0_RXD_PIN, SERCOM_USART0_RXD_FUNCTION);
    gpio_enable_module_pin(SERCOM_USART0_TXD_PIN, SERCOM_USART0_TXD_FUNCTION);
    break;
#endif
#if N_SERCOMS>=2
  case 1:
    it->usart = &SERCOM_USART1;
    gpio_enable_module_pin(SERCOM_USART1_RXD_PIN, SERCOM_USART1_RXD_FUNCTION);
    gpio_enable_module_pin(SERCOM_USART1_TXD_PIN, SERCOM_USART1_TXD_FUNCTION);
    break;
#endif
#if N_SERCOMS>=3
  case 2:
    it->usart = &SERCOM_USART2;
    gpio_enable_module_pin(SERCOM_USART2_RXD_PIN, SERCOM_USART2_RXD_FUNCTION);
    gpio_enable_module_pin(SERCOM_USART2_TXD_PIN, SERCOM_USART2_TXD_FUNCTION);
    break;
#endif
  default:
    panic_printf("sercom_create: bad usart number");
  }

  sercom_set_comm(it);

  Disable_global_interrupt();
  switch(usarti) {
#if N_SERCOMS>=1
  case 0:
    INTC_register_interrupt(&usart0_isr, SERCOM_USART0_IRQ, AVR32_INTC_INT0);
    break;
#endif
#if N_SERCOMS>=2
  case 1:
    INTC_register_interrupt(&usart1_isr, SERCOM_USART1_IRQ, AVR32_INTC_INT0);
    break;
#endif
#if N_SERCOMS>=3
  case 2:
    INTC_register_interrupt(&usart2_isr, SERCOM_USART2_IRQ, AVR32_INTC_INT0);
    break;
#endif
  }
  Enable_global_interrupt();

}

// ----------------------------------------------------------------------

#if defined(SERCOM_USE_PKT)
pkt_rx_buf *sercom_rx_pkt(volatile sercom *it)
{
  int looki = 0;
  int outi = 0;
  pkt_rx_buf *rx = NULL;

  int chunk_rem=0;
  int chunk_lastflag=0;
  int chunk_zeroflag=0;

  while (looki < sercom_rx_qlen(it)) {

    U8 data = sercom_rx_ahead(it, looki);
    looki++;

    if (data==0) {
      if (outi>0) {
        it->errors.rx_pktterm++;
      }
      chunk_rem = 0;
      chunk_lastflag = 0;
      chunk_zeroflag = 0;
      sercom_rx_consume(it, looki);
      outi = 0;
      looki = 0;
      continue;
    }

    if (!rx) {
      rx = alloc_rx_buf();
      rx->len = rx->rdpos = 0;
      rx->overrun = 0;
    }

    if (outi >= rx->alloc_size-2) {
      it->errors.rx_pktlong++;
      sercom_rx_consume(it, looki);
      looki = 0;
      outi = 0;
      continue;
    }

    if (chunk_rem == 0) {
      chunk_rem = (data&0x3f);
      chunk_lastflag = (data&0x80) ? 1 : 0;
      chunk_zeroflag = (data&0x40) ? 1 : 0;
    }
    else if (chunk_rem > 1) {
      rx->buf[outi++] = data;
      chunk_rem--;
    }

    if (chunk_rem == 1) {
      if (chunk_zeroflag) {
        rx->buf[outi++] = 0;
      }
      chunk_rem=0;

      if (chunk_lastflag) {
        if (outi < 5) {
          it->errors.rx_pktshort++;
        }
        else {
          rx->len = outi;
          if (pkt_rx_eat_crc(rx)) {
            sercom_rx_consume(it, looki);
            return rx;
          } else {
            sercom_rx_consume(it, looki);
            it->errors.rx_pktcrc++;
          }
        }
      }
    }
  }
  if (rx) free_rx_buf(rx);
  return NULL;
}

Bool sercom_tx_pkt_timeout(volatile sercom *it, pkt_tx_buf *tx, int timeout)
{

  if (sercom_tx_qspace(it) < (tx->len * 33 / 32 + 10)) {
    free_tx_buf(tx);
    return 0;
  }

  sercom_tx_u8(it, 0); // start with zero byte

  U8 checksum[4];
  for (int j=0; j<4; j++) checksum[j]=0;
  int chunk_len = 0;
  U8 chunk_buf[64];

  for (int i=0; i<tx->len; i++) {
    U8 data = tx->buf[i];
    if (data==0) {
      sercom_tx_u8(it, (chunk_len + 1) | 0x40);
      for (int i=0; i<chunk_len; i++) {
        sercom_tx_u8(it, chunk_buf[i]);
      }
      chunk_len = 0;
    } else {
      chunk_buf[chunk_len++] = data;
      if (chunk_len == 0x38) { // max 56 byte chunks

        sercom_tx_u8(it, chunk_len + 1);
        for (int i=0; i<chunk_len; i++) {
          sercom_tx_u8(it, chunk_buf[i]);
        }
        chunk_len = 0;
      }
    }
  }

  sercom_tx_u8(it, (chunk_len + 1) | 0x80);
  for (int i=0; i<chunk_len; i++) {
    sercom_tx_u8(it, chunk_buf[i]);
  }

  free_tx_buf(tx);
  return 1;
}

void sercom_tx_pkt(volatile sercom *it, pkt_tx_buf *tx)
{
  sercom_tx_pkt_timeout(it, tx, 0);
}

Bool sercom_tx_pkt_nowait(volatile sercom *it, pkt_tx_buf *tx)
{
  return sercom_tx_pkt_timeout(it, tx, 1);
}
#endif

// ----------------------------------------------------------------------

void sercom_report_errors(volatile sercom *it)
{
  printbuf out;

  Bool didsome = 0;

#define DO_ERROR(NAME) \
  if (it->errors.NAME) { \
    if (!didsome) { \
      printbuf_init(&out); \
      printbuf_append_str(&out, "sercom:"); \
      didsome = 1; \
    } \
    printbuf_printf(&out, " " #NAME "=%d", (int)it->errors.NAME);   \
    it->errors.NAME = 0; \
  }

  DO_ERROR(rx_overrun);
  DO_ERROR(rx_hw_overrun);
  DO_ERROR(rx_frame);
  DO_ERROR(rx_parity);
#if defined(SERCOM_USE_PKT)
  DO_ERROR(rx_pktshort);
  DO_ERROR(rx_pktlong);
  DO_ERROR(rx_pktcrc);
  DO_ERROR(rx_pkttz);
  DO_ERROR(rx_pktterm);
#endif
  DO_ERROR(tx_busywait);

  if (didsome) {
    printbuf_terminate(&out);
    debug_printbuf(&out);
  }
}

void sercom_poll(sercom *it)
{
}

void sercom_setup()
{
#if N_SERCOMS > 0
  sercom_init(0, 115200);
  host_sercom = &sercoms[0];
#endif
#if N_SERCOMS > 1
  sercom_init(1, 115200);
#endif
#if N_SERCOMS > 2
  sercom_init(2, 115200);
#endif

}

// ======================================================================

void sercom_log_status(sercom *it, char const *name)
{
#if 0
  debug_printf("  baudrate=%d charlength=%d paritytype=%d stopbits=%d channelmode=%d",
               it->usart_options.baudrate, it->usart_options.charlength, it->usart_options.paritytype,
	       it->usart_options.stopbits, it->usart_options.channelmode);
  debug_printf("  tx_q: %d %d  rx_q: %d %d",
               it->tx_head, it->tx_tail, it->rx_head, it->rx_tail);
  debug_printf("  usart: mr=%x csr=%x brgr=%x",
               it->usart->mr, it->usart->csr, it->usart->brgr);
#endif
}
