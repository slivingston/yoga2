#include "common/std_headers.h"
#include "../embedded_debug.h"

void test_byte(volatile U8 *p, U8 value)
{
    U8 orig = *p;

    *p = value;
    if (*p != value) log_printf("Mismatch %02x->%02x at %x\n", value, *p, p);
    *p = orig;
}

void test_memory()
{
  for (volatile U8 *p=0; p<(U8 *)50000; p++) {
    test_byte(p, 0x00);
    test_byte(p, 0xff);
    test_byte(p, 0x55);
    test_byte(p, 0xaa);
  }
}
