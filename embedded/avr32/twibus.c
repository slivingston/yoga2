#include "common/std_headers.h"
#include "hwdefs.h"
#include "gpio.h"
#include "intc.h"
#include "twi.h"
#include "../embedded_debug.h"
#include "../embedded_timing.h"
#include "../avr32/twibus.h"

twibus twibus0;

// ----------------------------------------------------------------------

static void twibus_transaction_set_mode_idle(volatile twibus_transaction_state *st)
{
  st->twi->idr = ~0;
  st->mode = TWIBUS_MODE_IDLE;
  st->twi->cr = AVR32_TWI_CR_MSDIS_MASK;
  st->twi->ier = 0;
}

static void twibus_transaction_set_mode_tx(volatile twibus_transaction_state *st)
{
  st->twi->idr = ~AVR32_TWI_IER_TXRDY_MASK;
  st->mode = TWIBUS_MODE_TX;
  st->twi->ier = AVR32_TWI_IER_TXRDY_MASK | AVR32_TWI_IER_NACK_MASK;
  st->twi->MMR.mread = 0;
  st->twi->cr = AVR32_TWI_CR_MSEN_MASK;
}

static void twibus_transaction_set_mode_tx_wait(volatile twibus_transaction_state *st)
{
  st->twi->idr = ~AVR32_TWI_IER_TXCOMP_MASK;
  st->mode = TWIBUS_MODE_TX_WAIT;
  st->twi->ier = AVR32_TWI_IER_TXCOMP_MASK;
}

static void twibus_transaction_set_mode_tx_complete(volatile twibus_transaction_state *st)
{
  st->twi->idr = ~0;
  st->mode = TWIBUS_MODE_TX_COMPLETE;
  st->twi->ier = 0;
}

static void twibus_transaction_set_mode_rx(volatile twibus_transaction_state *st)
{
  st->twi->idr = ~AVR32_TWI_IER_RXRDY_MASK;
  st->mode = TWIBUS_MODE_RX;
  st->twi->MMR.mread = 1;
  st->twi->ier = AVR32_TWI_IER_RXRDY_MASK;
  st->twi->cr = AVR32_TWI_CR_MSEN_MASK;
  if (st->rx_len==1) {
    st->twi->cr = AVR32_TWI_CR_START_MASK | AVR32_TWI_CR_STOP_MASK;
  } else {
    st->twi->cr = AVR32_TWI_CR_START_MASK; // kick it off
  }
}

static void twibus_transaction_set_mode_rx_complete(volatile twibus_transaction_state *st)
{
  st->twi->idr = ~0;
  st->mode = TWIBUS_MODE_RX_COMPLETE;
  st->twi->cr = AVR32_TWI_CR_MSDIS_MASK;
  st->twi->ier = 0;
}

static void twibus_transaction_isr(volatile twibus_transaction_state *st)
{
  st->counts.total_interrupts++;
  U32 status  = st->twi->sr;
  if (status & AVR32_TWI_SR_RXRDY_MASK) {
    U8 data = st->twi->rhr;
    if (st->mode == TWIBUS_MODE_RX) {
      if (st->rx_pos < st->rx_len) {
        st->rx_buf[st->rx_pos] = data;
        st->rx_pos++;
      }
      if (st->rx_pos == st->rx_len-1) {
        st->twi->cr = AVR32_TWI_STOP_MASK;
      }
      else if (st->rx_pos == st->rx_len) {
        twibus_transaction_set_mode_rx_complete(st);
      }
    } else {
      st->counts.spurious_reads++;
    }
  }
  else if ((status & AVR32_TWI_SR_TXRDY_MASK) && st->mode == TWIBUS_MODE_TX) {
    if (st->tx_pos < st->tx_len) {
      U8 data = st->tx_buf[st->tx_pos];
      st->twi->thr = data;
      st->tx_pos++;
    } else {
      twibus_transaction_set_mode_tx_wait(st);
    }
  }
  else if ((status & AVR32_TWI_SR_NACK_MASK) && st->mode == TWIBUS_MODE_TX) {
    st->counts.nacks++;
    twibus_transaction_set_mode_idle(st);
  }
  else if ((status & AVR32_TWI_SR_TXCOMP_MASK) && st->mode == TWIBUS_MODE_TX_WAIT) {
    if (st->rx_len > 0) {
      twibus_transaction_set_mode_rx(st);
    } else {
      twibus_transaction_set_mode_tx_complete(st);
    }
  }
  else {
    st->counts.spurious_interrupts++;
    twibus_transaction_set_mode_idle(st);
  }
}

__attribute__((__interrupt__))
static void twibus0_isr()
{
  volatile twibus_transaction_state *st = &twibus0.transaction;
  twibus_transaction_isr(st);
}

// ----------------------------------------------------------------------
// Upward API of transaction engine

void twibus_transaction_setup_hw(volatile twibus_transaction_state *st)
{
  st->twi->idr = ~0; // disable all interrupts
  st->twi->sr; // clear status register by reading it
  st->twi->cr = AVR32_TWI_CR_SWRST_MASK; // reset the module
  st->twi->cr = AVR32_TWI_CR_SVDIS_MASK; // abolish slavery
  st->twi->sr;

  // Prescale by 2 and divide by 165 to turn 16.5 MHZ into 50 kbits
  st->twi->CWGR.ckdiv = 1;
  st->twi->CWGR.cldiv = 165;
  st->twi->CWGR.chdiv = 165;

  st->mode = TWIBUS_MODE_IDLE;
}

void twibus_transaction_start(volatile twibus_transaction_state *st, int device_addr)
{
  st->tx_pos = 0;
  st->rx_pos = 0;

  st->twi->iadr = st->regaddr;
  st->twi->MMR.iadrsz = st->regaddrlen;
  st->twi->MMR.dadr = device_addr;
  st->transaction_start_time = get_ticks();

  if (st->tx_len) {
    twibus_transaction_set_mode_tx(st);
  } else {
    twibus_transaction_set_mode_rx(st);
  }

  if (0) debug_printf("twibus_transaction_start tx_len=%d rx_len=%d addr=0x%02x", st->tx_len, st->rx_len, device_addr);

}

// ----------------------------------------------------------------------

void twibus_add_engine(twibus *it, twibus_engine *engine)
{
  if (it->n_engines >= TWIBUS_MAX_ENGINES) {
    debug_printf("Exceeded TWIBUS_MAX_ENGINES");
    return;
  }

  it->engines[it->n_engines++] = engine;
}

void twibus_log_status(twibus *it, char const *name)
{
}

void twibus_poll(twibus *it)
{
  if (it->n_engines == 0) return;

  if (it->transaction.mode == TWIBUS_MODE_TX || it->transaction.mode == TWIBUS_MODE_RX || it->transaction.mode == TWIBUS_MODE_TX_WAIT) {
    U32 now = get_ticks();
    int dur = now - it->transaction.transaction_start_time;
    if (dur > SECONDSTOTICKS(0.2)) {

      if (0) debug_printf("twibus_transaction stalled in state %d, engine %d. si=%d nacks=%d ti=%d ",
                          it->transaction.mode,
			  it->active_engine,
                          it->transaction.counts.spurious_interrupts,
                          it->transaction.counts.nacks,
                          it->transaction.counts.total_interrupts);

      //twibus_transaction_setup_hw(&it->transaction);
      // Timeout, give up and go back to idle mode
      twibus_transaction_set_mode_idle(&it->transaction);
    }
  }

  twibus_engine *engine = it->engines[it->active_engine];
  if (!engine) {
    it->active_engine = 0; // Paranoia
    return;
  }

  if ((*engine->run_func)(engine, &it->transaction)) {
    it->active_engine = (it->active_engine+1) % it->n_engines;
  }
}

void twibus_setup()
{
  gpio_enable_module_pin(TWIBUS_SDA_PIN, TWIBUS_SDA_FUNCTION);
  gpio_enable_module_pin(TWIBUS_SCL_PIN, TWIBUS_SCL_FUNCTION);

  twibus0.transaction.twi = &TWIBUS_TWI;
  twibus0.n_engines = 0;
  twibus0.active_engine = 0;
  for (int i=0; i<TWIBUS_MAX_ENGINES; i++) twibus0.engines[i] = NULL;

  twibus_transaction_setup_hw(&twibus0.transaction);
  INTC_register_interrupt(&twibus0_isr, AVR32_TWI_IRQ, AVR32_INTC_INT2);
}

// ----------------------------------------------------------------------

void twibus_explorer_start_transaction_rd_reg8(volatile twibus_transaction_state *st, int devaddr, int regaddr)
{
  st->regaddr = regaddr;
  st->regaddrlen = 1;
  st->tx_len = 0;
  st->rx_len = 1;
  twibus_transaction_start(st, devaddr);
}

U8 twibus_explorer_collect_transaction_rd_reg8(volatile twibus_transaction_state *st)
{
  return st->rx_buf[0];
}


int twibus_explorer_engine_run(twibus_explorer_engine *it, volatile twibus_transaction_state *st)
{
  int tmode = st->mode;
  int tmode_idle = (tmode == TWIBUS_MODE_IDLE ||
                    tmode == TWIBUS_MODE_RX_COMPLETE ||
                    tmode == TWIBUS_MODE_TX_COMPLETE);

  switch (it->state) {
  case 0:
    if (tmode_idle) {
      twibus_explorer_start_transaction_rd_reg8(st, it->devaddr, 0);
      it->state=1;
    }
    return 0;

  case 1:
    if (tmode == TWIBUS_MODE_RX_COMPLETE) {
      U8 data = twibus_explorer_collect_transaction_rd_reg8(st);
      debug_printf("  twibus[0x%02x] = %02x ti=%d", it->devaddr, (int)data, st->counts.total_interrupts);
      it->devaddr++;
      if (it->devaddr<128) {
        it->state = 0;
      } else {
        it->state = 2;
      }
      return 1;
    }
    else if (tmode_idle) {
      debug_printf("twibus_explorer transaction failed (devaddr=0x%02x, tmode=%d)", it->devaddr, tmode);
      it->devaddr++;
      if (it->devaddr<128) {
        it->state = 0;
      } else {
        it->state = 2;
      }
      return 1;
    }
    return 0;

  case 2:
    return 1;

  default:
    it->state = 0;
    return 1;
  }
}


void twibus_explorer_engine_create(twibus_explorer_engine *it, twibus *bus)
{
  it->super.run_func = (int (*)(twibus_engine *, volatile twibus_transaction_state *st)) twibus_explorer_engine_run;
  it->devaddr = 0x40;
  twibus_add_engine(bus, &it->super);
}
