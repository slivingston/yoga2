#pragma once
#include "../dspcore/dspcore.h"

typedef struct debounce {

  int on;

  dsp824 cur;
  dsp824 lo_lim, hi_lim;

} debounce;

int debounce_run(debounce *it, int curvalue, dsp824 dt);
void debounce_init(debounce *it, dsp824 cur, dsp824 lo_lim, dsp824 hi_lim);
