/*
  If syslog_ep is set, this uses syslog. See https://tools.ietf.org/html/rfc3164

 */
#include "common/std_headers.h"
#include "./embedded_printbuf.h"
#include "./embedded_debug.h"
#include "./embedded_timing.h"
#include "./embedded_hostif.h"
#include "./avr32/sercom.h"
#include "./avr32/ethcom.h"

#if defined(USE_DEBUG_PACKETS)
pkt_endpoint syslog_ep;
#endif


void panic_printbuf(printbuf *out)
{
#ifndef __TARG_SIMULATOR__
  printbuf_terminate(out);
  panic_str(out->buf);
#else
  while(1);
#endif

}

void panic_printf(char const *format, ...)
{
  va_list ap;
  va_start(ap,format);

  printbuf out;
  printbuf_init(&out);
  printbuf_vprintf(&out, format, ap);
  printbuf_terminate(&out);
  panic_printbuf(&out);
}

#define N_PENDING_ERRORS 15
#define PENDING_MSG_SIZE PRINTBUF_SIZE

struct pending_error_t {
  int count;
  char msg[PENDING_MSG_SIZE];
};

struct pending_error_t pending_errors[N_PENDING_ERRORS];
int pending_error_overflows;

#ifndef __TARG_SIMULATOR__
void log_str(const char *msg)
{
  int msglen = strlen(msg);
  if (msglen > PENDING_MSG_SIZE - 1) {
    msg += PENDING_MSG_SIZE - 1 - msglen;
    msglen = PENDING_MSG_SIZE - 1;
  }

  for (int i=0; i<N_PENDING_ERRORS; i++) {
    if (!strcmp(msg, pending_errors[i].msg)) {
      pending_errors[i].count++;
      return;
    }
  }
  for (int i=0; i<N_PENDING_ERRORS; i++) {
    if (pending_errors[i].count==0) {
      strcpy(pending_errors[i].msg, msg);
      pending_errors[i].count++;
      return;
    }
  }
  pending_error_overflows++;

  return;
}

void log_flush()
{

  if (0) {
  }
#if defined(USE_DEBUG_PACKETS)
  else if (pkt_ep_valid(&syslog_ep)) {
    for (int i=0; i<N_PENDING_ERRORS; i++) {
      if (pending_errors[i].count > 0) {
        pkt_tx_buf *tx = ethcom_start_tx_udp(&ethcom0, &syslog_ep);
        if (!tx) break;
        pkt_tx_str(tx, "<15>");

        pkt_tx_str(tx, pending_errors[i].msg);
        if (pending_errors[i].count > 1) {
          pkt_tx_str(tx, " (");
          pkt_tx_dec(tx, pending_errors[i].count);
          pkt_tx_str(tx, " times)");
        }
        pending_errors[i].count = 0;
        ethcom_tx_pkt(&ethcom0, tx);

      }
    }
  }
#endif
#if N_SERCOMS >= 1
  else if (host_sercom) {
    for (int i=0; i<N_PENDING_ERRORS; i++) {
      if (pending_errors[i].count > 0) {
        if (sercom_tx_qspace(host_sercom) < strlen(pending_errors[i].msg)+10) {
          return;
        }
        sercom_tx_str(host_sercom, pending_errors[i].msg);
        if (pending_errors[i].count > 1) {
          sercom_tx_str(host_sercom, " (");
          sercom_tx_dec(host_sercom, pending_errors[i].count);
          sercom_tx_str(host_sercom, " times)");
        }
        sercom_tx_str(host_sercom, "\r\n");
      }
      pending_errors[i].count = 0;
    }
  }
#endif

  if (pending_error_overflows) {
    if (0) {
    }
#if defined(USE_DEBUG_PACKETS)
    if (pkt_ep_valid(&syslog_ep)) {
      pkt_tx_buf *tx = ethcom_start_tx_udp(&ethcom0, &syslog_ep);
      if (!tx) return;
      pkt_tx_str(tx, "<15>Pending error overflows: ");
      pkt_tx_dec(tx, pending_error_overflows);

      ethcom_tx_pkt(&ethcom0, tx);

      pending_error_overflows = 0;
    }
#endif
#if N_SERCOMS >= 1
    else if (host_sercom) {
      printbuf out;
      printbuf_printf(&out, "Pending error overflows: %d", pending_error_overflows);
      printbuf_terminate(&out);
      if (sercom_tx_qspace(host_sercom) < out.len+10) {
        return;
      }
      sercom_tx_str(host_sercom, out.buf);
      pending_error_overflows = 0;
    }
#endif
  }
}
#endif

void log_printbuf(printbuf *out)
{
#ifndef __TARG_SIMULATOR__
  printbuf_terminate(out);
  log_str(out->buf);
#endif
}

void log_printf(const char *format, ...)
{
  va_list ap;
  va_start(ap,format);

  printbuf out;
  printbuf_init(&out);
  printbuf_vprintf(&out, format, ap);
  printbuf_terminate(&out);
  log_printbuf(&out);
}


#ifndef __TARG_SIMULATOR__
void debug_str(char const *str)
{
  log_flush();

  if (0) {
  }
#if defined(USE_DEBUG_PACKETS)
  else if (pkt_ep_valid(&syslog_ep)) {
    pkt_tx_buf *tx = NULL;
    U32 busywait_start = get_ticks();
    while (true) {
      tx = ethcom_start_tx_udp(&ethcom0, &syslog_ep);
      if (tx) break;
      if (get_ticks() - busywait_start > SECONDSTOTICKS(0.1)) {
        return;
      }
    }
    pkt_tx_str(tx, "<15>");
    pkt_tx_str(tx, str);
    ethcom_tx_pkt(&ethcom0, tx);
  }
#endif
#if N_SERCOMS >= 1
  else if (host_sercom != 0) {
    sercom_tx_str(host_sercom, str);
    sercom_tx_str(host_sercom, "\r\n");
  }
#endif
}
#endif

void debug_printbuf(printbuf *out)
{
  printbuf_terminate(out); // should already be done, but doesn't hurt to be safe
#ifndef __TARG_SIMULATOR__
  debug_str(out->buf);
#endif
}

void debug_printf(const char *format, ...)
{
  va_list ap;
  va_start(ap,format);

  printbuf out;
  printbuf_init(&out);
  printbuf_vprintf(&out, format, ap);
  printbuf_terminate(&out);

  debug_printbuf(&out);
}



void panic_assert(const char *filename, int lineno, const char *msg)
{
  panic_printf("%s at %s:%d", msg, filename, lineno);
}
