#pragma once
#include "./embedded_pktcom.h"
#include "./embedded_printbuf.h"
#include "./embedded_timing.h"

/*
  If true, writes to the console are wrapped in packets (type='!'). Otherwise they're sent as plain text
  to a console
 */
#if defined(USE_DEBUG_PACKETS)
extern period_generator debug_use_packets;
extern pkt_endpoint debug_ep;
extern pkt_endpoint syslog_ep;
#endif

/*
  These versions complete asynchronously, storing the message in a queue for later flushing to the console.
  Duplicate messages in the queue are counted rather than stored multiple times, so it's OK
  if you blast the same error message a lot.
 */
void log_str(const char *msg);
void log_printbuf(printbuf *out);
void log_printf(const char *format, ...);
void log_flush();

#ifndef __cplusplus
#define flog_printf(FORMAT, ...) log_printf(__FILE__ ": " FORMAT, __VA_ARGS__)
#endif

/*
  Write to the console. Can slow things down.
 */
void debug_str(const char *msg);
void debug_printbuf(printbuf *out);
void debug_printf(const char *format, ...);
#ifndef __cplusplus
#define fdebug_printf(FORMAT, ...) debug_printf(__FILE__ ": " FORMAT, __VA_ARGS__)
#endif

/*
  For when you're really boned. These turn off interrupts and go into an infinite loop
  printing this out the serial port and blinking it on the LEDs. Appropriate for initialization errors
  or some sort of data corruption.
 */
void panic_str(char const *message) __attribute__((noreturn));
void panic_printf(char const *format, ...) __attribute__((noreturn));
void panic_printbuf(printbuf *out) __attribute__((noreturn));
void panic_assert(char const *filename, int line, char const *msg);

#define assert(e) ((e) ? (void)0 : panic_assert(__FILE__, __LINE__, #e))

/*
  On the simulator, like debug_printf (which goes to stderr, though it still uses out printbuf routines.)
  On the microcontroller it does nothing.
*/
#ifdef __EMBEDDED__
#ifdef __TARG_SIMULATOR__
#define simulator_printf debug_printf
#else
#define simulator_printf(...) \
  do {                        \
  } while (0)
#endif
#else

#define log_printf eprintf
#define debug_printf eprintf
#define simulator_printf eprintf

#endif
