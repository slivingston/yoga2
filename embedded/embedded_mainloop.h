#pragma once
#include "./embedded_timing.h"
#include "../dspcore/dspcore.h"

#define MAINLOOP_DT 0.001 // seconds
#define MAINLOOP_INVERSE_DT (1.0 / MAINLOOP_DT)

typedef void (*mainloop_poll_t)(void *);
typedef void (*mainloop_log_t)(void *, char const *);

typedef struct mainloop_entry {
  char const *name;
  mainloop_poll_t poll_func;
  mainloop_poll_t fastpoll_func;
  mainloop_log_t log_func;
  void *cookie;
  U32 slowest_ticks;
} mainloop_entry;

#define MAINLOOP_MAX_ENTRIES 24

typedef struct mainloop {

  U32 tickno;
  U32 tickmask;
  dsp824 time;
  dsp824 last_log_time, last_log_status_time, last_beacon_time;

  rate_generator rg;

  int n_entries;
  mainloop_entry entries[MAINLOOP_MAX_ENTRIES];

} mainloop;

extern struct mainloop mainloop0;

void mainloop_add_entry(
    mainloop *it,
    char const *name,
    mainloop_poll_t poll_func,
    mainloop_poll_t fastpoll_func,
    mainloop_log_t log_func,
    void *cookie);
void mainloop_poll();
void mainloop_setup();

void mainloop_log_all(mainloop *it);
void mainloop_poll_all(mainloop *it);
void mainloop_fastpoll_all(mainloop *it);
