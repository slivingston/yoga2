#include "common/std_headers.h"
#include "./embedded_pktcom.h"
#include "./embedded_debug.h"
#include "./embedded_utils.h"
#ifndef BYTE_ORDER
#include <sys/endian.h>
#endif

/*
  Packet-oriented communications between embedded system and host. In
  general the reference format is that of the host, so everything is
  sent little-endian.
*/

#if defined(PKT_USE_CRC)
static const U8 crc32_table[256][4] = {
#include "build.src/crc32.c"
};
#endif

// Allocation ----------------------------------------------------------------------

enum {
  MAX_FREE_TX_BUFS=16,
  MAX_FREE_RX_BUFS=16,
};

static pkt_tx_buf *free_tx_bufs[MAX_FREE_TX_BUFS];
static pkt_rx_buf *free_rx_bufs[MAX_FREE_RX_BUFS];

int free_tx_buf_overflows;
int free_rx_buf_overflows;

pkt_tx_buf *alloc_tx_buf()
{
  for (int i=0; i<MAX_FREE_TX_BUFS; i++) {
    if (free_tx_bufs[i]) {
      pkt_tx_buf *ret = free_tx_bufs[i];
      ret->len = 0;
      ret->overrun = 0;
      ret->ip4_hdr_pos = -1;
      ret->udp_hdr_pos = -1;
      ret->icmp_hdr_pos = -1;
      free_tx_bufs[i] = NULL;
      return ret;
    }
  }
  return NULL;
}

void free_tx_buf(pkt_tx_buf *it)
{
  if (it == NULL) return;
  for (int i=0; i<MAX_FREE_TX_BUFS; i++) {
    if (free_tx_bufs[i] == NULL) {
      free_tx_bufs[i] = it;
      return;
    }
  }
  free_tx_buf_overflows ++;
}

pkt_rx_buf *alloc_rx_buf()
{
  for (int i=0; i<MAX_FREE_RX_BUFS; i++) {
    if (free_rx_bufs[i]) {
      pkt_rx_buf *ret = free_rx_bufs[i];
      ret->len = 0;
      ret->rdpos = 0;
      ret->overrun = 0;
      memset(&ret->src, 0, sizeof(pkt_endpoint));
      free_rx_bufs[i] = NULL;
      return ret;
    }
  }
  return NULL;
}

void free_rx_buf(pkt_rx_buf *it)
{
  if (it == NULL) return;

  for (int i=0; i<MAX_FREE_RX_BUFS; i++) {
    if (free_rx_bufs[i] == NULL) {
      free_rx_bufs[i] = it;
      return;
    }
  }
  free_rx_buf_overflows ++;
}

bool pkt_ep_valid(pkt_endpoint *ep) {
  return ep->udp_port && (ep->mac_addr[0] || ep->mac_addr[1] || ep->mac_addr[2] || ep->mac_addr[3] || ep->mac_addr[4] || ep->mac_addr[5]);
}


// ----------------------------------------------------------------------

/*
  You can add more buffers here, subject to memory limitations. Be sure to increase
  MAX_FREE_*_BUFS above
 */

struct { pkt_tx_buf it; U8 space[PKT_MAX_SIZE]; } pkt_tx_buf_max[8];
struct { pkt_rx_buf it; U8 space[PKT_MAX_SIZE]; } pkt_rx_buf_max[4];

void pkt_setup()
{
#define LOADRX(X, SIZE) for (int i=0; i<sizeof(X)/sizeof(X[0]); i++) { X[i].it.alloc_size = SIZE; free_rx_buf(&X[i].it); }
#define LOADTX(X, SIZE) for (int i=0; i<sizeof(X)/sizeof(X[0]); i++) { X[i].it.alloc_size = SIZE; free_tx_buf(&X[i].it); }
  LOADTX(pkt_tx_buf_max, PKT_MAX_SIZE);
  LOADRX(pkt_rx_buf_max, PKT_MAX_SIZE);
}


// ----------------------------------------------------------------------

void pkt_tx_start(pkt_tx_buf *it)
{
  it->len = 0;
  it->overrun = 0;
}

#if defined(PKT_USE_CRC)
void update_crc(U8 data, U8 crc[4])
{
  // shift right 8
  U8 out = crc[3];
  crc[3] = crc[2] ^ crc32_table[out][3];
  crc[2] = crc[1] ^ crc32_table[out][2];
  crc[1] = crc[0] ^ crc32_table[out][1];
  crc[0] = data   ^ crc32_table[out][0];
}
#endif

void pkt_tx_u8p(pkt_tx_buf *it, const U8 *p)
{
  if (it->len >= it->alloc_size) {
    it->overrun++;
    return;
  }
  it->buf[it->len++] = *p;
}

void pkt_tx_str(pkt_tx_buf *it, const char *s)
{
  while (*s) {
    pkt_tx_u8p(it, (U8 *)s);
    s++;
  }
}

void pkt_tx_mem(pkt_tx_buf *it, const void *s, unsigned int n)
{
  if (it->len + n > it->alloc_size) {
    it->overrun += n;
    return;
  }
  memcpy(&it->buf[it->len], s, n);
  it->len += n;
}

/*
  AVR32 is 4321, intel is 1234
 */
void pkt_tx_u16p(pkt_tx_buf *it, const U16 *p)
{
  if (it->len + 2 > it->alloc_size) {
    it->overrun += 2;
    return;
  }
  it->buf[it->len + 0] = (U8)(*p >> 0);
  it->buf[it->len + 1] = (U8)(*p >> 8);
  it->len += 2;
}

void pkt_tx_netu16p(pkt_tx_buf *it, const U16 *p)
{
  if (it->len + 2 > it->alloc_size) {
    it->overrun += 2;
    return;
  }
  it->buf[it->len + 0] = (U8)(*p >> 8);
  it->buf[it->len + 1] = (U8)(*p >> 0);
  it->len += 2;
}

void pkt_tx_u32p(pkt_tx_buf *it, const U32 *p)
{
  if (it->len + 4 > it->alloc_size) {
    it->overrun += 4;
    return;
  }
  it->buf[it->len + 0] = (U8)(*p >> 0);
  it->buf[it->len + 1] = (U8)(*p >> 8);
  it->buf[it->len + 2] = (U8)(*p >> 16);
  it->buf[it->len + 3] = (U8)(*p >> 24);
  it->len += 4;
}
void pkt_tx_netu32p(pkt_tx_buf *it, const U32 *p)
{
  if (it->len + 4 > it->alloc_size) {
    it->overrun += 4;
    return;
  }
  it->buf[it->len + 0] = (U8)(*p >> 24);
  it->buf[it->len + 1] = (U8)(*p >> 16);
  it->buf[it->len + 2] = (U8)(*p >> 8);
  it->buf[it->len + 3] = (U8)(*p >> 0);
  it->len += 4;
}

void pkt_tx_u64p(pkt_tx_buf *it, const U64 *p)
{
  if (it->len + 8 > it->alloc_size) {
    it->overrun += 8;
    return;
  }
  it->buf[it->len + 0] = (U8)(*p >> 0);
  it->buf[it->len + 1] = (U8)(*p >> 8);
  it->buf[it->len + 2] = (U8)(*p >> 16);
  it->buf[it->len + 3] = (U8)(*p >> 24);
  it->buf[it->len + 4] = (U8)(*p >> 32);
  it->buf[it->len + 5] = (U8)(*p >> 40);
  it->buf[it->len + 6] = (U8)(*p >> 48);
  it->buf[it->len + 7] = (U8)(*p >> 56);
  it->len += 8;
}
void pkt_tx_netu64p(pkt_tx_buf *it, const U64 *p)
{
  if (it->len + 8 > it->alloc_size) {
    it->overrun += 8;
    return;
  }
  it->buf[it->len + 0] = (U8)(*p >> 56);
  it->buf[it->len + 1] = (U8)(*p >> 48);
  it->buf[it->len + 2] = (U8)(*p >> 40);
  it->buf[it->len + 3] = (U8)(*p >> 32);
  it->buf[it->len + 4] = (U8)(*p >> 24);
  it->buf[it->len + 5] = (U8)(*p >> 16);
  it->buf[it->len + 6] = (U8)(*p >> 8);
  it->buf[it->len + 7] = (U8)(*p >> 0);
  it->len += 8;
}

void pkt_tx_s16p(pkt_tx_buf *it, const S16 *p) {   pkt_tx_u16p(it, (const U16 *)p); }
void pkt_tx_s32p(pkt_tx_buf *it, const S32 *p) {   pkt_tx_u32p(it, (const U32 *)p); }
void pkt_tx_s64p(pkt_tx_buf *it, const S64 *p) {   pkt_tx_u64p(it, (const U64 *)p); }
void pkt_tx_intp(pkt_tx_buf *it, const int *p) {   pkt_tx_u32p(it, (const U32 *)p); }

void pkt_tx_dsp88p(pkt_tx_buf *it, const dsp88 *p)     {   pkt_tx_u16p(it, (const U16 *)p); }
void pkt_tx_dsp412p(pkt_tx_buf *it, const dsp412 *p)   {   pkt_tx_u16p(it, (const U16 *)p); }

void pkt_tx_dsp1616p(pkt_tx_buf *it, const dsp1616 *p) {   pkt_tx_u32p(it, (const U32 *)p); }
void pkt_tx_dsp824p(pkt_tx_buf *it, const dsp824 *p)   {   pkt_tx_u32p(it, (const U32 *)p); }
void pkt_tx_dsp230p(pkt_tx_buf *it, const dsp230 *p)   {   pkt_tx_u32p(it, (const U32 *)p); }

void pkt_tx_dsp4024p(pkt_tx_buf *it, const dsp4024 *p) {   pkt_tx_u64p(it, (const U64 *)p); }
void pkt_tx_dsp3232p(pkt_tx_buf *it, const dsp3232 *p) {   pkt_tx_u64p(it, (const U64 *)p); }
void pkt_tx_dsp2440p(pkt_tx_buf *it, const dsp2440 *p) {   pkt_tx_u64p(it, (const U64 *)p); }
void pkt_tx_dsp1846p(pkt_tx_buf *it, const dsp1846 *p) {   pkt_tx_u64p(it, (const U64 *)p); }
void pkt_tx_dsp1648p(pkt_tx_buf *it, const dsp1648 *p) {   pkt_tx_u64p(it, (const U64 *)p); }
void pkt_tx_dsp1054p(pkt_tx_buf *it, const dsp1054 *p) {   pkt_tx_u64p(it, (const U64 *)p); }
void pkt_tx_dsp460p(pkt_tx_buf *it, const dsp460 *p)   {   pkt_tx_u64p(it, (const U64 *)p); }

// By-value versions
void pkt_tx_u8(pkt_tx_buf *it, U8 p) {   pkt_tx_u8p(it, &p); }
void pkt_tx_u16(pkt_tx_buf *it, U16 p) {   pkt_tx_u16p(it, &p); }
void pkt_tx_u32(pkt_tx_buf *it, U32 p) {   pkt_tx_u32p(it, &p); }
void pkt_tx_u64(pkt_tx_buf *it, U64 p) {   pkt_tx_u64p(it, &p); }

void pkt_tx_s8(pkt_tx_buf *it, S8 p) {   pkt_tx_u8p(it, (const U8 *)&p); }
void pkt_tx_s16(pkt_tx_buf *it, S16 p) {   pkt_tx_u16p(it, (const U16 *)&p); }
void pkt_tx_s32(pkt_tx_buf *it, S32 p) {   pkt_tx_u32p(it, (const U32 *)&p); }
void pkt_tx_s64(pkt_tx_buf *it, S64 p) {   pkt_tx_u64p(it, (const U64 *)&p); }

void pkt_tx_netu16(pkt_tx_buf *it, U16 p) {   pkt_tx_netu16p(it, &p); }
void pkt_tx_netu32(pkt_tx_buf *it, U32 p) {   pkt_tx_netu32p(it, &p); }
void pkt_tx_netu64(pkt_tx_buf *it, U64 p) {   pkt_tx_netu64p(it, &p); }


void pkt_tx_udec(pkt_tx_buf *it, U32 data)
{
  if (data==0) {
    pkt_tx_u8(it, '0');
  }
  else {
    if (data >= 10) {
      pkt_tx_udec(it, data / 10);
    }
    pkt_tx_u8(it, '0' + (data%10));
  }
}

void pkt_tx_dec(pkt_tx_buf *it, S32 data)
{
  if (data < 0) {
    pkt_tx_u8(it, '-');
    pkt_tx_udec(it, -data);
  } else {
    pkt_tx_udec(it, data);
  }
}

void pkt_tx_u8_hex(pkt_tx_buf *it, U8 x)
{
  pkt_tx_u8(it, hex((x>>4)&0xf));
  pkt_tx_u8(it, hex((x>>0)&0xf));
}

void pkt_tx_u16_hex(pkt_tx_buf *it, U16 x)
{
  pkt_tx_u8(it, hex((x>>12)&0xf));
  pkt_tx_u8(it, hex((x>>8)&0xf));
  pkt_tx_u8(it, hex((x>>4)&0xf));
  pkt_tx_u8(it, hex((x>>0)&0xf));
}

void pkt_tx_u32_hex(pkt_tx_buf *it, U32 x)
{
  pkt_tx_u16_hex(it, x>>16);
  pkt_tx_u16_hex(it, x);
}

void pkt_tx_string(pkt_tx_buf *it, char const *buf)
{
  int len=strlen(buf);
  if (len > 250) {
    it->overrun++;
    return;
  }
  pkt_tx_u8(it, len);
  int i=0;
  while (i < len) {
    pkt_tx_u8(it, buf[i]);
    i++;
  }
}

// ----------------------------------------------------------------------

void pkt_tx_end(pkt_tx_buf *it)
{
}

int pkt_tx_size(pkt_tx_buf *it)
{
  return it->len;
}


// ======================================================================


int pkt_rx_remaining(pkt_rx_buf *it)
{
  return it->len - it->rdpos;
}
int pkt_rx_size(pkt_rx_buf *it)
{
  return it->len;
}

void pkt_rx_u8p(pkt_rx_buf *it, U8 *p)
{
  if (it->rdpos + 1 > it->len) {
    it->overrun += 1;
  } else {
    *p = it->buf[it->rdpos];
    it->rdpos += 1;
  }
}

void pkt_rx_mem(pkt_rx_buf *it, U8 *p, unsigned int n)
{
  assert(n < 1000000);
  if (it->rdpos + n > it->len) {
    it->overrun += n;
  } else {
    memcpy(p, &it->buf[it->rdpos], n);
    it->rdpos += n;
  }
}

void pkt_rx_u16p(pkt_rx_buf *it, U16 *p)
{
  if (it->rdpos + 2 > it->len) {
    it->overrun += 2;
  } else {
    *p = (((U16)it->buf[it->rdpos + 0] << 0) |
          ((U16)it->buf[it->rdpos + 1] << 8));
    it->rdpos += 2;
  }
}

void pkt_rx_netu16p(pkt_rx_buf *it, U16 *p)
{
  if (it->rdpos + 2 > it->len) {
    it->overrun += 2;
  } else {
    *p = (((U16)it->buf[it->rdpos + 0] << 8) |
          ((U16)it->buf[it->rdpos + 1] << 0));
    it->rdpos += 2;
  }
}
void pkt_rx_u32p(pkt_rx_buf *it, U32 *p)
{
  if (it->rdpos + 4 > it->len) {
    it->overrun += 4;
  } else {
    *p = (((U32)it->buf[it->rdpos + 0] << 0) |
          ((U32)it->buf[it->rdpos + 1] << 8) |
          ((U32)it->buf[it->rdpos + 2] << 16) |
          ((U32)it->buf[it->rdpos + 3] << 24));
    it->rdpos += 4;
  }
}
void pkt_rx_netu32p(pkt_rx_buf *it, U32 *p)
{
  if (it->rdpos + 4 > it->len) {
    it->overrun += 4;
  } else {
    *p = (((U32)it->buf[it->rdpos + 0] << 24) |
          ((U32)it->buf[it->rdpos + 1] << 16) |
          ((U32)it->buf[it->rdpos + 2] << 8) |
          ((U32)it->buf[it->rdpos + 3] << 0));
    it->rdpos += 4;
  }
}
void pkt_rx_u64p(pkt_rx_buf *it, U64 *p)
{
  if (it->rdpos + 8 > it->len) {
    it->overrun += 8;
  } else {
    *p = (((U64)it->buf[it->rdpos + 0] << 0) |
          ((U64)it->buf[it->rdpos + 1] << 8) |
          ((U64)it->buf[it->rdpos + 2] << 16) |
          ((U64)it->buf[it->rdpos + 3] << 24) |
          ((U64)it->buf[it->rdpos + 4] << 32) |
          ((U64)it->buf[it->rdpos + 5] << 40) |
          ((U64)it->buf[it->rdpos + 6] << 48) |
          ((U64)it->buf[it->rdpos + 7] << 56));
    it->rdpos += 8;
  }
}
void pkt_rx_netu64p(pkt_rx_buf *it, U64 *p)
{
  if (it->rdpos + 8 > it->len) {
    it->overrun += 8;
  } else {
    *p = (((U64)it->buf[it->rdpos + 0] << 56) |
          ((U64)it->buf[it->rdpos + 1] << 48) |
          ((U64)it->buf[it->rdpos + 2] << 40) |
          ((U64)it->buf[it->rdpos + 3] << 32) |
          ((U64)it->buf[it->rdpos + 4] << 24) |
          ((U64)it->buf[it->rdpos + 5] << 16) |
          ((U64)it->buf[it->rdpos + 6] << 8) |
          ((U64)it->buf[it->rdpos + 7] << 0));
    it->rdpos += 8;
  }
}

void pkt_rx_s8p(pkt_rx_buf *it, S8 *p)
{
  pkt_rx_u8p(it, (U8 *)p);
}
void pkt_rx_s16p(pkt_rx_buf *it, S16 *p)
{
  pkt_rx_u16p(it, (U16 *)p);
}
void pkt_rx_s32p(pkt_rx_buf *it, S32 *p)
{
  pkt_rx_u32p(it, (U32 *)p);
}

// ----------------------------------------------------------------------

// Convenience by-value return methods

U8 pkt_rx_u8(pkt_rx_buf *it)
{
  U8 ret = 0xff;
  pkt_rx_u8p(it, &ret);
  return ret;
}
U16 pkt_rx_u16(pkt_rx_buf *it)
{
  U16 ret = 0xffff;
  pkt_rx_u16p(it, &ret);
  return ret;
}
U16 pkt_rx_netu16(pkt_rx_buf *it)
{
  U16 ret = 0xffff;
  pkt_rx_netu16p(it, &ret);
  return ret;
}
U32 pkt_rx_u32(pkt_rx_buf *it)
{
  U32 ret = 0xffffffff;
  pkt_rx_u32p(it, &ret);
  return ret;
}
U32 pkt_rx_netu32(pkt_rx_buf *it)
{
  U32 ret = 0xffffffff;
  pkt_rx_netu32p(it, &ret);
  return ret;
}
U64 pkt_rx_u64(pkt_rx_buf *it)
{
  U64 ret = -1;
  pkt_rx_u64p(it, &ret);
  return ret;
}
U64 pkt_rx_netu64(pkt_rx_buf *it)
{
  U64 ret = -1;
  pkt_rx_netu64p(it, &ret);
  return ret;
}
S8 pkt_rx_s8(pkt_rx_buf *it)
{
  S8 ret = 0xff;
  pkt_rx_s8p(it, &ret);
  return ret;
}
S16 pkt_rx_s16(pkt_rx_buf *it)
{
  S16 ret = 0xffff;
  pkt_rx_s16p(it, &ret);
  return ret;
}
S32 pkt_rx_s32(pkt_rx_buf *it)
{
  S32 ret = 0xffffffff;
  pkt_rx_s32p(it, &ret);
  return ret;
}

// ----------------------------------------------------------------------

void pkt_rx_intp(pkt_rx_buf *it, int *p)             { pkt_rx_u32p(it, (U32 *)p); }

void pkt_rx_dsp412p(pkt_rx_buf *it, dsp412 *p)       { pkt_rx_u16p(it, (U16 *)p); }
void pkt_rx_dsp1616p(pkt_rx_buf *it, dsp1616 *p)     { pkt_rx_u32p(it, (U32 *)p); }
void pkt_rx_dsp824p(pkt_rx_buf *it, dsp824 *p)       { pkt_rx_u32p(it, (U32 *)p); }
void pkt_rx_dsp230p(pkt_rx_buf *it, dsp230 *p)       { pkt_rx_u32p(it, (U32 *)p); }

void pkt_rx_dsp4024p(pkt_rx_buf *it, dsp4024 *p) {   pkt_rx_u64p(it, (U64 *)p); }
void pkt_rx_dsp3232p(pkt_rx_buf *it, dsp3232 *p) {   pkt_rx_u64p(it, (U64 *)p); }
void pkt_rx_dsp2440p(pkt_rx_buf *it, dsp2440 *p) {   pkt_rx_u64p(it, (U64 *)p); }
void pkt_rx_dsp1846p(pkt_rx_buf *it, dsp1846 *p) {   pkt_rx_u64p(it, (U64 *)p); }
void pkt_rx_dsp1648p(pkt_rx_buf *it, dsp1648 *p) {   pkt_rx_u64p(it, (U64 *)p); }
void pkt_rx_dsp1054p(pkt_rx_buf *it, dsp1054 *p) {   pkt_rx_u64p(it, (U64 *)p); }
void pkt_rx_dsp460p(pkt_rx_buf *it, dsp460 *p)   {   pkt_rx_u64p(it, (U64 *)p); }

void pkt_rx_string(pkt_rx_buf *it, char *buf, int buflen)
{
  U8 len = pkt_rx_u8(it);
  if (len > buflen-1) {
    it->overrun++;
    buf[0] = 0;
    return;
  }
  int i=0;
  while (i < len) {
    buf[i] = pkt_rx_u8(it);
    i++;
  }
  buf[i] = 0;
}

// ----------------------------------------------------------------------

#if defined(PKT_USE_CRC)
void pkt_tx_add_crc(pkt_tx_buf *it)
{
  if (it->len + 4 > it->alloc_size) {
    it->overrun++;
    return;
  }
  U8 checksum[4];
  for (int j=0; j<4; j++) checksum[j]=0;

  for (int i=0; i<it->len; i++) {
    U8 data = it->buf[i];
    update_crc(data, checksum);
  }

  for (int j=0; j<4; j++) {
    it->buf[it->len + j] = checksum[j];
  }
  it->len += 4;
}

void pkt_tx_del_crc(pkt_tx_buf *it)
{
  if (it->len < 4) return;
  it->len -= 4;
}

Bool pkt_rx_eat_crc(pkt_rx_buf *it)
{
  if (it->len < 4) return 0;

  U8 checksum[4];
  for (int j=0; j<4; j++) checksum[j]=0;

  for (int i=0; i<it->len - 4; i++) {
    U8 data = it->buf[i];
    update_crc(data, checksum);
  }

  for (int j=0; j<4; j++) {
    if (it->buf[it->len-4+j] != checksum[j]) {
      return 0;
    }
  }
  it->len -= 4;
  return 1;
}
#endif
