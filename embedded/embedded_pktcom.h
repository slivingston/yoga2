#pragma once
#include "../dspcore/dspcore.h"
#include "../dspcore/fapproxdsp.h"
#include "../dspcore/smootherdsp.h"
#include "../dspcore/vec3dsp.h"

#undef PKT_USE_CRC
#define PKT_MAX_SIZE 1600

typedef struct pkt_endpoint {
  U8 ip4_addr[4];
  U8 mac_addr[6];
  U16 udp_port;
} pkt_endpoint;

typedef struct pkt_tx_buf {
  int16_t alloc_size;
  int16_t len;
  int16_t overrun;
  int16_t ip4_hdr_pos;
  int16_t udp_hdr_pos;
  int16_t icmp_hdr_pos;

  U8 buf[0];
} pkt_tx_buf;

typedef struct pkt_rx_buf {
  int16_t alloc_size;
  int16_t len;
  int16_t rdpos;
  int16_t overrun;
  pkt_endpoint src;

  U8 buf[0];
} pkt_rx_buf;

bool pkt_ep_valid(pkt_endpoint *ep);

void pkt_setup();

pkt_tx_buf *alloc_tx_buf(void);
void free_tx_buf(pkt_tx_buf *it);

pkt_rx_buf *alloc_rx_buf(void);
void free_rx_buf(pkt_rx_buf *it);

void update_crc(U8 data, U8 crc[4]);

void pkt_tx_start(pkt_tx_buf *it);
void pkt_tx_end(pkt_tx_buf *it);
int pkt_tx_size(pkt_tx_buf *it);

void pkt_tx_u8p(pkt_tx_buf *it, const U8 *p);
void pkt_tx_u16p(pkt_tx_buf *it, const U16 *p);
void pkt_tx_netu16p(pkt_tx_buf *it, const U16 *p);
void pkt_tx_s16p(pkt_tx_buf *it, const S16 *p);
void pkt_tx_u32p(pkt_tx_buf *it, const U32 *p);
void pkt_tx_netu32p(pkt_tx_buf *it, const U32 *p);
void pkt_tx_s32p(pkt_tx_buf *it, const S32 *p);
void pkt_tx_u64p(pkt_tx_buf *it, const U64 *p);
void pkt_tx_netu64(pkt_tx_buf *it, U64 p);
void pkt_tx_s64p(pkt_tx_buf *it, const S64 *p);
void pkt_tx_intp(pkt_tx_buf *it, const int *p);
void pkt_tx_dsp412p(pkt_tx_buf *it, const dsp412 *p);
void pkt_tx_dsp1616p(pkt_tx_buf *it, const dsp1616 *p);
void pkt_tx_dsp824p(pkt_tx_buf *it, const dsp824 *p);
void pkt_tx_dsp230p(pkt_tx_buf *it, const dsp230 *p);

void pkt_tx_dsp4024p(pkt_tx_buf *it, const dsp4024 *p);
void pkt_tx_dsp3232p(pkt_tx_buf *it, const dsp3232 *p);
void pkt_tx_dsp2440p(pkt_tx_buf *it, const dsp2440 *p);
void pkt_tx_dsp1846p(pkt_tx_buf *it, const dsp1846 *p);
void pkt_tx_dsp1648p(pkt_tx_buf *it, const dsp1648 *p);
void pkt_tx_dsp1054p(pkt_tx_buf *it, const dsp1054 *p);
void pkt_tx_dsp460p(pkt_tx_buf *it, const dsp460 *p);

void pkt_tx_str(pkt_tx_buf *it, const char *s);
void pkt_tx_mem(pkt_tx_buf *it, const void *s, unsigned int len);

void pkt_tx_u8(pkt_tx_buf *it, U8 data);
void pkt_tx_u16(pkt_tx_buf *it, U16 data);
void pkt_tx_u32(pkt_tx_buf *it, U32 data);
void pkt_tx_u64(pkt_tx_buf *it, U64 data);

void pkt_tx_s8(pkt_tx_buf *it, S8 data);
void pkt_tx_s16(pkt_tx_buf *it, S16 data);
void pkt_tx_s32(pkt_tx_buf *it, S32 data);
void pkt_tx_s64(pkt_tx_buf *it, S64 data);

void pkt_tx_netu8(pkt_tx_buf *it, U8 data);
void pkt_tx_netu16(pkt_tx_buf *it, U16 data);
void pkt_tx_netu32(pkt_tx_buf *it, U32 data);

void pkt_tx_udec(pkt_tx_buf *it, U32 data);
void pkt_tx_dec(pkt_tx_buf *it, S32 data);
void pkt_tx_u8_hex(pkt_tx_buf *it, U8 x);
void pkt_tx_u16_hex(pkt_tx_buf *it, U16 x);
void pkt_tx_u32_hex(pkt_tx_buf *it, U32 x);

void pkt_tx_string(pkt_tx_buf *it, char const *buf);

// ----------------------------------------------------------------------

int pkt_rx_remaining(pkt_rx_buf *it);
int pkt_rx_size(pkt_rx_buf *it);

void pkt_rx_mem(pkt_rx_buf *it, U8 *p, unsigned int n);
void pkt_rx_u8p(pkt_rx_buf *it, U8 *p);
void pkt_rx_u16p(pkt_rx_buf *it, U16 *p);
void pkt_rx_netu16p(pkt_rx_buf *it, U16 *p);
void pkt_rx_u32p(pkt_rx_buf *it, U32 *p);
void pkt_rx_netu32p(pkt_rx_buf *it, U32 *p);
void pkt_rx_u64p(pkt_rx_buf *it, U64 *p);
void pkt_rx_netu64p(pkt_rx_buf *it, U64 *p);
void pkt_rx_s8p(pkt_rx_buf *it, S8 *p);
void pkt_rx_s16p(pkt_rx_buf *it, S16 *p);
void pkt_rx_s32p(pkt_rx_buf *it, S32 *p);
void pkt_rx_intp(pkt_rx_buf *it, int *p);

void pkt_rx_dsp412p(pkt_rx_buf *it, dsp412 *p);

void pkt_rx_dsp1616p(pkt_rx_buf *it, dsp1616 *p);
void pkt_rx_dsp824p(pkt_rx_buf *it, dsp824 *p);
void pkt_rx_dsp230p(pkt_rx_buf *it, dsp230 *p);

void pkt_rx_dsp4024p(pkt_rx_buf *it, dsp4024 *p);
void pkt_rx_dsp3232p(pkt_rx_buf *it, dsp3232 *p);
void pkt_rx_dsp2440p(pkt_rx_buf *it, dsp2440 *p);
void pkt_rx_dsp1846p(pkt_rx_buf *it, dsp1846 *p);
void pkt_rx_dsp1648p(pkt_rx_buf *it, dsp1648 *p);
void pkt_rx_dsp1054p(pkt_rx_buf *it, dsp1054 *p);
void pkt_rx_dsp460p(pkt_rx_buf *it, dsp460 *p);

U8 pkt_rx_u8(pkt_rx_buf *it);
U16 pkt_rx_u16(pkt_rx_buf *it);
U16 pkt_rx_netu16(pkt_rx_buf *it);
U32 pkt_rx_u32(pkt_rx_buf *it);
U32 pkt_rx_netu32(pkt_rx_buf *it);
U64 pkt_rx_u64(pkt_rx_buf *it);
U64 pkt_rx_netu64(pkt_rx_buf *it);
S8 pkt_rx_s8(pkt_rx_buf *it);
S16 pkt_rx_s16(pkt_rx_buf *it);
S32 pkt_rx_s32(pkt_rx_buf *it);

void pkt_rx_string(pkt_rx_buf *it, char *buf, int buflen);

void pkt_tx_add_crc(pkt_tx_buf *it);
void pkt_tx_del_crc(pkt_tx_buf *it);
Bool pkt_rx_eat_crc(pkt_rx_buf *it);
