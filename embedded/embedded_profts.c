#include "common/std_headers.h"
#include "./embedded_timing.h"
#include "./embedded_debug.h"
typedef U32 timestamp_t;

#define MAX_PROFTS 1024

int profts_active;

static int n_profts;
static struct profts_entry_t {
  const char *label;
  int data;
  timestamp_t timestamp;
} profts_entries[MAX_PROFTS];

void profts_start()
{
  n_profts = 0;
  profts_active = 1;
}

void profts_real(const char *label, int data)
{
  if (!profts_active) return;

  int ei = n_profts++;   // fairly robust in multithreaded environment

  if (ei >= MAX_PROFTS) {
    profts_active = 0;
    return;
  }

  profts_entries[ei].label = label;
  profts_entries[ei].data = data;
  profts_entries[ei].timestamp = get_ticks();
}

int MAX_SPACEBUF = 20;
const char *spacebuf = "                    ";


void profts_dump(int mincount)
{
  int i;
  if (n_profts >= MAX_PROFTS || n_profts >= mincount) {
    timestamp_t base_timestamp = profts_entries[0].timestamp;
    timestamp_t last_timestamp = base_timestamp;
    int indent=0;
    profts_active = 0;

    log_printf("        Last      Total   Label");

    for (i=0; i<n_profts; i++) {
      if (!profts_entries[i].label) break;
      if (profts_entries[i].label[0]=='-') {
        indent--;
      }
      {
        dsp1616 us = ticks2microseconds_int_dsp1616(profts_entries[i].timestamp - base_timestamp);
        dsp1616 diffus = ticks2microseconds_int_dsp1616(profts_entries[i].timestamp - last_timestamp);
        log_printf("  %8.1{dsp1616}uS %8.1{dsp1616}uS   %s%s.0x%x", 
                   diffus, us,
                   &spacebuf[max(0, MAX_SPACEBUF-indent)], profts_entries[i].label, profts_entries[i].data);
      }
      last_timestamp = profts_entries[i].timestamp;
      if (profts_entries[i].label[0]=='+') {
        indent++;
      }
    }
    n_profts = 0;
  }
}

