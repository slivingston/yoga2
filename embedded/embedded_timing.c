#include "common/std_headers.h"
#include "./embedded_timing.h"
#include "./embedded_debug.h"
#include "hwdefs.h"

dsp824 ticks2seconds_int_dsp824(S32 ticks)
{
  return (dsp824)(S32)((ticks * (S64)((float)(1LL<<48) / (float)FCPU)) >> 24);
}

dsp1616 ticks2microseconds_int_dsp1616(S32 ticks)
{
  return (dsp1616)(S32)((ticks * (S64)((float)(1LL<<48) / ((float)FCPU / (float)1.0e6))) >> 32);
}

static U64 extended_ticks;
U64 get_ticks64() 
{
  extended_ticks += (S32)(get_ticks() - (U32)extended_ticks);
  return extended_ticks;
}


void rate_estimator_add(rate_estimator *it, int event_count)
{
  U32 now = get_ticks();

  it->event_counts[0] = event_count;
  it->end_times[0] = now;

  // Collapse buckets
  for (int i=1; i<31; i++) {
    if ((now - it->end_times[i]) >= (1<<i)) {
      it->event_counts[i] += it->event_counts[i-1];
      it->event_counts[i-1] = 0;
      it->end_times[i] = it->end_times[i-1];
    }
  }
}

void rate_estimator_dump(rate_estimator *it, U32 min_ticks)
{
  U32 now = it->end_times[0];

  U32 tot_events = 0; 
  for (int i=0; i<30; i++) {
    tot_events += it->event_counts[i];
    U32 interval = now - it->end_times[i+1];
    if (it->event_counts[i] && tot_events>=2 && interval >= min_ticks) {
        // Subtract 1 from events because we're measuring the interval from first to last
        dsp1616 rate = div_int_dsp824_dsp1616(tot_events-1, ticks2seconds_int_dsp824(interval));
        debug_printf("   %d/%{ticks} = %{dsp1616}", 
                     tot_events, 
                     interval,
                     rate);
    }
  }
}
  
dsp1616 rate_estimator_get_rate_dsp1616(rate_estimator *it, U32 min_ticks)
{
  U32 now = it->end_times[0];

  U32 tot_events = 0; 
  for (int i=0; i<30; i++) {
    tot_events += it->event_counts[i];
    U32 interval = now - it->end_times[i+1];
    if (tot_events>=2 && (interval > min_ticks || i==29)) {
      // Subtract 1 from events because we're measuring the interval from first to last
      dsp1616 rate = div_int_dsp824_dsp1616(tot_events-1, ticks2seconds_int_dsp824(interval));
      return rate;
    }
  }
  return 0;
}




// ----------------------------------------------------------------------


void sinewave_generator_dsp824_advance(sinewave_generator_dsp824 *it, dsp824 dt, dsp824 omega)
{
  it->phase += mul_dsp824_dsp824_dsp824(dt, omega);
  while (it->phase > DSP824(2.0 * M_PI)) {
    it->phase -= DSP824(2.0 * M_PI);
  }
  while (it->phase < -DSP824(2.0 * M_PI)) {
    it->phase += DSP824(2.0 * M_PI);
  }
  sincos_dsp824_dsp824_dsp824(it->phase, &it->sin, &it->cos);
}


// ----------------------------------------------------------------------

void rate_generator_enable(rate_generator *it, U32 interval_ticks, int max_backlog)
{
  if (it->interval_ticks == 0) {
    it->last_event_ticks = get_ticks();
    it->todo = 0;
    it->budget = 0;
    it->frac = DSP824(0.0);
  }

  it->max_backlog = max_backlog;
  it->interval_ticks = interval_ticks;
}

void rate_generator_disable(rate_generator *it)
{
  it->interval_ticks = 0;
  it->todo = 0;
  it->max_backlog = 0;
  it->budget = 0;
  it->frac = DSP824(0.0);
}

int rate_generator_update(rate_generator *it)
{
  if (it->interval_ticks == 0) return 0;

  U32 now = get_ticks();
  U32 delta = now - it->last_event_ticks;
  if (delta >= it->interval_ticks) {
    int n = max(0, min(delta / it->interval_ticks, min(it->budget, it->max_backlog - it->todo)));
    it->todo += n;
    it->budget -= n;
    if (n > 1) {
      it->last_event_ticks = now;
    } else {
      it->last_event_ticks += it->interval_ticks;
    }
    it->frac = DSP824(0.0);
  } else {
    it->frac = div_dsp824_dsp824_dsp824(delta, it->interval_ticks);
  }
  return it->todo;
}

void rate_generator_done(rate_generator *it)
{
  if (it->todo > 0) it->todo--;
}

void rate_generator_set_budget(rate_generator *it, int n)
{
  it->budget = n;
}

dsp824 rate_generator_frac(rate_generator *it)
{
  return it->frac;
}

// ----------------------------------------------------------------------

bool period_generator_is_enabled(period_generator *it)
{
  U32 cur_ticks = get_ticks();
  S32 delta = (S32)(cur_ticks - it->disable_ticks);
  if (delta >= 0) {
    it->enabled = false;
  }
  return it->enabled;
}

void period_generator_enable(period_generator *it, U32 duration)
{
  it->enabled = true;
  it->disable_ticks = get_ticks() + duration;
}
