#pragma once
#include "../dspcore/dspcore.h"
#if defined(__EMBEDDED__)
#include "hwdefs.h"
#endif
#include "./embedded_profts.h"

dsp824 ticks2seconds_int_dsp824(S32 ticks);
dsp1616 ticks2microseconds_int_dsp1616(S32 ticks);

// XXX should check for overflow, which is about 30 seconds at 66MHz
#define SECONDSTOTICKS(SEC) ((int)((SEC)*FCPU))

#ifdef __AVR32__
#include "CPU/CYCLE_COUNTER/cycle_counter.h"
static inline U32 get_ticks()
{
  return Get_sys_count();
}
#else
U32 get_ticks();
#endif
U64 get_ticks64();

// ----------------------------------------------------------------------

typedef struct rate_estimator {
  U32 end_times[32];
  U32 event_counts[32];
} rate_estimator;
void rate_estimator_add(rate_estimator *it, int event_count);
void rate_estimator_dump(rate_estimator *it, U32 min_ticks);
dsp1616 rate_estimator_get_rate_dsp1616(rate_estimator *it, U32 min_ticks);

// ----------------------------------------------------------------------

typedef struct sinewave_generator_dsp824 {
  dsp824 phase;
  dsp824 sin, cos;
} sinewave_generator_dsp824;

void sinewave_generator_dsp824_advance(sinewave_generator_dsp824 *it, dsp824 dt, dsp824 omega);

// ----------------------------------------------------------------------

typedef struct rate_generator {
  // Be a good citizen and use the accessor methods
  U32 interval_ticks;
  U32 last_event_ticks;
  int todo;
  int budget;
  int max_backlog;
  dsp824 frac;
} rate_generator;

void rate_generator_enable(rate_generator *it, U32 interval_ticks, int max_backlog);
void rate_generator_disable(rate_generator *it);
int rate_generator_update(rate_generator *it);
void rate_generator_done(rate_generator *it);
void rate_generator_set_budget(rate_generator *it, int n);
dsp824 rate_generator_frac(rate_generator *it);

// ----------------------------------------------------------------------

typedef struct period_generator {
  U32 disable_ticks;
  bool enabled;
} period_generator;

bool period_generator_is_enabled(period_generator *it);
void period_generator_enable(period_generator *it, U32 duration);
