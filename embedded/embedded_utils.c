#include "./embedded_utils.h"

static char const hextable[] = "0123456789abcdef";

char hex(U8 value) 
{
  if (value>15) return '?';
  return hextable[value];
}
