

require("yoga/drivers/dex4/dex4_types.yoga")
require("yoga/drivers/dex4/ssc_harness/ssc_harness_types.yoga");

onehot Dex4ToplevelMode {
  initial;
  stabilize;
  lower;
  balanceStart;
  balance;
  wobble;
  floppy;
  belay;
}

struct Dex4ToplevelState {
  Dex4ToplevelMode mode;
  string reason;
  Filter2State tapf;
  R phase;
}

struct Dex4ToplevelCmd {
  R stabilize;
  R lower;
  R balance;
  R wobble;
  R floppy;
  R belay;
  R toetap;
  R air;
}

onehot Dex4ToplevelGoal {
  balance;
  floppy;
}

function dex4Toplevel(
  out Dex4ToplevelCmd cmd,
  update Dex4ToplevelState s,
  in Dex4ToplevelGoal goal,
  in YogaKeyboard kbd,
  in Dex4HarnessState harness,
  in Dex4BalanceSensors balance?,
  in Cad42Positions kin)
{
  if (!sum(s.mode)) {
    if (goal.balance) {
      s.mode.stabilize = 1;
    }
    if (goal.floppy) {
      s.mode.floppy = 1;
    }
  }

  if (s.mode.stabilize) {
    if (s.phase > 3) {
      s.mode.lower = 1;
      s.mode.stabilize = 0;
      s.phase = 0;
    }
    cmd.stabilize = easeIn(., s.phase / 1);
    cmd.air = easeIn(., s.phase / 0.2);
  }

  if (s.mode.lower) {
    if (s.phase > 0.8) {
      s.mode.balanceStart = 1;
      s.mode.lower = 0;
      s.phase = 0;
    }
    cmd.lower = easeIn(., s.phase / 0.7);
    cmd.belay = easeOut(., s.phase / 0.7);
    cmd.air = 1;
  }

  if (s.mode.balanceStart) {
    if (balance.fallen) {
      s.mode.belay = 1;
      s.mode.balanceStart = 0;
      s.phase = 0;
    }
    else {
      if (s.phase > 1.5) {
        s.mode.balance = 1;
        s.mode.balanceStart = 0;
        s.phase = 0;
      }
    }
    cmd.balance = easeIn(., s.phase/0.1);
    cmd.air = 1.0;
    cmd.lower = 1;
    cmd.belay = 0;
  }

  if (s.mode.balance) {
    if (balance.fallen) {
      s.mode.belay = 1;
      s.mode.balance = 0;
      s.phase = 0;
    }
    cmd.balance = 1;
    cmd.air = 1;
    cmd.lower = 1;
    cmd.belay = 0;
  }

  if (s.mode.wobble) {
    cmd.wobble = easeIn(., s.phase / 1.0);
    cmd.air = 1;
  }

  if (s.mode.belay) {
    cmd.belay = easeIn(., s.phase / 0.1);
    cmd.air = 1;
    if (kbd.r) {
      s.mode.stabilize = 1;
      s.mode.belay = 0;
    }
  }
  ttraw = kbd.t + (s.mode.belay * (sin(s.phase/0.4) < -0.6));
  cmd.toetap = lpfilter2(., s.tapf, 0.1, 1.0, 1.1 * ttraw); 

  if (s.mode.floppy) {
    cmd.air = 0;
    cmd.floppy = 1;
  }

  default {
    cmd.air = 0.0;
    cmd.belay = 1;
    cmd.floppy = 0;
    s.phase = s.phase + dt;
  }
}
