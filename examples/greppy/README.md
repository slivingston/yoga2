# Greppy

Greppy, with hardware designed by Matt Moore <> and Chris Tacklind <>, is a two-wheeled self-balancing robot.
The code here makes it balance.


* Main balance feedback loop is in [balance.yoga](./balance.yoga).

* Orientation tracking code is in [inertial.yoga](./inertial.yoga). This accepts input from the IMU and outputs an orientation matrix with the robot's yaw factored out.

* Driving code is in [nav.yoga](./nav.yoga). It starts with a state machine to bring the robot into a standing position. Then it reads the keyboard and puck (if you have one) and lets you drive the robot with them.

* Configuration for running on a particular robot is in [main.yoga](./main.yoga).


The robot hardware consists of:

* Two wheels, taken from hoverboards

* An ODrive motor controller for the wheel motors

* A IMU, based on an [ADIS16480](https://www.analog.com/en/products/adis16480.html) together with an AVR32 microcontroller with an ethernet interface. The firmware for this is in [drivers/dex4/ssc_gyro/fw](../../drivers/dex4/ssc_gyro/fw)

* An Intel NUC running the code in this directory (using the yoga_botd runtime).

There is an open-access robot `greppy1` accessible through the firewall
you can test with.
