#pragma once
#include "common/std_headers.h"
#include "numerical/numerical.h"
#include "./yogagl.h"
#include "../geom/geom.h"
#include <typeindex>


struct StlSolid;

enum VertexType {
  VT_3D_VERT,
  VT_POINT_COLOR,
};

struct Panel3dVert {
  static const VertexType vertexType = VT_3D_VERT;
  Panel3dVert(vec3 const &_pos, vec3 const &_normal, vec2 const &_uv, U32 _col)
    : pos(_pos),
      normal(_normal),
      uv(_uv),
      col(_col)
  {
  }
  Panel3dVert(vec3 const &_pos, vec3 const &_normal, U32 _col)
    : pos(_pos),
      normal(_normal),
      uv(0.0f, 0.0f),
      col(_col)
  {
  }
  Panel3dVert(vec3 const &_pos, U32 _col)
    : pos(_pos),
      normal(0.0f, 0.0f, 0.0f),
      uv(0.0f, 0.0f),
      col(_col)
  {
  }
  Panel3dVert(vec3 const &_pos, vec2 const &_uv, U32 _col)
    : pos(_pos),
      normal(0.0f, 0.0f, 0.0f),
      uv(_uv),
      col(_col)
  {
  }
  vec3 pos;
  vec3 normal;
  vec2 uv;
  U32 col;
};



struct PanelPointColor {
  static const VertexType vertexType = VT_POINT_COLOR;
  PanelPointColor(vec3 const &_pos, U32 _col)
    : pos(_pos),
      col(_col)
  {
  }
  vec3 pos;
  U32 col;
};


struct Panel3dShader {
  Panel3dShader(
    string const &_fn,
    VertexType _vertexType,
    vector<string> const &_uniformNames,
    vector<string> const &_attribNames);

  void compile();
  void drawElements();
  void setupProgram(glm::mat4 const &proj, glm::mat4 const &view);
  void setupTexVideoProgram(U32 texId);

  U32 shaderIndex{0};
  U32 progHandle{0}, vtxHandle{0}, geoHandle{0}, tcHandle{0}, teHandle{0}, frgHandle{0};

  string fn;
  VertexType vertexType;
  vector<pair<string, U32>> uniforms;
  vector<pair<string, U32>> attribs;

};

extern Panel3dShader plainDotShader;
extern Panel3dShader plainLineShader;
extern Panel3dShader litSolidShader;
extern Panel3dShader flatSolidShader;

ostream & operator <<(ostream &s, const Panel3dVert &a);

struct Panel3dDrawCmd {
  Panel3dDrawCmd(
    U32 _shaderIndex,
    U32 _elemIndex,
    U32 _elemCount,
    U32 _drawMode,
    U32 _texId)
    : shaderIndex(_shaderIndex),
      elemIndex(_elemIndex),
      elemCount(_elemCount),
      drawMode(_drawMode),
      texId(_texId)
  {
  }
  U32 shaderIndex;
  U32 elemIndex;
  U32 elemCount;
  U32 drawMode;
  U32 texId;
};


struct Panel3dShaderSetup {
  U32 vertexArrayObject{0};
  U32 vertexBuffer{0};
  U32 elementBuffer{0};
  vector<Panel3dVert> vtxBuf3dVert;
  vector<PanelPointColor> vtxBufPointColor;
  vector<U32> idxBuf;
};

struct Panel3dDrawList {

  vector<Panel3dDrawCmd> cmdBuf;

  vector<Panel3dShaderSetup> setups;

  vec3 homeEyepos;
  vec3 homeLookat;
  vec3 homeUp;
  float homeFov;

  vec3 eyepos;
  vec3 lookat;
  vec3 up;
  float fov{40.0 * M_PI/180.0};
  float dt{1.0f/60.0f};
  float nearZ{0.1f};
  float farZ{20.0f};

  int vpL{0}, vpT{0}, vpR{0}, vpB{0};

  vec3 pickRayOrigin, pickRayDir;
  bool pickRayActive{false};

  vec3 saveDir;
  bool saveDirActive{false};

  Panel3dDrawList();
  ~Panel3dDrawList();
  void drawUsingCallback(std::function<void(Panel3dDrawList *)> cb);
  void glRender();
  void setupShader(Panel3dShader *shader, glm::mat4 &proj, glm::mat4 &view, U32 texUnit);

  void camFov(R target);
  void camEyepos(vec3 target);
  void camLookat(vec3 target);
  void camStrafe(vec3 dir);
  void camHoverFly(vec3 dir);
  void camFlyHome();
  void camPan(vec2 dir);
  void camOrbitElevate(R orbit, R elevate);
  void camHome();
  void setHoverHome();
  void camIso();
  void camCheckSanity();

  void firstPersonControls();
  void hoverControls();

  void setPickRay();

  void clear();

  void addCmd(Panel3dShader *shader, U32 elemIndex, U32 elemCount, U32 drawMode, U32 texId=0);

  void addDot(
    vec3 const &a,
    U32 col,
    Panel3dShader *shader=nullptr);

  void addLine(
    vec3 const &a, U32 aCol,
    vec3 const &b, U32 bCol,
    Panel3dShader *shader=nullptr);

  void addTriangle(
    vec3 const &a, U32 aCol,
    vec3 const &b, U32 bCol,
    vec3 const &c, U32 cCol,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addCircle(
    vec3 const &center,
    vec3 const &axis0,
    vec3 const &axis1,
    U32 col,
    int segments = 16,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addQuad(
    vec3 const &a, U32 aCol,
    vec3 const &b, U32 bCol,
    vec3 const &c, U32 cCol,
    vec3 const &d, U32 dCol,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addTriangle(
    vec3 const &a, vec3 const &aNorm, U32 aCol,
    vec3 const &b, vec3 const &bNorm, U32 bCol,
    vec3 const &c, vec3 const &cNorm, U32 cCol,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addQuad(
    vec3 const &a, vec3 const &aNorm, U32 aCol,
    vec3 const &b, vec3 const &bNorm, U32 bCol,
    vec3 const &c, vec3 const &cNorm, U32 cCol,
    vec3 const &d, vec3 const &dNorm, U32 dCol,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addArrow(
    vec3 const &root,
    vec3 const &axis,
    vec3 const &ca, 
    vec3 const &cb, 
    U32 col,
    Panel3dShader *shader=nullptr);

  void addArrowTriple(
    mat4 const &model,
    Panel3dShader *shader=nullptr);

  void addVideo(mat4 const &model, U32 videoTex, Panel3dShader *shader=nullptr);

  void addSolid(
    StlSolid const &solid,
    mat4 const &m,
    U32 col,
    Panel3dShader *shader=nullptr);

  bool hitTestTriangle(vec3 const &a, vec3 const &b, vec3 const &c, vec3 &hit, float &distance);

};

void panel3dInit();

void logGlErrors(char const *where);

#define LOG_GL_ERRORS() logGlErrors((string(__FILE__) + ": " + to_string(__LINE__)).c_str())
