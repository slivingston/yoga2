#pragma once
#include "common/std_headers.h"
#include "numerical/numerical.h"
#include "./geom.h"


struct StlMassProperties;

struct OctreeNode {
  explicit OctreeNode(dvec3 const &_center, double _scale);
  ~OctreeNode();
  OctreeNode(OctreeNode const &) = delete;
  OctreeNode(OctreeNode &&) = delete;
  OctreeNode &operator=(OctreeNode const &) = delete;
  OctreeNode &operator=(OctreeNode &&) = delete;

  OctreeNode *lookup(dvec3 const &pt, double maxScale);

  dvec3 center;
  double scale;

  OctreeNode *children[8];
};

struct StlFace {
  StlFace();
  explicit StlFace(dvec3 const &_v0, dvec3 const &_v1, dvec3 const &_v2);
  explicit StlFace(
      dvec3 const &_v0,
      dvec3 const &_v1,
      dvec3 const &_v2,
      dvec3 const &_normal);

  void calcNormal();

  bool rayIntersects(dvec3 const &p, dvec3 const &d, double &t) const;
  void transform(dmat4 const &m);
  double getArea() const;
  dvec3 getE1() const;
  dvec3 getE2() const;
  bool isDegenerate() const;
  dvec3 getCentroid() const;

  dvec3 v0, v1, v2;
  dvec3 normal;
};

bool operator==(StlFace const &a, StlFace const &b);

struct StlIntersection {
  double t;
  struct StlFace face;
};

struct StlWebglMesh {
  StlWebglMesh() {}

  vector<R> coords;
  vector<R> normals;
  vector<S32> indexes;
};

struct StlSolid {

  StlSolid();

  void readBinaryFile(FILE *fp, R scale);
  void writeBinaryFile(FILE *fp, R scale);
  void merge(StlSolid const *other);
  void calcBbox();
  R getMaxScale() const;
  bool rayIntersects(dvec3 const &p, dvec3 const &d) const;
  void transform(dmat4 const &m);
  bool isInterior(dvec3 const &pt) const;
  vector<StlIntersection> getIntersections(dvec3 const &p, dvec3 const &d) const;
  StlMassProperties getStlMassProperties(R density) const;
  StlWebglMesh exportWebglMesh(R eps) const;
  void removeTinyFaces(R minSize);
  dvec3 analyzeHole(int axisi);
  pair<R, dvec3> estimateVolume();

  static shared_ptr<StlSolid> getNamedSolid(string const &fn, R scale, dmat4 const &transform);

  dvec3 bboxLo, bboxHi;
  vector<StlFace> faces;
};

template<> string repr(StlSolid const &it);

struct StlMassProperties {
  StlMassProperties();
  explicit StlMassProperties(
      double _volume,
      double _mass,
      double _area,
      dvec3 const &_cm,
      dmat3 const &_inertiaOrigin);

  StlMassProperties multiplyDensity(double factor);

  void calcDerived();

  double density{0.0};
  double volume{0.0};
  double mass{0.0};
  double area{0.0};
  dvec3 cm;
  dmat3 inertiaOrigin;
  dmat3 inertiaCm;
  dvec3 rogOrigin;
  dvec3 rogCm;
};

template<> string repr(StlMassProperties const &it);

StlMassProperties operator+(StlMassProperties const &a, StlMassProperties const &b);
