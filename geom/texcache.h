#pragma once
#include "common/std_headers.h"
#include "imgui_impl_opengl3.h"
#include "../db/blob.h"

struct TexCache {
  TexCache();
  ~TexCache();

  void alloc(size_t _n_tex);
  bool getIndexFor(void const *key, int &bestIndex);
  void setIndexFor(void const *key, int bestIndex);
  void setPixelsRGB(int texIndex, U32 width, U32 height, U8 *pixels);
  void setPixelsRGBA(int texIndex, U32 width, U32 height, U8 *pixels);
  bool setRgb(int texIndex, int width, int height, Blob &buffer);
  bool setJpeg(int texIndex, Blob &buffer);
  bool setJpegFile(int texIndex, string const &fn);
  bool setPngFile(int texIndex, string const &fn);
  U32 getTexid(int texIndex);
  void *getTexidFromImagePng(void const *key, string const &fn);

  size_t n_tex;
  vector<U32> texIds;
  vector<U64> lastUses;
  vector<void const *> keys;
  vector<ImVec2> sizes;
  U64 lastUseCounter=0;

};
