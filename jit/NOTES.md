# Notes on the LLVM-based JIT

## Building LLVM from source

```sh
cd ~/sw/llvm-project
mkdir build
cd build
cmake -DLLVM_ENABLE_EH=on -DLLVM_ENABLE_RTTI=on ../llvm
make
```

## LLVM Documentation

* [Programming](https://llvm.org/docs/ProgrammersManual.html)
* [Index](https://llvm.org/doxygen/annotated.html)
* [IRBuilder](https://llvm.org/doxygen/classllvm_1_1IRBuilder.html)
  * and [IRBuilderBase](https://llvm.org/doxygen/classllvm_1_1IRBuilderBase.html)
* [Language Referense for LLVM IR](https://llvm.org/docs/LangRef.html)
* [GEP](https://llvm.org/docs/GetElementPtr.html)
* [Module](https://llvm.org/doxygen/classllvm_1_1Module.html)
* [Function](https://llvm.org/doxygen/classllvm_1_1Function.html)
* [Type](https://llvm.org/doxygen/classllvm_1_1Type.html)
* [ExecutionSession](https://llvm.org/doxygen/classllvm_1_1orc_1_1ExecutionSession.html)
* [JitDyLib](https://llvm.org/doxygen/classllvm_1_1orc_1_1JITDylib.html)

