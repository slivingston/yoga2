#pragma once
#include "./jit_utils.h"
#include "./sourcefile.h"
#include "./type.h"

struct YogaType;
struct ExprNode;
struct StructMember;
struct YogaParamInfo;

struct AnnoStructref;
struct AnnoCall;
struct AnnoParam;
struct AnnoTrain;

struct AnnoBase {
  AnnoBase()
  {
  }
  virtual ~AnnoBase()
  {
  }

  virtual AnnoStructref *asStructref() { return nullptr; }
  virtual AnnoCall *asCall() { return nullptr; }
  virtual AnnoParam *asParam() { return nullptr; }
  virtual AnnoTrain *asTrain() { return nullptr; }

  virtual string repr() const { return "?"; }

  YogaSourceLoc sourceLoc;
};

struct AnnoStructref : AnnoBase {
  AnnoStructref(
    ExprNode *_nodeXv,
    StructMember *_memberInfo,
    string const &_funcName,
    string const &_refName,
    bool _isLvalue)
    : nodeXv(_nodeXv),
      memberInfo(_memberInfo),
      funcName(_funcName),
      refName(_refName),
      isLvalue(_isLvalue)
  {
  }

  ExprNode *nodeXv;
  StructMember *memberInfo;
  string funcName;
  string refName;
  bool isLvalue;

  AnnoStructref *asStructref() override { return this; }

  string repr() const override {
    return "structref("s + funcName + ", " + refName + " at " + ::repr(sourceLoc) + ")";
  }
};


struct AnnoCall : AnnoBase {
  AnnoCall(
    ExprNode *_nodeXv,
    string const &_funcName)
    : nodeXv(_nodeXv),
      funcName(_funcName)
  {
  }

  ExprNode *nodeXv;
  string funcName;

  AnnoCall *asCall() override { return this; }

  string repr() const override {
    return "call("s + funcName + " at " + ::repr(sourceLoc) + ")";
  }

};

struct AnnoParam : AnnoBase {
  AnnoParam(ExprNode *_nodeXv, YogaParamInfo *_param)
    : nodeXv(_nodeXv),
      param(_param)
  {
  }

  ExprNode *nodeXv;
  YogaParamInfo *param;

  AnnoParam *asParam() override { return this; }

  string repr() const override {
    return "param("s + param->paramName + " at " + ::repr(sourceLoc) + ")";
  }

};

struct AnnoTrain : AnnoBase {
  AnnoTrain(AstAnnoCall *_call)
    : call(_call)
  {
  }

  AstAnnoCall *call;

  AnnoTrain *asTrain() override { return this; }

  string repr() const override {
    return "train("s + call->gloss() + " at " + ::repr(sourceLoc) + ")";
  }

};
