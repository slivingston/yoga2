#pragma once
#include "./ast.h"
#include "./compilation.h"

template<typename T, typename ...Args>
T * YogaContext::mkAst(Args&& ...args) const
{
  unique_ptr ptr = make_unique<T>(std::forward<Args>(args)...);
  T *ret = ptr.get();
  reg->astPool.emplace_back(std::move(ptr));

  ret->sourceLoc = lastLoc;

  if (0) cerr << ret->desc() + "\n";

  return ret;
}
