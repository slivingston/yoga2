/*
  This file declares a bunch of utilities for easier interaction with LLVM.
*/
#include "./jit_utils.h"
#include "./compilation.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/ExecutionEngine/JITSymbol.h"
#include "llvm/ExecutionEngine/Orc/CompileUtils.h"
#include "llvm/ExecutionEngine/Orc/Core.h"
#include "llvm/ExecutionEngine/Orc/ExecutionUtils.h"
#include "llvm/ExecutionEngine/Orc/IRCompileLayer.h"
#include "llvm/ExecutionEngine/Orc/JITTargetMachineBuilder.h"
#include "llvm/ExecutionEngine/Orc/RTDyldObjectLinkingLayer.h"
#include "llvm/ExecutionEngine/SectionMemoryManager.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Transforms/Scalar.h"


using namespace llvm;
using namespace llvm::orc;

/*
  An ExprOut represents a value, either an lvalue or rvalue. A ExprNode populates one with its .emit method.
  Normally you call e.getExprOut(ctx, n) to get the value of a node.

  It can have a value (v), pointer-to-value (p), or both.
  We keep around the YogaType (t) also, because the llvm::Value doesn't capture some information
  like whether the type is signed. We need that for conversions.

  They're mutable, so always pass by non-const reference.
  (Even as the source of an assignment they're mutable, because if there's only a pointer
  we might need to assign a value or vice versa.

  Given an YogaContext &ctx, EmitCtx &e, ExprOut &x, ExprOut &y;

    e.assign(ctx, x, y):
      Emits instructions to assign x=y, with appropriate type conversions.

    Value *v = e.getv(x):
      Returns a value, emiting a load instruction if there's only a pointer

    Value *p = e.getp(x):
      Returns a pointer to the value, allocating the space on the stack and storing from .v if it exists

    e.setv(ctx, x, v):
      Set the v in 

  ExprOut's boolean converssion is true if it has a value. Most often used as
      auto &v = e.getExprOut(ctx, n)l
      if (!v) return false;
  ctx.logError should have been called to record why a value wasn't returned.

  If an expr returns more than a single value, it can call e.setextra(ctx, x, name, v)
  For example, ExprCallYoga instructions return multiple values, and ExprReturnVal uses them.

*/
struct ExprOut {
  ExprOut() = default;
  ExprOut(YogaType *_t) : t(_t) {}
  Value *v{nullptr};
  Value *p{nullptr};
  YogaType *t{nullptr};

  shared_ptr<map<string, Value *>> extra;

  explicit operator bool() const {
    return v || p || extra;
  }
};

struct EmitCtx {
  EmitCtx(JITTargetMachineBuilder jtmb, DataLayout const &_dl, YogaCompilation *_reg);

  YogaCompilation *reg;
  LLVMContext l;

  ExecutionSession es;
  RTDyldObjectLinkingLayer objectLayer;
  IRCompileLayer compileLayer;
  DataLayout dl;
  MangleAndInterner mangle;
  ThreadSafeContext tsc;
  LocalCXXRuntimeOverrides rto;
  llvm::orc::JITDylib *mainJD{nullptr};

  string dumpStr;
  raw_string_ostream dumpStream;

  Module *m{nullptr};
  std::unique_ptr<IRBuilder<>> builder;
  std::unique_ptr<IRBuilder<>> entryBuilder;

  std::unique_ptr<llvm::FunctionPassManager> fpm;

  FunctionCallee curFunc;

  SymbolMap foreigns;

  map<string, vector<std::function<bool (void *addr)>>> onResolved;

  map<string, ExprOut> argPtrs;
  unordered_map<string, ExprOut> exprOutsByCseKey;

  deque<ExprOut> constantPool;

  size_t allocCount{0};

  Type *memoryResourceTy{nullptr};
  Type *yogaSetTy{nullptr};
  
  FunctionType *yogaFunctionTy{nullptr};
  FunctionType *yogaGradTy{nullptr};
  FunctionType *yogaDebugTy{nullptr};
  FunctionType *yogaInternalsTy{nullptr};
  FunctionType *yogaAccessorTy{nullptr};
  FunctionType *yogaLinearOpTy{nullptr};
  FunctionType *yogaNormTy{nullptr};

  FunctionType *yogicAllocTy{nullptr};
  FunctionType *yogicStrcatTy{nullptr};

  FunctionType *packetWrValueFuncTy{nullptr};
  FunctionType *packetRdValueFuncTy{nullptr};

  ExprOut &getExprOut(YogaContext const &ctx, ExprNode *n);
  AllocaInst *mkAlloca(YogaContext const &ctx, Type *ty, string const &name = "");
  AllocaInst *mkAlloca(YogaContext const &ctx, ExprNode *n);

  Value *cast(Value *src, Type *dstTy, bool isSigned=true);
  [[nodiscard]] bool load(YogaContext const &ctx, ExprOut &o);
  [[nodiscard]] bool alloc(YogaContext const &ctx, ExprOut &o);
  [[nodiscard]] bool allocZero(YogaContext const &ctx, ExprOut &o);
  [[nodiscard]] bool store(YogaContext const &ctx, ExprOut &o);
  [[nodiscard]] bool setv(YogaContext const &ctx, ExprOut &o, Value *v);
  [[nodiscard]] bool settv(YogaContext const &ctx, ExprOut &o, YogaType *t, Value *v);
  [[nodiscard]] bool setp(YogaContext const &ctx, ExprOut &o, Value *p);
  [[nodiscard]] bool settp(YogaContext const &ctx, ExprOut &o, YogaType *t, Value *p);
  [[nodiscard]] bool setextra(YogaContext const &ctx, ExprOut &o, string const &name, Value *v);
  Value *getv(ExprOut &o);
  Value *getp(ExprOut &o);
  Value *getextra(YogaContext const &ctx, ExprOut &o, string const &name);

  void setZero(AllocaInst *ptr);
  void setInitial(YogaContext const &ctx, AllocaInst *ptr);
  AllocaInst *storeResult(YogaContext const &ctx, ExprNode *n, Value *rv);

  [[nodiscard]] bool getMatrixElem(YogaContext const &ctx, ExprOut &o, ExprOut &mat, int ri, int ci);
  [[nodiscard]] bool getMatrixElems(YogaContext const &ctx, vector<ExprOut> &os, ExprOut &mat);
  [[nodiscard]] bool setMatrixElem(YogaContext const &ctx, ExprOut &mat, int ri, int ci, ExprOut &rv);
  [[nodiscard]] bool setMatrixElem(YogaContext const &ctx, ExprOut &mat, int ri, int ci, Value *rv);

  [[nodiscard]] bool getStructElem(YogaContext const &ctx, ExprOut &o, ExprOut &st, StructMember const *memInfo);

  [[nodiscard]] bool assign(YogaContext const &ctx, ExprOut &dst, ExprOut &src);
  [[nodiscard]] bool assignPluseqInplace(YogaContext const &ctx, ExprOut &dst, ExprOut &src);

  ExprOut promoteValue(YogaContext const &ctx, ExprOut src, YogaType *dstType);

  Value *emitTypedPtr(YogaContext const &ctx, Type *ty, void *ptr);


  void * lookup(YogaContext const &ctx, string const &name);
  FunctionCallee defineForeign(string const &implName, void *f, FunctionType *fTy);


  AllocaInst *createEntryBlockAlloca(Function *f, Type *t, std::string const &varName);
  void dumpLlvmInsns(ostream &os);

  Function *startFunction(FunctionType *fTy, string const &implName);
  [[nodiscard]] bool endFunction();

  llvm::Value *getYogaArg(int idx1, int idx2, Type *argType);


  ExprOut &emitInt(int x);
  ExprOut &emitDouble(double x);
  ExprOut &emitBool(bool x);

  Value *asDoublePtr(Value *p);
  Value *asU8Ptr(Value *p);

  Value * allocArgsPad(YogaContext const &ctx, size_t size);

  Value *emitRpRpRp(
    string const &name,
    Value *rp,
    Value *a0p,
    Value *a1p);

  Value *emitDgemm(YogaContext const &ctx, 
    Value *order, Value *transA, Value *transB, 
    Value *m, Value *n, Value *k,
    Value *alpha,
    Value *a, Value *lda,
    Value *b, Value *ldb,
    Value *beta,
    Value *c, Value *ldc);
  Value *emitDcopy(YogaContext const &ctx, 
      Value *n, Value *x, Value *incX, Value *y, Value *incY);
  Value *emitDaxpy(YogaContext const &ctx, 
      Value *n, Value *alpha, Value *x, Value *incX, Value *y, Value *incY);


  [[nodiscard]] bool emitCallo(YogaContext const &ctx, ExprOut &a,
      std::function<void(void *)> const &f);
  [[nodiscard]] bool emitCalloi(YogaContext const &ctx, ExprOut &a, ExprOut &b,
      std::function<void(void *, void *)> const &f);
  [[nodiscard]] bool emitCalloii(YogaContext const &ctx, ExprOut &a, ExprOut &b, ExprOut &c,
      std::function<void(void *, void *, void *)> const &f);

  bool emitLogPtr(YogaContext const &ctx, Value *a, std::function<void(void *)> const &f);

  [[nodiscard]] bool emitCos(YogaContext const &ctx, ExprOut &o, ExprOut &a);
  [[nodiscard]] bool emitSin(YogaContext const &ctx, ExprOut &o, ExprOut &a);
  [[nodiscard]] bool emitSqrt(YogaContext const &ctx, ExprOut &o, ExprOut &a);
  [[nodiscard]] bool emitFabs(YogaContext const &ctx, ExprOut &o, ExprOut &a);

  pair<Value *, Value *> loadCplx(YogaContext const &ctx, ExprOut &cplx);
  bool storeCplx(YogaContext const &ctx, ExprOut &cplx, Value *real, Value *imag);
  
  bool emitAdd(YogaContext const &ctx, ExprOut &o, ExprOut &a0, ExprOut &a1);
  bool emitMul(YogaContext const &ctx, ExprOut &o, ExprOut &a0, ExprOut &a1);
  bool emitConjugate(YogaContext const &ctx, ExprOut &o, ExprOut &a);
  bool emitStructref(YogaContext const &ctx, ExprOut &o, ExprOut &a, StructMember *memberInfo);
  bool emitConvDoubleBool(YogaContext const &ctx, ExprOut &o, ExprOut &a);
  bool emitNormSq(YogaContext const &ctx, ExprOut &o, ExprOut &a);

  bool emitCmp(YogaContext const &ctx, string const &cmp, ExprOut &o, ExprOut &a0, ExprOut &a1);

  Value *emitAlloc(YogaContext const &ctx, size_t size);

  bool emitSetInit(YogaContext const &ctx, ExprOut &o);
  bool emitSetCons(YogaContext const &ctx,
    ExprOut &o,
    ExprOut &next,
    ExprOut &value,
    ExprOut &modulation,
    YogaSourceLoc sourceLoc);

};


struct AccessorBuilder {
  AccessorBuilder(
    YogaContext const &_ctx,
    EmitCtx &_e,
    YogaType *_topLevelType,
    string _subName,
    string _opName,
    ExprOut _yv,
    ExprOut _cv)
  : ctx(_ctx),
    e(_e),
    topLevelType(_topLevelType),
    subName(move(_subName)),
    opName(move(_opName)),
    yv(move(_yv)),
    cv(move(_cv))
  {
    
  }

  YogaContext ctx;
  EmitCtx &e;
  YogaType *topLevelType;
  string subName;
  string opName;

  ExprOut yv;
  ExprOut cv;

  void setPointerType(YogaType *ptrType);
  [[nodiscard]] bool end();
};


struct LinearOpBuilder {
  LinearOpBuilder(
    YogaContext const &_ctx,
    EmitCtx &_e,
    YogaType *_topLevelType,
    string _opName)
  : ctx(_ctx),
    e(_e),
    topLevelType(_topLevelType),
    opName(move(_opName))
  {
    
  }

  YogaContext ctx;
  EmitCtx &e;
  YogaType *topLevelType;
  string opName;

  [[nodiscard]] bool end();
};

template<typename T>
inline T * notnull(T *a) {
  if (!a) throw logic_error("shouldn't be null");
  return a;
}

template<typename T>
inline T const * notnull(T const *a) {
  if (!a) throw logic_error("shouldn't be null");
  return a;
}
