#pragma once
#include "./jit_utils.h"
#include "./jit_fwd.h"
#include "./type.h"
#include "./sourcefile.h"
#include "./compilation.h"
#include "./llvm_fwd.h"

struct ExprNode {

  ExprNode(
    string _op)
      : op(move(_op))
  {
  }

  ExprNode(
    string _op,
    vector<ExprNode *> _args)
      : op(move(_op)),
        args(move(_args))
  {
  }

  ExprNode(
    string _op,
    ExprNode *_a)
      : op(move(_op)),
        args{_a}
  {
  }

  ExprNode(
    string _op,
    ExprNode *_a,
    ExprNode *_b)
      : op(move(_op)),
        args{_a, _b}
  {
  }

  ExprNode(
    string _op,
    ExprNode *_a,
    ExprNode *_b,
    ExprNode *_c)
      : op(move(_op)),
        args{_a, _b, _c}
  {
  }

  virtual ~ExprNode() = default;
  ExprNode(ExprNode const &) = delete;
  ExprNode(ExprNode &&) = delete;
  ExprNode & operator = (ExprNode const &) = delete;
  ExprNode & operator = (ExprNode &&) = delete;

  string op;
  vector<ExprNode *> args;

  YogaSourceLoc sourceLoc;
  YogaType *type{nullptr};
  string cseKey;
  string debugLocalName;

  void setCseKey(string const &_cseKey);
  void setCse(string const &prefix, string const &deets, string const &extra);
  void setCseFromArgs(string const &extraDeets = ""s, string const &extraLiteral = ""s);

  YogaType *argsAllSameType() const;

  [[nodiscard]] virtual bool getRefName(string &refName) const { return false; }
  [[nodiscard]] virtual bool isVarNamed(string const &n) const { return false; }
  [[nodiscard]] virtual bool isNumericConst(double &v) const { return false; }
  [[nodiscard]] virtual bool isNumericConst(int &v) const { return false; }

  virtual ExprObject *asObject() { return nullptr; }
  virtual ExprUnboundVar *asUnboundVar() { return nullptr; }
  virtual ExprVar *asVar() { return nullptr; }
  virtual ExprStructref *asStructref() { return nullptr; }
  virtual ExprMkStruct *asMkStruct() { return nullptr; }

  virtual ExprNode *peephole(YogaContext const &ctx);
  virtual ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt);  
  virtual void backprop(YogaContext const &ctx, GradientExprSet &grads);
  virtual void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g);
  virtual void addChildDeps(YogaContext const &ctx, GradientExprSet &grads);
  virtual bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o);
  virtual void dump(ostream &s, string const &indent, int depth) const;
};

ostream & operator <<(ostream &s, ExprNode const &a);

string repr_ref(ExprNode const *a);


struct ExprConst : ExprNode {
  ExprConst(string _op)
    : ExprNode(move(_op))
  {
  }

  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
};


struct ExprConstDouble : ExprConst {
  ExprConstDouble(
    double _literalValue)
    : ExprConst("cd"),
      literalValue(_literalValue)
  {
  }

  double literalValue;

  [[nodiscard]] bool isNumericConst(double &v) const override
  {
    v = literalValue;
    return true;
  }
  [[nodiscard]] bool isNumericConst(int &v) const override
  {
    if (literalValue == floor(literalValue)) {
      v = int(literalValue);
      return true;
    }
    return false;
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct ExprConstComplex : ExprConst {
  ExprConstComplex(
    std::complex<double> _literalValue)
    : ExprConst("cc"),
      literalValue(_literalValue)
  {
  }

  std::complex<double> literalValue;

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct ExprConstZero : ExprConst {
  ExprConstZero(
    YogaType *_type)
    : ExprConst("zero." + _type->clsName)
  {
    type = _type;
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct ExprConstOne : ExprConst {
  ExprConstOne(
    YogaType *_type)
    : ExprConst("one." + _type->clsName)
  {
    type = _type;
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct ExprConstString : ExprConst {
  ExprConstString(
    string _literalValue)
    : ExprConst("string"),
      literalValue(move(_literalValue))
  {
  }

  string literalValue;

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct ExprConstVoid : ExprConst {
  ExprConstVoid()
    : ExprConst("void")
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct ExprParam : ExprNode {
  ExprParam(
    YogaParamInfo *_paramInfo)
    : ExprNode("param"),
      paramInfo(_paramInfo)
  {
  }

  YogaParamInfo *paramInfo;
  [[nodiscard]] bool getRefName(string &refName) const override
  {
    refName = "yogaParams."s + paramInfo->paramName;
    return true;
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct ExprParamGrad : ExprNode {
  ExprParamGrad(
    YogaParamInfo *_paramInfo)
    : ExprNode("paramGrad"),
      paramInfo(_paramInfo)
  {
  }

  YogaParamInfo *paramInfo;
  [[nodiscard]] bool getRefName(string &refName) const override
  {
    refName = "yogaParamGrads."s + paramInfo->paramName;
    return true;
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct ExprVar : ExprNode {
  ExprVar(
    YogaType *_type,
    string _funcName,
    string _name,
    YogaDirection _dir)
    : ExprNode("var"),
      funcName(move(_funcName)),
      name(move(_name)),
      dir(_dir)
  {
    type = _type;
  }

  string funcName;
  string name;
  YogaDirection dir;


  ExprVar *asVar() override
  {
    return this;
  }

  [[nodiscard]] bool isVarNamed(string const &n) const override
  {
    return name == n;
  }

  [[nodiscard]] bool getRefName(string &refName) const override
  {
    refName = name; // was funcName + "."s + name;
    return true;
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};



struct ExprUnboundVar : ExprNode {
  ExprUnboundVar(
    string _name)
    : ExprNode("unboundVar"),
      name(move(_name))
  {
  }

  string name;

  ExprUnboundVar *asUnboundVar() override
  {
    return this;
  }

  [[nodiscard]] bool isVarNamed(string const &n) const override
  {
    return name == n;
  }

  [[nodiscard]] bool getRefName(string &refName) const override
  {
    refName = name;
    return true;
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct ExprMkComplex : ExprNode {
  ExprMkComplex(
    ExprNode *_real,
    ExprNode *_imag)
    : ExprNode("mkComplex", _real, _imag)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};

struct ExprExtractReal : ExprNode {
  ExprExtractReal(
    ExprNode *_cplx)
    : ExprNode("extractReal", _cplx)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};

struct ExprExtractImag : ExprNode {
  ExprExtractImag(
    ExprNode *_cplx)
    : ExprNode("extractImag", _cplx)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprStructref : ExprNode {
  ExprStructref(
    ExprNode *_lhs,
    string const &_memberName,
    StructMember *_memberInfo)
    : ExprNode("."s + _memberName, _lhs),
      memberName(_memberName),
      memberInfo(_memberInfo)
  {
  }

  string memberName;
  StructMember *memberInfo;


  [[nodiscard]] bool getRefName(string &refName) const override
  {
    string baseRef;
    if (!args[0]->getRefName(baseRef)) return false;
    refName = baseRef + "."s + memberName;
    return true;
  }

  ExprStructref *asStructref() override { return this; }
  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprMkStruct : ExprNode {
  ExprMkStruct(
    YogaTypeStruct *_type,
    vector<ExprNode *> const &_memberValues)
    : ExprNode("struct "s + _type->clsName, _memberValues)
  {
    type = _type;
  }

  ExprMkStruct *asMkStruct() override { return this; }
  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};

/*
  This op can't be lowered, but serves as a placeholder for literal arrays
  in the code.
*/
struct ExprMkList : ExprNode {
  ExprMkList(
    vector<ExprNode *> const &_values)
    : ExprNode("list"s, _values)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
};


struct ExprConvIntegerDouble : ExprNode {
  ExprConvIntegerDouble(
    ExprNode *_a)
    : ExprNode("convIntegerDouble", _a)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;

};

struct ExprConvBoolDouble : ExprNode {
  ExprConvBoolDouble(
    ExprNode *_a)
    : ExprNode("convBoolDouble", _a)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;

};


struct ExprConvDoubleBool : ExprNode {
  ExprConvDoubleBool(
    ExprNode *_a)
    : ExprNode("convDoubleBool", _a)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};




struct ExprAdd : ExprNode {
  ExprAdd(
    ExprNode *_a,
    ExprNode *_b)
    : ExprNode("add", _a, _b)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprSub : ExprNode {
  ExprSub(
    ExprNode *_a,
    ExprNode *_b)
    : ExprNode("sub", _a, _b)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprMul : ExprNode {
  ExprMul(
    ExprNode *_a,
    ExprNode *_b)
    : ExprNode("mul", _a, _b)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprConjugate : ExprNode {
  ExprConjugate(
    ExprNode *_a)
    : ExprNode("conjugate", _a)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  [[nodiscard]] static bool knownOp(string const &_op);
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};



struct ExprNograd : ExprNode {
  ExprNograd(
    ExprNode *_a)
    : ExprNode("nograd", _a)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  [[nodiscard]] static bool knownOp(string const &_op);
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};



struct ExprDot : ExprNode {
  ExprDot(
    ExprNode *_a,
    ExprNode *_b)
    : ExprNode("dot", _a, _b)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  [[nodiscard]] static bool knownOp(string const &_op);
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};

struct ExprDiv : ExprNode {
  ExprDiv(
    ExprNode *_a,
    ExprNode *_b)
    : ExprNode("div", _a, _b)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprPow : ExprNode {
  ExprPow(
    ExprNode *_a,
    ExprNode *_b)
    : ExprNode("pow", _a, _b)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprCompare : ExprNode {
  ExprCompare(
    string const &_op,
    ExprNode *_a,
    ExprNode *_b)
    : ExprNode(_op, _a, _b)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  [[nodiscard]] static bool knownOp(string const &_op);
};


struct ExprLogicalOr : ExprNode {
  ExprLogicalOr(
    ExprNode *_a,
    ExprNode *_b)
    : ExprNode("||", _a, _b)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};

struct ExprLogicalAnd : ExprNode {
  ExprLogicalAnd(
    ExprNode *_a,
    ExprNode *_b)
    : ExprNode("&&", _a, _b)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprNot : ExprNode {
  ExprNot(
    ExprNode *_a)
    : ExprNode("not", _a)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};

struct ExprNeg : ExprNode {
  ExprNeg(
    ExprNode *_a)
    : ExprNode("neg", _a)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprLim01 : ExprNode {
  ExprLim01(
    ExprNode *_a)
    : ExprNode("lim01", _a)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
};


struct ExprLinearComb : ExprNode {
  ExprLinearComb(
    vector<ExprNode *> const &_args)
    : ExprNode("linearComb", _args)
  {
  }

  vector<ExprNode *> terms;
  ExprNode *num{nullptr};
  ExprNode *den{nullptr};
  ExprNode *result{nullptr};

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};

struct ExprMinMax : ExprNode {
  ExprMinMax(
    string const &_op,
    vector<ExprNode *> const &_args)
    : ExprNode(_op, _args)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  [[nodiscard]] static bool knownOp(string const &_op);
};


struct ExprClamp : ExprNode {
  ExprClamp(
    string const &_op,
    vector<ExprNode *> const &_args)
    : ExprNode(_op, _args)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  [[nodiscard]] static bool knownOp(string const &_op);

};


struct ExprClampGrad : ExprNode {
  ExprClampGrad(
    string const &_op,
    vector<ExprNode *> const &_args)
    : ExprNode(_op, _args)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;

};


struct ExprTrig1 : ExprNode {
  ExprTrig1(
    string const &_op,
    ExprNode *_arg)
    : ExprNode(_op, _arg)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  [[nodiscard]] static bool knownOp(string const &_op);
};



struct ExprTrig2 : ExprNode {
  ExprTrig2(
    string const &_op,
    vector<ExprNode *> const &_args)
    : ExprNode(_op, _args)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  [[nodiscard]] static bool knownOp(string const &_op);
};



struct ExprObject : ExprNode {
  ExprObject(
    vector<string> _keys,
    vector<ExprNode *> _values,
    ExprNode *_defaultValue = nullptr)
    : ExprNode("Object", move(_values)),
      keys(move(_keys)),
      defaultValue(_defaultValue)
  {
    assert(keys.size() == args.size());
  }

  vector<string> keys;
  ExprNode *defaultValue;

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprObject *asObject() override { return this; }
};



struct ExprConstructor : ExprNode {
  ExprConstructor(
    string const &_clsName,
    vector<ExprNode *> _args)
    : ExprNode("construct." + _clsName, move(_args)),
      clsName(_clsName)
  {
  }

  string clsName;

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *peephole(YogaContext const &ctx) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;

  map<string, pair<vector<StructMember *>, vector<ExprNode *>>> makeNameMap(YogaTypeStruct *t);
};




struct ExprMatrixConstructor : ExprNode {
  ExprMatrixConstructor(
    string const &_op,
    vector<ExprNode *> const &_args)
    : ExprNode(_op, _args)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  [[nodiscard]] static bool knownOp(string const &_op);
};


struct ExprSum : ExprNode {
  ExprSum(
    string const &_op,
    ExprNode *_a0)
    : ExprNode(_op, _a0)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  [[nodiscard]] static bool knownOp(string const &_op);
};



struct ExprMatrixMath : ExprNode {
  ExprMatrixMath(
    string const &_op,
    vector<ExprNode *> const &_args)
    : ExprNode(_op, _args)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  [[nodiscard]] static bool knownOp(string const &_op);
};



struct ExprMatrixIndex : ExprNode {
  ExprMatrixIndex(
    vector<ExprNode *> const &_args)
    : ExprNode("matrixIndex", _args)
  {
  }

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};

enum AugMode {
  AUGMODE_NONE,
  AUGMODE_PLUSEQ,
  AUGMODE_PLUSEQ_INPLACE
};

inline char const *augModeToOp(AugMode m)
{
  switch (m) {
    case AUGMODE_NONE: return "=";
    case AUGMODE_PLUSEQ: return "+=";
    case AUGMODE_PLUSEQ_INPLACE: return "!+=";
    default: throw runtime_error("Unknown AugMode");
  }
}


struct AssignmentSet {
  AugMode mode{AUGMODE_NONE};
  ExprNode *dst{nullptr};
  // pair is value, modulation
  vector<pair<ExprNode *, ExprNode *>> srcs;
  ExprNode *srcPhi{nullptr};
  ExprNode *prohibitedByAssignmentTo{nullptr};

  void addSrc(ExprNode *value, ExprNode *strength) {
    if (srcPhi) throw runtime_error("srcPhi already set");
    srcs.emplace_back(value, strength);
  }

  vector<ExprNode *> getAllArgs() const
  {
    vector<ExprNode *> ret;
    ret.push_back(dst);
    for (auto const &[srcVal, srcMod] : srcs) {
      ret.push_back(srcVal);
      ret.push_back(srcMod);
    }
    return ret;
  }

  ExprNode *getSrcPhi(YogaContext const &ctx)
  {
    if (srcs.empty()) return nullptr;
    if (!srcPhi) {
      srcPhi = ctx("phi").mkExpr<ExprPhi>(*this);
    }
    return srcPhi;
  }

  ExprNode *getDefault(YogaContext const &ctx)
  {
    for (auto const &it : srcs) {
      if (it.second && it.second->cseKey == "cd_eps") return it.first;
    }
    return nullptr;
  }

  void dump(ostream &s, string const &indent, int depth) const;
};


struct ExprPhi : ExprNode {
  ExprPhi(AssignmentSet _aset)
    : ExprNode("phi", _aset.getAllArgs()),
      aset(move(_aset))
  {
  }

  AssignmentSet aset;

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  void addChildDeps(YogaContext const &ctx, GradientExprSet &grads) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct ExprCallYoga : ExprNode {
  ExprCallYoga(
    string const &_funcName,
    vector<ExprNode *> _inArgs,
    vector<YogaType *> _outTypes,
    YogaType *returnType,
    string _debugLocalMemberName)
    : ExprNode(_funcName + ".yoga", move(_inArgs)),
      funcName(_funcName),
      outTypes(move(_outTypes)),
      debugLocalMemberName(move(_debugLocalMemberName))
  {
    type = returnType;
  }

  string funcName;
  vector<YogaType *> outTypes;
  vector<ExprNode *> outVals;
  string debugLocalMemberName;

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};


struct ExprCallYogaGrad : ExprNode {
  ExprCallYogaGrad(
    string const &_funcName,
    vector<ExprNode *> const &_inArgs,
    vector<YogaType *> const &_outTypes,
    vector<ExprNode *> const &_outArgGradIns,
    YogaType *returnType)
    : ExprNode(_funcName + ".yogagrad", concat(_inArgs, _outArgGradIns)),
      funcName(_funcName),
      inArgs(_inArgs),
      outTypes(_outTypes),
      outArgGradIns(_outArgGradIns)
  {
    type = returnType;
  }

  string funcName;
  vector<ExprNode *> inArgs;
  vector<YogaType *> outTypes;
  vector<ExprNode *> outVals;
  vector<ExprNode *> outArgGradIns;
  vector<ExprNode *> inArgGradOuts;

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
};



struct ExprReturnVal : ExprNode {
  ExprReturnVal(
    YogaType *_type,
    ExprNode *_call,
    int _argi)
    : ExprNode("returnVal", _call),
      argi(_argi)
  {
    type = _type;
  }

  int argi;

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct ExprReturnGrad : ExprNode {
  ExprReturnGrad(
    YogaType *_type,
    ExprNode *_call,
    int _argi)
    : ExprNode("returnGrad", _call),
      argi(_argi)
  {
    type = _type;
  }

  int argi;

  [[nodiscard]] bool resolveTypes(YogaContext const &ctx);
  void addChildDeps(YogaContext const &ctx, GradientExprSet &grads) override;
  void backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g) override;
  ExprNode *deriv(YogaContext const &ctx, ExprNode *wrt) override;
  bool emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

