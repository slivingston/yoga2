#include "./expr.h"
#include "./type.h"
#include "./param.h"
#include "./effects.h"
#include "./runtime.h"
#include "../geom/geom.h"

#include "./expr_impl.h"

#include "./emit_ctx.h"

bool ExprNode::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  return ctx.logError("emit: not implemented", this);
}

/*
  Var
*/

bool ExprVar::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  o = e.argPtrs[name];
  if (!o) {
    return ctx.logError("var: nothing named "s + shellEscape(name));
  }
  return true;
}


/*
  ReturnVal
*/

bool ExprReturnVal::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  assert(args.size() == 1);
  auto callout = e.getExprOut(ctx, args[0]);
  if (!callout) return false;

  auto outArgsPad = e.getextra(ctx, callout, "outArgsPad");
  if (!outArgsPad) return ctx.logError("No outArgsPad from call");

  auto outArgSlot = e.builder->CreateStructGEP(
    outArgsPad,
    argi);

  auto returnPtrType = type->llType->getPointerTo();
  if (!returnPtrType || returnPtrType->isVoidTy()) throw runtime_error("missing return ptr type");
  auto rp = e.builder->CreateBitCast(
    e.builder->CreateLoad(outArgSlot),
    returnPtrType);
  
  return e.setp(ctx, o, rp);
}

void ExprReturnGrad::addChildDeps(YogaContext const &ctx, GradientExprSet &grads)
{
  // Nothing
}


bool ExprReturnGrad::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  assert(args.size() == 1);
  auto callout = e.getExprOut(ctx, args[0]);
  if (!callout) return false;

  auto inArgsGradOutPad = e.getextra(ctx, callout, "inArgsGradOutPad");
  if (!inArgsGradOutPad) {
    return ctx.logError("No inArgsGradOutPad from call");
  }

  auto inArgsGradOutSlot = e.builder->CreateStructGEP(
    inArgsGradOutPad,
    argi);

  auto returnPtrType = type->llType->getPointerTo();
  if (!returnPtrType || returnPtrType->isVoidTy()) throw runtime_error("missing return ptr type");
  auto rp = e.builder->CreateBitCast(
    e.builder->CreateLoad(inArgsGradOutSlot),
    returnPtrType);
  
  return e.setp(ctx, o, rp);
}


/*
 Complex
*/

bool ExprMkComplex::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  auto a1 = e.getExprOut(ctx, args[1]);
  if (!a0 || !a1) return false;

  if (!e.alloc(ctx, o)) return false;

  return e.storeCplx(ctx, o, e.getv(a0), e.getv(a1));
}

bool ExprExtractReal::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;
  auto [realPart, imagPart] = e.loadCplx(ctx, a0);
  return e.setv(ctx, o, realPart);
}

bool ExprExtractImag::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;
  auto [realPart, imagPart] = e.loadCplx(ctx, a0);
  return e.setv(ctx, o, imagPart);
}



/*
  Structref
*/

bool ExprStructref::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto base = e.getExprOut(ctx, args[0]);
  if (!base) return false;
  return e.emitStructref(ctx, o, base, memberInfo);
}

/*
  Const
*/

bool ExprConstDouble::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  return e.setv(ctx, o, ConstantFP::get(e.l, APFloat(literalValue)));
}

bool ExprConstComplex::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (!e.alloc(ctx, o)) return false;

  auto oRealPtr = e.builder->CreateStructGEP(o.p, 0);
  auto oImagPtr = e.builder->CreateStructGEP(o.p, 1);

  e.builder->CreateStore(ConstantFP::get(e.l, APFloat(literalValue.real())), oRealPtr);
  e.builder->CreateStore(ConstantFP::get(e.l, APFloat(literalValue.imag())), oImagPtr);

  return true;
}

bool ExprConstZero::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (!e.alloc(ctx, o)) return false;

  e.builder->CreateMemSet(
    e.getp(o),
    Constant::getNullValue(Type::getInt8Ty(e.l)),
    e.dl.getTypeStoreSize(this->type->llType),
    MaybeAlign(e.dl.getABITypeAlignment(this->type->llType)));
  return true;
}

bool ExprConstOne::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (!e.alloc(ctx, o)) return false;

  if (type->isReal()) {
    return e.setv(ctx, o, ConstantFP::get(type->llType, 1.0));
  }
  else if (type->isBool() || type->isScalar()) {
    return e.setv(ctx, o, ConstantInt::get(type->llType, 1));
  }

  return ctx.logError("emit: WRITEME: ExprConstOne::emit for "s + repr_type(type));
}

bool ExprConstString::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  return e.setv(ctx, o, e.emitTypedPtr(ctx, Type::getInt8PtrTy(e.l), (void *)yogic_intern(literalValue)));
}


/*
  Param
*/

bool ExprParam::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  return e.setp(ctx, o,
    e.builder->CreateBitCast(
      e.builder->CreateConstInBoundsGEP1_32(
        Type::getDoubleTy(e.l),
        e.getp(e.argPtrs["yogaParamsLin"]),
        paramInfo->paramIndex,
        "param."s + paramInfo->paramName),
    paramInfo->paramType->llType->getPointerTo()));
}

bool ExprParamGrad::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  return e.setp(ctx, o, 
    e.builder->CreateBitCast(
      e.builder->CreateConstInBoundsGEP1_32(
        Type::getDoubleTy(e.l),
        e.getp(e.argPtrs["yogaParamsLinGrad"]),
        paramInfo->paramIndex,
        "paramGrad."s + paramInfo->paramName),
    paramInfo->paramType->llType->getPointerTo()));
}

/*
  Phi
*/

bool ExprPhi::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (aset.mode == AUGMODE_NONE && 
      aset.srcs.size() == 1 &&
      (aset.srcs[0].second->cseKey == "cd_1" || aset.srcs[0].second->cseKey == "cd_eps")) {
    if (!e.setv(ctx, o, e.getv(e.getExprOut(ctx, aset.srcs[0].first)))) return false;
    return true;
  }
  if (aset.mode == AUGMODE_NONE &&
      !aset.srcs.empty()) {
    bool allSupportScalarMult = true;
    for (auto &[value, modulation] : aset.srcs) {
      if (!value->type->hasLinearOps()) allSupportScalarMult = false;
    }
 
    if (allSupportScalarMult) {
      ExprNode *dflt = nullptr;
      ExprNode *num = nullptr;
      ExprNode *den = nullptr;
      for (auto &[value, modulation] : aset.srcs) {
        if (modulation->cseKey == "cd_eps") {
          if (dflt) {
            return (bool)ctx(modulation).logError("emit: multiple default values");
          }
          dflt = value;
        }
        else {
          ExprNode *modv = ctx.mkExpr<ExprMul>(modulation, value);
          if (!modv) return false;
          if (num) {
            num = ctx.mkExpr<ExprAdd>(num, modv);
          }
          else {
            num = modv;
          }
          if (den) {
            den = ctx.mkExpr<ExprAdd>(den, modulation);
          }
          else {
            den = modulation;
          }
        }
      }
  
      auto combined = ctx.mkExpr<ExprDiv>(num, den);
      if (!combined) return false;

      if (!dflt) {
        if (!num) throw runtime_error("ExprPhi::emit: No values v1");
        dflt = ctx.mkExpr<ExprConstZero>(num->type);
      }

      auto denv = e.getExprOut(ctx, den);
      if (!denv) return false;
      auto zerov = e.emitDouble(0.0);

      auto isValid = e.builder->CreateFCmpOGT(e.getv(denv), e.getv(zerov));  // den > 0

      auto combinedv = e.getExprOut(ctx, combined);
      auto dfltv = e.getExprOut(ctx, dflt);
      if (!combinedv || !dfltv) return false;

      if (!e.setp(ctx, o, e.builder->CreateSelect(isValid, e.getp(combinedv), e.getp(dfltv)))) return false;
      return true;
    }
    else {

      Value * rp = nullptr;
      Value * maxv = nullptr;

      for (auto &[value, modulation] : aset.srcs) {
        auto src = e.getExprOut(ctx, value);
        auto mod = e.getExprOut(ctx, modulation);

        if (maxv) {
          auto isBetter = e.builder->CreateFCmpOGT(e.getv(mod), maxv);  // modv > maxv

          rp = e.builder->CreateSelect(isBetter, e.getp(src), rp);
          maxv = e.builder->CreateSelect(isBetter, e.getv(mod), maxv);
        }
        else {
          rp = e.getp(src);
          maxv = e.getv(mod);
        }
      }
      if (!e.setp(ctx, o, rp)) return false;
      return true;
    }
  }

  if ((aset.mode == AUGMODE_PLUSEQ || aset.mode == AUGMODE_PLUSEQ_INPLACE) && 
      !aset.srcs.empty() &&
      aset.dst->type == aset.srcs[0].first->type) {
    ExprNode *accum = nullptr;
    for (auto &[value, modulation] : aset.srcs) {
      auto srcMod = ctx(value).mkExpr<ExprMul>(modulation, value);
      accum = accum ? ctx(value).mkExpr<ExprAdd>(accum, srcMod) : srcMod;
    }
    if (!accum) throw runtime_error("ExprPhi::emit: No values v2");
    return accum->emit(ctx, e, o);
  }

  /*
    For example,
    Foo[] foos
    foos += Foo(...)
  */
  if (aset.mode == AUGMODE_PLUSEQ && 
      !aset.srcs.empty() && 
      aset.dst->type->isSet()) {

    auto dstTy = aset.dst->type->llType;
    if (!dstTy || dstTy->isVoidTy()) throw runtime_error("Missing llType");

    ExprOut accum(aset.dst->type);
    if (!e.emitSetInit(ctx, accum)) return false;

    for (auto &[value, modulation] : aset.srcs) {
      auto srcVal = e.getExprOut(ctx, value);
      auto srcMod = e.getExprOut(ctx, modulation);
      if (!srcVal || !srcMod) return false;

      ExprOut consp(aset.dst->type);
      if (!e.emitSetCons(ctx, consp, accum, srcVal, srcMod, value->sourceLoc)) return false;

      accum = consp;
    }
    if (!e.setv(ctx, o, e.getv(accum))) return false;
    return true;
  }

  return ctx(aset.dst).logError("emit: WRITEME: Unhandled phi configuration"s);
}


/*
  Add, Mul, Sub, Div, Pow
*/

bool ExprAdd::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  auto a1 = e.getExprOut(ctx, args[1]);
  if (!a0 || !a1) return false;

  return e.emitAdd(ctx, o, a0, a1);
}

bool ExprMul::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  auto a1 = e.getExprOut(ctx, args[1]);

  if (!a0 || !a1) return false;

  return e.emitMul(ctx, o, a0, a1);
}


bool ExprConjugate::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;

  return e.emitConjugate(ctx, o, a0);
}


bool ExprNograd::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  return args[0]->emit(ctx, e, o);
}

bool ExprDot::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  auto a1 = e.getExprOut(ctx, args[1]);
  if (!a0 || !a1) return false;

  if (a0.t->isMatrix() && a1.t->isMatrix()) {
    auto a0tm = a0.t->asMatrix();
    auto a1tm = a1.t->asMatrix();
    // WRITEME: handle large or dynamic matrices
    if (a0tm->rows > 0 && a0tm->cols > 0 && 
        a1tm->rows > 0 && a1tm->cols > 0 && 
        a0tm->rows == a1tm->rows && 
        a0tm->cols == a1tm->cols && 
        a0tm->elementType == a1tm->elementType) {

      Value *accum = nullptr;
      for (int ri=0; ri < a0tm->rows; ri++) {
        for (int ci=0; ci < a0tm->cols; ci++) {
          ExprOut a0el, a1el;
          if (!e.getMatrixElem(ctx, a0el, a0, ri, ci)) return false;
          if (!e.getMatrixElem(ctx, a1el, a1, ri, ci)) return false;
          auto prodel = e.builder->CreateFMul(e.getv(a0el), e.getv(a1el));
          accum = accum ? e.builder->CreateFAdd(accum, prodel) : prodel;
        }
      }
      return e.setv(ctx, o, accum ? accum : e.getv(e.emitDouble(0.0)));
    }
  }

  if (a0.t->isStruct() && a0.t == a1.t) {
    auto ts = a0.t->asStruct();
    Value *accum = nullptr;
    for (auto &mi : ts->members) {
      if (mi->memberType->isScalar()) {
        ExprOut a0el, a1el;

        if (!e.getStructElem(ctx, a0el, a0, mi)) return false;
        if (!e.getStructElem(ctx, a1el, a1, mi)) return false;
        auto prodel = e.builder->CreateFMul(e.getv(a0el), e.getv(a1el));
        accum = accum ? e.builder->CreateFAdd(accum, prodel) : prodel;
      }
    }
    return e.setv(ctx, o, accum ? accum : e.getv(e.emitDouble(0.0)));
  }

  return ctx.logError("emit: Invalid types for ."s, this);
}

bool ExprSub::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  auto a1 = e.getExprOut(ctx, args[1]);

  if (a0.t->isReal() && a1.t->isReal()) {
    return e.setv(ctx, o, e.builder->CreateFSub(
      e.getv(a0),
      e.getv(a1)));
  }

  if (a0.t->isComplex() && a1.t->isComplex()) {
    auto [a0Real, a0Imag] = e.loadCplx(ctx, a0);
    auto [a1Real, a1Imag] = e.loadCplx(ctx, a1);

    auto oReal = e.builder->CreateFSub(a0Real, a1Real);
    auto oImag = e.builder->CreateFSub(a0Imag, a1Imag);

    e.storeCplx(ctx, o, oReal, oImag);
    return true;
  }

  if (a0.t->isIntegral() && a1.t->isIntegral()) {
    return e.setv(ctx, o, e.builder->CreateFSub(
      e.getv(a0),
      e.getv(a1)));
  }
  if (a0.t->isStruct() && a1.t->isStruct()) {
    return ctx.logError("emit: WRITEME: ExprSub struct-struct");
  }

  if (a0.t->isMatrix() && a1.t->isMatrix()) {
    auto a0tm = a0.t->asMatrix();
    auto a1tm = a1.t->asMatrix();
    // WRITEME: handle large or dynamic matrices
    if (a0tm->rows > 0 && a0tm->cols > 0 && 
        a1tm->rows > 0 && a1tm->cols > 0 && 
        a0tm->rows == a1tm->rows && 
        a0tm->cols == a1tm->cols && 
        a0tm->elementType == a1tm->elementType) {

      for (int ri=0; ri < a0tm->rows; ri++) {
        for (int ci=0; ci < a0tm->cols; ci++) {
          ExprOut a0el, a1el, oel;
          if (!e.getMatrixElem(ctx, a0el, a0, ri, ci)) return false;
          if (!e.getMatrixElem(ctx, a1el, a1, ri, ci)) return false;
          if (!e.getMatrixElem(ctx, oel, o, ri, ci)) return false;
          if (!e.setv(ctx, oel, e.builder->CreateFSub(e.getv(a0el), e.getv(a1el)))) return false;
        }
      }
      return true;
    }
  }

  return ctx.logError("emit: Invalid types for -"s, this);
}

bool ExprDiv::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  auto a1 = e.getExprOut(ctx, args[1]);

  if (a0.t->isReal() && a1.t->isReal()) {
    return e.setv(ctx, o, 
      e.builder->CreateFDiv(
        e.getv(a0), e.getv(a1)));
  }

  if (a0.t->isComplex() && a1.t->isComplex()) {
    // WRITEME
  }

  if ((a0.t->isStruct() || a0.t->isMatrix()) && a1.t->isReal()) {
    auto rv = ctx.mkExpr<ExprMul>(
      args[0],
      ctx.mkExpr<ExprDiv>(
        ctx.mkExpr<ExprConstDouble>(1.0),
        args[1]));
    if (!rv) return false;
    return rv->emit(ctx, e, o);
  }

  return ctx.logError("emit: Invalid types for /"s, this);
}

bool ExprPow::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (args[1]->cseKey == "cd_2") {
    auto v = ctx(args[1]).mkExpr<ExprMul>(
      args[0],
      args[0]);
    if (!v) return ctx(args[1]).logError("emit: Simplifying pow(X,2) to X*X failed");
    return v->emit(ctx, e, o);
  }
  if (args[1]->cseKey == "cd_1") {
    return args[0]->emit(ctx, e, o);
  }
  if (args[1]->cseKey == "cd_neg1") {
    auto v = ctx(args[1]).mkExpr<ExprDiv>(
      ctx.mkExpr<ExprConstDouble>(1.0),
      args[0]);
    if (!v) return ctx(args[1]).logError("emit: Simplifying pow(X,-1) to 1/X failed");
    return v->emit(ctx, e, o);
  }
  if (args[1]->cseKey == "cd_neg2") {
    auto v = ctx(args[1]).mkExpr<ExprDiv>(
      ctx.mkExpr<ExprConstDouble>(1.0), 
      ctx.mkExpr<ExprMul>(args[0], args[0]));
    if (!v) return ctx(args[1]).logError("emit: Simplifying pow(X,-2) to 1/(X*X) failed");
    return v->emit(ctx, e, o);
  }
  if (args[1]->cseKey == "cd_neg3") {
    auto v = ctx(args[1]).mkExpr<ExprDiv>(
      ctx.mkExpr<ExprConstDouble>(1.0), 
      ctx.mkExpr<ExprMul>(args[0], 
        ctx.mkExpr<ExprMul>(args[0], args[0])));
    if (!v) return ctx(args[1]).logError("emit: Simplifying pow(X,-3) to 1/(X*X*x) failed");
    return v->emit(ctx, e, o);
  }

  if (args[1]->cseKey == "cd_half") {
    auto a0 = e.getExprOut(ctx, args[0]);
    if (!a0) return false;

    auto rv = e.builder->CreateUnaryIntrinsic(Intrinsic::sqrt, e.getv(a0));
    rv->setFast(true);
    return e.setv(ctx, o, rv);
  }
  if (args[1]->cseKey == "cd_one_third") {
    auto a0 = e.getExprOut(ctx, args[0]);
    if (!a0) return false;

    auto funcTy = FunctionType::get(Type::getDoubleTy(e.l), {
      Type::getDoubleTy(e.l)
    }, false);
    auto func = e.m->getOrInsertFunction("yogic_cbrt"s, funcTy);
    if (!func) {
      return ctx.logError("emit: Can't link in "s + op + " function");
    }
    return e.setv(ctx, o, e.builder->CreateCall(func, {
      e.getv(a0)
    }));
  }
  if (args[1]->cseKey == "cd_neg_two_thirds") {
    auto a0 = e.getExprOut(ctx, args[0]);
    if (!a0) return false;

    auto funcTy = FunctionType::get(Type::getDoubleTy(e.l), {
      Type::getDoubleTy(e.l)
    }, false);
    auto func = e.m->getOrInsertFunction("yogic_pow_neg_two_thirds"s, funcTy);
    if (!func) {
      return ctx.logError("emit: Can't link in "s + op + " function");
    }
    return e.setv(ctx, o, e.builder->CreateCall(func, {
      e.getv(a0)
    }));
  }
  return ctx.logError("emit: Invalid types for ^"s, this);
}

bool ExprNeg::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;

  if (a0.t->isReal()) {
    return e.setv(ctx, o, e.builder->CreateFNeg(e.getv(a0)));
  }
  return ctx.logError("emit: Invalid types for unary -"s, this);
}

/*
  ConvBoolDouble, ConvDoubleBool
*/

bool ExprConvBoolDouble::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;

  return e.setv(ctx, o, 
    e.builder->CreateSelect(
      e.getv(a0),
      e.getv(e.emitDouble(1.0)),
      e.getv(e.emitDouble(0.0)),
      cseKey));
}


bool ExprConvIntegerDouble::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;

  Value *ov=nullptr;
  if (a0.t->isSigned()) {
    ov = e.builder->CreateSIToFP(e.getv(a0), o.t->llType);
  }
  else {
    ov = e.builder->CreateUIToFP(e.getv(a0), o.t->llType);
  }

  return e.setv(ctx, o, ov);
}

bool ExprConvDoubleBool::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;

  return e.setv(ctx, o,
    e.builder->CreateFCmpOGE(
      e.getv(a0),
      e.getv(e.emitDouble(0.5)),
      cseKey));
}

/*
  Compare
*/

bool ExprCompare::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;
  auto a1 = e.getExprOut(ctx, args[1]);
  if (!a1) return false;
  if (!e.emitCmp(ctx, op, o, a0, a1)) return false;
  return true;
}

extern "C" bool yogic_string_eq(U8 *a, U8 *b) {
  return !strcmp(
    a ? reinterpret_cast<char const *>(a) : "", 
    b ? reinterpret_cast<char const *>(b) : "");
}

/*
  Logical ops: and, or, not
*/

bool ExprLogicalAnd::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;
  auto a1 = e.getExprOut(ctx, args[1]);
  if (!a1) return false;

  if (a0.t->isReal() && a1.t->isReal()) {
    return e.setv(ctx, o, e.builder->CreateFMul(e.getv(a0), e.getv(a1)));
  }
  if (a0.t->isBool() && a1.t->isBool()) {
    return e.setv(ctx, o, e.builder->CreateAnd(e.getv(a0), e.getv(a1)));
  }
  return ctx.logError("emit: Invalid types for &&"s, this);
}

bool ExprLogicalOr::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  auto a1 = e.getExprOut(ctx, args[1]);
  if (!a0 || !a1) return false;

  if (a0.t->isReal() && a1.t->isReal()) {
    auto one = e.emitDouble(1.0);
    return e.setv(ctx, o, e.builder->CreateFSub(
      e.getv(one), 
      e.builder->CreateFMul(
        e.builder->CreateFSub(e.getv(one), e.getv(a0)),
        e.builder->CreateFSub(e.getv(one), e.getv(a1)))));
  }
  if (a0.t->isBool() && a1.t->isBool()) {
    return e.setv(ctx, o, e.builder->CreateOr(e.getv(a0), e.getv(a1)));
  }
  return ctx.logError("emit: Invalid types for ||"s, this);
}

bool ExprNot::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;

  if (a0.t->isReal()) {
    return e.setv(ctx, o, e.builder->CreateFSub(e.getv(e.emitDouble(1.0)), e.getv(a0)));
  }
  if (a0.t->isBool()) {
    return e.setv(ctx, o, e.builder->CreateNot(e.getv(a0)));
  }
  return ctx.logError("emit: Invalid types for !"s, this);
}

/*
  Struct
*/

bool ExprMkStruct::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (!e.allocZero(ctx, o)) return false;

  auto st = o.t->asStruct();
  for (auto &memInfo : st->members) {
    auto val = e.getExprOut(ctx, args.at(memInfo->llIdx));
    if (!val) return false;
    ExprOut oel;
    if (!e.getStructElem(ctx, oel, o, memInfo)) return false;
    if (!e.setv(ctx, oel, e.getv(val))) return false;
  }
  return true;
}

/*
  LinearComb
*/

bool ExprLinearComb::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  // All the work was done converting args into result in .resolveTypes
  return result->emit(ctx, e, o);
}


/*
  MinMax
*/

bool ExprMinMax::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (args[0]->type->isReal()) {
    Value *accum = nullptr;

    for (auto &arg : args) {
      auto a = e.getExprOut(ctx, arg);
      if (!a) return false;

      if (op == "max") {
        // CreateMaximum (ie, the llvm.maximum intrinsic) doesn't work
        // on x86. CreateMaxNum (ie, llvm.maxnum) avoids propagating NaNs, 
        // which might be better anyway
        // See https://llvm.org/docs/LangRef.html#llvm-maxnum-intrinsic
        accum = accum ? e.builder->CreateMaxNum(accum, e.getv(a)) : e.getv(a);
      }
      else if (op == "min") {
        accum = accum ? e.builder->CreateMinNum(accum, e.getv(a)) : e.getv(a);
      }
      else {
        return ctx.logError("emit: Unknown MinMax op "s + op);
      }
    }
    return e.setv(ctx, o, accum ? accum : e.getv(e.emitDouble(0.0)));
  }
  return ctx.logError("emit: Invalid types for "s + op, this);
}


bool ExprClamp::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (op == "clamp" && args.size() == 3) {
    auto a0 = e.getExprOut(ctx, args[0]);
    auto a1 = e.getExprOut(ctx, args[1]);
    auto a2 = e.getExprOut(ctx, args[2]);
    if (!a0 || !a1 || !a2) return false;

    // WRITEME: handle vectors for arg 0
    if (a0.t->isReal() && a1.t->isReal() && a2.t->isReal()) {
      return e.setv(ctx, o, e.builder->CreateMinNum(
        e.builder->CreateMaxNum(e.getv(a0), e.getv(a1)),
          e.getv(a2)));
    }
  }
  else if (op == "relu" && args.size() == 1) {
    auto a0 = e.getExprOut(ctx, args[0]);
    if (a0.t->asMatrix() && o.t->asMatrix()) {
      auto a0tm = a0.t->asMatrix();
      auto zero = e.emitDouble(0.0);
      for (int ci = 0; ci < a0tm->cols; ci++) {
        for (int ri = 0; ri < a0tm->rows; ri++) {
          ExprOut ael;
          if (!e.getMatrixElem(ctx, ael, a0, ri, ci)) return false;
          auto poselv = e.builder->CreateMaxNum(e.getv(ael), e.getv(zero));
          if (!e.setMatrixElem(ctx, o, ri, ci, poselv)) return false;
        }
      }
      return true;
    }
  }
  return ctx.logError("emit: Invalid types for "s + op, this);
}


bool ExprClampGrad::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if ((op == "clampGradArg" || op == "clampGradMin" || op == "clampGradMax") && 
      args.size() == 4) {
    auto a0 = e.getExprOut(ctx, args[0]);
    auto a1 = e.getExprOut(ctx, args[1]);
    auto a2 = e.getExprOut(ctx, args[2]);
    auto g = e.getExprOut(ctx, args[3]);
    if (!a0 || !a1 || !a2 || !g) return false;

    if (a0.t->isReal() && a1.t->isReal() && a2.t->isReal() && g.t->isReal()) {

      ExprOut ltMin(ctx.reg->boolType), gtMax(ctx.reg->boolType);
      if (!e.emitCmp(ctx, "<", ltMin, a0, a1)) return false;
      if (!e.emitCmp(ctx, ">", gtMax, a0, a2)) return false;
      
      auto mutedG = e.builder->CreateFMul(e.getv(g), e.getv(e.emitDouble(0.000001)));
      auto zeroG = e.getv(e.emitDouble(0.0));

      if (op == "clampGradArg") {
        if (!e.setv(ctx, o, e.builder->CreateSelect(e.builder->CreateOr(e.getv(ltMin), e.getv(gtMax)),
          mutedG, e.getv(g)))) return false;
        return true;
      }
      else if (op == "clampGradMin") {
        if (!e.setv(ctx, o, e.builder->CreateSelect(e.getv(ltMin),
          e.getv(g), zeroG))) return false;
        return true;
      }
      else if (op == "clampGradMax") {
        if (!e.setv(ctx, o, e.builder->CreateSelect(e.getv(gtMax),
          e.getv(g), zeroG))) return false;
        return true;
      }
      else {
        return ctx.logError("emit: unknown clampGrad op "s + shellEscape(op), this);
      }
    }
  }
  else if (op == "reluGrad" && args.size() == 2) {
    auto a0 = e.getExprOut(ctx, args[0]);
    auto g = e.getExprOut(ctx, args[1]);
    if (!a0 || !g) return false;
    if (a0.t == g.t && g.t == o.t) {
      ExprOut zero = e.emitDouble(0.0);
      if (auto atm = a0.t->asMatrix()) {
        for (int ci = 0; ci < atm->cols; ci++) {
          for (int ri = 0; ri < atm->rows; ri++) {

            ExprOut a0elem, gelem, oelem(ctx.reg->rType), isneg(ctx.reg->boolType);
            if (!e.getMatrixElem(ctx, a0elem, a0, ri, ci)) return false;
            if (!e.getMatrixElem(ctx, gelem, g, ri, ci)) return false;

            if (!e.emitCmp(ctx, "<", isneg, a0elem, zero)) return false;

            if (!e.setv(ctx, oelem, e.builder->CreateSelect(e.getv(isneg),
              e.getv(zero), e.getv(gelem)))) return false;

            if (!e.setMatrixElem(ctx, o, ri, ci, e.getv(oelem))) return false;

          }
        }
      }
      return true;
    }
  }

  return ctx.logError("emit: Invalid types for clampGrad"s, this);
}


/*
  Trig
*/

bool ExprTrig1::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;

  if (a0.t->isReal()) {
    /*
      Some of these should be available as intrinsics, but something goes wrong trying to link in the math lib.
      Symptom is:
        JIT session error: Symbols not found: { sin }
      
      Similar problem reports:

        https://lists.llvm.org/pipermail/llvm-dev/2018-December/128686.html
        https://lists.llvm.org/pipermail/llvm-dev/2019-January/128857.html

      How does it know that 
        declare double @llvm.sin.f64(double)
      corresponds to calling a function named sin?


    */
    if (op == "abs") {
      return e.emitFabs(ctx, o, a0);
    }
    if (op == "sqrt") {
      return e.emitSqrt(ctx, o, a0);
    }
    if (1) {
      auto funcTy = FunctionType::get(Type::getDoubleTy(e.l), {
        Type::getDoubleTy(e.l)
      }, false);
      auto func = e.m->getOrInsertFunction("yogic_"s + op, funcTy);
      if (!func) {
        return ctx.logError("emit: Can't link in "s + op + " function");
      }
      return e.setv(ctx, o, e.builder->CreateCall(func, {
        e.getv(a0)
      }));
    }
  }
  return ctx.logError("emit: Invalid types for "s + op, this);
}

bool ExprTrig2::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  auto a1 = e.getExprOut(ctx, args[1]);
  if (!a0 || !a1) return false;

  if (args[0]->type->isReal() && args[1]->type->isReal()) {
    auto funcTy = FunctionType::get(Type::getDoubleTy(e.l), {
      Type::getDoubleTy(e.l), 
      Type::getDoubleTy(e.l)
    }, false);
    auto func = e.m->getOrInsertFunction("yogic_"s + op, funcTy);
    if (!func) {
      return ctx.logError("emit: Can't link in "s + op + " function");
    }
    return e.setv(ctx, o, e.builder->CreateCall(func, {
      e.getv(a0),
      e.getv(a1)
    }));
  }

  return ctx.logError("emit: Invalid types for "s + op, this);
}


/*
  Constructor
*/

map<string, pair<vector<StructMember *>, vector<ExprNode *>>> ExprConstructor::makeNameMap(YogaTypeStruct *t)
{
  map<string, pair<vector<StructMember *>, vector<ExprNode *>>> nameMap;

  for (auto &memit : t->members) {
    nameMap[memit->memberName].first.push_back(memit);
  }

  if (args.size() == 1 && args[0]->asObject()) {
    auto a0obj = args[0]->asObject();
    for (size_t keyi = 0; keyi < a0obj->keys.size(); keyi++) {
      nameMap[a0obj->keys[keyi]].second.push_back(a0obj->args[keyi]);
    }

    for (auto &mapit : nameMap) {
      if (!mapit.second.first.empty() && mapit.second.second.empty() && a0obj->defaultValue) {
        mapit.second.second.push_back(a0obj->defaultValue);
      }
    }
  }

  else if (args.size() == t->members.size()) {
    for (size_t argi=0; argi < t->members.size(); argi++) {
      auto &slot = nameMap[t->members[argi]->memberName];
      slot.second.push_back(args[argi]);
    }
  }

  return nameMap;
}

bool ExprConstructor::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (o.t->isStruct()) {
    auto ots = o.t->asStruct();
    if (!e.allocZero(ctx, o)) return false;

    auto nameMap = makeNameMap(ots);

    for (auto &[memberName, dstSrc] : nameMap) {
      auto &[dsts, srcs] = dstSrc;
      if (dsts.size() != 1) {
        return ctx.logError("Invalid keys for " + shellEscape(memberName));
      }
      if (srcs.size() != 1) {
        return ctx.logError("Invalid values for " + shellEscape(memberName));
      }
      auto srcv = e.getExprOut(ctx, srcs[0]);
      if (!srcv) return false;

      ExprOut oel;
      if (!e.getStructElem(ctx, oel, o, dsts[0])) return false;
      if (!e.assign(ctx, oel, srcv)) return false;
    }
    return true;
  }
  else if (o.t->isMatrix()) {
    auto otm = o.t->asMatrix();
    if (!e.allocZero(ctx, o)) return false;

    if (args.size() != otm->rows * otm->cols) {
      return ctx(this).logError("emit: Wrong number of arguments to matrix constructor. Expected "s + 
        to_string(otm->rows) + '*' + to_string(otm->cols) + ", got " + to_string(args.size()));
    }

    int argi=0;
    for (int ci=0; ci < otm->cols; ci++) {
      for (int ri=0; ri < otm->rows; ri++) {
        auto memSrc = e.getExprOut(ctx, args.at(argi));
        if (!memSrc) return false;

        ExprOut oel;
        if (!e.getMatrixElem(ctx, oel, o, ri, ci)) return false;
        if (!e.assign(ctx, oel, memSrc)) return false;
        argi++;
      }
    }
    return true;
  }
  return ctx.logError("emit: Invalid types for constructor"s, this);
}


/*
  MatrixConstructor
*/

bool ExprMatrixConstructor::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (!e.allocZero(ctx, o)) return false;

  if (op == "mat44Identity") {
    return e.emitCallo(ctx, o, [](void *ap) {
      auto &a = *reinterpret_cast<glm::dmat4 *>(ap);
      a = glm::dmat4(1.0);
    });
  }
  if (op == "mat33Identity") {
    return e.emitCallo(ctx, o, [](void *ap) {
      auto &a = *reinterpret_cast<glm::dmat3 *>(ap);
      a = glm::dmat3(1.0);
    });
  }
  if (op == "mat22Identity") {
    return e.emitCallo(ctx, o, [](void *ap) {
      auto &a = *reinterpret_cast<glm::dmat2 *>(ap);
      a = glm::dmat2(1.0);
    });
  }

  if (op == "mat44Translation" && args.size() == 3) {
    for (int i=0; i < 3; i++) {
      auto a = e.getExprOut(ctx, args[i]);
      if (!a) return false;
      if (!e.setMatrixElem(ctx, o, i, 3, a)) return false;
    }
    for (int i=0; i < 4; i++) {
      if (!e.setMatrixElem(ctx, o, i, i, e.emitDouble(1.0))) return false;
    }
    return true;
  }

  if (op == "mat44TranslationDeriv" && args.size() == 6) {
    for (int i=0; i < 3; i++) {
      if (!e.setMatrixElem(ctx, o, i, 3, e.getExprOut(ctx, args[i * 2 + 1]))) return false;
    }
    return true;
  }


  if (op == "mat44RotationX" && args.size() == 1) {
    auto angleSin = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("sin"s, args[0]));
    auto angleCos = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("cos"s, args[0]));
    if (!angleSin || !angleCos) return false;

    if (!e.setMatrixElem(ctx, o, 1, 1, e.getv(angleCos))) return false;
    if (!e.setMatrixElem(ctx, o, 1, 2, e.builder->CreateFNeg(e.getv(angleSin)))) return false;
    if (!e.setMatrixElem(ctx, o, 2, 1, e.getv(angleSin))) return false;
    if (!e.setMatrixElem(ctx, o, 2, 2, e.getv(angleCos))) return false;

    if (!e.setMatrixElem(ctx, o, 0, 0, e.emitDouble(1.0))) return false;
    if (!e.setMatrixElem(ctx, o, 3, 3, e.emitDouble(1.0))) return false;
    return true;
  }

  if (op == "mat44RotationXDeriv" && args.size() == 2) {
    auto angleSin = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("sin"s, args[0]));
    auto angleCos = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("cos"s, args[0]));
    if (!angleSin || !angleCos) return false;

    auto dTheta = e.getExprOut(ctx, args[1]);
    if (!dTheta) return false;

    if (!e.setMatrixElem(ctx, o, 1, 1, 
      e.builder->CreateFMul(e.builder->CreateFNeg(e.getv(angleSin)), e.getv(dTheta)))) return false;;

    if (!e.setMatrixElem(ctx, o, 1, 2, 
      e.builder->CreateFMul(e.builder->CreateFNeg(e.getv(angleCos)), e.getv(dTheta)))) return false;

    if (!e.setMatrixElem(ctx, o, 2, 1, 
      e.builder->CreateFMul(e.getv(angleCos), e.getv(dTheta)))) return false;

    if (!e.setMatrixElem(ctx, o, 2, 2, 
      e.builder->CreateFMul(e.builder->CreateFNeg(e.getv(angleSin)), e.getv(dTheta)))) return false;

    return true;
  }

  if (op == "mat44RotationY" && args.size() == 1) {
    auto angleSin = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("sin"s, args[0]));
    auto angleCos = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("cos"s, args[0]));
    if (!angleSin || !angleCos) return false;

    if (!e.setMatrixElem(ctx, o, 0, 0, e.getv(angleCos))) return false;
    if (!e.setMatrixElem(ctx, o, 0, 2, e.getv(angleSin))) return false;
    if (!e.setMatrixElem(ctx, o, 2, 0, e.builder->CreateFNeg(e.getv(angleSin)))) return false;
    if (!e.setMatrixElem(ctx, o, 2, 2, e.getv(angleCos))) return false;

    if (!e.setMatrixElem(ctx, o, 1, 1, e.emitDouble(1.0))) return false;
    if (!e.setMatrixElem(ctx, o, 3, 3, e.emitDouble(1.0))) return false;

    return true;
  }

  if (op == "mat44RotationYDeriv" && args.size() == 2) {
    auto angleSin = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("sin"s, args[0]));
    auto angleCos = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("cos"s, args[0]));
    if (!angleSin || !angleCos) return false;

    auto dTheta = e.getExprOut(ctx, args[1]);
    if (!dTheta) return false;

    if (!e.setMatrixElem(ctx, o, 0, 0, 
      e.builder->CreateFMul(e.builder->CreateFNeg(e.getv(angleSin)), e.getv(dTheta)))) return false;

    if (!e.setMatrixElem(ctx, o, 0, 2, 
      e.builder->CreateFMul(e.getv(angleCos), e.getv(dTheta)))) return false;

    if (!e.setMatrixElem(ctx, o, 2, 0, 
      e.builder->CreateFMul(e.builder->CreateFNeg(e.getv(angleCos)), e.getv(dTheta)))) return false;

    if (!e.setMatrixElem(ctx, o, 2, 2, 
      e.builder->CreateFMul(e.builder->CreateFNeg(e.getv(angleSin)), e.getv(dTheta)))) return false;

    return true;
  }

  if (op == "mat44RotationZ" && args.size() == 1) {
    auto angleSin = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("sin"s, args[0]));
    auto angleCos = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("cos"s, args[0]));
    if (!angleSin || !angleCos) return false;

    if (!e.setMatrixElem(ctx, o, 0, 0, e.getv(angleCos))) return false;
    if (!e.setMatrixElem(ctx, o, 0, 1, e.builder->CreateFNeg(e.getv(angleSin)))) return false;
    if (!e.setMatrixElem(ctx, o, 1, 0, e.getv(angleSin))) return false;
    if (!e.setMatrixElem(ctx, o, 1, 1, e.getv(angleCos))) return false;

    if (!e.setMatrixElem(ctx, o, 2, 2, e.emitDouble(1.0))) return false;
    if (!e.setMatrixElem(ctx, o, 3, 3, e.emitDouble(1.0))) return false;

    return true;
  }

  if (op == "mat44RotationZDeriv" && args.size() == 2) {
    auto angleSin = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("sin"s, args[0]));
    auto angleCos = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("cos"s, args[0]));
    if (!angleSin || !angleCos) return false;

    auto dTheta = e.getExprOut(ctx, args[1]);
    if (!dTheta) return false;

    if (!e.setMatrixElem(ctx, o, 0, 0, 
      e.builder->CreateFMul(e.builder->CreateFNeg(e.getv(angleSin)), e.getv(dTheta)))) return false;

    if (!e.setMatrixElem(ctx, o, 0, 1, 
      e.builder->CreateFMul(e.builder->CreateFNeg(e.getv(angleCos)), e.getv(dTheta)))) return false;

    if (!e.setMatrixElem(ctx, o, 1, 0, 
      e.builder->CreateFMul(e.getv(angleCos), e.getv(dTheta)))) return false;

    if (!e.setMatrixElem(ctx, o, 1, 1, 
      e.builder->CreateFMul(e.builder->CreateFNeg(e.getv(angleSin)), e.getv(dTheta)))) return false;

    return true;
  }

  if (op == "mat33RotationZ" && args.size() == 1) {
    auto angleSin = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("sin"s, args[0]));
    auto angleCos = e.getExprOut(ctx, ctx.mkExpr<ExprTrig1>("cos"s, args[0]));
    if (!angleSin || !angleCos) return false;

    if (!e.setMatrixElem(ctx, o, 0, 0, e.getv(angleCos))) return false;
    if (!e.setMatrixElem(ctx, o, 0, 1, e.builder->CreateFNeg(e.getv(angleSin)))) return false;
    if (!e.setMatrixElem(ctx, o, 1, 0, e.getv(angleSin))) return false;
    if (!e.setMatrixElem(ctx, o, 1, 1, e.getv(angleCos))) return false;

    if (!e.setMatrixElem(ctx, o, 2, 2, e.emitDouble(1.0))) return false;

    return true;
  }

  return ctx.logError("emit: Invalid types for "s + op, this);
}

bool ExprMatrixMath::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (!e.allocZero(ctx, o)) return false;

  if (op == "fromHomo") {
    auto a0 = e.getExprOut(ctx, args[0]);
    if (!a0) return false;
    auto a0tm = a0.t->asMatrix();
    if (!a0tm) return ctx.logError("emit: Invalid types for "s + op, this);
    if (a0tm->rows == 4 && a0tm->cols == 1) {
      for (int ri=0; ri<3; ri++) {
        ExprOut elnum, elden;
        if (!e.getMatrixElem(ctx, elnum, a0, ri, 0)) return false;
        if (!e.getMatrixElem(ctx, elden, a0, 3, 0)) return false;
        if (!e.setMatrixElem(ctx, o, ri, 0, e.builder->CreateFDiv(e.getv(elnum), e.getv(elden)))) return false;
      }
      return true;
    }
    return ctx.logError("emit: Invalid size for "s + op, this);
  } 
  if (op == "matToHomo") {
    auto a0 = e.getExprOut(ctx, args[0]);
    if (!a0) return false;
    auto a0tm = a0.t->asMatrix();
    auto otm = o.t->asMatrix();
    if (!a0tm || !otm) return ctx.logError("emit: Invalid types for "s + op, this);
    if (a0tm->rows == 3 && a0tm->cols == 3 && otm->rows == 4 && otm->cols == 4) {
      for (int ci=0; ci<3; ci++) {
        for (int ri=0; ri<3; ri++) {
          ExprOut a0el;
          if (!e.getMatrixElem(ctx, a0el, a0, ri, ci)) return false;
          if (!e.setMatrixElem(ctx, o, ri, ci, a0el)) return false;
        }
      }
      for (int i=0; i<2; i++) {
        if (!e.setMatrixElem(ctx, o, i, 3, e.emitDouble(0.0))) return false;
        if (!e.setMatrixElem(ctx, o, 3, i, e.emitDouble(0.0))) return false;
      }
      if (!e.setMatrixElem(ctx, o, 3, 3, e.emitDouble(1.0))) return false;
      return true;
    }
    return ctx.logError("emit: Invalid size for "s + op, this);
  }
  return ctx.logError("emit: Invalid types for "s + op, this);
}

bool ExprSum::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  if (op == "hypot") {
    auto a0 = e.getExprOut(ctx, args[0]);
    if (!a0) return false;

    ExprOut osq(ctx.reg->rType);
    if (!e.emitNormSq(ctx, osq, a0)) return false;

    auto rv = e.builder->CreateUnaryIntrinsic(Intrinsic::sqrt, e.getv(osq));
    rv->setFast(true);
    return e.setv(ctx, o, rv);
  }

  else if (op == "normsq") {
    auto a0 = e.getExprOut(ctx, args[0]);
    if (!a0) return false;

    if (!e.emitNormSq(ctx, o, a0)) return false;
    return true;
  }

  else if (op == "sum") {
    auto a0 = e.getExprOut(ctx, args[0]);
    if (!a0) return false;
    auto a0ts = a0.t->asStruct();
    if (!a0ts) return ctx.logError("emit: Invalid types for "s + op, this);

    Value *rv{nullptr};

    for (auto &memInfo : a0ts->members) {
      ExprOut a0el;
      if (!e.getStructElem(ctx, a0el, a0, memInfo)) return false;

      rv = rv ? e.builder->CreateFAdd(rv, e.getv(a0el)) : e.getv(a0el);
    }
    if (!rv) {
      rv = e.getv(e.emitDouble(0.0));
    }
    return e.setv(ctx, o, rv);
  }
  return ctx.logError("emit: Invalid types for "s + op, this);
}

/*
  MatrixIndex
*/

bool ExprMatrixIndex::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  auto a0 = e.getExprOut(ctx, args[0]);
  if (!a0) return false;

  if (args.size() == 3 && 
      a0.t->isMatrix() &&
      args[1]->type->isPrimitive() &&
      args[2]->type->isPrimitive()) {
    
    auto a0tm = a0.t->asMatrix();

    int ri{0}, ci{0};
    if (args[1]->isNumericConst(ri) && args[2]->isNumericConst(ci)) {
      if (ri < 0 || ri >= a0tm->rows ||
          ci < 0 || ci >= a0tm->cols) {
        return ctx.logError("emit: Matrix index out of bounds"s);
      }
      return e.getMatrixElem(ctx, o, a0, int(ri), int(ci));
    }
    return ctx.logError("emit: WRITEME: non-constant matrix index"s);
  }

  if (args.size() == 2 && 
      a0.t->isMatrix() &&
      args[1]->type->isPrimitive()) {

    auto a0tm = a0.t->asMatrix();

    int ri{0};
    if (args[1]->isNumericConst(ri)) {
      if (ri < 0 || ri >= a0tm->rows) {
        return ctx.logError("emit: Matrix index out of bounds"s);
      }
      return e.getMatrixElem(ctx, o, a0, int(ri), 0);
    }

    return ctx.logError("emit: WRITEME: non-constant matrix index"s);
  }

  return ctx.logError("emit: Invalid types for "s + op, this);
}

/*
  Call
*/

bool ExprCallYoga::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  ctx.logCall("ExprCallYoga::emit "s + op);

  auto &slot = ctx.lookup(funcName);
  if (!slot.astFunction) return ctx.logError("emit: No yoga function "s + funcName);

  auto inArgsPad = e.allocArgsPad(ctx, args.size());
  auto outArgsPad = e.allocArgsPad(ctx, outTypes.size());

  for (auto i = 0; i < args.size(); i++) {
    auto actual = e.getExprOut(ctx, args[i]);
    if (!actual) return false;
    auto inArgSlot = e.builder->CreateStructGEP(inArgsPad, i, op + ".inArgSlot" + repr(i));
    e.builder->CreateStore(e.asU8Ptr(e.getp(actual)), inArgSlot);
    ctx.logCall("  inArgPad[" + repr(i) + "]:\n" + repr(*args[i]));
  }

  for (auto i = 0; i < outTypes.size(); i++) {
    ExprOut actual(outTypes[i]);
    if (!e.allocZero(ctx, actual)) return false;
    auto outArgSlot = e.builder->CreateStructGEP(outArgsPad, i, op + ".outArgSlot" + repr(i));
    e.builder->CreateStore(e.asU8Ptr(e.getp(actual)), outArgSlot);
    ctx.logCall("  outArgsPad[" + repr(i) + "]: " + repr_type(outTypes[i]));
  }

  vector<Value *> fArgs {
    e.builder->CreateBitCast(outArgsPad, Type::getInt8PtrTy(e.l)->getPointerTo()),
    e.builder->CreateBitCast(inArgsPad, Type::getInt8PtrTy(e.l)->getPointerTo()),
    e.getp(e.argPtrs["yogaParamsLin"]),
    e.getp(e.argPtrs["astonishment"]),
    e.getv(e.argPtrs["dt"]),
  };

  llvm::FunctionCallee fVal;
  if (auto debugLocals = e.argPtrs["debugLocals"]) {
    if (!debugLocals.t || !debugLocals.t->asStruct()) {
      throw runtime_error("No type for debugLocals");
    }
    auto memInfo = debugLocals.t->asStruct()->getMember(debugLocalMemberName);
    if (!memInfo) throw runtime_error("No member " + debugLocalMemberName + " in " + repr_type(debugLocals.t));
    ExprOut subLocals;
    if (!e.getStructElem(ctx, subLocals, debugLocals, memInfo)) return false;
    fArgs.push_back(e.asU8Ptr(e.getp(subLocals)));
    fVal = e.m->getOrInsertFunction(funcName + ".yogadebug"s, e.yogaDebugTy);
  }
  else {
    fVal = e.m->getOrInsertFunction(funcName + ".yoga"s, e.yogaFunctionTy);
  }
  fArgs.push_back(e.getv(e.argPtrs["mem"]));

  auto callv = e.builder->CreateCall(
    fVal,
    fArgs);

  if (!e.setv(ctx, o, callv)) return false;
  if (!e.setextra(ctx, o, "outArgsPad", outArgsPad)) return false;

  return true;
}

bool ExprCallYogaGrad::emit(YogaContext const &ctx, EmitCtx &e, ExprOut &o)
{
  ctx.logCall("ExprCallYogaGrad::emit "s + op);

  auto &slot = ctx.lookup(funcName);
  if (!slot.astFunction) return ctx.logError("emit: No yoga function "s + funcName);

  auto inArgsPad = e.allocArgsPad(ctx, inArgs.size());
  auto inArgsGradOutPad = e.allocArgsPad(ctx, inArgs.size());
  auto outArgsPad = e.allocArgsPad(ctx, outTypes.size());
  auto outArgsGradInPad = e.allocArgsPad(ctx, outTypes.size());
  assert(inArgsPad && inArgsGradOutPad && outArgsPad && outArgsGradInPad);

  for (auto i = 0; i < inArgs.size(); i++) {
    auto actual = e.getExprOut(ctx, inArgs[i]);
    if (!actual) return false;
    auto inArgSlot = e.builder->CreateStructGEP(inArgsPad, i, op + ".inArgSlot" + repr(i));
    e.builder->CreateStore(e.asU8Ptr(e.getp(actual)), inArgSlot);
    ctx.logCall("  inArgPad[" + repr(i) + "]:\n" + repr_type(inArgs[i]->type));
  }

  for (auto i = 0; i < inArgs.size(); i++) {
    if (! inArgs[i]->type->hasLinearOps()) continue;
    ExprOut actual(inArgs[i]->type);
    if (!e.allocZero(ctx, actual)) return false;
    auto outArgSlot = e.builder->CreateStructGEP(inArgsGradOutPad, i, op + ".inArgGradOutSlot" + repr(i));
    e.builder->CreateStore(e.asU8Ptr(e.getp(actual)), outArgSlot);
    ctx.logCall("  inArgsGradOutPad[" + repr(i) + "]: " + repr_type(inArgs[i]->type));
  }

  for (auto i = 0; i < outTypes.size(); i++) {
    ExprOut actual(outTypes[i]);
    if (!e.allocZero(ctx, actual)) return false;
    auto outArgSlot = e.builder->CreateStructGEP(outArgsPad, i, op + ".outArgSlot" + repr(i));
    e.builder->CreateStore(e.asU8Ptr(e.getp(actual)), outArgSlot);
    ctx.logCall("  outArgsPad[" + repr(i) + "]: " + repr_type(outTypes[i]));
  }

  for (auto i = 0; i < outTypes.size(); i++) {
    if (!outTypes[i]->hasLinearOps()) continue;
    auto actual = e.getExprOut(ctx, outArgGradIns[i]);
    if (!actual) return false;
    auto outArgSlot = e.builder->CreateStructGEP(outArgsGradInPad, i, op + ".outArgGradInSlot" + repr(i));
    e.builder->CreateStore(e.asU8Ptr(e.getp(actual)), outArgSlot);
    ctx.logCall("  outArgsGradInPad[" + repr(i) + "]: " + repr_type(outTypes[i]));
  }


  vector<Value *> fArgs {
    e.builder->CreateBitCast(outArgsPad, Type::getInt8PtrTy(e.l)->getPointerTo()),
    e.builder->CreateBitCast(outArgsGradInPad, Type::getInt8PtrTy(e.l)->getPointerTo()),
    e.builder->CreateBitCast(inArgsPad, Type::getInt8PtrTy(e.l)->getPointerTo()),
    e.builder->CreateBitCast(inArgsGradOutPad, Type::getInt8PtrTy(e.l)->getPointerTo()),
    e.getp(e.argPtrs["yogaParamsLin"]),
    e.getp(e.argPtrs["yogaParamsLinGrad"]),
    e.getp(e.argPtrs["astonishment"]),
    e.getv(e.argPtrs["dt"]),
    e.getv(e.argPtrs["mem"])
  };
  
  auto fVal = e.m->getOrInsertFunction(funcName + ".yogagrad"s, e.yogaGradTy);

  auto callv = e.builder->CreateCall(
    fVal,
    fArgs);

  if (ctx.reg->verbose >= 3) {
    e.emitLogPtr(ctx, inArgsPad, [this, ctx](void *p) {
      cerr << "CallYogaGrad " + op + ":\n";
      cerr << " inArgsPad at " + repr_ptr(p) + "\n";
      auto pp = reinterpret_cast<U8 **>(p);
      for (int i = 0; i < args.size(); i++) {
        YogaValue yv(ctx.reg, inArgs[i]->type, pp[i]);
        cerr << "    [" + repr(i) + "] = " + repr(yv) + "\n";
      }
    });
    e.emitLogPtr(ctx, inArgsGradOutPad, [this, ctx](void *p) {
      cerr << " inArgsGradOutPad at " + repr_ptr(p) + "\n";
      auto pp = reinterpret_cast<U8 **>(p);
      for (int i = 0; i < args.size(); i++) {
        YogaValue yv(ctx.reg, inArgs[i]->type, pp[i]);
        cerr << "    [" + repr(i) + "] = " + repr(yv) + "\n";
      }
    });
    e.emitLogPtr(ctx, outArgsPad, [this, ctx](void *p) {
      cerr << " outArgsPad at " + repr_ptr(p) + "\n";
      auto pp = reinterpret_cast<U8 **>(p);
      for (int i = 0; i < outTypes.size(); i++) {
        YogaValue yv(ctx.reg, outTypes[i], pp[i]);
        cerr << "    [" + repr(i) + "] = " + repr(yv) + "\n";
      }
    });
    e.emitLogPtr(ctx, outArgsGradInPad, [this, ctx](void *p) {
      cerr << " outArgsGradInPad at " + repr_ptr(p) + "\n";
      auto pp = reinterpret_cast<U8 **>(p);
      for (int i = 0; i < outTypes.size(); i++) {
        YogaValue yv(ctx.reg, outTypes[i], pp[i]);
        cerr << "    [" + repr(i) + "] = " + repr(yv) + "\n";
      }
    });
  }

  if (!e.setv(ctx, o, callv)) return false;
  if (!e.setextra(ctx, o, "outArgsPad", outArgsPad)) return false;
  if (!e.setextra(ctx, o, "inArgsGradOutPad", inArgsGradOutPad)) return false;

  return true;
}


