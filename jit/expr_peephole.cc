#include "./compilation.h"
#include "./context.h"
#include "./expr.h"
#include "./param.h"
#include "./effects.h"
#include "./expr_impl.h"

ExprNode *ExprNode::peephole(YogaContext const &ctx)
{
  return nullptr;
}

ExprNode *ExprConstZero::peephole(YogaContext const &ctx)
{
  if (type == ctx.reg->rType) {
    return ctx.mkExpr<ExprConstDouble>(0.0);
  }
  return nullptr;
}

ExprNode *ExprConstOne::peephole(YogaContext const &ctx)
{
  if (type == ctx.reg->rType) {
    return ctx.mkExpr<ExprConstDouble>(1.0);
  }
  return nullptr;
}

ExprNode *ExprMkComplex::peephole(YogaContext const &ctx)
{
  R realPart=0.0, imagPart=0.0;
  if (args[0]->isNumericConst(realPart) && args[1]->isNumericConst(imagPart)) {
    return ctx.mkExpr<ExprConstComplex>(std::complex<R>(realPart, imagPart));
  }
  return nullptr;
}


ExprNode *ExprExtractReal::peephole(YogaContext const &ctx)
{
  // WRITEME
  return nullptr;
}

ExprNode *ExprExtractImag::peephole(YogaContext const &ctx)
{
  // WRITEME
  return nullptr;
}


ExprNode *ExprAdd::peephole(YogaContext const &ctx)
{
  if (args[0]->cseKey == "cd_0") {
    return args[1];
  }
  if (args[1]->cseKey == "cd_0") {
    return args[0];
  }
  {
    R a0c=0.0, a1c=0.0;
    if (args[0]->isNumericConst(a0c) && args[1]->isNumericConst(a1c)) {
      return ctx.mkExpr<ExprConstDouble>(a0c + a1c);
    }
  }

  return nullptr;
}

ExprNode *ExprSub::peephole(YogaContext const &ctx)
{
  if (args[0]->cseKey == "cd_0") {
    return args[1];
  }
  if (args[1]->cseKey == "cd_0") {
    return args[0];
  }
  {
    R a0c=0.0, a1c=0.0;
    if (args[0]->isNumericConst(a0c) && args[1]->isNumericConst(a1c)) {
      return ctx.mkExpr<ExprConstDouble>(a0c - a1c);
    }
  }
  return nullptr;
}

ExprNode *ExprMul::peephole(YogaContext const &ctx)
{
  if (args[0]->cseKey == "cd_1") {
    return args[1];
  }
  if (args[1]->cseKey == "cd_1") {
    return args[0];
  }
  if (args[0]->cseKey == "cd_0" && args[1]->type == args[0]->type) {
    return args[0];
  }
  if (args[1]->cseKey == "cd_0" && args[0]->type == args[1]->type) {
    return args[1];
  }
  return nullptr;
}


ExprNode *ExprDiv::peephole(YogaContext const &ctx)
{
  if (args[1]->cseKey == "cd_1") {
    return args[0];
  }
  {
    R a0c=0.0, a1c=0.0;
    if (args[0]->isNumericConst(a0c) && args[1]->isNumericConst(a1c)) {
      if (a1c == 0.0) {
        if (a0c > 0.0) {
          return ctx.mkExpr<ExprConstDouble>(numeric_limits<double>::infinity());
        }
        else if (a0c < 0.0) {
          return ctx.mkExpr<ExprConstDouble>(-numeric_limits<double>::infinity());
        }
        ctx.logWarning("0/0 is undefined");
        return nullptr;
      }
      return ctx.mkExpr<ExprConstDouble>(a0c / a1c);
    }
  }
  return nullptr;
}


ExprNode *ExprPow::peephole(YogaContext const &ctx)
{
  if (args[1]->cseKey == "cd_1") return args[0];

  return nullptr;
}

ExprNode *ExprMinMax::peephole(YogaContext const &ctx)
{
  if (args.size() == 1) {
    return args[0];
  }
  return nullptr;
}


ExprNode *ExprConstructor::peephole(YogaContext const &ctx)
{
  if (args.size() == 1) {
    if (args[0]->type == type) {
      return args[0];
    }
  }
  return nullptr;
}


ExprNode *ExprStructref::peephole(YogaContext const &ctx)
{
  auto a0Structref = args[0]->asMkStruct();
  if (a0Structref) {
    return a0Structref->args[memberInfo->llIdx];
  }

  auto a0Obj = args[0]->asObject();
  if (a0Obj) {
    int keyIndex = 0;
    for (auto &it : a0Obj->keys) {
      if (it == memberName) {
        return a0Obj->args[keyIndex];
      }
      keyIndex++;
    }
    ctx.logError("No member "s + shellEscape(memberName) + " in object");
    return nullptr;
  }
  return nullptr;
}
