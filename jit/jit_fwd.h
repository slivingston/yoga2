#pragma once

struct EmitCtx;
struct YogaContext;

struct YogaValue;
struct YogaCaller;

struct YogaTokenizer;
struct AstNode;
struct ExprNode;
struct YogaCompilation;
struct StructMember;

struct YogaType;
struct YogaTypePtr;
struct YogaTypePrimitive;
struct YogaTypeStruct;
struct YogaTypeSet;
struct YogaTypeMatrix;

struct AccessorBuilder;
struct LinearOpBuilder;

struct Trace;
struct GradientSet;

struct AstNode;
struct AstFunction;

struct YogaSourceFile;

struct YogaCompiledFuncInfo;

struct YogaParamInfo;
struct FunctionEffects;

struct AnnoBase;
struct AnnoStructref;

struct YogaCompiledFuncInfo;

struct StructMember;
struct YogaLayout;

struct YogaPassBase;

struct AssignmentSet;
struct GradientExprSet;
struct ExprOut;

struct ExprObject;
struct ExprUnboundVar;
struct ExprCallYoga;
struct ExprVar;
struct ExprStructref;
struct ExprPhi;
struct ExprMkStruct;
struct ExprConvBoolDouble;
struct ExprConvIntegerDouble;

struct YogaSetCons;

struct TopLevelSymbolValue;

struct YogaRef;

// Must match YogaCompilation::getageLlCodeForYogaFunction
using CallableYogaFunction = void (*)(
  U8 * const *outArgs, 
  U8 const * const *inArgs,
  double const *params, 
  double *astonishment,
  double dt,
  apr_pool_t *mem);

// Must match YogaCompilation::getageLlCodeForYogaGrad
using CallableYogaGrad = void (*)(
  U8 * const *outArgs,
  U8 const * const *outGradIn, 
  U8 const * const *inArgs,
  U8 * const *inGradOut, 
  double const *params,
  double *paramsGradOut,
  double *astonishment,
  double dt,
  apr_pool_t *mem);
  
using CallableYogaDebug = void (*)(
  U8 * const *outArgs,
  U8 const * const *inArgs,
  double const *params,
  double *astonishment,
  double dt,
  U8 *debugLocals,
  apr_pool_t *mem);

using CallableYogaLinearOp = void (*)(
  U8 *outSum,
  R cA,
  U8 const *inA,
  R cB,
  U8 const *inB,
  apr_pool_t *mem);

using CallableYogaNormOp = double (*)(U8 const *inA);

using CallableYogaAccessor = void (*)(U8 *yvp, U8 *cvp);

using CallableYogaPacketWr = void (*)(::packet &p, U8 const *buf);
using CallableYogaPacketRd = void (*)(::packet &p, U8 *buf, apr_pool_t *mem);

