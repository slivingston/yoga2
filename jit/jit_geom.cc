#include "../geom/geom.h"
#include "numerical/numerical.h"

extern "C" void yogic_sub_dmat4_dmat4_dmat4(dmat4 *ret, dmat4 *a, dmat4 *b)
{
  *ret = *a - *b;
}

extern "C" void yogic_add_dmat4_dmat4_dmat4(dmat4 *ret, dmat4 *a, dmat4 *b)
{
  *ret = *a + *b;
}

extern "C" void yogic_mul_dmat4_dmat4_dmat4(dmat4 *ret, dmat4 *a, dmat4 *b)
{
  *ret = *a * *b;
}

extern "C" double yogic_sin(double x) { return sin(x); }
extern "C" double yogic_cos(double x) { return cos(x); }
extern "C" double yogic_tan(double x) { return tan(x); }
extern "C" double yogic_sinh(double x) { return sinh(x); }
extern "C" double yogic_cosh(double x) { return cosh(x); }
extern "C" double yogic_tanh(double x) { return tanh(x); }
extern "C" double yogic_acos(double x) { return acos(x); }
extern "C" double yogic_asin(double x) { return asin(x); }
extern "C" double yogic_atan(double x) { return atan(x); }
extern "C" double yogic_atan2(double y, double x) { return atan2(y, x); }
extern "C" double yogic_exp(double x) { return exp(x); }
extern "C" double yogic_log(double x) { return log(x); }
extern "C" double yogic_sqrt(double x) { return sqrt(x); }
extern "C" double yogic_sqr(double x) { return x*x; }
extern "C" double yogic_normangle(double x) { return normangle(x); }
extern "C" double yogic_sign(double x) { return x == 0.0 ? 0.0 : copysign(1.0, x); }
extern "C" double yogic_abs(double x) { return abs(x); }
extern "C" double yogic_cbrt(double x) { return cbrt(x); }
extern "C" double yogic_pow_neg_two_thirds(double x) {
  return copysign(pow(abs(x), -2.0/3.0), x);
}

extern "C" double yogic_one_minus_tanhsq(double x) { return 1.0 - sqr(tanh(x)); }


extern "C" double yogic_normangle2(double x) { 
  if (x > 3*M_PI) {
    return fmod((x + M_PI), M_2PI) - M_PI;
  }
  else if (x < -3*M_PI) {
    return -(fmod((-x + M_PI), M_2PI) - M_PI);
  }
  else {
    return x;
  }
}


ostream & operator << (ostream &s, glm::dmat2 const &a) { s << to_string(a); return s; }
ostream & operator << (ostream &s, glm::dmat3 const &a) { s << to_string(a); return s; }
ostream & operator << (ostream &s, dmat4 const &a) { s << to_string(a); return s; }

ostream & operator << (ostream &s, glm::dvec2 const &a) { s << to_string(a); return s; }
ostream & operator << (ostream &s, glm::dvec3 const &a) { s << to_string(a); return s; }
ostream & operator << (ostream &s, glm::dvec4 const &a) { s << to_string(a); return s; }



ostream & operator << (ostream &s, glm::mat2 const &a) { s << to_string(a); return s; }
ostream & operator << (ostream &s, glm::mat3 const &a) { s << to_string(a); return s; }
ostream & operator << (ostream &s, glm::mat4 const &a) { s << to_string(a); return s; }

ostream & operator << (ostream &s, glm::vec2 const &a) { s << to_string(a); return s; }
ostream & operator << (ostream &s, glm::vec3 const &a) { s << to_string(a); return s; }
ostream & operator << (ostream &s, glm::vec4 const &a) { s << to_string(a); return s; }
