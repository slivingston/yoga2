#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct foo {
  double m1;
  double m2;
  double m3;
};

struct foo foo_times_scalar_sret(struct foo it, double s)
{
  struct foo ret = {
    it.m1*s,
    it.m2*s,
    it.m3*s,
  };
  return ret;
}


double foo_times_scalar_yoga(struct foo *itOut, struct foo *itIn, double s)
{
  itOut->m1 = itIn->m1 * s;
  itOut->m2 = itIn->m2 * s;
  itOut->m3 = itIn->m3 * s;
  return 0.0;
}

struct mat33 {
  double elems[9];
};

double mat33_copy(struct mat33 *itOut, struct mat33 *itIn, double s)
{
  *itOut = *itIn;
  return 0.0;
}

double dummy(struct mat33 *itIn)
{
  return itIn->elems[5];
}


double mat33_call(struct mat33 *itOut, struct mat33 *itIn, double s)
{
  *itOut = *itIn;
  return dummy(itIn);
}

double mat33_angle(struct mat33 *itOut, double s)
{
  itOut->elems[0] = sin(s);
  return 0.0;
}


double test_sin(double *r, double *x, double dt)
{
  *r = sin(*x);
  return 0.0;
}


void test_memset(struct mat33 *itOut)
{
  memset((void *)itOut, 0, sizeof(struct mat33));
}

void test_zero(struct mat33 *itOut)
{
  for (int i=0; i<9; i++) {
    itOut->elems[i] = 0.0;
  }
}


int main(int argc, char **argv)
{
  double r, x;
  x = 2.0;
  test_sin(&r, &x, 0.0);
  printf("%g\n", r);
}
