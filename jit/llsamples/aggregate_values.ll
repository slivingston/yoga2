; ModuleID = 'aggregate_values.c'
source_filename = "aggregate_values.c"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.14.0"

%struct.foo = type { double, double, double }
%struct.mat33 = type { [9 x double] }

@.str = private unnamed_addr constant [4 x i8] c"%g\0A\00", align 1

; Function Attrs: norecurse nounwind ssp uwtable
define void @foo_times_scalar_sret(%struct.foo* noalias nocapture sret, %struct.foo* byval nocapture readonly align 8, double) local_unnamed_addr #0 {
  %4 = bitcast %struct.foo* %1 to <2 x double>*
  %5 = load <2 x double>, <2 x double>* %4, align 8, !tbaa !3
  %6 = insertelement <2 x double> undef, double %2, i32 0
  %7 = shufflevector <2 x double> %6, <2 x double> undef, <2 x i32> zeroinitializer
  %8 = fmul fast <2 x double> %7, %5
  %9 = bitcast %struct.foo* %0 to <2 x double>*
  store <2 x double> %8, <2 x double>* %9, align 8, !tbaa !3
  %10 = getelementptr inbounds %struct.foo, %struct.foo* %0, i64 0, i32 2
  %11 = getelementptr inbounds %struct.foo, %struct.foo* %1, i64 0, i32 2
  %12 = load double, double* %11, align 8, !tbaa !7
  %13 = fmul fast double %12, %2
  store double %13, double* %10, align 8, !tbaa !7
  ret void
}

; Function Attrs: norecurse nounwind ssp uwtable
define double @foo_times_scalar_yoga(%struct.foo* nocapture, %struct.foo* nocapture readonly, double) local_unnamed_addr #0 {
  %4 = bitcast %struct.foo* %1 to <2 x double>*
  %5 = load <2 x double>, <2 x double>* %4, align 8, !tbaa !3
  %6 = insertelement <2 x double> undef, double %2, i32 0
  %7 = shufflevector <2 x double> %6, <2 x double> undef, <2 x i32> zeroinitializer
  %8 = fmul fast <2 x double> %7, %5
  %9 = bitcast %struct.foo* %0 to <2 x double>*
  store <2 x double> %8, <2 x double>* %9, align 8, !tbaa !3
  %10 = getelementptr inbounds %struct.foo, %struct.foo* %1, i64 0, i32 2
  %11 = load double, double* %10, align 8, !tbaa !7
  %12 = fmul fast double %11, %2
  %13 = getelementptr inbounds %struct.foo, %struct.foo* %0, i64 0, i32 2
  store double %12, double* %13, align 8, !tbaa !7
  ret double 0.000000e+00
}

; Function Attrs: nounwind ssp uwtable
define double @mat33_copy(%struct.mat33* nocapture, %struct.mat33* nocapture readonly, double) local_unnamed_addr #1 {
  %4 = bitcast %struct.mat33* %0 to i8*
  %5 = bitcast %struct.mat33* %1 to i8*
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %4, i8* %5, i64 72, i32 8, i1 false), !tbaa.struct !9
  ret double 0.000000e+00
}

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #2

; Function Attrs: norecurse nounwind readonly ssp uwtable
define double @dummy(%struct.mat33* nocapture readonly) local_unnamed_addr #3 {
  %2 = getelementptr inbounds %struct.mat33, %struct.mat33* %0, i64 0, i32 0, i64 5
  %3 = load double, double* %2, align 8, !tbaa !3
  ret double %3
}

; Function Attrs: nounwind ssp uwtable
define double @mat33_call(%struct.mat33* nocapture, %struct.mat33* nocapture readonly, double) local_unnamed_addr #1 {
  %4 = bitcast %struct.mat33* %0 to i8*
  %5 = bitcast %struct.mat33* %1 to i8*
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* %4, i8* %5, i64 72, i32 8, i1 false), !tbaa.struct !9
  %6 = getelementptr inbounds %struct.mat33, %struct.mat33* %1, i64 0, i32 0, i64 5
  %7 = load double, double* %6, align 8, !tbaa !3
  ret double %7
}

; Function Attrs: nounwind ssp uwtable
define double @mat33_angle(%struct.mat33* nocapture, double) local_unnamed_addr #1 {
  %3 = tail call fast double @llvm.sin.f64(double %1)
  %4 = getelementptr inbounds %struct.mat33, %struct.mat33* %0, i64 0, i32 0, i64 0
  store double %3, double* %4, align 8, !tbaa !3
  ret double 0.000000e+00
}

; Function Attrs: nounwind readnone speculatable
declare double @llvm.sin.f64(double) #4

; Function Attrs: nounwind ssp uwtable
define double @test_sin(double* nocapture, double* nocapture readonly, double) local_unnamed_addr #1 {
  %4 = load double, double* %1, align 8, !tbaa !3
  %5 = tail call fast double @llvm.sin.f64(double %4)
  store double %5, double* %0, align 8, !tbaa !3
  ret double 0.000000e+00
}

; Function Attrs: nounwind ssp uwtable
define void @test_memset(%struct.mat33*) local_unnamed_addr #1 {
  %2 = bitcast %struct.mat33* %0 to i8*
  %3 = tail call i64 @llvm.objectsize.i64.p0i8(i8* %2, i1 false, i1 true)
  %4 = tail call i8* @__memset_chk(i8* %2, i32 0, i64 72, i64 %3) #6
  ret void
}

; Function Attrs: nounwind
declare i8* @__memset_chk(i8*, i32, i64, i64) local_unnamed_addr #5

; Function Attrs: nounwind readnone speculatable
declare i64 @llvm.objectsize.i64.p0i8(i8*, i1, i1) #4

; Function Attrs: norecurse nounwind ssp uwtable
define void @test_zero(%struct.mat33* nocapture) local_unnamed_addr #0 {
  %2 = bitcast %struct.mat33* %0 to i8*
  call void @llvm.memset.p0i8.i64(i8* %2, i8 0, i64 72, i32 8, i1 false)
  ret void
}

; Function Attrs: nounwind ssp uwtable
define i32 @main(i32, i8** nocapture readnone) local_unnamed_addr #1 {
  %3 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i64 0, i64 0), double 0x3FED18F6EAD1B446)
  ret i32 0
}

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #5

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i32, i1) #2

attributes #0 = { norecurse nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="true" "no-jump-tables"="false" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #1 = { nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="true" "no-jump-tables"="false" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind }
attributes #3 = { norecurse nounwind readonly ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="true" "no-jump-tables"="false" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #4 = { nounwind readnone speculatable }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="true" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"Apple LLVM version 10.0.0 (clang-1000.11.45.5)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"double", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!8, !4, i64 16}
!8 = !{!"foo", !4, i64 0, !4, i64 8, !4, i64 16}
!9 = !{i64 0, i64 72, !10}
!10 = !{!5, !5, i64 0}
