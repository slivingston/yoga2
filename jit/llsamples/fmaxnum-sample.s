Disassembly for easeIn at 0x101c640b0:
  	subq	$24, %rsp
  	movabsq	$4324732968, %rax
  	vmovsd	(%rax), %xmm1
  	movq	(%rdi), %rax
  	movq	(%rsi), %rsi
  	vmovsd	(%rsi), %xmm2
  	movabsq	$4324732960, %rsi
  	vmovsd	(%rsi), %xmm3
  	vminsd	%xmm2, %xmm3, %xmm4
  	vcmpunordsd	%xmm2, %xmm2, %xmm2
  	vblendvpd	%xmm2, %xmm3, %xmm4, %xmm2
  	vxorps	%xmm3, %xmm3, %xmm3
  	vmaxsd	%xmm2, %xmm3, %xmm3
  	vcmpunordsd	%xmm2, %xmm2, %xmm2
  	vandnpd	%xmm3, %xmm2, %xmm2
  	vmulsd	%xmm1, %xmm2, %xmm1
  	movabsq	$4448794384, %rsi
  	movb	$1, %cl
  	vmovsd	%xmm0, 16(%rsp)
  	vmovaps	%xmm1, %xmm0
  	movq	%rax, 8(%rsp)
  	movb	%cl, %al
  	movq	%rdx, (%rsp)
  	callq	*%rsi
  	movabsq	$4324732952, %rdx
  	vmovsd	(%rdx), %xmm1
  	movabsq	$4324732960, %rdx
  	vmovsd	(%rdx), %xmm2
  	vsubsd	%xmm0, %xmm2, %xmm0
  	vmulsd	%xmm1, %xmm0, %xmm0
  	movq	8(%rsp), %rdx
  	vmovsd	%xmm0, (%rdx)
  	vxorps	%xmm0, %xmm0, %xmm0
  	addq	$24, %rsp
  	retq



Disassembly for easeIn at 0x1020000b0:
  	subq	$24, %rsp
  	movabsq	$4328517672, %rax
  	vmovsd	(%rax), %xmm1
  	movq	(%rdi), %rax
  	movq	(%rsi), %rsi
  	vmovsd	(%rsi), %xmm2
  	vxorps	%xmm3, %xmm3, %xmm3
  	vmaxsd	%xmm2, %xmm3, %xmm3
  	vcmpunordsd	%xmm2, %xmm2, %xmm2
  	vandnpd	%xmm3, %xmm2, %xmm2
  	movabsq	$4328517664, %rsi
  	vmovsd	(%rsi), %xmm3
  	vminsd	%xmm2, %xmm3, %xmm4
  	vcmpunordsd	%xmm2, %xmm2, %xmm2
  	vblendvpd	%xmm2, %xmm3, %xmm4, %xmm2
  	vmulsd	%xmm1, %xmm2, %xmm1
  	movabsq	$4473632240, %rsi
  	movb	$1, %cl
  	vmovsd	%xmm0, 16(%rsp)
  	vmovaps	%xmm1, %xmm0
  	movq	%rax, 8(%rsp)
  	movb	%cl, %al
  	movq	%rdx, (%rsp)
  	callq	*%rsi
  	movabsq	$4328517656, %rdx
  	vmovsd	(%rdx), %xmm1
  	movabsq	$4328517664, %rdx
  	vmovsd	(%rdx), %xmm2
  	vsubsd	%xmm0, %xmm2, %xmm0
  	vmulsd	%xmm1, %xmm0, %xmm0
  	movq	8(%rsp), %rdx
  	vmovsd	%xmm0, (%rdx)
  	vxorps	%xmm0, %xmm0, %xmm0
  	addq	$24, %rsp
  	retq
