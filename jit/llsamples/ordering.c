


int foo(int *p)
{
  (*p)++;
  return *p;
}

extern int bar(int *p);

static int glub;

int main()
{
  int foo1, foo2;
  foo1 = bar(&glub);
  foo2 = bar(&glub);

  return 3*foo1 + 7*foo2;
}


