#include "./memory.h"
#include <atomic>

#define LOGGING 0

#if LOGGING
ofstream memlog("memlog.txt");
#endif

int yogic_abort(int errcode)
{
  char buf[256];
  throw runtime_error("apr pool error: "s + apr_strerror(errcode, buf, sizeof(buf)));
}

apr_pool_t *new_yogic_pool(apr_pool_t *parent, char const *name)
{
  apr_pool_t *ret = nullptr;
  apr_pool_create(&ret, parent);
  apr_pool_abort_set(yogic_abort, ret);
#if LOGGING
  memlog << "newpool " + repr_ptr(ret) + " " + repr_ptr(parent) + " " + name + "\n";
#endif
  return ret;
}

void free_yogic_pool(apr_pool_t *mem)
{
#if LOGGING
  memlog << "freepool " + repr_ptr(mem) + "\n";
#endif
  apr_pool_destroy(mem);
}

extern "C" U8 *yogic_alloc(S64 size, apr_pool_t *mem)
{
  if (size < 0 || size > 1000000) {
    throw runtime_error("yogic_alloc("s + repr(size) + "): too large");
  }

  U8 *ret;
  if (mem) {
    ret = reinterpret_cast<U8 *>(apr_palloc(mem, (apr_size_t)size));
  }
  else {
    ret = reinterpret_cast<U8 *>(malloc((size_t)size));
  }

#if LOGGING
  memlog << "alloc " + repr(size) + " " + repr_ptr(ret) + " " + repr_ptr(mem) + "\n";
#endif

  return ret;
}


YogaPool::YogaPool(apr_pool_t *_parent)
  :it(new_yogic_pool(_parent, "tmp"))
{
}

YogaPool::~YogaPool()
{
  free_yogic_pool(it);
  it = nullptr;
}


unordered_map<string, U8 *> stringTable;

// WRITEME: need spin lock around stringTable
U8 *yogic_intern(string const &s)
{
  auto &slot = stringTable[s];
  if (!slot) {
    slot = (U8 *)strdup(s.c_str());
  }
  return slot;
}
