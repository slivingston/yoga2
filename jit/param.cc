#include "./param.h"
#include "../lib/scope_math.h"
#include "../timeseq/gradient_set.h"
#include "./compilation.h"
#include "./context.h"
#include "./param_impl.h"
#include "./type.h"
#include "./value.h"
#include "nlohmann-json/json.hpp"

void YogaCompilation::applyParamsGrad(vector<R> &paramsGrad, R lr)
{
  assert(paramsGrad.size() == paramValues.size());
  auto gradNormSq = 0.0;
  for (size_t i = 0; i < paramsGrad.size(); i++) {
    gradNormSq += sqr(paramsGrad[i]);
  }
  if (0) cerr << "paramsGradNormSq = " + repr(gradNormSq) + "\n";

  for (size_t i = 0; i < paramsGrad.size(); i++) {
    paramValues[i] += lr * paramsGrad[i];
  }
  for (size_t i = 0; i < paramsGrad.size(); i++) {
    paramsGrad[i] = 0.0;
  }
}


YogaParamInfo::YogaParamInfo(
  string _paramName,
  size_t _paramIndex,
  YogaType *_paramType)
  : paramName(std::move(_paramName)),
    paramIndex(_paramIndex),
    paramType(_paramType)
{
  if (paramType->isReal()) {
    paramDimension = 1;
  }
  else if (auto *t = paramType->asMatrix()) {
    paramDimension = t->rows * t->cols;
  }
  else {
    throw runtime_error("Can't create param of type " + repr_type(paramType));
  }
}

YogaParamInfo::~YogaParamInfo() = default;


R &YogaParamInfo::getValue(YogaContext const &ctx, vector<R> *overrideParams) const
{
  if (overrideParams) {
    return overrideParams->at(paramIndex);
  }
  else {
    return ctx.reg->paramValues.at(paramIndex);
  }
}

void YogaParamInfo::setValue(YogaContext const &ctx, R v, vector<R> *overrideParams) const
{
  if (overrideParams) {
    overrideParams->at(paramIndex) = v;
  }
  else {
    R oldv = ctx.reg->paramValues.at(paramIndex);
    if (v != oldv) {
      ctx.reg->paramValues.at(paramIndex) = v;
      ctx.reg->paramValueEpoch++;
    }
  }
}

void YogaParamInfo::revertValue(YogaContext const &ctx) const
{
  ctx.reg->paramValues.at(paramIndex) = ctx.reg->savedParamValues.at(paramIndex);
}

R YogaParamInfo::getGrad(YogaContext const &ctx, GradientSet *grads) const
{
  if (grads) {
    if (paramIndex < grads->paramGradAccum.size()) {
      return grads->paramGradAccum[paramIndex]; 
    }
  }
  return 0.0; // WRITEME: which grad are we talking about here?
}

R YogaParamInfo::normalizeValue(R rawValue) const
{
  if (literalDist == "normal") {
    return rawValue / literalScale;
  }
  else if (literalDist == "lognormal") {
    return log(rawValue / literalScale);
  }
  else {
    throw runtime_error("normalizeValue: unknown dist");
  }
}

R YogaParamInfo::denormalizeValue(R normValue) const
{
  if (literalDist == "normal") {
    return normValue * literalScale;
  }
  else if (literalDist == "lognormal") {
    return exp(normValue) * literalScale;
  }
  else {
    throw runtime_error("normalizeValue: unknown dist");
  }
}

R YogaParamInfo::normalizeGrad(R rawGrad) const
{
  if (literalDist == "normal") {
    return pow(rawGrad * literalScale, 1.0/3.0);
  }
  else if (literalDist == "lognormal") {
    return pow(rawGrad * literalScale, 1.0/3.0);
  }
  else {
    throw runtime_error("normalizeGrad: unknown dist");
  }
}


R YogaParamInfo::getNormValue(YogaContext const &ctx, vector<R> *overrideParams) const
{
  return normalizeValue(getValue(ctx, overrideParams));
}

void YogaParamInfo::setNormValue(YogaContext const &ctx, R nv, vector<R> *overrideParams) const
{
  setValue(ctx, denormalizeValue(nv), overrideParams);
}

R YogaParamInfo::getNormGrad(YogaContext const &ctx, GradientSet *grads) const
{
  return normalizeGrad(getGrad(ctx, grads));
}

string YogaParamInfo::getDesc(YogaContext const &ctx, GradientSet *grads, vector<R> *overrideParams) const
{
  R value = getValue(ctx, overrideParams);
  string label = paramName + "\n" + fmteng(value, 3);
  if (literalDist == "normal") {
    label += " ~ "s + fmta2(literalScale);
  }
  else if (literalDist == "lognormal") {
    label += " ~ *"s + fmta3(literalScale);
  }
  auto savedValue = ctx.reg->savedParamValues[paramIndex];
  if (value != savedValue) {
    label += " (was " + fmteng(savedValue, 3) + ")";
  }

  R grad = getGrad(ctx, grads);
  if (grad != 0.0) {
    label += "\ngrad = "s + fmteng(grad, 2);
  }

  return label;
}

string YogaParamInfo::getRangeDesc(R base, R scale) const
{
  if (literalDist == "normal") {
    int prec = (scale >= 0.1) ? 2 : 3;
    return fmteng(base * literalScale, 3) + " [" + fmteng((base - scale) * literalScale, prec) + " - " + fmteng((base + scale) * literalScale, prec) + "]";
  }
  else if (literalDist == "lognormal") {
    return fmta2(exp(base) * literalScale) + " [" + fmta2(exp(base - scale) * literalScale) + " - " + fmta2(exp(base + scale) * literalScale) + "]";
  }
  else {
    throw runtime_error("setNormValue: unknown dist");
  }

}

string YogaParamInfo::getFileName() const
{
  if (isExtern && paramSourceLoc.file) {
    if (paramSourceLoc.file->isBuiltin) return "";
    auto fn = paramSourceLoc.file->fn;
    return fn + "." + (externName.empty() ? paramName : externName) + ".param.json";
  }
  return ""s;
}

void YogaParamInfo::randomize(YogaContext const &ctx) const
{
  static uint32_t seed;
  seed ++;
  std::mt19937 gen(seed);
  if (auto *ptm = paramType->asMatrix()) {
    std::normal_distribution<R> paramDist(0.0, literalScale);

    for (int ci = 0; ci < ptm->cols; ci++) {
      for (int ri = 0; ri < ptm->rows; ri++) {
        int paramCelli = ci * ptm->rows + ri;
        ctx.reg->paramValues.at(paramIndex + paramCelli) = paramDist(gen);
      }
    }
  }
  else if (paramType->isReal()) {
    std::normal_distribution<R> paramDist(0.0, 1.0);
    setNormValue(ctx, paramDist(gen));
  }
  ctx.reg->paramValueEpoch++;
}


bool YogaParamInfo::readFromFile(YogaContext const &ctx) const
{
  auto paramFn = getFileName();

  if (auto *ptm = paramType->asMatrix()) {
    if (0) cerr << "Read extern param from "s + paramFn + "\n";

    bool needsWrite = false;
    auto j = json::object();
    string js;
    if (!paramFn.empty()) {
      int fd = open(paramFn.c_str(), O_RDONLY);
      if (fd < 0) {
        needsWrite = true;
      }
      else {
        js = readFile(fd, paramFn);
        close(fd);
        j = json::parse(js);
      }
    }

    randomize(ctx);

    int fileRows = j.value("rows", 0);
    int fileCols = j.value("cols", 0);
    auto data = j.value("data", json::array());

    for (int ci = 0; ci < min(ptm->cols, fileCols); ci++) {
      for (int ri = 0; ri < min(ptm->rows, fileRows); ri++) {
        int fileCelli = ci * fileRows + ri;
        int paramCelli = ci * ptm->rows + ri;
        R fileValue = data.at(fileCelli);

        ctx.reg->paramValues.at(paramIndex + paramCelli) = fileValue;
        ctx.reg->savedParamValues.at(paramIndex + paramCelli) = fileValue;
      }
    }
    if (needsWrite) {
      cerr << "Autocreating " + paramFn + "\n";
      writeFileIfChanged(paramFn, getExternJson(ctx));
    }
    return true;
  }
  return ctx.logError("Unknown param type");
}


string YogaParamInfo::getExternJson(YogaContext const &ctx) const
{
  if (auto *ptm = paramType->asMatrix()) {
    if (paramIndex + ptm->cols * ptm->rows > ctx.reg->paramValues.size()) {
      throw runtime_error("Param index overrun");
    }
    vector<R> paramSlice(&ctx.reg->paramValues[paramIndex], &ctx.reg->paramValues[paramIndex + ptm->cols * ptm->rows]);

    json j({
      {"paramName", paramName},
      {"sourceLoc", repr(paramSourceLoc)},
      {"rows", ptm->rows},
      {"cols", ptm->cols},
      {"data", paramSlice}
    });
    return j.dump(2);
  }
  return "<error: paraminfo for type " + repr_type(paramType) + ">\n";
}



bool YogaParamInfo::isValueChanged(YogaContext const &ctx) const
{
  for (auto i = 0; i < paramDimension; i++) {
    if (ctx.reg->paramValues[paramIndex + i] != ctx.reg->savedParamValues[paramIndex + i]) {
      return true;
    }
  }
  return false;
}

R YogaParamInfo::normChangeFromSaved(YogaContext const &ctx) const
{
  R normSqAccum = 0.0;
  for (auto i = 0; i < paramDimension; i++) {
    normSqAccum += sqr(ctx.reg->paramValues[paramIndex + i] - ctx.reg->savedParamValues[paramIndex + i]);
  }
  return sqrt(normSqAccum);
}

YogaValue YogaParamInfo::getYogaValue(YogaContext const &ctx) const
{
  return YogaValue(ctx.reg, paramType, reinterpret_cast<U8 *>(&ctx.reg->paramValues[paramIndex]));
}


void YogaParamInfo::scaleUp(YogaContext const &ctx)
{
  literalScale = tidyScaleUp(literalScale);
  ctx.reg->paramValueEpoch++;
}

void YogaParamInfo::scaleDn(YogaContext const &ctx)
{
  literalScale = tidyScaleDn(literalScale);
  ctx.reg->paramValueEpoch++;
}
