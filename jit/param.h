#pragma once
#include "./jit_utils.h"
#include "./jit_fwd.h"
#include "./sourcefile.h"


struct YogaParamInfo {
  YogaParamInfo(
    string _paramName,
    size_t _paramIndex,
    YogaType *_paramType);
  ~YogaParamInfo();
  YogaParamInfo(YogaParamInfo const &other) = delete;
  YogaParamInfo(YogaParamInfo &&other) = delete;
  YogaParamInfo & operator = (YogaParamInfo const &other) = delete;
  YogaParamInfo & operator = (YogaParamInfo &&other) = delete;

  string paramName;
  size_t paramIndex{0};
  size_t paramDimension{1};
  YogaType *paramType;

  YogaSourceLoc sourceLoc;
  YogaSourceLoc paramSourceLoc;
  YogaSourceLoc scaleSourceLoc;

  R literalScale{0.0};
  R savedLiteralScale{0.0};
  string literalDist;
  int literalExponent{0};
  bool isExtern{false};
  string externName;

  R &getValue(YogaContext const &ctx, vector<R> *overrideParams = nullptr) const;
  void setValue(YogaContext const &ctx, R v, vector<R> *overrideParams = nullptr) const;
  void revertValue(YogaContext const &ctx) const;

  R getGrad(YogaContext const &ctx, GradientSet *grads=nullptr) const;

  R normChangeFromSaved(YogaContext const &ctx) const;

  string getRangeDesc(R base, R scale) const;

  R normalizeValue(R rawValue) const;
  R denormalizeValue(R normValue) const;
  R normalizeGrad(R rawGrad) const;
  R denormalizeGrad(R normGrad) const;

  R getNormValue(YogaContext const &ctx, vector<R> *overrideParams = nullptr) const;
  void setNormValue(YogaContext const &ctx, R nv, vector<R> *overrideParams = nullptr) const;
  R getNormGrad(YogaContext const &ctx, GradientSet *grads=nullptr) const;

  YogaValue getYogaValue(YogaContext const &ctx) const;

  bool isValueChanged(YogaContext const &ctx) const;

  string getDesc(YogaContext const &ctx, GradientSet *grads=nullptr, vector<R> *overrideParams = nullptr) const;

  void randomize(YogaContext const &ctx) const;
  string getFileName() const;
  bool readFromFile(YogaContext const &ctx) const;
  bool writeToFile(YogaContext const &ctx) const;
  string getExternJson(YogaContext const &ctx) const;

  void scaleUp(YogaContext const &ctx);
  void scaleDn(YogaContext const &ctx);
};

ostream & operator <<(ostream &s, YogaParamInfo const &a);
