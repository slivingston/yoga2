#pragma once
#include "./compilation.h"
#include "./param.h"

template <typename T, typename... Args>
T *YogaContext::mkParam(string const &paramName, Args &&... args) const
{
  unique_ptr ptr = make_unique<T>(paramName, reg->paramCount, std::forward<Args>(args)...);
  T *paramInfo = ptr.get();
  reg->paramPool.emplace_back(std::move(ptr));

  reg->paramCount += paramInfo->paramDimension;
  if (reg->paramCount > reg->paramValues.size()) reg->paramsByIndex.resize(reg->paramCount, nullptr);
  if (reg->paramCount > reg->paramValues.size()) reg->paramValues.resize(reg->paramCount, 0.0);
  if (reg->paramCount > reg->savedParamValues.size()) reg->savedParamValues.resize(reg->paramCount, 0.0);

  reg->paramsByName[paramInfo->paramName] = paramInfo;
  reg->paramsByIndex[paramInfo->paramIndex] = paramInfo;

  paramInfo->sourceLoc = lastLoc;
  return paramInfo;
}
