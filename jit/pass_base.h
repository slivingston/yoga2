#pragma once
#include "./jit_utils.h"
#include "./jit_fwd.h"
#include "./compilation.h"
#include "./ast.h"
#include "./expr.h"

struct YogaPassBase {
  YogaPassBase()
  {
  }

  [[nodiscard]] bool run(YogaContext const &ctx)
  {
    bool ok = true;
    for (auto &[symName, node] : ctx.reg->topLevelsInOrder) {
      if (auto n = node->asFunction()) {
        if (doTopLevel(ctx, n)) {
          if (!scan(ctx(symName.c_str()), n)) ok = false;
          if (!endTopLevel(ctx, n)) ok = false;
        }
      }
      else if (auto n = node->asStructDef()) {
        if (doTopLevel(ctx, n)) {
          if (!scan(ctx(symName.c_str()), n)) ok = false;
          if (!endTopLevel(ctx, n)) ok = false;
        }
      }
      else if (auto n = node->asOnehot()) {
        if (doTopLevel(ctx, n)) {
          if (!scan(ctx(symName.c_str()), n)) ok = false;
          if (!endTopLevel(ctx, n)) ok = false;
        }
      }
      else if (auto n = node->asEnumDecl()) {
        if (doTopLevel(ctx, n)) {
          if (!scan(ctx(symName.c_str()), n)) ok = false;
          if (!endTopLevel(ctx, n)) ok = false;
        }
      }
      else if (auto n = node->asSimple()) {
        if (doTopLevel(ctx, n)) {
          if (!scan(ctx(symName.c_str()), n)) ok = false;
          if (!endTopLevel(ctx, n)) ok = false;
        }
      }
      else {
        throw runtime_error("Unknown topLevel Ast node "s + typeid(*node).name());
      }
    }
    return ok;
  }

  template<typename T>
  bool scan(YogaContext const &ctx, vector<T *> const &nodes)
  {
    for (auto &it : nodes) {
      if (!scan(ctx, it)) return false;
    }
    return true;
  }

  bool scan(YogaContext ctx, AstNode *node)
  {
    if (node == nullptr) {
      return true;
    }

    ctx.lastLoc = node->sourceLoc;

    return node->scan(ctx, this);
  }

#define FORTYPE(T) \
  [[nodiscard]] virtual bool preScan(YogaContext const &ctx, T *node) { return true; } \
  [[nodiscard]] virtual bool doRecurse(YogaContext const &ctx, T *node) { return true; } \
  [[nodiscard]] virtual bool postScan(YogaContext const &ctx, T *node) { return true; } \

  FORTYPE(AstName)
  FORTYPE(AstTernop)
  FORTYPE(AstBinop)
  FORTYPE(AstUnop)
  FORTYPE(AstCall)
  FORTYPE(AstIndex)
  FORTYPE(AstStructref)
  FORTYPE(AstNumber)
  FORTYPE(AstRangeSpec)
  FORTYPE(AstParam)

  FORTYPE(AstBoolean)
  FORTYPE(AstNull)
  FORTYPE(AstString)
  FORTYPE(AstDeriv)
  FORTYPE(AstKeyValue)
  FORTYPE(AstObject)
  FORTYPE(AstArray)
  FORTYPE(AstExtern)
  FORTYPE(AstAnnoCall)

  FORTYPE(AstBlock)
  FORTYPE(AstIf)
  [[nodiscard]] virtual bool postCondScan(YogaContext const &ctx, AstIf *node) { return true; }
  [[nodiscard]] virtual bool postIfTrueScan(YogaContext const &ctx, AstIf *node) { return true; }
  FORTYPE(AstDefault)

  [[nodiscard]] virtual bool doTopLevel(YogaContext const &ctx, AstSimple *node) { return false; }
  FORTYPE(AstSimple)
  [[nodiscard]] virtual bool endTopLevel(YogaContext const &ctx, AstSimple *node) { return true; }

  FORTYPE(AstTypeDecl)
  FORTYPE(AstMemberDecl)

  [[nodiscard]] virtual bool doTopLevel(YogaContext const &ctx, AstStructDef *node) { return false; }
  FORTYPE(AstStructDef)
  [[nodiscard]] virtual bool postNameScan(YogaContext const &ctx, AstStructDef *node) { return true; }
  [[nodiscard]] virtual bool endTopLevel(YogaContext const &ctx, AstStructDef *node) { return true; }

  FORTYPE(AstArgDecl)

  [[nodiscard]] virtual bool doTopLevel(YogaContext const &ctx, AstEnumDecl *node) { return false; }
  FORTYPE(AstEnumDecl)
  [[nodiscard]] virtual bool endTopLevel(YogaContext const &ctx, AstEnumDecl *node) { return true; }

  [[nodiscard]] virtual bool doTopLevel(YogaContext const &ctx, AstOnehot *node) { return false; }
  FORTYPE(AstOnehot)
  [[nodiscard]] virtual bool endTopLevel(YogaContext const &ctx, AstOnehot *node) { return true; }

  [[nodiscard]] virtual bool doTopLevel(YogaContext const &ctx, AstFunction *node) { return false; }
  FORTYPE(AstFunction)
  [[nodiscard]] virtual bool postArgsScan(YogaContext const &ctx, AstFunction *node) { return true; }
  [[nodiscard]] virtual bool endTopLevel(YogaContext const &ctx, AstFunction *node) { return true; }

#undef FORTYPE


};
