#pragma once
#include "./compilation.h"
#include "./ast.h"
#include "./expr.h"
#include "./pass_base.h"
#include "./annotation.h"
#include "./annotation_impl.h"
#include "./runtime.h"

struct YogaPassDebugInfo : YogaPassBase {
  YogaPassDebugInfo()
  {
  }

  string inFuncName;

  bool doTopLevel(YogaContext const &ctx, AstFunction *node) override { return true; }
  bool doTopLevel(YogaContext const &ctx, AstSimple *node) override { return true; }


  bool preScan(YogaContext const &ctx, AstFunction *node) override
  {
    inFuncName = node->funcName;
    return true;
  }

  bool postScan(YogaContext const &ctx, AstFunction *node) override
  {
    inFuncName = "";
    return true;
  }

  bool postScan(YogaContext const &ctx, AstStructref *node) override
  {
    if (!node->parent->asStructref()) {

      /*
        See 
      */
      if (node->lv && node->lv->type && node->lhs->lv && node->lhs->lv->type) {
        auto nodeXv = node->lv;
        auto lhsXv = node->lhs->lv;
        auto parentType = lhsXv->type;

        StructMember *memberInfo = parentType->asStruct() ? parentType->asStruct()->getMember(node->literalMemberName) : nullptr;

        string refName;
        if (nodeXv->getRefName(refName)) {
          ctx(node->memberName).mkAnnotation<AnnoStructref>(nodeXv, memberInfo, inFuncName, refName, true);
        }
      }
      if (node->rv && node->rv->type && node->lhs->rv && node->lhs->rv->type) {
        auto nodeXv = node->rv;
        auto lhsXv = node->lhs->rv;
        auto parentType = lhsXv->type;

        StructMember *memberInfo = parentType->asStruct() ? parentType->asStruct()->getMember(node->literalMemberName) : nullptr;

        string refName;
        if (nodeXv->getRefName(refName)) {
          ctx(node->memberName).mkAnnotation<AnnoStructref>(nodeXv, memberInfo, inFuncName, refName, false);
        }
      }
    }
    return true;
  }

};
