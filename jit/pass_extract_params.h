#pragma once
#include "./compilation.h"
#include "./ast.h"
#include "./pass_base.h"
#include "./annotation.h"

struct YogaPassExtractParams : YogaPassBase {
  YogaPassExtractParams()
  {
  }

  bool doTopLevel(YogaContext const &ctx, AstFunction *node) override { return true; }
  bool doTopLevel(YogaContext const &ctx, AstSimple *node) override { return true; }

  // TODO: make this an unordered_map, which requires a hash function that supports pairs
  // (std::hash doesn't)
  map<pair<AstNode *, AstNode *>, string> paramNameCache;
  
  string argName(int argi)
  {
    if (argi < 26) {
      return string(1, 'a' + argi);
    }
    return "arg"s + to_string(argi);
  }

  string contextualParamName(AstNode *node, AstNode *child)
  {
    if (0 && !node) throw runtime_error("null node");
    if (!node) return "static"s;

    string cached = paramNameCache[make_pair(node, child)];
    if (!cached.empty()) return cached;

    if (auto n = node->asFunction()) {
      return setCache(node, child, n->funcName);
    }
    else if (auto n = node->asNumber()) {
      return contextualParamName(node->parent, node);
    }
    else if (auto n = node->asParam()) {
      if (child == n->rangeSpec) {
        return "param"s;
      } 
      else {
        return appendSetCache(node, child, ""s);
      }
    }
    else if (auto n = node->asBinop()) {
      if (n->isAssign()) {
        return appendSetCache(node, child, contextualValueName(n->lhs));
      }
      else if (child == n->rhs) {
        if (n->op == "*") {
          return appendSetCache(node, child, contextualValueName(n->lhs));
        }
        else {
          return appendSetCache(node, child, ""s);
        }
      }
      else if (child == n->lhs) {
        if (n->op == "*"s || n->op == "/"s || n->op == "^"s) {
          return appendSetCache(node, child, contextualValueName(n->rhs));
        }
        else {
          return appendSetCache(node, child, ""s);
        }
      }
      else {
        throw runtime_error("Missing child");
      }
    }
    else if (auto n = node->asIf()) {
      if (child == n->cond) {
        return appendSetCache(node, child, contextualValueName(n->cond));
      }
      else if (child == n->ifTrue) {
        return appendSetCache(node, child, contextualValueName(n->cond) + ".t"s);
      }
      else if (child == n->ifFalse) {
        return appendSetCache(node, child, contextualValueName(n->cond) + ".f"s);
      }
      else {
        throw runtime_error("Missing child");
      }
    }
    else if (auto n = node->asKeyValue()) {
      return appendSetCache(node, child, n->literalKey);
    }
    else if (auto n = node->asCall()) {
      int argi = 0;
      for (auto &it : n->args) {
        if (child == it) {
          return appendSetCache(node, child, argName(argi));
        }
        argi++;
      }
      if (child == n->func) {
        return appendSetCache(node, child, n->literalFuncName);
      }
      throw runtime_error("Missing child");

    }

    else if (auto n = node->asIndex()) {
      return appendSetCache(node, child, contextualValueName(n));
    }
    else {
      return appendSetCache(node, child, ""s);
    }
  }

  string contextualValueName(AstNode *node)
  {
    if (auto n = node->asStructref()) {
      return contextualValueName(n->lhs) + "."s + n->literalMemberName;
    }
    else if (auto n = node->asName()) {
      return n->name;
    }
    else if (auto n = node->asUnop()) {
      if (n->op == "!") {
        return contextualValueName(n->arg) + ".not"s;
      }
      else if (n->op == "-") {
        return contextualValueName(n->arg) + ".neg"s;
      }
      else if (n->op == "~") {
        return contextualValueName(n->arg) + ".tilda"s;
      }
      else {
        return contextualValueName(n->arg);
      }
    }
    else if (auto n = node->asBinop()) {
      return contextualValueName(n->lhs);
    }
    else if (auto n = node->asNumber()) {
      return "n"s + n->literalNumber;
    }
    else if (auto n = node->asParam()) {
      return contextualValueName(n->lhs);
    }
    else if (auto n = node->asCall()) {
      return n->literalFuncName;
    }
    else if (auto n = node->asIndex()) {
      string subName;
      for (auto &it : n->args) {
        if (!subName.empty()) subName += "."s;
        subName += contextualValueName(it);
      }
      return subName;
    }
    else if (auto n = node->asString()) {
      return "string"s;
    }
    else {
      return ""s;
    }
  }

  YogaType *contextualValueType(YogaContext const &ctx, AstNode *node)
  {
    if (auto n = node->asCall()) {
      auto fnt = ctx.getType(n->literalFuncName);
      if (fnt) return fnt;
      return ctx.logErrorNull("Invalid type");
    }
    if (auto n = node->asParam()) {
      return contextualValueType(ctx, n->parent);
    }
    if (auto n = node->asExtern()) {
      return contextualValueType(ctx, n->parent);
    }
    return nullptr;
  }

  string setCache(AstNode *node, AstNode *child, string const &paramName) {
    paramNameCache[make_pair(node, child)] = paramName;
    return paramName;
  }

  string appendSetCache(AstNode *node, AstNode *child, string const &suffix) {
    auto base = contextualParamName(node->parent, node);
    if (base == ""s || base == "param"s) return setCache(node, child, base);
    auto final = base;
    if (!suffix.empty() && suffix[0] != '.') final += '.';
    final += suffix;
    // WRITEME: simplify final
    return setCache(node, child, final);
  };

  AstRangeSpec *contextualParamRange(AstNode *node) {
    if (node == nullptr) {
      return nullptr;
    }
    if (auto n = node->asParam()) {
      return n->rangeSpec;
    }
    else if (auto n = node->asIf()) {
      return nullptr;
    }
    else if (auto n = node->asBlock()) {
      return nullptr;
    }
    else if (auto n = node->asBinop()) {
      if (n->isAssign()) {
        return nullptr;
      }
      else {
       return contextualParamRange(node->parent);
      }
    }
    else {
      return contextualParamRange(node->parent);
    }
  }

  bool doRecurse(YogaContext const &ctx, AstFunction *node) override
  {
    return true;
  }

  bool postScan(YogaContext const &ctx, AstNumber *node) override
  {
    auto paramRange = contextualParamRange(node);
    if (!paramRange) return true;

    auto paramName = contextualParamName(node, nullptr);
    if (paramName == ""s || paramName == "param"s) return true;


    if (ctx.reg->paramsByName.count(paramName)) {
      auto index = 1;
      while (ctx.reg->paramsByName.count(paramName + ".i"s + to_string(index))) {
        index++;
      }
      paramName += ".i"s + to_string(index);
    }

    auto paramInfo = ctx(node).mkParam<YogaParamInfo>(paramName, ctx.reg->rType);
    
    paramInfo->literalDist = paramRange->dist;
    paramInfo->literalScale = paramInfo->savedLiteralScale = paramRange->scaleValue;
    paramInfo->literalExponent = node->literalExponent;

    paramInfo->paramSourceLoc = node->sourceLoc;
    paramInfo->scaleSourceLoc = paramRange->sourceLoc;

    ctx.reg->paramValues[paramInfo->paramIndex] = node->doubleValue;
    ctx.reg->savedParamValues[paramInfo->paramIndex] = node->doubleValue;

    if (ctx.reg->verbose >= 5) {
      cerr << repr(*paramInfo) + "\n";
    }

    ctx(node).mkAnnotation<AnnoParam>(node->rv, paramInfo);

    node->paramInfo = paramInfo;
    return true;
  }
  
  bool postScan(YogaContext const &ctx, AstExtern *node) override
  {
    auto paramRange = contextualParamRange(node);
    if (!paramRange) return ctx.logError("No range for extern param");

    auto paramName = contextualParamName(node, nullptr);
    if (paramName == ""s || paramName == "param"s) return true;

    auto fnt = contextualValueType(ctx, node);
    if (!fnt) return false;

    auto paramInfo = ctx(node).mkParam<YogaParamInfo>(paramName, fnt);
    
    paramInfo->literalDist = paramRange->dist;
    paramInfo->literalScale = paramInfo->savedLiteralScale = paramRange->scaleValue;

    paramInfo->paramSourceLoc = node->sourceLoc;
    paramInfo->scaleSourceLoc = paramRange->sourceLoc;
    paramInfo->isExtern = true;
    paramInfo->externName = node->name;

    paramInfo->readFromFile(ctx);

    if (ctx.reg->verbose >= 5) {
      cerr << repr(*paramInfo) + "\n";
    }

    ctx(node).mkAnnotation<AnnoParam>(node->rv, paramInfo);

    node->paramInfo = paramInfo;
    return true;
  }

};
