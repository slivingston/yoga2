#pragma once
#include "./compilation.h"
#include "./ast.h"
#include "./expr.h"
#include "./pass_base.h"
#include "./annotation.h"
#include "./runtime.h"

struct YogaPassWarnings : YogaPassBase {
  YogaPassWarnings()
  {
  }

  YogaCompiledFuncInfo *inFunc{nullptr};

  bool doTopLevel(YogaContext const &ctx, AstFunction *node) override { return true; }
  bool doTopLevel(YogaContext const &ctx, AstSimple *node) override { return true; }


  bool preScan(YogaContext const &ctx, AstFunction *node) override
  {
    auto &slot = ctx.lookup(node->funcName);
    inFunc = slot.compiledFunc;
    return true;
  }

  bool postScan(YogaContext const &ctx, AstFunction *node) override
  {
    inFunc = nullptr;
    return true;
  }

  bool postScan(YogaContext const &ctx, AstBlock *node) override
  {
    for (auto &it : node->statements) {
      if (it->asSimple()) {
        auto ex = it->asSimple()->expr;
        if (ex->asBinop() && (ex->asBinop()->isAssign() || ex->asBinop()->isExpect())) {
          // ok
        }
        else if (ex->asCall()) {
          // ok
        }
        else {
          ctx(it).logWarning("Probable useless statement");
        }
      }
      else if (it->asIf() || it->asDefault() || it->asCall() || it->asTypeDecl()) {
        // ok
      }
      else {
        ctx(it).logWarning("What statement is this?");
      }
    }
    return true;
  }

};
