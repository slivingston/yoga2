#pragma once
#include "../jit/compilation.h"

bool createPolyfit(YogaContext const &ctx, AstObject *fit, ostream &outStream);
