#include "./runtime.h"
#include "./memory.h"
#include "./type.h"
#include "./value.h"
#include "../geom/solid_geometry.h"


int YogaCompiledFuncInfo::findInArg(string const &name)
{
  for (size_t i=0; i < inArgs.size(); i++) {
    if (inArgs[i].second == name) return i;
  }
  return -1;
}

int YogaCompiledFuncInfo::findInArgOrThrow(string const &name)
{
  int ret = findInArg(name);
  if (ret < 0) throw runtime_error("No in arg named "s + name);
  return ret;
}

int YogaCompiledFuncInfo::findOutArg(string const &name)
{
  for (size_t i=0; i < outArgs.size(); i++) {
    if (outArgs[i].second == name) return i;
  }
  return -1;
}

int YogaCompiledFuncInfo::findOutArgOrThrow(string const &name)
{
  int ret = findOutArg(name);
  if (ret < 0) throw runtime_error("No out arg named "s + name);
  return ret;
}


YogaCaller::YogaCaller()
{
}

YogaCaller::YogaCaller(YogaCompilation *_reg, string const &_funcName)
  : reg(_reg),
    funcName(_funcName),
    f(reg->getCompiledFunc(funcName))
{
  validate();
}

void YogaCaller::assign(YogaCompilation *_reg, string const &_funcName)
{
  reg = _reg;
  funcName = _funcName;
  f = reg->getCompiledFunc(funcName);
  validate();
}

void YogaCaller::validate()
{
  if (!reg) throw runtime_error("Uninitialized YogaCaller");
  if (!f) throw runtime_error("No function found");
  if (!f->funcAddr) throw runtime_error("Empty function found");
}


void YogaCaller::operator()(vector<void *> const &outArgs, vector<void *> const &inArgs, vector<R> const &paramValues, R &astonishment, R dt, apr_pool_t *mem) const
{
  if (!f) {
    throw runtime_error("No f found");
  }
  if (outArgs.size() != f->outArgs.size()) {
    throw runtime_error("Expected "s + repr(f->outArgs.size()) + " out args, got " + repr(outArgs.size()));
  }
  if (inArgs.size() != f->inArgs.size()) {
    throw runtime_error("Expected "s + repr(f->inArgs.size()) + " in args, got " + repr(inArgs.size()));
  }
  (*f->funcAddr)(
      (U8 * const *)outArgs.data(),
      (U8 * const *)inArgs.data(),
      paramValues.data(),
      &astonishment,
      dt,
      mem ? mem : reg->mem);
}


void YogaCaller::operator()(vector<void *> const &outArgs, vector<void *> const &inArgs, R &astonishment, R dt, apr_pool_t *mem) const
{
  return (*this)(outArgs, inArgs, reg->paramValues, astonishment, dt, mem);
}

void YogaCaller::operator()(vector<YogaValue> const &outArgs, vector<YogaValue> const &inArgs, R &astonishment, R dt, apr_pool_t *mem) const
{
  vector<void *> outArgPtrs, inArgPtrs;
  outArgPtrs.reserve(outArgs.size());
  inArgPtrs.reserve(inArgs.size());
  for (auto &a : outArgs) outArgPtrs.push_back(a.buf);
  for (auto &a : inArgs) inArgPtrs.push_back(a.buf);

  (*this)(outArgPtrs, inArgPtrs, astonishment, dt, mem);
}


void YogaCaller::grad(
  vector<void *> const &outArgs,
  vector<void *> const &outArgsGradIn,
  vector<void *> const &inArgs,
  vector<void *> const &inArgsGradOut,
  vector<R> const &paramValues,
  vector<R> &paramsGradOut,
  R &astonishment,
  R dt,
  apr_pool_t *mem) const
{
  if (!f->gradAddr) {
    throw runtime_error("No fGrad found");
  }
  if (outArgs.size() != f->outArgs.size()) {
    throw runtime_error("Expected outArgs="s + repr(f->outArgs) + ", got " + repr(outArgs.size()));
  }
  if (outArgsGradIn.size() != f->outArgs.size()) {
    throw runtime_error("Expected outArgsGradIn="s + repr(f->outArgs) + ", got " + repr(outArgsGradIn.size()));
  }
  if (inArgs.size() != f->inArgs.size()) {
    throw runtime_error("Expected inArgs="s + repr(f->inArgs) + ", got " + repr(inArgs.size()));
  }
  if (inArgsGradOut.size() != f->inArgs.size()) {
    throw runtime_error("Expected inArgsGradOut="s + repr(f->inArgs) + ", got " + repr(inArgsGradOut.size()));
  }
  if (paramsGradOut.size() != paramValues.size()) {
    throw runtime_error("Expected paramsGradOut size " + repr(paramValues.size()) + ", got " + repr(paramsGradOut.size()));
  }
  (*f->gradAddr)(
      (U8 * const *)outArgs.data(),
      (U8 * const *)outArgsGradIn.data(),
      (U8 * const *)inArgs.data(),
      (U8 * const *)inArgsGradOut.data(),
      paramValues.data(),
      paramsGradOut.data(),
      &astonishment,
      dt,
      mem ? mem : reg->mem);
}


void YogaCaller::grad(
  vector<YogaValue> const &outArgs,
  vector<YogaValue> const &outArgsGradIn,
  vector<YogaValue> const &inArgs,
  vector<YogaValue> const &inArgsGradOut,
  vector<R> &paramsGradOut,
  R &astonishment,
  R dt,
  apr_pool_t *mem) const
{
  vector<void *> outArgPtrs, inArgPtrs, outArgGradInPtrs, inArgGradOutPtrs;
  outArgPtrs.reserve(outArgs.size());
  inArgPtrs.reserve(inArgs.size());
  outArgGradInPtrs.reserve(outArgsGradIn.size());
  inArgGradOutPtrs.reserve(inArgsGradOut.size());
  for (auto &a : outArgs) outArgPtrs.push_back(a.buf);
  for (auto &a : inArgs) inArgPtrs.push_back(a.buf);
  for (auto &a : outArgsGradIn) outArgGradInPtrs.push_back(a.buf);
  for (auto &a : inArgsGradOut) inArgGradOutPtrs.push_back(a.buf);

  this->grad(
    outArgPtrs, outArgGradInPtrs,
    inArgPtrs, inArgGradOutPtrs,
    reg->paramValues,
    paramsGradOut,
    astonishment,
    dt,
    mem);
}

void YogaCaller::allocInTypes(vector<YogaValue> &a, apr_pool_t *mem)
{
  while (a.size() < f->inArgs.size()) {
    auto i = a.size();
    a.emplace_back(reg, f->inArgs[i].first, mem);
  }
}

void YogaCaller::allocOutTypes(vector<YogaValue> &a, apr_pool_t *mem)
{
  while (a.size() < f->outArgs.size()) {
    auto i = a.size();
    a.emplace_back(reg, f->outArgs[i].first, mem);
  }
}

void YogaCaller::allocParams(vector<R> &params)
{
  params.resize(reg->paramValues.size());
}


void YogaCaller::debug(
  vector<void *> const &outArgs,
  vector<void *> const &inArgs,
  vector<R> const &paramValues,
  R &astonishment,
  R dt,
  void *debugLocals,
  apr_pool_t *mem) const
{
  if (!f->debugAddr) {
    throw runtime_error("No fDebug found");
  }
  if (outArgs.size() != f->outArgs.size()) {
    throw runtime_error("Expected outArgs="s + repr(f->outArgs) + ", got " + repr(outArgs.size()));
  }
  if (inArgs.size() != f->inArgs.size()) {
    throw runtime_error("Expected inArgs="s + repr(f->inArgs) + ", got " + repr(inArgs.size()));
  }
  (*f->debugAddr)(
      (U8 * const *)outArgs.data(),
      (U8 * const *)inArgs.data(),
      paramValues.data(),
      &astonishment,
      dt,
      (U8 *) debugLocals,
      mem ? mem : reg->mem);
}


void YogaCaller::debug(
  vector<YogaValue> const &outArgs,
  vector<YogaValue> const &inArgs,
  R &astonishment,
  R dt,
  YogaValue &debugLocals,
  apr_pool_t *mem) const
{
  assert(outArgs.size() == f->outArgs.size());

  vector<void *> outArgPtrs, inArgPtrs;
  outArgPtrs.reserve(outArgs.size());
  inArgPtrs.reserve(inArgs.size());
  for (auto &a : outArgs) outArgPtrs.push_back(a.buf);
  for (auto &a : inArgs) inArgPtrs.push_back(a.buf);

  this->debug(
    outArgPtrs,
    inArgPtrs,
    reg->paramValues,
    astonishment,
    dt,
    debugLocals.buf,
    mem);
}


namespace packetio {

void packet_wr_value(::packet &p, ::YogaSetCons * const &x)
{
  if (x) {
    packet_wr_value(p, true);
    packet_wr_value(p, x->type->clsName);
    x->type->packetWr(p, x->value);
    packet_wr_value(p, x->modulation);
    packet_wr_value(p, x->next);
  } else {
    packet_wr_value(p, false);
  }
}

void packet_rd_value(::packet &p, ::YogaSetCons *&x, YogaContext const &ctx)
{
  bool exists{false};
  packet_rd_value(p, exists);
  if (exists) {
    x = (YogaSetCons *)yogic_alloc(sizeof(YogaSetCons), nullptr);
    string clsName;
    packet_rd_value(p, clsName);
    x->type = ctx.getTypeOrThrow(clsName);
    x->type->packetRd(p, x->value, nullptr);
    packet_rd_value(p, x->modulation);
    packet_rd_value(p, x->next, ctx);
  }
  else {
    x = nullptr;
  }
}

}


vector<YogaSetCons *> setAsVector(YogaSetCons *set)
{
  vector<YogaSetCons *> ret;
  for (auto dl = set; dl; dl = dl->next) {
    ret.emplace_back(dl);
  }
  return ret;
}

vector<YogaSetCons *> setSortedBySourceLoc(YogaSetCons *set, YogaSourceFile *fileFilter)
{
  vector<YogaSetCons *> ret;
  for (auto dl = set; dl; dl = dl->next) {
    if (dl->loc && (!fileFilter || dl->loc->file == fileFilter)) {
      ret.emplace_back(dl);
    }
  }
  sort(ret.begin(), ret.end(), [](YogaSetCons * const &a, YogaSetCons * const &b)
    {
      if (a->loc->file && b->loc->file && a->loc->file != b->loc->file) {
        if (a->loc->file->fn < b->loc->file->fn) return true;
        if (a->loc->file->fn > b->loc->file->fn) return false;
      }
      return a->loc->start < b->loc->start;
    }
  );
  return ret;
}

ostream & operator <<(ostream &s, YogaCompiledFuncInfo const &a)
{
  s << "CompiledFunc(";
  string sep = "";
  for (auto &it : a.inArgs) {
    s << sep;
    sep = ", ";
    s << "in "s << repr_type(it.first) << " " << it.second;
  }
  for (auto &it : a.outArgs) {
    s << sep;
    sep = ", ";
    s << "out "s << repr_type(it.first) << " " << it.second;
  }
  s << ")";
  return s;
}

YogaValue linearOpSum(R ca, YogaValue const &a, R cb, YogaValue const &b, apr_pool_t *mem)
{
  if (!a.type || !b.type || a.type != b.type) throw runtime_error("linearOpSum: type mismatch");

  YogaValue ret(a.reg, a.type, mem);
  (*a.type->linearOp)(ret.buf, ca, a.buf, cb, b.buf, mem);

  return ret;
}


R normSq(YogaValue const &a)
{
  if (!a.isValid()) return 0.0;
  return a.type->normSqOp(a.buf);
}

R norm(YogaValue const &a)
{
  return sqrt(normSq(a));
}


extern "C" U8 * yogic_strcat(U8 *a, U8 *b, apr_pool_t *mem)
{
  if (!a || !*a) return b;
  if (!b || !*b) return a;

  string as((char *)a), bs((char *)b);
  
  return yogic_intern(as + bs);
}





string YogaRef::summary() const
{
  return typeName + " " + fullName;
}


YogaRef YogaRef::member(string const &name) const
{
  YogaRef ret(*this);
  ret.fullName = dotJoin(fullName, name);
  ret.subName = dotJoin(subName, name);
  ret.memberName = name;
  ret.itemType = nullptr;

  if (!error.empty()) {
    ret.error = error;
    return ret;
  }
  if (name == "interval" && subName == "") {
    ret.typeName = "interval";
    ret.itemType = nullptr;
  }
  else if (itemType) {
    if (auto ts = itemType->asStruct()) {
      auto memInfo = ts->getMember(name);
      if (memInfo) {
        ret.itemType = memInfo->memberType;
        ret.typeName = memInfo->memberType->clsName;
      }
      else {
        ret.error = "no member named " + name;
      }
    }
    else {
      ret.error = "not a struct";
    }
  }
  return ret;
}



YogaRef YogaRef::cell(int row, int col) const
{
  YogaRef ret(*this);
  string name = "r" + to_string(row) + "c" + to_string(col);
  ret.fullName = dotJoin(fullName, name);
  ret.subName = dotJoin(subName, name);
  ret.memberName = name;
  ret.itemType = nullptr;

  if (!error.empty()) {
    ret.error = error;
    return ret;
  }
  if (name == "interval" && subName == "") {
    ret.typeName = "interval";
    ret.itemType = nullptr;
  }
  else if (itemType) {
    if (auto tm = itemType->asMatrix()) {
      ret.itemType = tm->elementType;
      ret.typeName = tm->elementType->clsName;
    }
    else {
      ret.error = "not a matrix";
    }
  }
  return ret;
}

string YogaRef::fmtFunctionLabel(string const &funcName, vector<YogaRef> const &refs)
{
  string label = funcName + "(";
  for (auto &ref : refs) {
    if (label.back() != '(') label += ',';
    label += ref.fullName;
  }
  label += ')';
  return label;
}


string YogaRef::fmtStandaloneLabel(vector<YogaRef> const &refs)
{
  string label;
  for (auto &ref : refs) {
    if (!label.empty()) label += ' ';
    label += ref.fullName;
  }
  return label;
}

