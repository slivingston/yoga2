#pragma once
#include "./jit_utils.h"

struct AnnoBase;
struct YogaLayout;
struct YogaContext;

struct YogaSourceFile {
  YogaSourceFile(string const &_fn, string const &_text, bool _isBuiltin);
  YogaSourceFile();
  int lineCount();

  string fn;
  string text;
  bool isBuiltin{false};
};

struct YogaSourceLoc {
  YogaSourceLoc(YogaSourceFile *_file, int _start, int _end);
  YogaSourceLoc();

  YogaSourceFile *file;
  // Typically these refer to the start and end pos (in *file) of a complete token.
  // An AstNode::sourceLoc has these refer to the first token of an AST node.
  int start;
  int end;

  pair<int, int> calcLineNumber() const;
  string highlightedLine() const;
  string tokenText() const;
  string gloss() const;
};

ostream & operator <<(ostream &s, YogaSourceLoc const &a);

struct YogaSourceSet {
  YogaSourceSet();
  ~YogaSourceSet();

  YogaSourceFile *getFile(YogaContext const &ctx, string const &fn, bool isBuiltin);

  YogaSourceFile *mkSourceFile(string const &fn, string const &text, bool isBuiltin);

  void tx(packet &p);
  void rx(packet &p);

  YogaLayout &layout;

  map<string, YogaSourceFile *> requiredFilesByName;
  vector<YogaSourceFile *> requiredFilesInOrder;
  deque<unique_ptr<YogaSourceFile>> sourceFilePool;

  string mainFn;
  map<string, string> requireNameMap;
  bool expectOnlyPseudoFiles{false};
};
