#include "./compilation.h"
#include "./runtime.h"
#include "../test/test_utils.h"
#include "../geom/geom.h"

TEST_CASE("Compile yoga/lib/lib.yoga", "[compiler][bloat]") {
  auto reg = make_shared<YogaCompilation>();
  REQUIRE(reg->compileMain(get<0>(YogaLayout::instance().requirePath("yoga/lib/lib.yoga"))));
}

TEST_CASE("Compile yoga/studio/test_render.yoga", "[compiler][bloat]") {
  auto reg = make_shared<YogaCompilation>();
  REQUIRE(reg->compileMain(get<0>(YogaLayout::instance().requirePath("yoga/studio/test_render.yoga"))));
}

TEST_CASE("Compile yoga/drivers/dex4/cad42/test_render_cad42.yoga", "[compiler][bloat]") {
  auto reg = make_shared<YogaCompilation>();
  REQUIRE(reg->compileMain(get<0>(YogaLayout::instance().requirePath("yoga/drivers/dex4/cad42/test_render_cad42.yoga"))));
}

TEST_CASE("Compile yoga/test/test_draw_code.yoga", "[compiler][bloat]") {
  auto reg = make_shared<YogaCompilation>();
  REQUIRE(reg->compileMain(get<0>(YogaLayout::instance().requirePath("yoga/test/test_draw_code.yoga"))));
}

TEST_CASE("matop 1", "[compiler][bloat]") {
  auto reg = YOGAC(R"(
    function matop(out R[4,4] o, in R[4,4] i) {
      o = i*i;
    }
  )");
  REQUIRE(checkerrs(reg, {}));
}

TEST_CASE("matop 2", "[compiler][bloat]") {
  auto reg = YOGAC(R"(
    function matop(out R[4,4] o, in R[4,4] i) {
      foo = i*i;
      o = foo * i;
    }
  )");
  REQUIRE(checkerrs(reg, {}));
}

TEST_CASE("matop 4", "[compiler][bloat]") {
  auto reg = YOGAC(R"(
    function matop(out R[4,4] o, in R[4,4] i) {
      o = i*i*i*i;
    }
  )");
  REQUIRE(checkerrs(reg, {}));
}

TEST_CASE("matop 8", "[compiler][bloat]") {
  auto reg = YOGAC(R"(
    function matop(out R[4,4] o, in R[4,4] i) {
      o = i*i*i*i*i*i*i*i;
    }
  )");
  REQUIRE(checkerrs(reg, {}));
}

TEST_CASE("matop 16", "[compiler][bloat]") {
  auto reg = YOGAC(R"(
    function matop(out R[4,4] o, in R[4,4] i) {
      o = i*i*i*i*i*i*i*i*i*i*i*i*i*i*i*i;
    }
  )");
  REQUIRE(checkerrs(reg, {}));
}

TEST_CASE("matop 32", "[compiler][bloat]") {
  auto reg = YOGAC(R"(
    function matop(out R[4,4] o, in R[4,4] i, in R[4,4] j) {
      o = i*j*j*i*i*j*i*j*i*j*j*i*i*j*i*i*j*i*i*j*i*i*j*i*i*j*i*i*j*j*i*j;
    }
  )");
  REQUIRE(checkerrs(reg, {}));
}

TEST_CASE("matop 32 run 1M", "[compiler][bloat]") {
  auto reg = YOGAC(R"(
    function matop(out R[4,4] o, in R[4,4] i, in R[4,4] j) {
      o = i*j*j*i*i*j*i*j*i*j*j*i*i*j*i*i*j*i*i*j*i*i*j*i*i*j*i*i*j*j*i*j;
    }
  )");
  REQUIRE(checkerrs(reg, {}));
  auto ctx = reg->mkCtx("test");

  YogaCaller matop(ctx.reg, "matop");

  glm::dmat4 o, i(1.0), j(1.0);
  glm::dmat4 expecto = i*j*j*i*i*j*i*j*i*j*j*i*i*j*i*i*j*i*i*j*i*i*j*i*i*j*i*i*j*j*i*j;

  R astonishment = 0.0;
  for (int iter=0; iter<1000000; iter++) {
    matop({glm::value_ptr(o)}, {glm::value_ptr(i), glm::value_ptr(j)}, astonishment);
  }

  CHECK(to_string(o) == to_string(expecto));
}
