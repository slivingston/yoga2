#include "./compilation.h"
#include "../test/test_utils.h"


TEST_CASE("Simple call yoga 1", "[compiler]") {

  auto reg = YOGAC(R"(
    function doubleit(out TestState o, in TestState i) {
      o.foo = i.foo * 2;
      o.bar = i.bar * 2;
      o.buz = i.buz * 2;
    }
    function tp1(out TestState o) {
      doubleit(o, TestState(1,2,3));
    }
    function tp2(out TestState o) {
      o = doubleit(., TestState(1,2,3));
    }
    function tp3(out TestState o) {
      doubleit(tmpo, TestState(1,2,3));
      o = tmpo;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  YogaCaller tp;
  SECTION("tp1") {
    tp = ctx.mkCaller("tp1");
  }
  SECTION("tp2") {
    tp = ctx.mkCaller("tp2");
  }
  SECTION("tp3") {
    tp = ctx.mkCaller("tp3");
  }
  vector<YogaValue> outs;
  tp.allocOutTypes(outs, reg->mem);
  R astonishment = 0.0;
  tp(outs, {}, astonishment);
  R oFoo=0.0, oBar=0.0, oBuz=0.0;
  outs[0].rd("foo", oFoo);
  outs[0].rd("bar", oBar);
  outs[0].rd("buz", oBuz);
  CHECK(oFoo == 2.0);
  CHECK(oBar == 4.0);
  CHECK(oBuz == 6.0);
}

