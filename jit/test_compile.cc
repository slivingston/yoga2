#include "./compilation.h"
#include "../test/test_utils.h"

// 

TEST_CASE("Yoga syntax errors", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      if = 3;
    }
    )"), {
      atline(3, "error:.*Expected \"\\(\"")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      (if = 3);
    }
    )"), {
      atline(3, "error:.*Expected value")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = (3 + if);
    }
    )"), {
      atline(3, "error:.*Expected value")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = 1;
    )"),  {
      atline(4, "error:.*Expected value")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = 1;
    }}}
    )"), {
      atline(4, "error:.*Expected value")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      {foo: "bar", 7: "wug"}
    }
    )"), {
      atline(3, "error:.*Expected key")
    }));
}

TEST_CASE("Yoga Numbers", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = (3e3 - 3.0e-5 + (-3.9e-5) + 123 + (+123) + (-123) - (-123));
    }
  )"), {
  }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = 1.2.3;
    }
    )"), {
      atline(3, "error:.*Expected \";\"")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = 0123;
    }
    )"), {
      atline(3, "error:.*Expected value")
    }));
}

TEST_CASE("Yoga Constants", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    PI = 3.14;
    EPS = 0.001;
    INF = 1/0;
    function test(update TestState s, in TestConfig c) {
      s.foo = PI + EPS;
      s.bar = INF;
    }
  )"), {
  }));
}

TEST_CASE("Parsing", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s) {
      s.buz = s.buz * 2;
    }
  )"), {
  }));
}

TEST_CASE("Variables", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      foo.bar = 3;
    }
    )"), {
      atline(3, "error: Unbound variable foo")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = 6 * blub;
    }
    )"), {
      atline(3, "error: Unbound variable blub")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = blub * 6;
    }
    )"), {
      atline(3, "error: Unbound variable blub")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = sin(blub);
    }
    )"), {
      atline(3, "error: Unbound variable blub")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s = TestState(1, 2, blub, 4);
    }
    )"), {
      atline(3, "error: Unbound variable blub")
    }));
}

TEST_CASE("Yoga struct members", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = 17;
      s = TestState({foo: 1, bar: 2, buz: 3});
    }
    )"), {
      atline(4, "error: Assignment prohibited by previous assignment to member"),
      atline(3, "note: Previous assignment is here")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s = TestState({foo: 1, bar: 2, buz: 3});
      s.foo = 17;
    }
    )"), {
      atline(4, "error: Assignment prohibited by previous assignment to container"),
      atline(3, "note: Previous assignment is here")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s = TestState({foo: 1, bar: 3, buz: 1, rumpledump: 99});
    }
    )"), {
      atline(3, "error: No member named rumpledump")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s = TestState({bar: 3, buz: 1});
    }
    )"), {
      atline(3, "error: No value for foo")
    }));
}


TEST_CASE("Yoga type checking", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = c;
    }
    )"), {
      atline(3, "error: Type mismatch in assignment: expected R got TestConfig")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function test(update TestState s, in TestConfig c) {
      s.foo = "foo";
    }
    )"), {
      atline(3, "error: Type mismatch")
    }));
}


TEST_CASE("Yoga name collision", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    struct Foob {
    }
    function Foob() {
    }
    )"), {
      atline(4, "error: Foob previously defined as struct")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function Foob() {
    }
    struct Foob {
    }
    )"), {
      atline(4, "error: Foob previously defined as function")
    }));
}


TEST_CASE("Yoga Matrix constructors", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    function Foob() {
      x = R[3,3](1,2,3,4,5);
    }
    )"), {
      atline(3, "error: Expected 9 args for construct\\.R\\[3,3\\], got 5")
    }));
}


TEST_CASE("Visual definition", "[compiler][runtime]") {

  auto reg = YOGAC(R"(
    panel acrobotRender(ui.render2d, foo.bar, buz.wug);
    panel cad42RenderSolid(ui.render3d, zub.x);
    predict acrobotPredict(plant.sense, agent.state) {
      dur: 1.0,
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  if (1) {
    auto pd = ctx.reg->panelDefs.at(0);
    CHECK(pd->call->literalFuncName == "acrobotRender");
  }
  if (1) {
    auto pd = ctx.reg->panelDefs.at(1);
    CHECK(pd->call->literalFuncName == "cad42RenderSolid");
  }
  if (1) {
    auto pd = ctx.reg->predictDefs.at(0);
    CHECK(pd->call->literalFuncName == "acrobotPredict");
    R dur;
    CHECK(pd->options->getValueForKey("dur", dur, true));
    CHECK(dur == 1.0);
  }

}




TEST_CASE("arg type check 1", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    function fb1(out TestState ts, in Foobar fb) {
      ts.foo = fb.foo;
      ts.bar = fb.bar;
      ts.buz = 3;
    }

    function fb2(out TestState ts, in TestState fb) {
      ts = fb1(., fb);
    }
    )"), {
      atline(9, "Type mismatch for arg 2: expected Foobar got TestState")
    }));
}


TEST_CASE("arg type check 2", "[compiler]") {

  CHECK(checkerrs(YOGAC(R"(
    function fb1(out TestState ts, in Foobar fb) {
      ts.foo = 2*fb.foo;
      ts.bar = 3*fb.bar;
      ts.buz = 4;
    }

    function fb2(out TestState ts, in Foobar fb) {
      fb1(ts, fb);
    }

    function fb3(out TestState ts, in Foobar fb) {
      fb1(tstmp, fb);
      ts = tstmp;
    }

    function fb4(out TestState ts, in Foobar fb) {
      ts = fb1(., fb);
    }
    )"), {
    }));

  CHECK(checkerrs(YOGAC(R"(
    function fb1(out TestState ts, in Foobar fb) {
      ts.foo = 2*fb.foo;
      ts.bar = 3*fb.bar;
      ts.buz = 4;
    }

    function fb2(out /*OOPS*/ Foobar ts, in Foobar fb) {
      fb1(ts, fb);
    }
    )"), {
      atline(9, "Type mismatch for arg 1: expected TestState got Foobar")
    }));

  CHECK(checkerrs(YOGAC(R"(
    function fb1(out TestState ts, in Foobar fb) {
      ts.foo = 2*fb.foo;
      ts.bar = 3*fb.bar;
      ts.buz = 4;
    }

    function fb3(out Foobar ts, in Foobar fb) {
      fb1(tstmp, fb);
      ts = tstmp;
    }
    )"), {
      atline(10, "Type mismatch in assignment: expected Foobar got TestState")
    }));

}




TEST_CASE("Variable block scoping", "[compiler]") {

  auto reg = YOGAC(R"(
    function tp(out R foo, in R bar) {
      a = 1;
      if (bar > 0) {
        b = 1;
        foo = a + b;
      }
      else {
        b = 2;
        foo = a + b;
      }
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  R foo, bar = 1;
  R astonishment = 0.0;
  tp({&foo}, {&bar}, astonishment);
  CHECK(foo == 2.0);

  bar = -1;
  tp({&foo}, {&bar}, astonishment);
  CHECK(foo == 3.0);

}


TEST_CASE("Variable block scoping with shadow", "[compiler]") {

  auto reg = YOGAC(R"(
    function tp(out R foo, in R bar) {
      a = 1;
      b = 0;
      if (bar > 0) {
        b = 1;
        foo = a + b;
      }
      else {
        b = 2;
        foo = a + b;
      }
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {
    atline(6, "No lvalue for lhs of =")
  }));

}
