#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"
#include "../geom/geom.h"


TEST_CASE("Debug: call", "[compiler][debug][new]") {
  auto reg = YOGAC(R"(
    function sub1(out R y, in R x) {
      y = 2 * x;
    }
    function sub2(out R y, in R x) {
      y = 3 * x;
    }
    function tp(out R o1, out Foobar fb, in R i1) {
      foo = sub1(., 5) + sub2(., 5);
      o1 = 3 * i1 + foo;
      fb.foo = 7;
      fb.bar = 9;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  YogaPool tmpmem;

  auto tp = ctx.mkCaller("tp");

  YogaValue i1 = ctx.mkValue("R")
    .wr("", 13.5);
  
  YogaValue o1 = ctx.mkValue("R");
  YogaValue fb = ctx.mkValue("Foobar");

  YogaValue tpDebugLocals = ctx.mkValue("tp.d");

  R astonishment = 0.0;

  tp.debug({o1, fb}, {i1}, astonishment, 0.0, tpDebugLocals, tmpmem.it);

  auto debugStr = repr(tpDebugLocals);

  CHECK(debugStr == "tp.d({sub1: sub1.d({out.y: 10}), sub2: sub2.d({out.y: 15}), foo: 25, out.o1: 65.5, out.fb.foo: 7, out.fb.bar: 9})");

}

