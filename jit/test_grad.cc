#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"
#include "../geom/geom.h"


TEST_CASE("Gradients: basic", "[compiler][grad]") {
  auto reg = YOGAC(R"(
    function tp(out R o1, in R i1) {
      o1 = 3 * i1;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  YogaValue i1 = ctx.mkValue("R")
    .wr("", 13.5);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1", 3.0));
}

TEST_CASE("Gradients: struct", "[compiler][grad]") {
  auto reg = YOGAC(R"(
    function tp(out Foobar o1, in Foobar i1) {
      o1 = 3 * i1;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", 11.5)
    .wr("bar", 12.5);

  CHECK(checkGradient(tp, { i1 }, "o1.foo", "i1.foo", 3.0));
  CHECK(checkGradient(tp, { i1 }, "o1.bar", "i1.bar", 3.0));
  CHECK(checkGradient(tp, { i1 }, "o1.foo", "i1.bar", 0.0));
  CHECK(checkGradient(tp, { i1 }, "o1.bar", "i1.foo", 0.0));
}

TEST_CASE("Gradients: sin", "[compiler][grad]") {

  auto reg = YOGAC(R"(
    function tp(out R o1, in Foobar i1) {
      o1 = sin(2*i1.foo) + sin(3*i1.bar);
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  auto exFoo = GENERATE(0.5, 0.75, 1.0, 2.0);
  auto exBar = GENERATE(0.5, 0.75, 1.0, 2.0);

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", exFoo)
    .wr("bar", exBar);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.foo", 2.0*cos(2.0*exFoo)));
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.bar", 3.0*cos(3.0*exBar)));
}

TEST_CASE("Gradients: div", "[compiler][grad]") {
  auto reg = YOGAC(R"(
    function tp(out R o1, in Foobar i1) {
      o1 = i1.foo / i1.bar;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", 1.5)
    .wr("bar", 0.5);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.foo", 2.0));
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.bar", -6.0));
}


TEST_CASE("Gradients: pow 2", "[compiler][grad]") {
  auto reg = YOGAC(R"(
    function tp(out R o1, in Foobar i1) {
      o1 = i1.foo ^ 2 + i1.bar ^ -2;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", 1.5)
    .wr("bar", 0.5);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.foo", 3.0));
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.bar", -16.0));
}


TEST_CASE("Gradients: pow 1", "[compiler][grad]") {
  auto reg = YOGAC(R"(
    function tp(out R o1, in Foobar i1) {
      o1 = i1.foo ^ 1 + i1.bar ^ -1;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", 1.5)
    .wr("bar", 0.5);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.foo", 1.0));
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.bar", -4.0));
}


TEST_CASE("Gradients: add-mul") {
  auto reg = YOGAC(R"(
    function tp(out R o1, in Foobar i1) {
      o1 = 2*i1.foo + 3*i1.bar;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", 1.0)
    .wr("bar", 0.5);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.foo", 2.0));
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.bar", 3.0));
}


TEST_CASE("Gradients: callYoga", "[compiler][grad]") {

  auto reg = YOGAC(R"(
    function childf(out R o1, in R i1) {
      o1 = sin(i1 * 3);
    }
    function tp(out R o1, in Foobar i1) {
      o1 = childf(., 2*i1.foo);
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  auto exFoo = GENERATE(0.05, 0.25, 0.5);

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", exFoo)
    .wr("bar", 2.0);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.foo", 6.0*cos(6.0*exFoo)));
}


TEST_CASE("Gradients: callYoga with params", "[compiler][grad]") {

  auto reg = YOGAC(R"(
    function childf(out R o1, in R i1) {
      o1 = sin(i1 * 3~5);
    }
    function tp(out R o1, in Foobar i1) {
      o1 = childf(., 2~5 *i1.foo);
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  auto exFoo = GENERATE(0.05, 0.25, 0.5);

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", exFoo)
    .wr("bar", 2.0);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.foo", 6.0*cos(6.0*exFoo)));
}


TEST_CASE("Gradients: callYoga with shared params", "[compiler][grad]") {

  auto reg = YOGAC(R"(
    zerg = {
      blaf: 3~5,
      krug: 1~2
    };
    function childf(out R o1, in R i1) {
      o1 = sin(i1 * zerg.blaf);
    }
    function tp(out R o1, in Foobar i1) {
      o1 = childf(., zerg.blaf * i1.foo) * zerg.krug + i1.bar;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  auto exFoo = GENERATE(0.05, 0.25, 0.5);

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", exFoo)
    .wr("bar", 2.0);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.foo"));
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.bar", 1.0));
}


TEST_CASE("Gradients: flow into params", "[compiler][grad]") {

  auto reg = YOGAC(R"(
    zerg = {
      blaf: 2~5,
      krug: 1~2
    };
    function tp(out R o1, in Foobar i1) {
      o1 = clamp(zerg.blaf * i1.foo, -1~1, 1~1) * zerg.krug;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  auto exFoo = GENERATE(-1.0, -0.125, 0.0, +0.125, +1.0);

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", exFoo)
    .wr("bar", 0.0);
  
  CHECK(checkGradient(tp, { i1 }, "o1", "i1.foo"));
}


TEST_CASE("Gradients: flow through out.structrefs", "[compiler][grad]") {

  auto reg = YOGAC(R"(
    zerg = {
      blaf: 2~5,
      krug: 1~2
    };
    function tp(out Foobar o1, in Foobar i1) {
      o1.foo = i1.foo * 2;
      o1.bar = i1.bar * 3;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  auto exFoo = GENERATE(-1.0, -0.125, 0.0, +0.125, +1.0);

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", exFoo)
    .wr("bar", exFoo);
  
  CHECK(checkGradient(tp, { i1 }, "o1.foo", "i1.foo"));
  CHECK(checkGradient(tp, { i1 }, "o1.bar", "i1.bar"));
}



TEST_CASE("Gradients: flow through sign and clamp", "[compiler][grad]") {

  auto reg = YOGAC(R"(
    zerg = {
      blaf: 0.25~1,
      krug: 1~2
    };
    function tp(out Foobar o1, in Foobar i1) {
      o1.foo = clamp(zerg.blaf * sign(i1.foo+0.01) + i1.bar, -1, 1);
      o1.bar = 0;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  auto exFoo = GENERATE(-1.0, -0.125, 0.0, +0.125, +1.0);

  YogaValue i1 = ctx.mkValue("Foobar")
    .wr("foo", exFoo)
    .wr("bar", exFoo);
  
  CHECK(checkGradient(tp, { i1 }, "o1.foo", "i1.foo"));
  CHECK(checkGradient(tp, { i1 }, "o1.bar", "i1.bar"));
}



