#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"
#include "../geom/geom.h"


TEST_CASE("Order of operations: ^ before *", "[compiler]") {
  auto reg = YOGAC(R"(
    function tp(out R o1, out R o2, out R o3) {
      o1 = 3^2;
      o2 = 3^2*2;
      o3 = 5*3^2*2;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  R o1, o2, o3;
  R astonishment = 0.0;
  tp({&o1, &o2, &o3}, {}, astonishment);

  CHECK(o1 == 9.0);
  CHECK(o2 == 18.0);
  CHECK(o3 == 90.0);
}

TEST_CASE("sum(struct)", "[compiler]") {
  auto reg = YOGAC(R"(
    struct Mask {
      R foo;
      R bar;
      R buz;
    };
    function sumtest(out R o1, in Mask i) {
      o1 = i.foo + i.bar + i.buz;
    }
    function tp(out R o1) {
      sumtest(o1, Mask({foo: 0.125, bar: 0.25, buz: 0.5}));
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  R o1=9.9;
  R astonishment = 0.0;
  tp({&o1}, {}, astonishment);

  CHECK(o1 == 0.875);
}


TEST_CASE("sum(onehot)", "[compiler]") {
  auto reg = YOGAC(R"(
    onehot Mask {
      foo;
      bar;
      buz;
    };
    function sumtest(out R o1, in Mask i) {
      o1 = sum(i);
    }
    function tp(out R o1) {
      sumtest(o1, Mask({foo: 0.125, bar: 0.25, buz: 0.5}));
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  R o1=9.9;
  R astonishment = 0.0;
  tp({&o1}, {}, astonishment);

  CHECK(o1 == 0.875);
};




TEST_CASE("Complex add", "[compiler][complex]") {
  auto reg = YOGAC(R"(
    function tp(out C o1) {
      o1 = 5 + 3i;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  std::complex<R> o1;
  R astonishment = 0.0;
  tp({&o1}, {}, astonishment);
  CHECK(astonishment < 2.0);

  CHECK(o1 == (5.0 + 3.0i));
}


TEST_CASE("Complex mul", "[compiler][complex]") {
  auto reg = YOGAC(R"(
    function tp(out C o1) {
      o1 = (5 + 3i) * (2 + 4i);
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  R astonishment = 0.0;
  std::complex<R> o1;
  tp({&o1}, {}, astonishment);
  CHECK(astonishment < 2.0);

  CHECK(o1 == (-2.0 + 26.0i));
}


TEST_CASE("Complex extract with .r or .i", "[compiler][complex]") {
  auto reg = YOGAC(R"(
    function tp(out R o1) {
      o1 = (5 + 3i).r + (6 + 4i).i;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  R astonishment = 0.0;
  R o1;
  tp({&o1}, {}, astonishment);
  CHECK(astonishment < 2.0);

  CHECK(o1 == (5.0 + 4.0));
}


TEST_CASE("Complex with C()", "[compiler][complex]") {
  auto reg = YOGAC(R"(
    function tp(out C o1) {
      o1 = C(5+4, 2*3);
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  std::complex<R> o1;
  R astonishment = 0.0;
  tp({&o1}, {}, astonishment);
  CHECK(astonishment < 2.0);

  CHECK(o1 == std::complex(9.0, 6.0));
}

