#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"
#include "../geom/geom.h"

TEST_CASE("Packet IO Struct read/write", "[compiler][packetio]") {

  auto reg = YOGAC(R"(
    struct PacketTest {
      R foo;
      R bar;
      R[3,3] m33;
      R buz;
    }
    function mkpt(out PacketTest o) {
      o.foo = 3.5;
      o.bar = 4.5;
      o.m33 = R[3,3](1,2,3,4,5,6,7,8,9);
      o.buz = 5.5;
    }
  )");
  auto ctx = reg->mkCtx("test");

  auto t = ctx.getTypeOrThrow("PacketTest");

  auto yv1 = ctx.mkValue(t);

  YogaValueAccessor<double> accFoo(t, "foo");
  YogaValueAccessor<double> accBar(t, "bar");
  YogaValueAccessor<glm::mat3> accM33(t, "m33");
  YogaValueAccessor<double> accBuz(t, "buz");
  accFoo.wr(yv1, 29.5);
  accBar.wr(yv1, 30.5);
  accM33.wr(yv1, glm::mat3(1,2,3,4,5.5,6,7,8,9));
  accBuz.wr(yv1, 31.5);

  CHECK(accFoo.rd(yv1) == 29.5);
  CHECK(accBar.rd(yv1) == 30.5);
  CHECK(accM33.rd(yv1) == glm::mat3(1,2,3,4,5.5,6,7,8,9));
  CHECK(accBuz.rd(yv1) == 31.5);

  auto yv2 = yv1.dup(reg->mem);
  CHECK(accFoo.rd(yv2) == 29.5);
  CHECK(accBar.rd(yv2) == 30.5);
  CHECK(accM33.rd(yv2) == glm::mat3(1,2,3,4,5.5,6,7,8,9));
  CHECK(accBuz.rd(yv2) == 31.5);
  CHECK(!memcmp(yv1.buf, yv2.buf, t->llAllocSize));

  packet p;
  (*t->packetWr)(p, yv1.buf);
  CHECK(p.size() == (2+9+1)*sizeof(double));
  
  YogaValue yv3 = ctx.mkValue(t);
  (*t->packetRd)(p, yv3.buf, reg->mem);

  CHECK(accFoo.rd(yv3) == 29.5);
  CHECK(accBar.rd(yv3) == 30.5);
  CHECK(accM33.rd(yv3) == glm::mat3(1,2,3,4,5.5,6,7,8,9));
  CHECK(accBuz.rd(yv3) == 31.5);
  CHECK(!memcmp(yv1.buf, yv3.buf, t->llAllocSize));
}



