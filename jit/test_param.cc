#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"
#include "../geom/geom.h"
#include "numerical/haltonseq.h"
#include "glm/gtx/norm.hpp"


TEST_CASE("Basic param", "[compiler][param]") {
  auto reg = YOGAC(R"(
    function tp(out R o1, out R o2, out R o3) {
      o1 = 1~1;
      o2 = 2+1~1;
      o3 = 1~1+5;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  R o1, o2, o3;
  R astonishment = 0.0;
  tp({&o1, &o2, &o3}, {}, astonishment);

  CHECK(o1 == 1.0);
  CHECK(o2 == 3.0);
  CHECK(o3 == 6.0);
}

TEST_CASE("Param naming", "[compiler][param]") {
  auto reg = YOGAC(R"(
    function tp(out Foobar o1) {
      o1.foo = 3.5~10 * cos(1);
      o1.bar = sin(2) * 4.5~10;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  // These should change if we want to change the naming convention
  REQUIRE(ctx.reg->paramsByIndex.size() == 2);
  CHECK(ctx.reg->paramsByIndex[0]->paramName == "tp.o1.foo.cos");
  CHECK(ctx.reg->paramsByIndex[1]->paramName == "tp.o1.bar.sin");
}



TEST_CASE("Constructor param", "[compiler][param]") {
  auto reg = YOGAC(R"(
    function tp(out Foobar o1) {
      o1 = Foobar({foo:1, bar: 2}~10);
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  YogaFoobar o1;
  R astonishment = 0.0;
  tp({&o1}, {}, astonishment);

  CHECK(o1.foo == 1.0);
  CHECK(o1.bar == 2.0);

  // These should change if we want to change the naming convention
  REQUIRE(ctx.reg->paramsByIndex.size() == 2);
  CHECK(ctx.reg->paramsByIndex[0]->paramName == "tp.o1.a.foo");
  CHECK(ctx.reg->paramsByIndex[1]->paramName == "tp.o1.a.bar");
}


TEST_CASE("Constructor param nested", "[compiler][param]") {
  auto reg = YOGAC(R"(
    struct Mugwump {
      R mug;
      R wump;
      Foobar fb;
    };
    function tp(out Mugwump o1) {
      o1 = Mugwump({fb:Foobar({foo: 1, bar: 2}), mug: 3, wump: 4}~10);
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  struct TestMugwump {
    R mug;
    R wump;
    YogaFoobar fb;
  };

  TestMugwump o1;
  R astonishment = 0.0;
  tp({&o1}, {}, astonishment);

  CHECK(o1.fb.foo == 1.0);
  CHECK(o1.fb.bar == 2.0);
  CHECK(o1.mug == 3.0);
  CHECK(o1.wump == 4.0);

  // These should change if we want to change the naming convention
  REQUIRE(ctx.reg->paramsByIndex.size() == 4);
  CHECK(ctx.reg->paramsByIndex[0]->paramName == "tp.o1.a.fb.a.foo");
  CHECK(ctx.reg->paramsByIndex[1]->paramName == "tp.o1.a.fb.a.bar");
  CHECK(ctx.reg->paramsByIndex[2]->paramName == "tp.o1.a.mug");
  CHECK(ctx.reg->paramsByIndex[3]->paramName == "tp.o1.a.wump");
}

TEST_CASE("Extern params & gradient descent", "[compiler][param]") {
  auto reg = YOGAC(R"(
    function tp(out R[2] o1, in R[2] i1) {
      a1 = R[8,2](extern~0.25);
      b1 = R[8](extern~1);
      a2 = R[2,8](extern~0.25);
      b2 = R[2](extern~1);
      o1 = a2 * relu(a1 * i1 + b1) + b2;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  REQUIRE(ctx.reg->paramsByIndex.size() == 42);
  YogaPool tmpmem;

  auto tp = ctx.mkCaller("tp");

  REQUIRE(tp.f->gradAddr);

  vector<R> paramsGrad;
  tp.allocParams(paramsGrad);

  R lossaccum = 0.0;
  R curloss = -1;
  for (int i = 0; i < 100000; i++) {
    dvec2 i1, o1, targ1;
    auto g1 = gaussianR2Row(i, 2);
    i1[0] = g1[0];
    i1[1] = g1[1];

    targ1[0] = sin(i1[0]);
    targ1[1] = cos(i1[1]);

    R astonishment = 0.0;
    tp({&o1}, {&i1}, astonishment);
    if (0) cerr << to_string(i1) + " => " + to_string(o1) + "\n";

    dvec2 i1Grad;
    dvec2 o1Grad = targ1 - o1;

    tp.grad(
      {&o1}, {&o1Grad},
      {&i1}, {&i1Grad},
      ctx.reg->paramValues,
      paramsGrad,
      astonishment,
      0.0,
      tmpmem.it);

    lossaccum += length2(o1Grad);
    
    if (i % 10 == 9) {
      ctx.reg->applyParamsGrad(paramsGrad, 5e-4);
    }

    if (i % 1000 == 999) {
      curloss = lossaccum / 1000.0;
      lossaccum = 0.0;
      if (0) cerr << "loss = " + repr(curloss) + "\n";
    }

    if (0) cerr << "o1Grad=" + to_string(o1Grad) + " i1Grad=" + to_string(i1Grad) + 
      " paramsGrad=" + repr(paramsGrad) + "\n";
  }
  CHECK(curloss > 0.01);
  CHECK(curloss < 0.1);

}

