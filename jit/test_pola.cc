#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"
#include "../geom/geom.h"


TEST_CASE("Astonishment", "[compiler]") {
  auto reg = YOGAC(R"(

    function judge(out R o, in R i) {
      astonishment += 3;
      o = 10;
    }

    function tp(out R o1, in R i1) {
      o1 = i1;
      astonishment += judge(., i1);
      astonishment += i1^2;
      astonishment += 100;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  R o1, i1=0.5;
  R astonishment = 0.5;
  tp({&o1}, {&i1}, astonishment);

  CHECK(astonishment == 113.75);

  CHECK(o1 == 0.5);
}
