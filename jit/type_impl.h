#pragma once
#include "./compilation.h"
#include "./type.h"

template<typename T, typename ...Args>
T * YogaContext::mkType(Args&& ...args) const
{
    auto ptr = make_unique<T>(std::forward<Args>(args)...);
    auto ret = ptr.get();
    reg->typePool.emplace_back(std::move(ptr));

    ret->sourceLoc = lastLoc;
    ret->codeEpoch = reg->codeEpoch;
    return ret;
}


template<typename ...Args>
StructMember * YogaContext::mkStructMember(Args&& ...args) const
{
  unique_ptr ptr = make_unique<StructMember>(std::forward<Args>(args)...);
  StructMember *ret = ptr.get();
  reg->structMemberPool.emplace_back(std::move(ptr));

  ret->sourceLoc = lastLoc;
  return ret;
}


