#pragma once
#include "common/std_headers.h"
#include "./jit_utils.h"
#include "./type.h"
#include "./param.h"
#include "./memory.h"

struct YogaValuePrintOpts {
  bool compact{false};
};

struct YogaValue {

  YogaValue()
    : reg(nullptr),
      type(nullptr),
      buf(nullptr),
      ts(0.0)
  {
  }

  YogaValue(YogaCompilation *_reg,
            YogaType *_type,
            apr_pool_t *mem)
    : reg(_reg),
      type(_type),
      buf(yogic_alloc(_type->llAllocSize, mem)),
      ts(0.0)
  {
    memset(buf, 0, _type->llAllocSize);
  }

  YogaValue(YogaCompilation *_reg,
            YogaType *_type,
            R _ts,
            apr_pool_t *mem)
    : reg(_reg),
      type(_type),
      buf(yogic_alloc(_type->llAllocSize, mem)),
      ts(_ts)
  {
    memset(buf, 0, _type->llAllocSize);
  }

  YogaValue(YogaCompilation *_reg,
            YogaType *_type,
            U8 *_buf)
    : reg(_reg),
      type(_type),
      buf(_buf),
      ts(0.0)
  {
  }

  YogaValue(YogaCompilation *_reg,
            YogaType *_type,
            U8 *_buf,
            double _ts)
    : reg(_reg),
      type(_type),
      buf(_buf),
      ts(_ts)
  {
  }

  YogaValue(YogaCompilation *_reg, YogaSetCons *it);

  bool isValid() const
  {
    return buf != nullptr && type != nullptr;
  }

  bool nonZeroTs() const
  {
    return isValid() && ts != 0.0;
  }

  bool nonZeroValue() const
  {
    return isValid() && memcmp(buf, type->zeroValue, type->llAllocSize);
  }

  bool copyValueFrom(YogaValue const &other);

  void writeValue(ostream &s, YogaValuePrintOpts opts) const;

  YogaValue dup(apr_pool_t *mem) const;

  void setZero(apr_pool_t *mem);
  void setCrap(apr_pool_t *mem);
  void checkForCrap();

  explicit operator bool () const { return isValid(); }

  template<typename T>
  YogaValue & wr(string const &name, T const &value);

  template<typename T>
  void rd(string const &name, T &value) const;

  template<typename T>
  T rd(string const &name) const;

  YogaCompilation *reg;
  YogaType *type;

  U8 *buf;
  double ts;

};

bool operator == (YogaValue const &a, YogaValue const &b);

ostream & operator <<(ostream &s, const YogaValue &a);
string repr_0_3f(YogaValue const &a);

vector<YogaValue> dup(vector<YogaValue> const &a, apr_pool_t *mem);


// A Set type is a linked list of these
struct YogaSetCons {
  YogaSetCons *next;
  YogaType *type;
  double modulation;
  U8 *value;
  YogaSourceLoc *loc;
};
