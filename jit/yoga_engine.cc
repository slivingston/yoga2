#include "./yoga_engine.h"
#include "../timeseq/trace.h"
#include "../jit/runtime.h"

YogaEngine::YogaEngine()
{
}

YogaEngine::~YogaEngine()
{
}

void YogaEngine::update(EngineUpdateContext &ctx)
{
  if (verbose >= 3) consolef("update");
  vector<U8 *> inPtrs;
  inPtrs.reserve(inArgs.size());

  R maxOutTs{-1.0};
  R maxInTs = rti->yogaParamsUpdateTs;
  for (auto &arg : inArgs) {
    auto v = arg.seq->getLast();
    maxInTs = max(maxInTs, v.ts);
    inPtrs.push_back(v.buf);
  }
  for (auto &arg : outArgs) {
    auto v = arg.seq->getLast();
    maxOutTs = max(maxOutTs, v.ts);
  }

  if (inArgs.empty()) {
    maxInTs = max(maxInTs, floor((ctx.now - rti->realTimeOffset) * 16.0) / 16.0);
  }

  if (maxInTs > ctx.rtFence) return;

  if (maxInTs + advanceDt <= maxOutTs) {
    if (verbose >= 3) consolef("no work. maxInTs=%0.3f + advanceDt=%0.3f <= maxOutTs=%0.3f",
      maxInTs, advanceDt, maxOutTs);
    return;
  }
  if (maxOutTs == -1.0) {
    maxOutTs = maxInTs;
  }
  else if (maxInTs - maxOutTs > 0.1) {
    maxInTs = maxOutTs + 0.1;
  }
  else if (advanceDt > 0.0 && maxInTs - maxOutTs < advanceDt) {
    maxInTs = maxOutTs + advanceDt;
  }
  if (maxInTs - maxOutTs < minUpdateDt) {
    return;
  }
  double dt = maxInTs - maxOutTs;
  double newOutTs = maxOutTs + dt;

  vector<U8 *> outPtrs;
  outPtrs.reserve(outArgs.size());
  vector<YogaValue> todoCrapCheck;
  
  for (auto &arg : outArgs) {
    auto val = arg.seq->addNew(newOutTs);
    outPtrs.push_back(val.buf);
    if (rti->reg->initializeWithByte != 0) todoCrapCheck.push_back(val);
  }

  R astonishment = 0.0;
  if (debugLocalsSeq) {
    YogaValue debugLocals = debugLocalsSeq->addNew(maxInTs);
    (*yogaFuncInfo->debugAddr)(
      outPtrs.data(),
      inPtrs.data(), 
      rti->paramValues, &astonishment, dt, debugLocals.buf, rti->reg->mem);
  }
  else {
    (*yogaFuncInfo->funcAddr)(
      outPtrs.data(),
      inPtrs.data(), 
      rti->paramValues, &astonishment, dt, rti->reg->mem);
  }

  if (verbose >= 2) consolef("calling jitted code OK, maxInTs=%0.3f maxOutTs=%0.3f dt=%0.6f astonishment=%g", 
    maxInTs, maxOutTs,
    dt, astonishment);

  for (auto &outVal : todoCrapCheck) {
    outVal.checkForCrap();
  }

  if (ctx.gradFwdTrace) {
    ctx.gradFwdTrace->push_back(
      [this, inPtrs, outPtrs, dt, newOutTs](GradientSet &grads) {
        vector<U8 *> outGradIn;
        outGradIn.reserve(outPtrs.size());
        vector<U8 *> inGradOut;
        inGradOut.reserve(inArgs.size());

        for (auto &a : inArgs) {
          inGradOut.push_back(a.type->mkInstance(grads.mem));
        }

        for (size_t i = 0; i < outArgs.size(); i++) {
          outGradIn.push_back(grads.getGrad(outPtrs[i], outArgs[i].type));
        }

        R astonishment = 0.0;
        (*yogaFuncInfo->gradAddr)(
          outPtrs.data(), outGradIn.data(),
          inPtrs.data(), inGradOut.data(), 
          rti->paramValues, grads.paramGradAccum.data(),
          &astonishment,
          dt, grads.mem);

        if (0) {
          string s;
          s += "in=[";
          for (size_t i = 0; i < inArgs.size(); i++) {
            if (i) s += " ";
            s += repr(YogaValue(rti->reg.get(), inArgs[i].type, inPtrs[i]));
          }

          s += "] inGradOut=[";
          for (size_t i = 0; i < inArgs.size(); i++) {
            if (i) s += " ";
            s += repr(YogaValue(rti->reg.get(), inArgs[i].type, inGradOut[i]));
          }

          s += "] out=[";
          for (size_t i = 0; i < outArgs.size(); i++) {
            if (i) s += " ";
            s += repr(YogaValue(rti->reg.get(), outArgs[i].type, outPtrs[i]));
          }

          s += "] outGradIn=[";
          for (size_t i = 0; i < outArgs.size(); i++) {
            if (i) s += " ";
            s += repr(YogaValue(rti->reg.get(), outArgs[i].type, outGradIn[i]));
          }
          s += "]";

          cerr << engineName + ": " + repr_0_3f(newOutTs) + " " + s + "\n";
        }

        for (int i=0; i < inArgs.size(); i++) {
          grads.addWindowedGrad(inPtrs[i], inGradOut[i], 1.0, newOutTs, inArgs[i].type);
        }
      }
    );
  }

  ctx.updateCnt++;
}

void YogaEngine::debugUpdate(EngineUpdateContext &ctx, R ts, YogaValue &debugLocals, Trace *specTrace, apr_pool_t *mem)
{
  vector<U8 *> outPtrs;
  outPtrs.reserve(outArgs.size());
  
  vector<U8 *> inPtrs;
  inPtrs.reserve(inArgs.size());

  const int verbose = 0;
  string logstr;
  if (verbose) {
    logstr += "engine " + engineName;
    logstr += specTrace ? " with specTrace" : "";
    logstr += "(";
  }

  for (auto &arg : inArgs) {
    YogaValue val;
    YogaTimeseq *seq = nullptr;
    if (specTrace) {
      seq = specTrace->getTimeseq(arg.seqName).get();
    }
    else {
      seq = arg.seq.get();
    }
    if (seq) {
      val = seq->getEqualBefore(ts);
    }
    else {
      val = YogaValue(rti->reg.get(), arg.type, mem);
    }
    if (verbose) {
      if (!(logstr.empty() || logstr.back() == '(')) logstr += ",";
      logstr += arg.formalArgName ;
      if (seq) {
        logstr += "[from " + seq->seqName + "]";
      }
      logstr + "=" + repr(val);
    }
    inPtrs.push_back(val.buf);
  }
  if (verbose) logstr += ")";

  R dt = 0.01; // FIXME. 
  for (auto &arg : outArgs) {
    YogaTimeseq *seq = nullptr;
    if (specTrace) {
      seq = specTrace->getTimeseq(arg.seqName).get();
    }
    else {
      seq = arg.seq.get();
    }
    if (seq) {
      auto valAfter = seq->getAfter(ts);
      auto valBefore = seq->getEqualBefore(ts);
      if (valAfter && valBefore && valAfter.ts > valBefore.ts) {
        dt = min(dt, valAfter.ts - valBefore.ts);
      }
    }
    YogaValue val(rti->reg.get(), arg.type, ts, mem);
    outPtrs.push_back(val.buf);
  }
  if (verbose) {
    logstr += " dt=" + repr(dt) + "\n";
  }

  if (!logstr.empty()) cerr << logstr;

  debugLocals = YogaValue(rti->reg.get(), yogaFuncInfo->debugLocalsType, ts, mem);

  R astonishment = 0.0;
  (*yogaFuncInfo->debugAddr)(
    outPtrs.data(),
    inPtrs.data(), 
    rti->paramValues, &astonishment, dt, debugLocals.buf, mem);

}



EngineRegister::EngineRegister(
  string const &_name,
  vector<string> const &_argTypeNames,
  std::function<bool (YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)> _create,
  bool _liveOnly)
  : name(_name),
    argTypeNames(_argTypeNames),
    create(_create),
    liveOnly(_liveOnly)
{
  if (!registry) registry = new map<string, EngineRegister *>();
  (*registry)[_name] = this;
}

EngineRegister *EngineRegister::forName(string const &name)
{
  if (registry) {
    auto it = registry->find(name);
    if (it != registry->end()) {
      return it->second;
    }
  }
  return nullptr;
}

shared_ptr<Trace> YogaCompilation::startTrace()
{
  if (!runtime) throw runtime_error("No runtime defined");

  auto trace = make_shared<Trace>(shared_from_this());
  trace->createNewDirForScript(runtimeParams.tracePrefix);
  trace->setupConsole();
  return trace;
}

AstAnnoCall * YogaCompilation::findEngineFunction(string const &fn)
{
  for (auto &engineIt : engineDefs) {
    if (engineIt->call->literalFuncName == fn) return engineIt;
  }
  return nullptr;
}


bool setupTimeseqTypes(YogaContext const &ctx)
{
  if (!ctx.reg->runtime) return true;

  for (auto &engineIt : ctx.reg->engineDefs) {
    string engineFn = engineIt->call->literalFuncName;
    string engineName = engineFn;

    auto er = EngineRegister::forName(engineFn);
    if (er) {

      if (engineIt->call->args.size() > er->argTypeNames.size()) {
        return ctx(engineIt).logError("Wrong number of arguments (expected " + 
          repr(er->argTypeNames.size()) + " got " + repr(engineIt->call->args.size()) +"\n");
      }

      for (size_t argi = 0; argi < er->argTypeNames.size(); argi++) {
        if (auto a = engineIt->call->args[argi]) {
          string seqName;
          if (!a->getLiteral(seqName)) {
            return ctx(a).logError("Invalid seqName");
          }

          auto formalType = ctx.getType(er->argTypeNames[argi]);
          if (!formalType) {
            return ctx.logError("unknown type: " + er->argTypeNames[argi]);
          }

          auto &tsSlot = ctx.reg->timeseqTypes[seqName];
          if (!tsSlot) {
            tsSlot = formalType;
            if (0) cerr << "seq "s + repr_type(formalType) + " " + seqName + "\n";
          }
          else if (tsSlot != formalType) {
            return ctx(engineIt).logError("Type mismatch for arg " + repr_arg(argi) +
              ": expected " + repr_type(er->argTypeNames[argi]) +
              " but sequence " + seqName + " is of type " + repr_type(tsSlot));
          }
        }
      }
    }
    else {

      auto &slot = ctx.lookup(engineFn);

      auto astf = slot.astFunction;
      if (!astf) {
        return ctx(engineIt).logError("No function " + shellEscape(engineFn));
      }
      if (engineIt->call->args.size() != astf->args.size()) {
        return ctx(engineIt).logError("Wrong number of arguments (expected " + 
          repr(astf->args.size()) + " got " + repr(engineIt->call->args.size()) +"\n");
      }
      if (slot.compiledFunc && slot.compiledFunc->debugLocalsType) {
        ctx.reg->timeseqTypes["debug." + engineFn] = slot.compiledFunc->debugLocalsType;
      }
      for (auto argi = 0; argi < astf->args.size(); argi++) {
        auto formal = astf->args[argi];
        auto actual = engineIt->call->args[argi];
        auto formalType = ctx.getType(formal->literalType);
        if (!formalType) {
          return ctx(formal).logError("unknown type");
        }

        string seqName;
        if (!actual->getLiteral(seqName)) {
          return ctx(actual).logError("Invalid seqName");
        }

        auto &tsSlot = ctx.reg->timeseqTypes[seqName];
        if (!tsSlot) {
          tsSlot = formalType;
          if (0) cerr << "seq "s + repr_type(formalType) + " " + seqName + "\n";
        }
        else if (tsSlot != formalType) {
          return ctx(actual).logError("Type mismatch for arg " + repr_arg(argi) +
            ": expected " + repr_type(formalType) + 
            " but sequence " + seqName + " is of type " + repr_type(tsSlot));
        }
      }
    }
  }
  return true;
}



bool setupYogaEngines(YogaContext const &ctx, Trace *trace, bool rerunMode)
{
  if (!ctx.reg->runtime) return ctx.logError("No runtime defined");

  {
    R duration = ctx.reg->runtimeParams.duration;
    if (duration != 0.0) {
      if (trace->rti->hardHaltTs != 0.0) {
        trace->rti->hardHaltTs = min(trace->rti->hardHaltTs, duration);
      }
      else {
        trace->rti->hardHaltTs = duration;
      }
    }
  }

  for (auto &engineIt : ctx.reg->engineDefs) {
    string engineFn = engineIt->call->literalFuncName;
    string engineName = engineFn;
    if (!engineIt->options->getValueForKey("name", engineName)) {
      return ctx(engineIt).logError("Invalid name");
    }

    bool disable{false};
    if (!engineIt->options->getValueForKey("disable", disable)) {
      return ctx(engineIt).logError("Invalid disable");
    }

    if (disable) {
      if (0) cerr << "Engine "s + engineName + " disabled\n";
      continue;
    }

    auto er = EngineRegister::forName(engineFn);
    if (er) {
      /*
        A built-in engine, like for network access
      */
      vector<shared_ptr<YogaTimeseq>> seqArgs;
      for (size_t argi = 0; argi < er->argTypeNames.size(); argi++) {
        if (auto a = engineIt->call->args[argi]) {
          string seqName;
          if (!a->getLiteral(seqName)) {
            return ctx(a).logError("Invalid seqName");
          }

          auto formalType = ctx.getType(er->argTypeNames[argi]);
          if (!formalType) {
            return ctx.logError("unknown type: " + er->argTypeNames[argi]);
          }

          auto seq = trace->getOrAddTimeseq(seqName, formalType);

          seqArgs.push_back(seq);
        }
      }
      if (er->liveOnly && rerunMode) {
        // don't create engine, but note we still created the timeseqs above
      }
      else {
        if (!er->create(ctx, trace, engineName, engineIt, seqArgs)) return false;
      }
    }
    else {
      /*
        A Yoga engine
      */
      auto &slot = ctx.lookup(engineFn);

      auto astf = slot.astFunction;
      if (!astf) {
        return ctx(engineIt).logError("No function " + shellEscape(engineFn));
      }
      auto compf = slot.compiledFunc;
      if (!compf) {
        return ctx(engineIt).logError("No function " + shellEscape(engineFn));
      }

      auto engine = make_shared<YogaEngine>();
      engine->verbose = 0;
      engine->yogaFuncName = engineFn;
      trace->addEngine(engineName, engine, true);
      engine->yogaFuncInfo = compf;

      if (!engine->setYogaConfig(ctx, engineIt)) {
        return false;
      }
      if (!engine->setEngineConfig(ctx, engineIt)) {
        return false;
      }

      if (compf->debugLocalsType) {
        engine->debugLocalsSeq = trace->getOrAddTimeseq("debug." + engineName, compf->debugLocalsType);
      }

      if (astf->args.size() != engineIt->call->args.size()) {
        return ctx(engineIt).logError("Expected "s + to_string(astf->args.size()) +
          " args, got " + to_string(engineIt->call->args.size()));
      }

      for (auto argi = 0; argi < astf->args.size(); argi++) {
        auto formal = astf->args[argi];
        auto actual = engineIt->call->args[argi];
        auto formalType = ctx.getType(formal->literalType);

        string seqName;
        if (!actual->getLiteral(seqName)) {
          return ctx(actual).logError("Invalid seqName");
        }

        auto seq = trace->getOrAddTimeseq(seqName, formalType);

        YogaEngineArg a;
        a.formalArgName = formal->literalName;
        a.seqName = seqName;
        a.type = formalType;
        a.seq = seq;
        a.isOptional = formal->optional;

        if (0) cerr << "Add engine "s + engineFn + " arg " + a.formalArgName + " seq " + a.seqName + " size=" + repr(a.seq->sampleCount()) + "\n";

        if (formal->dirInUp) {
          engine->inArgs.push_back(a);
        }
        if (formal->dirOutUp) {
          engine->outArgs.push_back(a);
        }
      }
    }
  }

  for (auto &init : ctx.reg->initDefs) {
    string initFn = init->call->literalFuncName;

    bool disable{false};
    if (!init->options->getValueForKey("disable", disable)) {
      return ctx(init).logError("Invalid disable");
    }
    if (disable) {
      if (0) cerr << "Init "s + initFn + " disabled\n";
      continue;
    }

    YogaCaller funcCaller(ctx.reg, initFn);

    if (funcCaller.f->inArgs.size()) {
      return ctx(init).logError("Only out args allowed in init functions");
    }
    if (funcCaller.f->outArgs.size() != init->call->args.size()) {
      return ctx(init).logError("Function " + initFn + " expects " + repr(funcCaller.f->outArgs.size()) + " args, got " + repr(init->call->args.size()));
    }

    vector<YogaValue> outArgs;

    for (size_t argi = 0; argi < init->call->args.size(); argi++) {
      auto formalType = funcCaller.f->outArgs[argi].first;
      auto actual = init->call->args[argi];
   
      string seqName;
      if (!actual->getLiteral(seqName)) {
        return ctx(actual).logError("Invalid seqName");
      }

      auto seq = trace->getOrAddTimeseq(seqName, formalType);

      auto yv = seq->addNew(0.0);
      if (!yv.isValid()) {
        return ctx(actual).logError("Can't create init value");
      }
      outArgs.push_back(yv);
    }
    R astonishment = 0.0;
    funcCaller(outArgs, {}, astonishment, 0.0, ctx.reg->mem);
    if (0) cerr << "Init " + initFn + " => " + repr(outArgs) + "\n";
  }
  return true;
}

bool YogaEngine::setYogaConfig(YogaContext const &ctx, AstAnnoCall *spec)
{
  advanceDt = 0.0;
  if (!spec->options->getValueForKey("advanceDt", advanceDt)) {
    return ctx.logError("Invalid advanceDt");
  }
  if (!spec->options->getValueForKey("minUpdateDt", minUpdateDt)) {
    return ctx.logError("Invalid minUpdateDt");
  }
  return true;
}
