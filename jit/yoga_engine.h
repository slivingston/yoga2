#pragma once
#include "./compilation.h"
#include "./type.h"
#include "./value.h"

#include "../comm/engine.h"

struct YogaEngineArg {
  string formalArgName;
  string seqName;
  YogaType *type{nullptr};
  shared_ptr<YogaTimeseq> seq;
  bool isOptional{false};
};

struct YogaEngine : GenericEngine {
  YogaEngine();
  ~YogaEngine() override;
  YogaEngine(YogaEngine const &) = delete;
  YogaEngine(YogaEngine &&) = delete;
  YogaEngine &operator=(YogaEngine const &) = delete;
  YogaEngine &operator=(YogaEngine &&) = delete;

  vector<YogaEngineArg> inArgs;
  vector<YogaEngineArg> outArgs;

  shared_ptr<YogaTimeseq> debugLocalsSeq;

  string yogaFuncName;
  YogaCompiledFuncInfo *yogaFuncInfo{nullptr};

  double advanceDt{0.0};
  double minUpdateDt{0.0};

  void update(EngineUpdateContext &ctx) override;
  void debugUpdate(EngineUpdateContext &ctx, R ts, YogaValue &debugLocals, Trace *specTrace, apr_pool_t *mem) override;
  bool setYogaConfig(YogaContext const &ctx, AstAnnoCall *spec);

};


[[nodiscard]] bool setupYogaEngines(YogaContext const &ctx, Trace *trace, bool rerunMode = false);
[[nodiscard]] bool setupTimeseqTypes(YogaContext const &ctx);
