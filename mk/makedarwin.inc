CXX = clang++
CC = clang

CFLAGS += 

INCLUDES += -I /usr/local/include
INCLUDES += -I /usr/local/include/eigen3/.
INCLUDES += -I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/apr-1
INCLUDES += -I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/Headers
INCLUDES += $(IMGUI_INCLUDES)

LDFLAGS += -sectcreate __TEXT __info_plist studio/Info.plist
LDFLAGS += -framework OpenGL -framework CoreFoundation
LDFLAGS += -framework Accelerate

ifneq (,$(wildcard /usr/local/lib/libale.so))
CXXFLAGS += -DUSE_ALE
LDFLAGS += -lale
endif

LLVM_ROOT := /usr/local/opt/llvm
LLVM_CONFIG := $(LLVM_ROOT)/bin/llvm-config
LLVM_VERSION := $(shell $(LLVM_CONFIG) --version)

ifneq ($(LLVM_VERSION),10.0.1)
check.deps ::
	echo "Need llvm 10.0.1, found $(LLVM_VERSION)"
endif

LLVM_CXXFLAGS += $(shell $(LLVM_CONFIG) --cppflags)
CXXFLAGS += $(LLVM_CXXFLAGS)
LLVM_LDFLAGS += $(shell $(LLVM_CONFIG) --ldflags --libs core orcjit native passes --system-libs) -lLLVMBitWriter

STUDIO_LDFLAGS += $(SDL_LDFLAGS) -lpng

ifneq (,$(wildcard /Library/Frameworks/3DconnexionClient.framework))
STUDIO_LDFLAGS += -F /Library/Frameworks -framework 3DconnexionClient
CXXFLAGS += -DUSE_3DX
endif

SDL_LDFLAGS += $(shell sdl2-config --libs)
SDL_CXXFLAGS += $(shell sdl2-config --cflags)
#SDL_LDFLAGS += -lGL

CXXFLAGS += $(SDL_CXXFLAGS)

check.deps :: 
	@file $(shell $(LLVM_CONFIG) --libfiles) || ( echo "Yoga requires llvm-10-dev. make install.deps can install it for you"; false )
	@which $(CC) || ( echo "Yoga requires an executable for $(CC)"; false )
	@which $(CXX) || ( echo "Yoga requires an executable for $(CXX)" ; false )
