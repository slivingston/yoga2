
.PHONY: install.npm install.deps install.tftp install.openni install.realsense install.mujoco setup.dirs

ifeq ($(CI_SERVER), yes)
SUDO =
else
SUDO = sudo
endif


ifeq ($(UNAME_SYSTEM),Linux)

update-alternatives.llvm9 ::
	$(SUDO) update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-10 105
	$(SUDO) update-alternatives --install /usr/bin/clang clang /usr/bin/clang-10 105
	$(SUDO) update-alternatives --install /usr/bin/clangd clangd /usr/bin/clangd-10 105
	$(SUDO) update-alternatives --install /usr/bin/lldb lldb /usr/bin/lldb-10 105

install.deps :: ## Install (using $(SUDO) apt-get install) dependencies on Ubuntu
	$(SUDO) DEBIAN_FRONTEND=noninteractive apt-get -y install \
		curl \
		git \
		make \
		software-properties-common \
		tzdata \
		pkg-config \
		cmake
	$(SUDO) apt-get -y install \
		llvm-10-dev \
		libclang-10-dev \
		clang-10 \
		clang-format-10 \
		clang-tools-10 \
		lldb-10 \
		lld-10 \
		libsqlite3-dev \
		libasound2-dev \
		libsdl2-dev \
		libjpeg-dev \
		libturbojpeg0-dev \
		libpng-dev \
		libz-dev \
		libspnav-dev \
		libapr1-dev \
		libaprutil1-dev \
		libx11-dev \
		libuv1-dev \
		libglm-dev \
		libopenblas-dev \
		liblapack-dev \
		libarpack2-dev \
		libeigen3-dev

install.extra-deps ::
	$(SUDO) apt-get -y install \
		clangd-10 \
		clang-doc-10



ifneq (,$(OPENNI_DIR))
install.openni :: ## Install OpenNI drivers
	cd $(OPENNI_DIR) && $(SUDO) sh install.sh
	$(SUDO) apt-get -y install libusb-1.0.0-dev libudev-dev
	$(SUDO) ln -sf libudev.so.1 /lib/x86_64-linux-gnu/libudev.so.0
endif

install.realsense ::
	# From https://github.com/IntelRealSense/librealsense/blob/development/doc/distribution_linux.md
	$(SUDO) apt-key adv --keyserver keys.gnupg.net --recv-key C8B3A55A6F3EFCDE || $(SUDO) apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key C8B3A55A6F3EFCDE
	$(SUDO) add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo bionic main" -u
	$(SUDO) apt-get update
	$(SUDO) apt-get install librealsense2-dkms librealsense2-utils librealsense2-dev librealsense2-dbg

# See http://askubuntu.com/questions/201505/how-do-i-install-and-run-a-tftp-server
install.tftp :: ## Install (using $(SUDO) apt-get) a tftp server for Ubuntu
	$(SUDO) apt-get -y install tftpd-hpa

install.atmel_stk :: ## Install (using $(SUDO) install) the atmel STK configuration
	$(SUDO) install -c embedded/avr32/z70_avrone.rules /etc/udev/rules.d/z70_avrone.rules

install.ale ::
	mkdir /tmp/ale
	cd /tmp/ale && git clone https://github.com/mgbellemare/Arcade-Learning-Environment.git
	mkdir /tmp/ale/Arcade-Learning-Environment/build
	cd /tmp/ale/Arcade-Learning-Environment/build && cmake -DUSE_RLGLUE=OFF -DBUILD_EXAMPLES=ON ..
	cd /tmp/ale/Arcade-Learning-Environment/build && make -j9
	cd /tmp/ale/Arcade-Learning-Environment/build && make install

endif

ifeq ($(UNAME_SYSTEM),Darwin)

install.deps :: ## Install (using brew install) deps for OSX
	brew install \
		pkg-config \
		jpeg \
		libpng \
		libuv \
		glm \
		eigen \
		git \
		sdl2 \
		llvm \
		sqlite3 \


install.mujoco ::
	install_name_tool -id "@rpath/libmujoco140.dylib" $(MUJOCO_DIR)/bin/libmujoco140.dylib
endif

build : setup.dirs

setup.dirs ::
	@mkdir -p traces data blobs

check.deps ::
