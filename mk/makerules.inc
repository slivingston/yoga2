
#test :: build

.PHONY: default build stage1 clean

clean ::
	rm -rf $(ODIR) build.src bin

.PHONY: test size lint

cross.% : pushsrc.%
	ssh $* 'cd yoga2 && make -j`nproc`'

pushsrc.% :
	@echo rsync . to $*
	@rsync -avR $(PUSHDIST_EXCLUDES) `git ls-files | grep -v .vscode/` $*:yoga2/.

pushbin.% : build
	@echo rsync to $*
	@rsync -avR $(PUSHDIST_EXCLUDES) bin/. $*:yoga2/.


force :


$(ODIR)/%.o : %.cc
	@mkdir -p $(dir $@)
	@echo c++ $<
	@$(CXX) -o $@ -c -MMD -MF $(@:.o=.d) $(CXXFLAGS) $(INCLUDES) $<

$(ODIR)/%.o : %.cpp
	@mkdir -p $(dir $@)
	@echo c++ $<
	@$(CXX) -o $@ -c -MMD -MF $(@:.o=.d) $(CXXFLAGS) $(INCLUDES) $<

$(ODIR)/%.o : %.c
	@mkdir -p $(dir $@)
	@echo cc $<
	@$(CC) -o $@ -c -MMD -MF $(@:.o=.d) $(CFLAGS) $(INCLUDES) $<

$(ODIR)/%.o : %.m
	@mkdir -p $(dir $@)
	@echo cc $<
	@$(CC) -o $@ -c -MMD -MF $(@:.o=.d) $(CFLAGS) $(INCLUDES) $<

MAKEFILES = Makefile mk/makerules.inc mk/makedefs.inc mk/makelinux.inc mk/makedarwin.inc

$(ODIR)/%.cc.compdb_entry: %.cc $(MAKEFILES)
	@mkdir -p $(dir $@)
	@echo "  {\"command\": \"$(CXX) -c $(CXXFLAGS) $(INCLUDES) $(IMGUI_INCLUDES) $(SDL_CXXFLAGS) $<\", \"directory\": \"$(CURDIR)\", \"file\": \"$<\"}," >$@

$(ODIR)/%.c.compdb_entry: %.c $(MAKEFILES)
	@mkdir -p $(dir $@)
	@echo "  {\"command\": \"$(CC) -c $(CFLAGS) $(INCLUDES) $(IMGUI_INCLUDES) $<\", \"directory\": \"$(CURDIR)\", \"file\": \"$<\"}," >$@

$(ODIR)/%.cpp.compdb_entry: %.cpp $(MAKEFILES)
	@mkdir -p $(dir $@)
	@echo "  {\"command\": \"$(CXX) -c $(CXXFLAGS) $(INCLUDES) $(IMGUI_INCLUDES) $(SDL_CXXFLAGS) $<\", \"directory\": \"$(CURDIR)\", \"file\": \"$<\"}," >$@

$(ODIR)/%.h.compdb_entry: %.h $(MAKEFILES)
	@mkdir -p $(dir $@)
	@echo "  {\"command\": \"$(CXX) -c $(CXXFLAGS) $(INCLUDES) $(IMGUI_INCLUDES) $(SDL_CXXFLAGS) $<\", \"directory\": \"$(CURDIR)\", \"file\": \"$<\"}," >$@

COMPDB_ENTRIES = $(patsubst %, $(ODIR)/%.compdb_entry, $(ALL_CXX_SRC) $(CXX_HDRS))

# Sed command means delete the tailing comma on the last line
compile_commands.json: $(COMPDB_ENTRIES)
	@echo "Generate $@"
	@( echo "["; cat /dev/null $^ | sed -e '$$s/,$$//' ; echo "]" )> $@.tmp
	@mv $@.tmp $@


iwyu: $(IWYU_ENTRIES)

IWYU_ENTRIES = $(patsubst %, $(ODIR)/%.iwyu, $(ALL_CXX_SRC))

ifeq ($(UNAME_SYSTEM),Darwin)
IWYU_INCLUDES = \
	-I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/ \
	-I /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/11.0.3
endif

$(ODIR)/%.cc.iwyu: %.cc
	include-what-you-use -c $(IWYU_INCLUDES) $(CXXFLAGS) $(INCLUDES) $<


drivers.dex4 : gensrc
	cd drivers/dex4/ssc_gyro/fw && make -j10 BOARD_INSTANCE=0
	cd drivers/dex4/ssc_leg/fw && make -j10 BOARD_INSTANCE=0
	cd drivers/dex4/ssc_leg/fw && make -j10 BOARD_INSTANCE=1
	cd drivers/dex4/ssc_foot/fw && make -j10 BOARD_INSTANCE=0
	cd drivers/dex4/ssc_foot/fw && make -j10 BOARD_INSTANCE=1
	cd drivers/dex4/ssc_harness/fw && make -j10 BOARD_INSTANCE=0


# After power fail, do this:
#  make dex42.harness.normal
#  make dex42.harness.reset
#   -- take weight off robot
#  make dex42.harness.reset
#  make dex42.leg.open
#   -- make robot legs exactly staight
#  make dex42.leg.reset
#  make dex42.leg.normal

dex42.leg.reset :
	node drivers/ssc/ssctool.js --mode R ssc_leg_0
	node drivers/ssc/ssctool.js --mode R ssc_leg_1

dex42.leg.open :
	node drivers/ssc/ssctool.js --mode O ssc_leg_0
	node drivers/ssc/ssctool.js --mode O ssc_leg_1

dex42.leg.normal :
	node drivers/ssc/ssctool.js --mode 0 ssc_leg_0
	node drivers/ssc/ssctool.js --mode 0 ssc_leg_1

dex42.harness.normal:
	node drivers/ssc/ssctool.js --mode 0 ssc_harness_0

dex42.harness.drop:
	node drivers/ssc/ssctool.js --mode D ssc_harness_0

dex42.harness.reset:
	node drivers/ssc/ssctool.js --mode R ssc_harness_0

dex42.gyro.reboot :
	node drivers/ssc/ssctool.js --reboot ssc_gyro_0



$(ODIR)/db/yoga_layout.o : build.src/path_config.h

build.src/path_config.h : $(MAKEFILES) mk/makedeps.inc
	@echo "Construct $@"
	@mkdir -p $(dir $@)
	@echo "#define YOGA_DIR \"$(YOGA_DIR)\"" > $@
	@echo "#define INSTALL_DIR \"$(INSTALL_DIR)\"" >> $@

gensrc : build.src/dsp_math_ops.h build.src/smoother_coeffs.h build.src/cylVol_dsp.h

build.src/dsp_math_ops.h : dspcore/mk_dspmath.js
	@mkdir -p $(dir $@)
	node dspcore/mk_dspmath.js

build.src/smoother_coeffs.h : dspcore/mk_filters.js
	@mkdir -p $(dir $@)
	node dspcore/mk_filters.js

build.src/cylVol_dsp.h : bin/yogastudio drivers/dex4/cad42/cad42_dsp.yoga drivers/dex4/cad42/cad42.yoga
	bin/yogastudio -o $@ drivers/dex4/cad42/cad42_dsp.yoga

#clang-tidy: compile_commands.json
#run-clang-tidy.py

clang-tidy:
	/usr/local/opt/llvm/bin/clang-tidy 


cppcheck: compile_commands.json
	cppcheck --enable=all --inconclusive --std=c99 --project=$(CURDIR)/compile_commands.json --template=gcc

ifeq ($(UNAME_SYSTEM),Darwin)
compile.dex4.profile: bin/yogastudio force
	@sudo echo test # get pw if needed
	sudo sample yogastudio -w --file $@ &
	bin/yogastudio compile examples/dex4/main.yoga
	@echo "Try filtercalltree $@ -pruneCount 20 | less"
endif

ifeq ($(UNAME_SYSTEM),Darwin)
YogaStudio.app: bin/yogastudio
	rm -rf $@
	mkdir -p $@/Contents/MacOS
	mkdir -p $@/Contents/Resources
	echo "APPL????" > $@/Contents/PkgInfo
	install -c $< $@/Contents/MacOS/YogaStudio
	install -c studio/Info.plist $@/Contents
endif
