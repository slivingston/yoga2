#include "../comm/engine.h"

#include "./livestream_tx_engine.h"
#include "../drivers/video/video_types.h"
#include "common/packetbuf_types.h"

LivestreamTxEngine::LivestreamTxEngine()
{
}


void LivestreamTxEngine::afterSetRunning()
{
  auto ltrace = trace.lock();
  for (auto &it : ltrace->timeseqs) {

    LivestreamSrc src;
    src.seq = it.timeseq;
    src.seqName = src.seq->seqName;
    src.interval = 0.05;

    if (verbose >= 1) console("will tx "s + shellEscape(src.seqName) + " interval=" + repr_0_3f(src.interval));
    srcs.emplace_back(src);
  }

  lastLiveActiveTime = realtime();
  uiPipe->addApiMethod(
    "keepAlive",
    [this, thisp=shared_from_this()](packet &rx, PacketApiCb cb) {
      lastLiveActiveTime = realtime();
      if (verbose >= 2) console("Got keepalive"s);
      cb.ok();
    }
  );

  GenericEngine::afterSetRunning();
}

void LivestreamTxEngine::update(EngineUpdateContext &ctx)
{
  if (ctx.now - lastUpdateTime > 0.020 && outstandingGrows < 3 && schemaSent) {
    lastUpdateTime = ctx.now;

    outstandingGrows++;
    uiPipe->rpc(
      "grow",
      [this](packet &tx) {
        for (size_t si = 0; si < srcs.size(); si++) {
          sourcei = (sourcei + 1) % srcs.size();
          auto &src = srcs[sourcei];

          YogaValue pt;

          pt = src.seq->getLast();
          if (!pt.buf || pt.ts - src.lastGrowTs < src.interval) continue;
          src.lastGrowTs = pt.ts;
          traceEndTs = max(traceEndTs, pt.ts);

          tx.add('P');
          tx.add(src.seqName);
          tx.add(pt.ts);
          src.seq->type->packetWr(tx, pt.buf);
          if (tx.size() > 100000) break;
        }

        if (auto ltrace = trace.lock()) {
          tx.add('L');
          tx.add(ltrace->blobLocationsByChunkId);
        }

        tx.add('.');
      },
      [this](string const &err, packet &rx) {
        outstandingGrows--;
        if (err.size()) {
          console("peer: grow error "s + err);
        }
      }
    );
  }
  GenericEngine::update(ctx);
}
