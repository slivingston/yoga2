
#vertex
uniform mat4 ProjMtx;
uniform mat4 ViewMtx;

in vec3 Position;
in vec2 TexCoord;

out vec2 Frag_Tex;

void main()
{
  Frag_Tex = TexCoord;
  gl_Position = ProjMtx * ViewMtx * vec4(Position, 1);
}


#fragment

in vec2 Frag_Tex;
uniform sampler2D TexUnit;

out vec4 Out_Color;

void main()
{
  Out_Color = texture(TexUnit, Frag_Tex);
}
