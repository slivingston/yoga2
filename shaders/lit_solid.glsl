
#vertex
uniform mat4 ProjMtx;
uniform mat4 ViewMtx;

in vec3 Position;
in vec3 Normal;
in vec4 Color;

out vec3 Frag_Pos;
out vec3 Frag_Normal;
out vec4 Frag_Color;
out vec3 Frag_LightPos;

void main()
{
  Frag_Color = Color;
  Frag_Normal = mat3(ViewMtx) * Normal;
  Frag_Pos = (ViewMtx * vec4(Position, 1)).xyz;
  Frag_LightPos = (ViewMtx * vec4(1, -1, 5, 1)).xyz;
  gl_Position = ProjMtx * ViewMtx * vec4(Position, 1);
}


#fragment

in vec3 Frag_Pos;
in vec3 Frag_Normal;
in vec4 Frag_Color;
in vec3 Frag_LightPos;

out vec4 Out_Color;

void main()
{
  vec3 norm = normalize(Frag_Normal);
  vec3 lightDir = normalize(Frag_LightPos - Frag_Pos);
  vec3 viewDir = normalize(-Frag_Pos);
  vec3 reflectDir = reflect(-lightDir, norm);

  float ambient = 0.3;
  float diffuse = 0.7 * max(dot(norm, lightDir), 0.0);
  vec4 coloradj = vec4(ambient+diffuse, ambient+diffuse, ambient+diffuse, 1);

  vec4 specular = pow(max(dot(viewDir, reflectDir), 0.0), 16) * vec4(0.85, 0.85, 0.90, 0);  

  Out_Color = specular + coloradj * Frag_Color;
}
