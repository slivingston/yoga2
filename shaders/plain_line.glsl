
#vertex
uniform mat4 ProjMtx;
uniform mat4 ViewMtx;

in vec3 Position;
in vec4 Color;

out vec4 Frag_Color;

void main()
{
  Frag_Color = Color;
  gl_Position = ProjMtx * ViewMtx * vec4(Position, 1);
}


#fragment

in vec4 Frag_Color;

out vec4 Out_Color;

void main()
{
  Out_Color = Frag_Color;
}
