#include "./policy_search.h"

#include "common/uv_wrappers.h"
#include "common/asyncjoy.h"


PolicySearchState::Particle::Particle(size_t dim)
  : paramValues(dim),
    velocities(dim),
    evaluated(false),
    evaluation(0.0)
{
}

PolicySearchState::PolicySearchState(YogaContext const &_ctx, PolicyParamSearchSpec const &_spec)
  : ctx(_ctx),
    spec(_spec)
{
  for (auto const &it : spec.participatingParams) {
    activeParams.push_back(it);
  }
  addRandomParticles(spec.populationTarget);
}

void PolicySearchState::delRandomParticles(size_t nDelete)
{
  for (int i = 0; i < nDelete && !particles.empty(); i++) {
    particles.pop_back();
  }
}

void PolicySearchState::addRandomParticles(size_t nAdd)
{
  for (int i = 0; i < nAdd; i++) {
    particles.push_back(mkRandomParticle(R(i) / R(nAdd)));
  }
}


bool PolicySearchState::isInBounds(Particle const &p)
{
  for (size_t i = 0; i < p.paramValues.size(); i++) {
    if (p.paramValues[i] < -2.0 || p.paramValues[i] > 2.0) return false;
  }
  return true;
}

shared_ptr<PolicySearchState::Particle> PolicySearchState::mkRandomParticle(R variance)
{

  size_t dim = activeParams.size();
  auto newp = make_shared<Particle>(dim);

  static uint32_t seed;
  seed ++;
  std::mt19937 gen(seed);
  std::normal_distribution<R> paramDist(0.0, variance * spec.initialParamSigma);
  std::normal_distribution<R> velDist(0.0, variance * spec.initialVelSigma);

  for (size_t i = 0; i < dim; i++) {
    auto param = activeParams[i];
    auto v = param->getNormValue(ctx);
    newp->paramValues[i] = clamp(v + paramDist(gen), -2.0, 2.0);
    newp->velocities[i] = velDist(gen);
  }
  return newp;
}

void PolicySearchState::writeBackBestParams()
{
  auto bestParticle = particles.at(0);
  if (!bestParticle) return;

  for (size_t i = 0; i < activeParams.size(); i++) {
    auto param = activeParams[i];
    param->setNormValue(ctx, bestParticle->paramValues[i]);
  }
}


shared_ptr<AltTraceInfo> PolicySearchState::mkAltTraceForParticle(ScopeModel *m, Particle *newp)
{
  auto ret = m->mkAltTrace();
  ret->durationLimit = spec.rMetric.sampleTime + 0.01;

  for (size_t i = 0; i < newp->paramValues.size(); i++) {
    auto param = activeParams.at(i);
    param->setNormValue(m->ctx, newp->paramValues[i], &ret->overrideParamValues);
  }

  return ret;
}

void PolicySearchState::evalParticles(ScopeModel *m)
{
  auto newps = make_shared<vector<shared_ptr<Particle>>>();

  auto bestParticle = particles[0];
  for (auto &oldp : particles) {
    if (newps->size() >= spec.populationTarget - max(0, 100 - iterCount)/10) break;

    auto newp = make_shared<Particle>(oldp->paramValues.size());

    for (int i=0; i<newp->paramValues.size(); i++) {
      auto newVel = oldp->velocities[i] * spec.velCoeff +
        spec.accelTowardsBest * (bestParticle->paramValues[i] - oldp->paramValues[i]);

      newp->velocities[i] = newVel;
      newp->paramValues[i] = oldp->paramValues[i] + newVel;
    }

    if (isInBounds(*newp)) {
      newps->push_back(newp);
    }
  }
  while (newps->size() < spec.populationTarget) {
    newps->push_back(mkRandomParticle(0.5));
  }
  iterCount ++;

  startEvals(m, newps);

}

void PolicySearchState::startEvals(ScopeModel *m, shared_ptr<vector<shared_ptr<Particle>>> const &newps)
{
  threadsRunning ++;
  auto t0 = clock();
  auto aj = make_shared<AsyncJoy>([newps, t0, this, this1=shared_from_this()](string const &err) {
    threadsRunning --;
    auto t1 = clock();
    if (0) cerr << "Calculated "s + repr(newps->size()) + " particle evals points in " + repr_clock(t1-t0) + "\n";

    sort(newps->begin(), newps->end(), [](shared_ptr<Particle> const &a, shared_ptr<Particle> const &b) {
      return a->evaluation > b->evaluation; // reverse order, highest evaluation first
    });

    particles = *newps;
    uvUiActive = true;
  });

  for (auto &newp : *newps) {

    auto newTraceInfo = mkAltTraceForParticle(m, newp.get());

    aj->start();
    uvWork(
        [newTraceInfo, m1 = m->shared_from_this()](string &error, shared_ptr<void> &result) {
          m1->rerunTrace(newTraceInfo.get());
        },
        [newTraceInfo, newp, aj, this, this1=shared_from_this()](string const &error, shared_ptr<void> const &result) {
          if (!error.empty()) return aj->error(error);
          newp->evaluation = calcMetric(newTraceInfo->trace.get(), spec.rMetric);
          newp->evaluated = true;

          if (0) cerr << "Evaluation " + repr_0_3f(newp->evaluation) + " for " +
            repr(newp->paramValues) + "\n";

          aj->end();
          //if (onProgress) onProgress(aj->fractionDone());
        });
  }
  aj->end();
}

void PolicySearchState::poll(ScopeModel *m)
{
  if (threadsRunning == 0) evalParticles(m);
}
