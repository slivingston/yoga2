#pragma once
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "../jit/runtime.h"

struct Trace;

struct PredictTrainingTrace {

  shared_ptr<Trace> trace;
  R beginTs{0.0}, endTs{0.0};
  vector<function<YogaValue(R t0, R t1)>> inArgGens;
  vector<function<YogaValue(R t0, R t1)>> outArgGens;

  vector<function<YogaValue(R t0, R t1)>> outArgGradInGens;
  vector<function<YogaValue(R t0, R t1)>> inArgGradOutGens;

};

struct PredictTrainingSet {
  PredictTrainingSet(YogaContext const &_ctx)
    :ctx(_ctx)
  {
  }
  YogaContext ctx;
  string error;
  AstAnnoCall *trainDef{nullptr};
  YogaCaller trainCaller;
  YogaValue lossHolder;

  vector<R> improvedParams;
  vector<bool> isParamTrained;
  R predictDur{1.0};
  R learningRate{1e-3};
  R gradNormLimit{50.0};
  u_int sampleSeed{1};
  bool runnable{false};

  vector<PredictTrainingTrace> traces;

  vector<R> lossGraph, gradNormGraph;

};
