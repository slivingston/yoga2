#pragma once
#include "../jit/context.h"
#include "../jit/runtime.h"
#include "../geom/solid_geometry.h"
#include "../geom/panel3d.h"

struct ImDrawList;
struct ImDrawCmd;

struct RenderModel {
  RenderModel(YogaContext const &_ctx, string const &_modelName)
    : ctx(_ctx),
      modelName(_modelName)
  {
  }

  void loadSolids();
  void drawRenderView(const ImDrawList* parent_list, const ImDrawCmd* cmd);
  void addContents();

  static vector<string> getModelNames(YogaContext const &ctx);

  YogaContext ctx;

  string modelName;
  map<string, vector<shared_ptr<StlSolid>>> solids;

  Panel3dDrawList drawList;

  YogaCompiledFuncInfo *renderSolidFunc{nullptr};
  YogaType *renderSolidType{nullptr};
  YogaValueAccessor<glm::mat4> renderSolidPos;
  YogaValueAccessor<char *> renderSolidName;
  YogaCaller renderSolidCaller;

};
