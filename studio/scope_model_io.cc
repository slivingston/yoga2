#include "./scope_model.h"
#include "common/uv_wrappers.h"
#include "../lib/scope_math.h"
#include "../db/yoga_db.h"
#include "../timeseq/trace.h"
#include "../jit/type.h"
#include "../jit/compilation.h"
#include "../jit/runtime.h"
#include "../jit/yoga_engine.h"
#include "nlohmann-json/json.hpp"
#include "./policy_search.h"
using json = nlohmann::json;


void ScopeModel::ensureTraceLocal(std::function<void(bool)> const &cb)
{
  YogaDb::instance().ensureTraceLocal(traceName,
    [this, cb](string const &err)
    {
      if (!err.empty()) {
        setError(err);
        return cb(false);
      }
      else {
        return cb(true);
      }
    },
    [this](string const &status)
    {
      setLoadStatus(status);
      uvUiActive = true;
    });
}

bool ScopeModel::loadFromDb(string const &name, std::function<void(string const &error)> cb)
{
  traceName = canonTraceName(name);

  YogaDb::instance().loadTrace(traceName, ctx.reg->shared_from_this(),
    [this, cb, thisp=shared_from_this()](string const &err, shared_ptr<Trace> const &newTrace, json const &manifest)
    {
      if (!err.empty()) {
        cerr << "ScopeModel: Loading "s + traceName + ": " + err + "\n";
        setError(err);
        setTrace(newTrace);
        return cb(err);
      }
      else {
        if (0) cerr << "ScopeModel: Loaded "s + traceName + "\n";
        setLoadStatus("");
        assert(newTrace->engines.empty());
        setTrace(newTrace);
        if (0) {
          if (!setupYogaEngines(ctx, newTrace.get(), true)) {
            return cb("ScopeModel: setupYogaEngines failed");
          }
        }
        setTraceManifest(manifest);
        return cb("");
      }
    },
    [this](string const &status)
    {
      setLoadStatus(status);
    });
  return true;
}

bool ScopeModel::reloadFromTrace(string const &name)
{
  visTraces.clear();
  visPanels.clear();
  isLiveRobot = false;
  loadFromDb(name, [this, thisp=shared_from_this()](string const &err) {
    uvUiActive = true;
    liveClient = nullptr;
  });
  return true;
}


static string withNewSuffix2(string const &s, string const &oldSuffix, string const &newSuffix)
{
  if (s.size() >= oldSuffix.size() && s.compare(s.size() - oldSuffix.size(), oldSuffix.size(), oldSuffix, 0, oldSuffix.size()) == 0) {
    return s.substr(0, s.size() - oldSuffix.size()) + newSuffix;
  }
  return "";
}

void ScopeModel::restoreVisInfo()
{
  string saveKey;
  if (!traceName.empty()) {
    saveKey = traceName;
  }
  else {
    saveKey = ctx.reg->sources->mainFn;
  }

  if (!saveKey.empty()) {
    YogaDbVisInfoRow visInfoRow;
    if (YogaDb::instance().getVisInfo(visInfoRow, saveKey)) {
      if (visInfoRow.visible.size() >= 2) {
        try {
          setVisInfo(visInfoRow.visible);
        }
        catch (json::exception &e) {
          cerr << "Exception restoring visInfo: "s + e.what() + "\n";
          cerr << "visible = \n" + indentString(visInfoRow.visible, "  ") + "\n";
        }
        if (visTraces.size() >= 1) goto done;
      }
    }
  }
  {
    YogaDbLastTraceRow ltbsRow;
    if (YogaDb::instance().getLastTraceByScript(ltbsRow, ctx.reg->sources->mainFn)) {

      YogaDbVisInfoRow visInfoRow;
      if (YogaDb::instance().getVisInfo(visInfoRow, ltbsRow.name)) {
        if (visInfoRow.visible.size() >= 2) {
          try {
            setVisInfo(visInfoRow.visible);
          }
          catch (json::exception &e) {
            cerr << "Exception restoring visInfo: "s + e.what() + "\n";
            cerr << "visible = \n" + indentString(visInfoRow.visible, "  ") + "\n";
          }
          if (visTraces.size() >= 1) goto done;
        }
      }
    }
  }
  {
    auto jsonFn = withNewSuffix2(ctx.reg->sources->mainFn, ".yoga", ".vis.json");
    if (!jsonFn.empty() && !access(jsonFn.c_str(), R_OK)) {
      auto visJson = readFile(jsonFn);
      setVisInfo(visJson);
      goto done;
    }
  }

done:
  updateVisTraces();
  updateVisPanels();
}

void ScopeModel::saveVisInfo()
{
  // WRITEME: rate limit
  string saveKey;
  if (!traceName.empty()) {
    saveKey = traceName;
  }
  else {
    saveKey = ctx.reg->sources->mainFn;
  }
  if (!saveKey.empty()) {
    YogaDbVisInfoRow visInfoRow(saveKey, getVisInfo());
    YogaDb::instance().addVisInfoByName(visInfoRow);
    YogaDbLastTraceRow ltbsRow(ctx.reg->sources->mainFn, saveKey);
    YogaDb::instance().addLastTraceByScript(ltbsRow);
  }
}



string ScopeModel::getVisInfo()
{
  json info;
  for (auto &visTrace : visTraces) {
    if (visTrace->removed) continue;
    info["visTraces"][visTrace->spec.uniqueKey] = {
      {"fullName", visTrace->spec.ref.fullName},
      {"heightTarget", visTrace->visHeightTarget},
      {"scale", visTrace->scale},
    };
  }
  for (auto &visPanel : visPanels) {
    if (!visPanel->spec.refs.empty()) {
      info["visPanels"][visPanel->spec.uniqueKey] = {
        {"fullName", visPanel->spec.refs[0].fullName},
        {"panelStyle", visPanel->spec.style}
      };
    }
  }
  info["policyTerrain"] = desiredPolicyTerrainSpec.toJson();
  info["policySquiggle"] = desiredPolicySquiggleSpec.toJson();
  info["visDur"] = visDur;
  if (visDeets) {
    info["visDeets"] = *visDeets;
  }
  info["visSourceFile"] = visSourceFile;

  info["desiredPolicySearchSpec"] = desiredPolicySearchSpec.toJson();

  info["desiredPredictDef"] = desiredPredictDef ? desiredPredictDef->gloss() : "";

  auto ret = info.dump(2);

  if (0) cerr << "saveVisInfo:" + ret + "\n";
  return ret;

}

void ScopeModel::setVisInfo(string const &visInfoJson)
{
  if (0) cerr << "visInfo:" + visInfoJson + "\n";
  auto info = json::parse(visInfoJson);

  auto availLin = getAvailTreeLin();

  if (info.contains("visTraces")) {
    visTraces.clear();
    auto visTracesInfo = info["visTraces"];
    for (auto &availIt : availLin) {
      auto &key = availIt->traceSpec.uniqueKey;
      if (visTracesInfo.contains(key)) {
        visTraces.push_back(mkVisTrace(availIt->traceSpec, visTracesInfo[key]));
        visTracesChecked[key] = true;
      }
    }
  }
  if (info.contains("policyTerrain")) {
    desiredPolicyTerrainSpec = PolicyTerrainSpec::fromJson(ctx, info["policyTerrain"]);
    desiredPolicyTerrainSpec.policyTerrainEpoch ++;
  }
  if (info.contains("policySquiggle")) {
    desiredPolicySquiggleSpec = PolicySquiggleSpec::fromJson(ctx, info["policySquiggle"]);
    desiredPolicySquiggleSpec.policySquiggleEpoch ++;
  }
  if (info.contains("visPanels")) {
    visPanels.clear();

    auto visPanelsInfo = info["visPanels"];

    for (auto &availIt : availLin) {
      for (auto &spec : availIt->panelSpecs) {

        if (visPanelsInfo.contains(spec.uniqueKey)) {
          visPanels.push_back(mkVisPanel(spec, visPanelsInfo[spec.uniqueKey]));

          visPanelsChecked[spec.uniqueKey] = true;
        }
      }
    }
  }
  if (!scrollCursor && info.contains("visDur")) {
    visDurTarget = visDur = max(1.0, min(traceEndTs - traceBeginTs, info.value("visDur", 1.0)));
  }
  
  visDeets = make_shared<json>(info.value("visDeets", json::object()));

  visSourceFile = info.value("visSourceFile", ctx.reg->sources->mainFn);

  if (info.contains("desiredPolicySearchSpec")) {
    desiredPolicySearchSpec = PolicyParamSearchSpec::fromJson(ctx, info["desiredPolicySearchSpec"]);
  }

  if (info.contains("desiredPredictDef")) {
    auto gloss = info.value("desiredPredictDef", "");
    for (auto pd : ctx.reg->predictDefs) {
      if (pd->gloss() == gloss) {
        desiredPredictDef = pd;
      }
    }
  }

}
