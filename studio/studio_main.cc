#include "common/std_headers.h"
#include "./main_window.h"
#include "imgui.h"
#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"
#include "./yogastudio_ui.h"
#include "../db/yoga_layout.h"
#include "../jit/compilation.h"
#include "../jit/yoga_engine.h"
#include "../timeseq/trace.h"
#include "../remote/client.h"
#include "../jit/polyfit.h"
#include "common/uv_wrappers.h"
#include <getopt.h>
#include <SDL.h>
#include <thread>
#include "nlohmann-json/json.hpp"
extern void panel3dInit();

void usage()
{
  cerr << R"(usage: yogastudio [-v] [-C dir] main.yoga gui-mode...
         gui-mode can be:
           traces tracename
           scope {latest|tracename}
           live options?
           sim options?
           test testname
       yogastudio compile file.yoga ...
       yogastudio main.yoga rerun tracename ...
       yogastudio main.yoga csv tracename ...
       yogastudio main.yoga run
       yogastudio main.yoga showAccessors typename
       yogastudio setup-db
       yogastudio scan-traces-dir
       yogastudio -o outfile polyfit file.yoga ...
     Global options:
       --ignore-user-config: don't read ~/.yoga/config.json      
)";
}



bool runScript(YogaContext const &ctx)
{
  auto trace = ctx.reg->startTrace();

  if (!setupYogaEngines(ctx("setupYogaEngines"), trace.get())) {
    cerr << "Runtime failure:\n" << ctx.reg->diagLog.str() << "\n";
    return false;
  }

  if (trace->isAnyExtIO()) {
    trace->startUpdateThread();

    trace->onHalts.push_back([trace]() {
      trace->save([trace](string const &error, size_t totSize) {
        uvAssertMainThread();
        if (error.size()) {
          cerr << "saving trace: "s + error + "\n";
        }
        else {
          cerr << "saved trace: "s + trace->rti->traceName + "\n";
          YogaDb::instance().addTrace(trace.get());
        }
      });
    });
  }
  else {
    trace->rerunUpdate();
    trace->save([trace](string const &error, size_t totSize) {
      uvAssertMainThread();
      if (error.size()) {
        cerr << "saving trace: "s + error + "\n";
      }
      else {
        cerr << "saved trace: "s + trace->rti->traceName + "\n";
        YogaDb::instance().addTrace(trace.get());
      }
    });
  }
  uvRunMainThread();
  return true;
}

bool showAccessors(YogaContext const &ctx, string const &typeName)
{
  auto t = ctx.getType(typeName);
  if (!t) {
    return ctx.logError("No type named " + typeName);
  }
  vector<string> lines;
  for (auto &[name, ptrf] : t->accessorsByName) {
    lines.push_back(name + (ptrf ? "" : " (null)"));
  }
  sort(lines.begin(), lines.end());
  for (auto &l : lines) {
    cerr << l + "\n";
  }
  return true;
}

struct StudioOptions {
  int startUi = true;
  int tweakForPodcast = 0;
  int verbose{0};
  int noEmitDebugVersions{0};
  int noEmitGradVersions{0};
  string outFn;
};

int studioMain(NavPath &args, StudioOptions &options)
{
  auto &layout = YogaLayout::instance();

  if (args[0] == "setup-db") {
    unlink((layout.dataDir + "/traces.sqlite").c_str());
    if (!YogaDb::instance().setupTables()) return 1;
    return 0;
  }

  if (args[0] == "rescan-traces") {
    if (!YogaDb::instance().rebuildTracesTable()) return 1;
    return 0;
  }

  if (args[0] == "compile") {
    for (int argi=1; !args[argi].empty(); argi++) {
      auto reg = make_shared<YogaCompilation>();
      reg->verbose = options.verbose;
      reg->emitDebugVersions = !options.noEmitDebugVersions;
      reg->emitGradVersions = !options.noEmitGradVersions;
      auto fn = args[argi];
      auto t0 = clock();
      if (!reg->compileMain(fn)) {
        return 1;
      }
      auto t1 = clock();
      cerr << "Compiled "s + shellEscape(fn) + " in " + repr_clock(t1-t0) + "\n";
    }
    return 0;
  }


  auto reg = make_shared<YogaCompilation>();
  reg->verbose = options.verbose;

  if (args[0] == "polyfit") {
    YogaContext ctx = reg->mkCtx("polyfit");
    if (options.outFn.empty()) {
      cerr << "Must specify output file with -o\n";
      return 1;
    }
    ofstream outFile(options.outFn);

    for (int argi=1; args[argi] != ""; argi++) {
      auto fn = args[argi];
      if (!ctx.reg->compileMain(fn)) {
        return 1;
      }
    }

    if (!ctx.reg->polyfitDefs.empty()) {
      for (auto pf : ctx.reg->polyfitDefs) {
        if (!createPolyfit(ctx(pf), pf, outFile)) return 1;
      }
    }
    return 0;
  }

  if (args[0].empty()) {
    usage();
    return 2;
  }
  auto argMainFn = myRealpath(args.pop_front());

  if (!reg->compileMain(argMainFn)) {
    return 1;
  }
  reg->summarizeProgram(cerr);

  if (args[0] == "rerun") {
    YogaContext ctx = reg->mkCtx("rerun");
    for (int argi=1; args[argi] != ""; argi++) {
      auto traceName = args[argi];
      auto m = make_shared<ScopeModel>(ctx);
      m->loadFromDb(traceName, [m](string const &err) {
        uvAssertMainThread();
        int iters = 0;
        auto t0 = clock();
        while (iters < 5 || clock() - t0 < 2000000) {
          iters++;

          auto newTraceInfo = make_shared<AltTraceInfo>();
          newTraceInfo->baseTrace = m->trace;
          newTraceInfo->paramValueEpoch = 1;

          m->rerunTrace(newTraceInfo.get());
        }
        auto t1 = clock();
        if (1) {
          cerr << "Reran "s + repr_time(m->traceEndTs) + " "
          " of trace " + repr(iters) + " times," +
          " avg " + repr_clock((t1-t0)/iters) + " " +
          " speedup=" + repr_0_3f(m->traceEndTs / (R(t1-t0)/R(1000000.0*iters)))
          + "\n";
        }
      });
    }
    uvRunMainThread();
    return 0;
  }

  if (args[0] == "csv") {
    YogaContext ctx = reg->mkCtx("csv");
    if (args[3].empty()) {
      throw runtime_error("Syntax: csv seqName,... dt,... traceName ...");
    }
    if (options.outFn.empty()) {
      cerr << "Must specify output file with -o\n";
      return 1;
    }

    FILE *f = fopen(options.outFn.c_str(), "w");
    if (!f) throw runtime_error("Can't open "s + options.outFn);
    bool doHeader = true;

    auto seqNames = splitChar(args[1], ',');
    if (!seqNames.size()) throw runtime_error("No seqNames");
    auto dtsStr = splitChar(args[2], ',');
    if (!dtsStr.size()) throw runtime_error("No dts");
    vector<R> dts;
    for (auto &dtStr : dtsStr) { dts.push_back(strtod(dtStr.c_str(), nullptr)); }

    for (int argi=3; args[argi] != ""; argi++) {
      auto traceName = args[argi];
      auto m = make_shared<ScopeModel>(ctx);
      m->loadFromDb(traceName, [m, f, seqNames, dts, &doHeader](string const &err) {
        uvAssertMainThread();
        m->trace->exportCsv(f, seqNames, dts, doHeader);
        doHeader = false;
      });
    }
    uvRunMainThread();
    fclose(f);
    return 0;
  }

  if (args[0] == "run") {
    YogaContext ctx = reg->mkCtx("run");
    return runScript(ctx) ? 0 : 1;
  }

  if (args[0] == "showAccessors") {
    YogaContext ctx = reg->mkCtx("showAccessors");
    return showAccessors(ctx, args[1]) ? 0 : 1;
  }

  if (options.startUi) {
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER | SDL_INIT_AUDIO) != 0) {
      cerr << "Error: "s + SDL_GetError() + "\n";
      return 1;
    }
    signal(SIGPIPE, SIG_IGN);

    showDebugUi = layout.config->value("showDebugUi", false);
    
    mainWindow0 = make_shared<MainWindow>(std::move(reg)); // takes ownership of shared_ptr
    mainWindow0->tweakForPodcast = options.tweakForPodcast;
    mainWindow0->setup();
    mainWindow0->setupAudio();
    panel3dInit();
    mainWindow0->mainui->path = args;

    uvRunMainThread();

    mainWindow0->teardown();

    ImGui_ImplSDL2_Shutdown();
    SDL_Quit();
  }

  return 0;
}




int main(int argc, char * const *argv)
{
  apr_initialize();
  atexit(apr_terminate);

  StudioOptions options;

  static struct option longopts[] = {
    {"ignore-user-config", no_argument, &YogaLayout::ignoreUserConfig, 1},
    {"podcast", no_argument, &options.tweakForPodcast, 1},
    {"no-emit-debug-versions", no_argument, &options.noEmitDebugVersions, 1},
    {"no-emit-grad-versions", no_argument, &options.noEmitGradVersions, 1},
    {nullptr, 0, nullptr, 0}
  };

  int ch;
  while ((ch = getopt_long(argc, argv, "C:vo:", longopts, nullptr)) != -1) {
    switch (ch) {
      case 'C':
        if (chdir(optarg) < 0) {
          cerr << string(optarg) + ": "s + strerror(errno);
          return 1;
        }
        break;

      case 'o':
        options.outFn = optarg;
        options.startUi = false;
        break;

      case 'v':
        options.verbose ++;
        break;

      case 0:
        break;

      default:
        usage();
        return 2;
    }
  }

  argc -= optind;
  argv += optind;

  NavPath args;
  for (int i=0; i < argc; i++) args.path.push_back(argv[i]);

  int mainRet = studioMain(args, options);

  return mainRet;
}

