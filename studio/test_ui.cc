#include "./yogastudio_ui.h"


void YogaStudioView::drawTestPolyline()
{
  drawHamburgerMenu();

  float x=0, y=50;
  if (1) {
    vector<ImVec2> path;
    path.emplace_back(x+0, y+100);
    path.emplace_back(x+98, y+100);
    path.emplace_back(x+99, y+110);
    path.emplace_back(x+100, y+50);
    path.emplace_back(x+110, y+100);
    path.emplace_back(x+120, y+100);
    path.emplace_back(x+200, y+100);
    GetWindowDrawList()->AddPolyline(&path[0], path.size(), IM_COL32(0xff, 0x5b, 0xfc, 0xff), false, 2.0f);

    y += 200;
  }
  if (1) {
    vector<ImVec2> path;
    path.emplace_back(x+0, y+100);
    path.emplace_back(x+98, y+100);
    path.emplace_back(x+99, y+110);
    path.emplace_back(x+99, y+110);
    path.emplace_back(x+100, y+50);
    path.emplace_back(x+100, y+50);
    path.emplace_back(x+110, y+100);
    path.emplace_back(x+120, y+100);
    path.emplace_back(x+200, y+100);
    GetWindowDrawList()->AddPolyline(&path[0], path.size(), IM_COL32(0xff, 0x5b, 0xfc, 0xff), false, 2.0f);

    y += 200;
  }
  if (1) {
    PolylineBuf path;
    path(x+0, y+100);
    path(x+98, y+100);
    path(x+99, y+110);
    path(x+100, y+50);
    path(x+110, y+100);
    path(x+120, y+100);
    path(x+200, y+100);
    path.draw(IM_COL32(0xff, 0x5b, 0xfc, 0xff), false, 2.0f);
    y += 200;
  }
}

void YogaStudioView::drawTestRenderView()
{
  auto m = ScopeModel::getSavedScopeModel(ctx, "test");
  if (!m) return draw404();
  initScopeModelDo(m);
  drawHamburgerMenu();
  auto modelName = path[2];
  auto &rm = getRenderModel(modelName);
  if (BeginChild("renderView", ImVec2(-1, GetContentRegionAvail().y * 0.625f - 50.0f))) {
    drawRenderView(rm);
  }
  EndChild();
  drawVisSource(*m);
}

void YogaStudioView::drawTestCode()
  {
  auto m = ScopeModel::getSavedScopeModel(ctx, "test");
  if (!m) return draw404();
  initScopeModelDo(m);
  drawHamburgerMenu();
  drawVisSource(*m);
}
