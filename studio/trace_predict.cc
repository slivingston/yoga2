#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "common/uv_wrappers.h"
#include "../comm/engine.h"
#include "./scope_model.h"
#include "../jit/runtime.h"
#include "../jit/context.h"
#include "../jit/compilation.h"
#include "../jit/yoga_engine.h"
#include "../timeseq/timeseq_pipe.h"
#include "./predict_train.h"
#include "numerical/haltonseq.h"

void ScopeModel::updatePredictTraces()
{
  if (!trace) return;
  if (traceLoadActive || traceSimActive) return; // wait until done
  if (predictThreadActive) return;
  if (liveClient || liveSim) return;

  if (!desiredPredictDef) {
    if (predictJob) {
      predictJob = nullptr;
    }
    return;
  }

  if (!predictJob ||
      predictJob->paramValueEpoch != ctx.reg->paramValueEpoch ||
      predictJob->predictDef != desiredPredictDef ||
      predictJob->predictStartTs != visTime) {

    predictThreadActive = true;

    auto job = mkPredictJob(desiredPredictDef);

    auto t0 = clock();
    uvWork(
        [job](string &error, shared_ptr<void> &result) {
          rerunPredictJob(job.get());
        },
        [this, t0, job, this1=shared_from_this()](string const &error, shared_ptr<void> const &result) {
          uvUiActive = true;
          predictThreadActive = false;
          if (!job->error.empty()) {
            cerr << "prediction error: " + job->error + "\n";
            desiredPredictDef = nullptr;
            predictJob = nullptr;
            return;            
          }
          predictJob = job;
          auto t1 = clock();
          if (0) cerr << "Reran "s + repr_0_3f(traceEndTs) + " S of trace in " + to_string(t1-t0) + " uS\n";
        });
  }
}


shared_ptr<PredictJob> ScopeModel::mkPredictJob(AstAnnoCall *pd)
{
  auto ret = make_shared<PredictJob>();
  ret->baseTrace = trace;
  ret->paramValueEpoch = ctx.reg->paramValueEpoch;
  ret->overrideParamValues = ctx.reg->paramValues; // copy, may get munged later
  ret->predictDef = pd;
  ret->predictStartTs = visTime;
  ret->predictDur = 1.0;
  if (!pd->options->getValueForKey("duration", ret->predictDur, false)) {
    ctx(pd->options).logError("Invalid duration");
    return nullptr;
  }
  return ret;
}

static bool forEachArg(YogaContext const &ctx, AstCall *call,
  function<bool(YogaContext const &ctx, AstExpr *actual, AstArgDecl *formal)> cb)
{

  auto &funcSlot = ctx.reg->symbolTable[call->literalFuncName];
  if (!funcSlot.compiledFunc || !funcSlot.astFunction) {
    return ctx.logError("No function named " + call->literalFuncName);
  }
  auto func = funcSlot.astFunction;

  if (func->args.size() != call->args.size()) {
    return ctx(call).logError("Expected " + repr(func->args.size()) + " args, got " +
      repr(call->args.size()));
  }

  for (int argi = 0; argi < func->args.size(); argi++) {
    auto formal = func->args[argi];
    auto actual = call->args[argi];
    if (!cb(ctx(actual), actual, formal)) return false;
  }
  return true;
}

static YogaTimeseq *findSeq(YogaContext const &ctx, Trace *trace, AstExpr *actual)
{
  string seqName;
  if (actual->getLiteral(seqName)) {
    if (auto seq = trace->getTimeseq(seqName).get()) {
      return seq;
    }
  }
  return nullptr;
}

static bool matchLiteral(YogaContext const &ctx, AstExpr *actual, string const &expect)
{
  string name;
  if (actual->getLiteral(name)) {
    if (name == expect) return true;
  }
  return false;
}

static bool consumeIndex(YogaContext const &ctx, AstExpr *&actual, string const &expect)
{
  if (auto ai = actual->asIndex()) {
    if (ai->args.size() == 1) {
      string indexLiteral;
      if (!ai->args[0]->getLiteral(indexLiteral)) return ctx(actual).logError("Index must be a literal name");
      if (indexLiteral == expect) {
        actual = ai->lhs;
        return true;
      }
    }
  }
  return false;
}

void ScopeModel::rerunPredictJob(PredictJob *job)
{  
  auto ctx = job->baseTrace->rti->reg->mkCtx("runPredictJob");

  auto newTrace = make_shared<Trace>(ctx.reg->shared_from_this());
  if (!job->overrideParamValues.empty()) {
    newTrace->rti->paramValues = job->overrideParamValues.data();
  }
  job->trace = newTrace;

  YogaCaller caller(ctx.reg, job->predictDef->call->literalFuncName);

  // FIXME: needs several different arg sources:
  // seq, seq[t], t
  vector<function<YogaValue(R t0, R t1)>> inArgGens;
  vector<function<YogaValue(R t0, R t1)>> outArgGens;

  YogaPool tmpmem; // THINK: can anything stored in outVals survive this?

  if (!forEachArg(ctx, job->predictDef->call,
    [job, &inArgGens, &outArgGens, &tmpmem](YogaContext const &ctx, AstExpr *actual, AstArgDecl *formal)
  {
    bool hasSubt = consumeIndex(ctx, actual, "t");

    if (!hasSubt && matchLiteral(ctx, actual, "t") && formal->dirInOnly) {
      YogaValue tHolder(ctx.reg, ctx.reg->rType, tmpmem.it);
      inArgGens.push_back([tHolder](R t0, R t1) {
        *(R *)(tHolder.buf) = t1 - t0;
        return tHolder;
      });
      return true;
    }

    else if (hasSubt && matchLiteral(ctx, actual, "ui.confidence") && formal->dirOutOnly) {
      auto newSeq = job->trace->getOrAddTimeseq("ui.confidence", ctx.reg->rType).get();
      outArgGens.push_back([newSeq](R t0, R t1) {
        return newSeq->addNew(t1);
      });
      return true;
    }

    else if (auto seq = findSeq(ctx, job->baseTrace.get(), actual)) {
      if (formal->dirInUp) {
        if (hasSubt) {
          inArgGens.push_back([seq](R t0, R t1) {
            return seq->getEqualBefore(t1);
          });
        }
        else {
          inArgGens.push_back([seq](R t0, R t1) {
            return seq->getEqualBefore(t0);
          });
        }
      }
      if (formal->dirOutUp) {
        auto newSeq = job->trace->getOrAddTimeseq(seq->seqName, seq->type).get();
        if (hasSubt) {
          outArgGens.push_back([newSeq](R t0, R t1) {
            return newSeq->addNew(t1);
          });
        }
        else {
          outArgGens.push_back([newSeq](R t0, R t1) {
            return newSeq->addNew(t0);
          });
        }
      }
      return true;
    }
    return ctx(actual).logError("Unknown arg");
  })) {
    job->error = "Couldn't call predict function (see console)";
    return;
  }

  for (R dt = 0.0; dt < job->predictDur; dt += 0.01) {
    R t0 = job->predictStartTs;
    R t1 = t0 + dt;
    vector<YogaValue> inVals;
    for (auto &f : inArgGens) {
      inVals.push_back(f(t0, t1));
    }
    if (0) cerr << "predict inVals=" + repr(inVals) + "\n";

    vector<YogaValue> outVals;
    for (auto &f : outArgGens) {
      outVals.push_back(f(t0, t1));
    }
    R astonishment = 0.0;
    caller(outVals, inVals, astonishment, dt, tmpmem.it);
  }

}




void ScopeModel::updateTraining()
{
  if (!trace) return;
  if (traceLoadActive || traceSimActive) return; // wait until done
  if (liveClient || liveSim) return;
  if (trainThreadActive) return;

  if (desiredTrainDef && (!trainJob || trainJob->trainDef != desiredTrainDef || !trainJob->runnable)) {
    trainJob = mkPredictTrainingSet(desiredTrainDef);
    trainJob->runnable = true;
    visPanelsNeedsUpdate = true;
  }
  if (!desiredTrainDef && trainJob && trainJob->runnable) {
    trainJob->runnable = false;
  }
  if (trainJob && trainJob->runnable) {
    trainThreadActive = true;

    auto t0 = clock();
    uvWork(
        [pts = trainJob](string &error, shared_ptr<void> &result) {
          runPredictTrainingSet(pts.get());
        },
        [this, t0, pts=trainJob, this1=shared_from_this()](string const &error, shared_ptr<void> const &result) {
          uvUiActive = true;
          trainThreadActive = false;
          if (!pts->error.empty()) {
            cerr << "prediction error: " + pts->error + "\n";
            desiredTrainDef = nullptr;
            return;            
          }
          if (pts == trainJob) {
            for (size_t i = 0; i < ctx.reg->paramValues.size(); i++) {
              if (pts->isParamTrained[i]) {
                ctx.reg->paramValues[i] = pts->improvedParams[i];
              }
              else {
                pts->improvedParams[i] = ctx.reg->paramValues[i];
              }
            }
            fill(pts->isParamTrained.begin(), pts->isParamTrained.end(), false);
            ctx.reg->paramValueEpoch ++;
          }
          auto t1 = clock();
          if (0) cerr << "Reran "s + repr_0_3f(traceEndTs) + " S of trace in " + to_string(t1-t0) + " uS\n";
        });
  }
}



shared_ptr<PredictTrainingSet> ScopeModel::mkPredictTrainingSet(AstAnnoCall *trainDef)
{
  auto pts = make_shared<PredictTrainingSet>(ctx);
  pts->trainDef = trainDef;
  pts->improvedParams = ctx.reg->paramValues;
  pts->isParamTrained.resize(pts->improvedParams.size());
  pts->trainCaller = YogaCaller(ctx.reg, trainDef->call->literalFuncName);
  if (!trainDef->options->getValueForKey("duration", pts->predictDur, false)) {
    return ctx(trainDef->options).logErrorNull("Invalid duration");
  }
  if (!trainDef->options->getValueForKey("learningRate", pts->learningRate, false)) {
    return ctx(trainDef->options).logErrorNull("Invalid learningRate");
  }
  if (!trainDef->options->getValueForKey("gradNormLimit", pts->gradNormLimit, false)) {
    return ctx(trainDef->options).logErrorNull("Invalid gradNormLimit");
  }


  vector<shared_ptr<Trace>> traces {trace}; // FIXME: support multiple
  for (auto &tr1 : traces) {
    auto endTs = tr1->getEndTs();
    if (endTs < pts->predictDur) continue;

    pts->traces.push_back(PredictTrainingTrace());
    auto &ptt = pts->traces.back();
    ptt.trace = tr1;
    ptt.beginTs = 0.0;
    ptt.endTs = tr1->getEndTs();

    if (!forEachArg(ctx, trainDef->call,
      [&pts, &ptt](YogaContext const &ctx, AstExpr *actual, AstArgDecl *formal)
    {
      bool hasSubt = consumeIndex(ctx, actual, "t");

      if (!hasSubt && matchLiteral(ctx, actual, "t") && formal->dirInOnly) {
        YogaValue tHolder(ctx.reg, ctx.reg->rType, ctx.reg->mem);
        ptt.inArgGens.push_back([tHolder](R t0, R t1) {
          *(R *)(tHolder.buf) = t1 - t0;
          return tHolder;
        });
        YogaValue dummy(ctx.reg, ctx.reg->rType, ctx.reg->mem);
        ptt.inArgGradOutGens.push_back([dummy](R t0, R t1) {
          return dummy;
        });
        return true;
      }

      else if (!hasSubt && matchLiteral(ctx, actual, "loss") && formal->dirOutOnly) {
        pts->lossHolder = YogaValue(ctx.reg, ctx.reg->rType, ctx.reg->mem);
        YogaValue lossGradHolder(ctx.reg, ctx.reg->rType, ctx.reg->mem);
        ptt.outArgGens.push_back([lossHolder = pts->lossHolder](R t0, R t1) {
          *(R *)(lossHolder.buf) = 0.0;
          return lossHolder;
        });
        ptt.outArgGradInGens.push_back([lossGradHolder](R t0, R t1) {
          *(R *)(lossGradHolder.buf) = -1.0;
          return lossGradHolder;
        });
        return true;
      }

      else if (auto seq = findSeq(ctx, ptt.trace.get(), actual)) {
        if (formal->dirInUp) {
          if (hasSubt) {
            ptt.inArgGens.push_back([seq](R t0, R t1) {
              return seq->getEqualBefore(t1);
            });
            YogaValue dummy(ctx.reg, seq->type, ctx.reg->mem);
            ptt.inArgGradOutGens.push_back([dummy](R t0, R t1) {
              return dummy;
            });
          }
          else {
            ptt.inArgGens.push_back([seq](R t0, R t1) {
              return seq->getEqualBefore(t0);
            });
            YogaValue dummy(ctx.reg, seq->type, ctx.reg->mem);
            ptt.inArgGradOutGens.push_back([dummy](R t0, R t1) {
              return dummy;
            });
          }
        }
        if (formal->dirOutUp) {
          auto newSeq = ptt.trace->getOrAddTimeseq(seq->seqName, seq->type).get();
          if (hasSubt) {
            ptt.outArgGens.push_back([newSeq](R t0, R t1) {
              return newSeq->addNew(t1);
            });
            YogaValue dummy(ctx.reg, seq->type, ctx.reg->mem);
            ptt.outArgGradInGens.push_back([dummy](R t0, R t1) {
              return dummy;
            });
          }
          else {
            ptt.outArgGens.push_back([newSeq](R t0, R t1) {
              return newSeq->addNew(t0);
            });
            YogaValue dummy(ctx.reg, seq->type, ctx.reg->mem);
            ptt.outArgGradInGens.push_back([dummy](R t0, R t1) {
              return dummy;
            });
          }
        }
        return true;
      }
      return ctx(actual).logError("Unknown arg");
    })) return nullptr;
  }

  if (pts->traces.empty()) return nullptr;

  return pts;
}



void ScopeModel::runPredictTrainingSet(PredictTrainingSet *pts)
{
  YogaPool tmpmem;

  R lossAccum = 0.0;
  R lossDenom = 0.0;
  R gradMagSqAccum = 0.0;
  R gradMagSqDenom = 0.0;
  for (int mbCount = 0; mbCount < 1000; mbCount++) {
    vector<R> paramsGradAccum(pts->improvedParams.size());

    auto &ptt = pts->traces[random() % pts->traces.size()];

    for (int pttIter = 0; pttIter < 10; pttIter++) {

      R dur = unipolarHaltonAxis(pts->sampleSeed++, 3) * min(pts->predictDur, 0.75 * (ptt.endTs - ptt.beginTs));

      R t0 = ptt.beginTs + (ptt.endTs - ptt.beginTs - dur) * unipolarHaltonAxis(pts->sampleSeed++, 5);
      R t1 = t0 + dur;

      vector<U8 *> inVals, outVals, inValGradOuts, outValGradIns;
      for (auto &f : ptt.inArgGens) {
        auto yv = f(t0, t1);
        inVals.push_back(yv.buf);
      }
      for (auto &f : ptt.inArgGradOutGens) {
        auto yv = f(t0, t1);
        inValGradOuts.push_back(yv.buf);
      }
      for (auto &f : ptt.outArgGens) {
        auto yv = f(t0, t1);
        outVals.push_back(yv.buf);
      }
      for (auto &f : ptt.outArgGradInGens) {
        auto yv = f(t0, t1);
        outValGradIns.push_back(yv.buf);
      }
      R astonishment = 0.0;
      R dt = 0.0;
      (*pts->trainCaller.f->gradAddr)(
        outVals.data(), outValGradIns.data(),
        inVals.data(), inValGradOuts.data(),
        pts->improvedParams.data(),
        paramsGradAccum.data(),
        &astonishment, dt, tmpmem.it);
      lossAccum += *(R *)pts->lossHolder.buf;
      lossDenom += 1.0;
    }

    for (auto &g : paramsGradAccum) {
      gradMagSqAccum += sqr(g);
    }
    gradMagSqDenom += 1.0;
    if (mbCount % 100 == 99) {
      pts->lossGraph.push_back(lossAccum / max(1.0, lossDenom));
      pts->gradNormGraph.push_back(sqrt(gradMagSqAccum / gradMagSqDenom));
      lossAccum = lossDenom = 0.0;
      gradMagSqAccum = gradMagSqDenom = 0.0;
    }
    auto gradMul = 1e-3 / (1.0 + sqrt(gradMagSqAccum)/50.0);
    for (size_t gi = 0; gi < paramsGradAccum.size(); gi++) {
      if (paramsGradAccum[gi] != 0.0) {
        pts->isParamTrained[gi] = true;
        pts->improvedParams[gi] += gradMul * paramsGradAccum[gi];
      }
    }
  }
}
