#include "./trace_render.h"
#include "./ysgui.h"
#include "./yogastudio_ui.h"
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "../jit/runtime.h"
#include "./policy_search.h"

ScopeTraceRenderer::ScopeTraceRenderer(ScopeModel &_m, ScopeModelVisTrace &_tr)
  : m(_m), tr(_tr)
{
}

ScopeTraceRenderer::~ScopeTraceRenderer()
{
}


void ScopeTraceRenderer::drawAll(RenderQueue &q)
{
  if (lo.plotB - lo.plotT < 25.0) return;

  auto &io = GetIO();

  bool showHover = IsItemHovered() &&
      io.MousePos.y >= rangeT &&
      io.MousePos.y <= rangeB &&
      io.MousePos.x >= lo.plotL &&
      io.MousePos.x <= lo.plotR;

  auto mouseTime = lo.convXToTime(io.MousePos.x);
  auto mouseVal = showHover ? convYToData(io.MousePos.y) : 0.0;

  if (mouseTime < m.traceBeginTs || mouseTime > m.traceEndTs) showHover = false;

  if (0) cerr << "drawAll " + repr(lo) + "\n";
  drawYAxisLabels(q);
  drawWinopButtons(q);
  drawYAxis(q);
  drawXAxis(q);
  drawXAxisTics(q);
  drawPending(q);
  drawData(q);
  drawValues(q);
  drawPolicyTerrainYardstick(q);
  drawPolicySquiggleYardstick(q);

  ImGuiID id = GetID("popup");
  if (showHover && !IsPopupOpen(id, 0) && IsMouseClicked(0)) {
    clickMouseTime = mouseTime;
    clickMouseVal = mouseVal;
    OpenPopupEx(id);
  }
  else if (!IsPopupOpen(id, 0)) {
    clickMouseTime = -1.0;
    clickMouseVal = 0.0;
  }
  if (BeginPopupEx(id, ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoSavedSettings)) {
    drawPopupMenu(q);
    EndPopup();
  }
  if (!m.isLiveRobot) {
    drawDragGradients(q, mouseTime, mouseVal, showHover);
  }
}

void ScopeTraceRenderer::setupLayout(ScopeGraphLayout &_lo)
{
  lo = _lo;
  rangeT = lo.plotT + GetTextLineHeight()*1.25;
  rangeB = lo.plotB - GetTextLineHeight()*0.5;

  if (positiveOnly()) {
    xAxisY = snap5(lo.plotB);
    scaleY = floor((rangeB - rangeT) * -0.99);
  }
  else {
    xAxisY = snap5((rangeB + rangeT) * 0.5);
    scaleY = floor((rangeB - rangeT) * -0.49);
  }

  labelT = floor(lo.plotT + GetTextLineHeight());
  labelH = 12.0;

  winopT = labelT - GetTextLineHeight();
  winopL = lo.plotL;
  winopX = winopL;

  if (tr.autoScalePending) {
    tr.autoScalePending = false;
    doAutoScale();
    if (0) cerr << tr.spec.uniqueKey + ": set scale to " + fmteng(tr.scale, 3) + "\n";
  }

  convDataToY = Polyfit1(
    xAxisY,
    scaleY / tr.scale);
  convYToData = Polyfit1(
    - xAxisY / scaleY * tr.scale,
    tr.scale / scaleY);
}

void ScopeTraceRenderer::doAutoScale()
{

}


void ScopeTraceRenderer::drawYAxisLabels(RenderQueue &q)
{
  q.textLayer.push_back([this]() mutable {
    auto topLabelStr = fmtTopLabel();
    auto botLabelStr = fmtBotLabel();
    auto topTypeStr = fmtTypeStr();
    //auto supportsScale1 = supportsScale(lo, q);
    auto positiveOnly1 = positiveOnly();

    R textR = lo.textL + max(
      CalcTextSize(topLabelStr.c_str()).x,
      CalcTextSize(botLabelStr.c_str()).x);

    R topTextY = xAxisY + scaleY;
    R botTextY = xAxisY - scaleY;

    drawText(lo.textL, topTextY, yss->axisLabelCol, topLabelStr, 0.0, 0.5);
    if (!positiveOnly1) {
      drawText(lo.textL, botTextY, yss->axisLabelCol, botLabelStr, 0.0, 0.5);
    }

    drawText(textR + 12, topTextY, yss->axisLightLabelCol, topTypeStr, 0.0, 0.5);
  });
}

void ScopeTraceRenderer::drawWinopButtons(RenderQueue &q)
{
  if (tr.visHeightTarget == 0.0) return;

  q.buttonLayer.push_back([this]() {
    if (drawWinopIcon(winopX, winopT, yss->closeIconCol, ICON_FA_WINDOW_CLOSE)) {
      m.removeTrace(tr.spec.uniqueKey);
    }
    if (drawWinopIcon(winopX, winopT, yss->expandIconCol, ICON_FA_PLUS_CIRCLE)) {
      tr.expand();
      m.saveVisInfo();
    }
    if (drawWinopIcon(winopX, winopT, yss->boringIconCol, ICON_FA_SEARCH_MINUS)) {
      tr.scaleUp();
      m.saveVisInfo();
    }
    if (drawWinopIcon(winopX, winopT, yss->boringIconCol, ICON_FA_SEARCH_PLUS)) {
      tr.scaleDn();
      m.saveVisInfo();
    }
    if (drawWinopIcon(winopX, winopT, yss->boringIconCol, ICON_FA_MAGIC)) {
      doAutoScale();
    }

    for (auto &change : m.desiredGradientSpec.desiredChanges) {
      if (change.uniqueKey == tr.spec.uniqueKey) {
        if (drawWinopIcon(winopX, winopT, yss->boringIconCol, ICON_FA_ERASER)) {
          m.desiredGradientSpec.clearDesiredChange(tr.spec.ref);
        }
        break;
      }
    }

  });

}


void ScopeTraceRenderer::drawYAxis(RenderQueue &q)
{
  R pos = 1.0;
  R neg = positiveOnly() ? 0.0 : -1.0;
  array pts {
    ImVec2(lo.plotL + 4.0, xAxisY + neg * scaleY),
    ImVec2(lo.plotL, xAxisY + neg * scaleY),
    ImVec2(lo.plotL, xAxisY + pos * scaleY),
    ImVec2(lo.plotL + 4.0, xAxisY + pos * scaleY),
  };
  GetWindowDrawList()->AddPolyline(
    pts.data(), pts.size(),
    yss->axisLightLineCol, 
    false,
    0.75);
}

void ScopeTraceRenderer::drawXAxis(RenderQueue &q)
{
  GetWindowDrawList()->AddLine(
    ImVec2(lo.convTimeToX(max(m.traceBeginTs, m.visTime - m.cursPos * m.visDur)), xAxisY),
    ImVec2(lo.convTimeToX(min(m.traceEndTs, m.visTime + (1.0 - m.cursPos) * m.visDur)), xAxisY),
    yss->axisLightLineCol,
    0.75);
}

void ScopeTraceRenderer::drawXAxisTics(RenderQueue &q)
{
  if (m.visDur < m.majorTicInterval * 100) {
    R startTime = max(m.traceBeginTs, ceil((m.visTime - m.cursPos * m.visDur) / m.ticInterval) * m.ticInterval);
    R endTime = min(m.traceEndTs, m.visTime + (1.0 - m.cursPos) * m.visDur);
    for (R ticPos = startTime; ticPos <= endTime; ticPos += m.ticInterval) {
      bool isMajor = fmod(ticPos + m.ticInterval/4.0, m.majorTicInterval) < m.ticInterval/2;
      if (isMajor || m.visDur < m.ticInterval * 100) {
        R ticSize = isMajor ? 5 : 2;
        R ticX = lo.convTimeToX(ticPos);
        GetWindowDrawList()->AddLine(
          ImVec2(ticX, xAxisY + ticSize),
          ImVec2(ticX, xAxisY - ticSize),
          yss->axisTicCol,
          0.75);
      }
    }
  }
}

void ScopeTraceRenderer::drawPending(RenderQueue &q)
{
}

void ScopeTraceRenderer::drawData(RenderQueue &q)
{
}

void ScopeTraceRenderer::drawValues(RenderQueue &q)
{
}

string ScopeTraceRenderer::fmtTopLabel()
{
  return "+"s + fmtScale() + " " + tr.spec.ref.fullName;
}

string ScopeTraceRenderer::fmtBotLabel()
{
  return "-"s + fmtScale();
}

string ScopeTraceRenderer::fmtScale()
{
  return fmta2(tr.scale);
}

string ScopeTraceRenderer::fmtValue(R value)
{
  return fmteng(value, 3);
}

string ScopeTraceRenderer::fmtTypeStr()
{
  return tr.spec.ref.typeName;
}

bool ScopeTraceRenderer::supportsScale()
{
  return true;
}

bool ScopeTraceRenderer::positiveOnly()
{
  return false;
}

R ScopeTraceRenderer::getInitialVisHeight()
{
  return 1.0;
}


void ScopeTraceRenderer::drawDragGradients(RenderQueue &q, R mouseTime, R mouseVal, bool showHover)
{
  for (auto &change : m.desiredGradientSpec.desiredChanges) {
    if (change.uniqueKey == tr.spec.uniqueKey) {
      drawOneDragGradient(q, change.ts, change.delta, change.label, change.labelColor);
    }
  }
  if (clickMouseTime >= 0.0) {
    drawOneDragGradient(q, clickMouseTime, clickMouseVal, ""s, 0);
  }
  else if (showHover) {
    drawOneDragGradient(q, mouseTime, mouseVal, ""s, 0);
  }
}

void ScopeTraceRenderer::drawOneDragGradient(RenderQueue &q, R ts, R gradVal, string const &label, U32 labelColor)
{
  R gradSign = copysign(1.0, gradVal);
  R y0 = convDataToY(0.0);
  R y1 = convDataToY(gradVal);
  R x = lo.convTimeToX(ts);

  if (0) {
    cerr << "drawDragGradients1 gradVal=" + repr(gradVal) + repr(x) + "," + repr(y0) + "-" + repr(y1) + "\n";
  }
  auto color = yss->gradientCol;

  GetWindowDrawList()->AddLine(ImVec2(x - 2, y0), ImVec2(x + 2, y0), color, 1.0f);
  GetWindowDrawList()->AddLine(ImVec2(x, y0), ImVec2(x, y1), color, 3.0f);
  GetWindowDrawList()->AddLine(ImVec2(x, y1), ImVec2(x+3, y1+3*gradSign), color, 1.0f);
  GetWindowDrawList()->AddLine(ImVec2(x, y1), ImVec2(x-3, y1+3*gradSign), color, 1.0f);

  if (!label.empty()) {
    drawText(x, y1 - GetTextLineHeight()*0.6*gradSign, labelColor, label, 0.5f, 0.5f);
  }
}

vector<pair<string, PolicyRewardMetric>> ScopeTraceRenderer::getClickMousePolicyRewardMetrics()
{
  return {};
}

vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>>
ScopeTraceRenderer::getClickMouseGradBases()
{
  return {};
}

bool ScopeTraceRenderer::areTerrainAndSquiggleSame()
{
  return m.desiredPolicySquiggleSpec.rMetric.ref.fullName == m.desiredPolicyTerrainSpec.rMetric.ref.fullName &&
    abs(m.desiredPolicySquiggleSpec.rMetric.sampleTime - m.desiredPolicyTerrainSpec.rMetric.sampleTime) < 0.01;
}

void ScopeTraceRenderer::drawPopupMenu(RenderQueue &q)
{
  auto rMetrics = getClickMousePolicyRewardMetrics();
  if (rMetrics.size() == 1) {
    if (MenuItem("Set terrain metric")) {
      m.desiredPolicyTerrainSpec.rMetric = rMetrics[0].second;
      m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
    }
    if (MenuItem("Set squiggle metric")) {
      m.desiredPolicySquiggleSpec.rMetric = rMetrics[0].second;
      m.desiredPolicySquiggleSpec.policySquiggleEpoch ++;
    }
    if (MenuItem("Set both")) {
      m.desiredPolicySquiggleSpec.rMetric = m.desiredPolicyTerrainSpec.rMetric = rMetrics[0].second;
      m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
      m.desiredPolicySquiggleSpec.policySquiggleEpoch ++;
    }
    if (MenuItem("Clear both")) {
      m.desiredPolicySquiggleSpec.rMetric = m.desiredPolicyTerrainSpec.rMetric = PolicyRewardMetric();
    }
    if (MenuItem("Policy Search")) {
      m.desiredPolicySearchSpec.rMetric = rMetrics[0].second;
      m.policySearchState = make_shared<PolicySearchState>(m.ctx, m.desiredPolicySearchSpec);
      m.visPanelsNeedsUpdate = true;
    }
  }
  else if (rMetrics.size() > 1) {
    if (BeginMenu("Set terrain metric")) {
      for (auto &[label, rMetric] : rMetrics) {
        if (MenuItem(label.c_str())) {
          m.desiredPolicyTerrainSpec.rMetric = rMetric;
          m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
        }
      }
      EndMenu();
    }
    if (BeginMenu("Set squiggle metric")) {
      for (auto &[label, rMetric] : rMetrics) {
        if (MenuItem(label.c_str())) {
          m.desiredPolicySquiggleSpec.rMetric = rMetric;
          m.desiredPolicySquiggleSpec.policySquiggleEpoch ++;
        }
      }
      EndMenu();
    }
    if (BeginMenu("Set both")) {
      for (auto &[label, rMetric] : rMetrics) {
        if (MenuItem(label.c_str())) {
          m.desiredPolicySquiggleSpec.rMetric = m.desiredPolicyTerrainSpec.rMetric = rMetric;
          m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
          m.desiredPolicySquiggleSpec.policySquiggleEpoch ++;
        }
      }
      EndMenu();
    }
    if (BeginMenu("Policy Search")) {
      for (auto &[label, rMetric] : rMetrics) {
        if (MenuItem(label.c_str())) {
          m.desiredPolicySearchSpec.rMetric = rMetric;
          m.policySearchState = make_shared<PolicySearchState>(m.ctx, m.desiredPolicySearchSpec);
          m.visPanelsNeedsUpdate = true;
        }
      }
      EndMenu();
    }
    if (MenuItem("Clear all metrics")) {
      m.desiredPolicySquiggleSpec.rMetric = 
        m.desiredPolicyTerrainSpec.rMetric = 
        m.desiredPolicySearchSpec.rMetric = PolicyRewardMetric();
      m.policySearchState = nullptr;
    }
  }
  
  auto gradBases = getClickMouseGradBases();
  if (gradBases.size() == 1) {
    for (auto &[label, color, deltaFunc] : gradBases) {
      auto summary = label + " = " + fmteng(clickMouseVal, 1);
      if (MenuItem(("Add " + summary).c_str())) {
        m.desiredGradientSpec.addDesiredChange(tr.spec.ref, clickMouseTime, clickMouseVal, summary, color, deltaFunc(m.ctx));
      }
      if (m.desiredGradientSpec.hasDesiredChange(tr.spec.ref, clickMouseTime - lo.selTimeWindow, clickMouseTime + lo.selTimeWindow)) {
        if (MenuItem(("Remove " + label).c_str())) {
          m.desiredGradientSpec.clearDesiredChange(tr.spec.ref, clickMouseTime - lo.selTimeWindow, clickMouseTime + lo.selTimeWindow);
        }
      }
    }
  }
  else if (gradBases.size() > 1) {
    if (BeginMenu("Add gradient")) {
      for (auto &[label, color, deltaFunc] : gradBases) {
        auto summary = label + " = " + fmteng(clickMouseVal, 1);
        if (MenuItem(summary.c_str())) {
          m.desiredGradientSpec.addDesiredChange(tr.spec.ref, clickMouseTime, clickMouseVal, summary, color, deltaFunc(m.ctx));
        }
      }
      EndMenu();
    }
    if (m.desiredGradientSpec.hasDesiredChange(tr.spec.ref, clickMouseTime - lo.selTimeWindow, clickMouseTime + lo.selTimeWindow)) {
      if (MenuItem("Remove gradient")) {
        m.desiredGradientSpec.clearDesiredChange(tr.spec.ref, clickMouseTime - lo.selTimeWindow, clickMouseTime + lo.selTimeWindow);
      }
    }
  }

}

void ScopeTraceRenderer::drawPolicyTerrainYardstick(RenderQueue &q)
{
  if (m.desiredPolicyTerrainSpec.rMetric.ref.fullName == tr.spec.ref.fullName) {
    R x = lo.convTimeToX(m.desiredPolicyTerrainSpec.rMetric.sampleTime);

    R y0 = convDataToY(0.0);
    R y2 = convDataToY(tr.scale);

    auto ptm = m.policyTerrainMap;

    if (ptm && ptm->paramValueEpoch == m.ctx.reg->paramValueEpoch) {
      for (auto r : ptm->rs) {
        R yr = convDataToY(r);
        GetWindowDrawList()->AddLine(ImVec2(x-5, yr), ImVec2(x+5, yr), IM_COL32(0x3b, 0xee, 0xac, 0xcc), 0.5f);
      }
    }
    GetWindowDrawList()->AddLine(ImVec2(x, y0), ImVec2(x, y2), IM_COL32(0x88, 0x88, 0x88, 0xff), 3.0f);
    if (areTerrainAndSquiggleSame()) {
      drawText(x, y0, yss->textMedCol, "Metrics", 0.5, -0.1);
    }
    else {
      drawText(x, y0, yss->textMedCol, "T-Metric", 0.5, -0.1);
    }
  }
  if (auto pss = m.policySearchState) {
    if (pss->spec.rMetric.ref.fullName == tr.spec.ref.fullName) {
      R x = lo.convTimeToX(pss->spec.rMetric.sampleTime);
      R y0 = convDataToY(0.0);
      R y2 = convDataToY(tr.scale);
      GetWindowDrawList()->AddLine(ImVec2(x, y0), ImVec2(x, y2), IM_COL32(0x88, 0x88, 0x88, 0xff), 3.0f);
      drawText(x, y0, yss->textMedCol, "Search", 0.5, -0.1);
      for (auto &p : pss->particles) {
        if (p && p->evaluated) {
          R yr = convDataToY(p->evaluation);
          auto wid = (p == pss->particles[0]) ? 10 : 5;
          GetWindowDrawList()->AddLine(ImVec2(x-wid, yr), ImVec2(x+wid, yr), IM_COL32(0x3b, 0xee, 0xac, 0xcc), 0.5f);
        }
      }
    }
  }
}


void ScopeTraceRenderer::drawPolicySquiggleYardstick(RenderQueue &q)
{
  if (m.desiredPolicySquiggleSpec.rMetric.ref.fullName == tr.spec.ref.fullName) {
    R x = lo.convTimeToX(m.desiredPolicySquiggleSpec.rMetric.sampleTime);

    R y0 = convDataToY(0.0);
    R y2 = convDataToY(tr.scale);

    auto psm = m.policySquiggleMap;

    if (psm && psm->paramValueEpoch == m.ctx.reg->paramValueEpoch) {
      for (auto r : psm->rs) {
        R yr = convDataToY(r);
        GetWindowDrawList()->AddLine(ImVec2(x-5, yr), ImVec2(x+5, yr), IM_COL32(0x3b, 0xee, 0xac, 0xcc), 0.5f);
      }
    }
    if (areTerrainAndSquiggleSame()) {
    }
    else {
      GetWindowDrawList()->AddLine(ImVec2(x, y0), ImVec2(x, y2), IM_COL32(0x88, 0x88, 0x88, 0xff), 3.0f);
      drawText(x, y0, yss->textMedCol, "S-Metric", 0.5, -0.1);
    }
  }
}



ScopeTraceRendererMissing::ScopeTraceRendererMissing(
  ScopeModel &_m,
  ScopeModelVisTrace &_tr)
  : ScopeTraceRenderer(_m, _tr)
{
}

ScopeTraceRendererMissing::~ScopeTraceRendererMissing()
{
}


string ScopeTraceRendererMissing::fmtTopLabel()
{
  return "Missing data: " + tr.spec.uniqueKey;
}

string ScopeTraceRendererMissing::fmtBotLabel()
{
  return ""s;
}

void ScopeTraceRendererMissing::drawYAxis(RenderQueue &q)
{
}

void ScopeTraceRendererMissing::drawXAxis(RenderQueue &q)
{
}

void ScopeTraceRendererMissing::drawXAxisTics(RenderQueue &q)
{
}



ScopeTraceRendererRealStrip::ScopeTraceRendererRealStrip(
  ScopeModel &_m,
  ScopeModelVisTrace &_tr)
  : ScopeTraceRenderer(_m, _tr),
    seqValue(tr.spec.ref)
{
}

ScopeTraceRendererRealStrip::~ScopeTraceRendererRealStrip()
{
}

U32 ScopeTraceRendererRealStrip::lineColor()
{
  return yss->darkGraphColor(0);
}

U32 ScopeTraceRendererRealStrip::lineColor(ScopeModelAlt &alt)
{
  return alt.adjustColor(yss->darkGraphColor(0)); // 1.0f, 0.0f, 0.0f, 1.0f);
}

vector<pair<string, PolicyRewardMetric>>
ScopeTraceRendererRealStrip::getClickMousePolicyRewardMetrics()
{
  vector<pair<string, PolicyRewardMetric>> ret;
  ret.emplace_back("", PolicyRewardMetric{
    tr.spec.ref,
    clickMouseTime,
    clickMouseVal
  });
  return ret;
}

vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>>
ScopeTraceRendererRealStrip::getClickMouseGradBases()
{
  vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>> ret;
  ret.emplace_back("\ue004", lineColor(), [this](YogaContext const &ctx) {
    auto deltaValue = ctx.mkValue(tr.spec.ref.seqType);
    deltaValue.wr(tr.spec.ref.subName, clickMouseVal);
    return deltaValue;
  });
  return ret;
}


void ScopeTraceRendererRealStrip::drawData(RenderQueue &q)
{
  if (!seqValue.isValid()) return;
  for (auto &alt : m.alts(tr.spec.ref)) {
    if (alt.grads) {
      
      auto yvs = alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs());
      PolybandBuf band;
      band.reserve(yvs.size());

      for (auto &yv : yvs) {
        auto value = seqValue.rd(yv);
        R y = convDataToY(value);
        R x = lo.convTimeToX(yv.ts);
        auto yg = alt.grads->getGrad(yv);
        auto g = seqValue.rd(yg);

        band(x, y, y + 0.1 * (lo.plotT - lo.plotB) * compressGrad(g));
      }
      band.fill(alt.adjustColor(1.0f, 0.75f, 1.0f, 1.0f));
    }
    if (alt.isPredict) {

      if (auto uiPredictConfidenceSeq = alt.trace->getTimeseq("ui.confidence")) {
        auto *dl = GetWindowDrawList();
        auto yValues = alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs());
        auto cValues = uiPredictConfidenceSeq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs());
        auto yvp = yValues.begin();
        auto cvp = cValues.begin();
        R confidence = 1.0;
        R lasty = -1, lastx = -1;
        auto baseColor = ImColor(lineColor(alt));
        while (yvp != yValues.end()) {
          while (cvp != cValues.end() && cvp->ts <= yvp->ts) {
            confidence = *reinterpret_cast<R *>(cvp->buf);
            cvp ++;
          }
          auto value = seqValue.rd(*yvp);
          R y = convDataToY(value);
          R x = lo.convTimeToX(yvp->ts);
          if (lastx != -1) {
            auto confColor = baseColor;
            confColor.Value.w *= (0.1 + 0.9 * confidence);
            dl->AddLine(ImVec2(lastx, lasty), ImVec2(x, y), confColor, 3.0f);
          }
          lastx = x; lasty = y;
          yvp++;
        }
      }
      else {
        auto yvs = alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs());
        PolylineBuf pts;
        pts.reserve(yvs.size());
        for (auto &yv : yvs) {
          auto value = seqValue.rd(yv);
          R y = convDataToY(value);
          R x = lo.convTimeToX(yv.ts);
          pts(x, y);
        }
        pts.draw(lineColor(alt), false, alt.isPredict ? 3.0 : 1.0);
      }
    }

    else {
      auto yvs = alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs());
      PolylineBuf pts;
      pts.reserve(yvs.size());

      for (auto &yv : yvs) {
        auto value = seqValue.rd(yv);
        R y = convDataToY(value);
        R x = lo.convTimeToX(yv.ts);
        pts(x, y);
      }

      pts.draw(lineColor(alt), false, alt.isPredict ? 3.0 : 1.0);
    }
  }
}

void ScopeTraceRendererRealStrip::drawValues(RenderQueue &q)
{
  if (!seqValue.isValid()) return;
  R textY = lo.plotT + 2;
  for (auto &alt : m.alts(tr.spec.ref)) {
    if (alt.isPredict) continue;
    auto yv = alt.seq->getEqualBefore(m.visTime);
    if (!yv.isValid()) continue;
    auto value = seqValue.rd(yv);
    auto label = fmtValue(value);
    // WRITEME: if lo.labelOnLeft, then right-align
    drawText(lo.infoX, textY, alt.adjustColor(0.0f, 0.0f, 0.0f, 1.0f), label);
    textY += GetTextLineHeight();
  }
}

void ScopeTraceRendererRealStrip::doAutoScale()
{
  if (!seqValue.isValid()) return;

  R peak = 0.0;
  for (auto &alt : m.alts(tr.spec.ref)) {
    for (auto &yv : alt.seq->getValuesInRange(m.traceBeginTs, m.traceEndTs)) {
      auto value = seqValue.rd(yv);
      peak = max(peak, abs(value));
    }
  }
  if (peak > 0) {
    tr.scale = quantizeIntervalUp(peak);
  }
}


struct ScopeTraceRendererInterval : ScopeTraceRenderer {
  ScopeTraceRendererInterval(
    ScopeModel &_m,
    ScopeModelVisTrace &_tr)
    : ScopeTraceRenderer(_m, _tr)
  {
  }
  virtual ~ScopeTraceRendererInterval()
  {
  }

  YogaValueAccessor<double> seqValue;

  void drawData(RenderQueue &q) override
  {
    for (auto &alt : m.alts(tr.spec.ref)) {
      //if (alt.isSecondary) return;
      R lastTime = 0.0;
      bool lastTimeSet = false;
      for (auto &yv : alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs())) {
        if (lastTimeSet) {
          R x = lo.convTimeToX(yv.ts);
          R y0 = convDataToY(0.0);
          R y1 = convDataToY(yv.ts - lastTime) - 1.0;
          GetWindowDrawList()->AddLine(ImVec2(x, y0), ImVec2(x, y1), alt.adjustColor(1.0f, 0.0f, 0.0f, 1.0f), 1.0);
        }
        lastTime = yv.ts;
        lastTimeSet = true;
      }
    }
  }

  void drawValues(RenderQueue &q) override
  {
    R textY = lo.plotT + 2;
    for (auto &alt : m.alts(tr.spec.ref)) {
      //if (alt.isSecondary) return;
      if (alt.isPredict) continue;
      auto [yv1, yv2] = alt.seq->getBracket(m.visTime);
      auto value = yv2.ts - yv1.ts;
      auto label = fmtValue(value);
      drawText(lo.infoX, textY, alt.adjustColor(0.0f, 0.0f, 0.0f, 1.0f), label);
      textY += GetTextLineHeight();
    }
  }

  void doAutoScale() override
  {
    R peak = 0.0;
    R lastTime = 0.0;
    bool lastTimeSet = false;
    for (auto &alt : m.alts(tr.spec.ref)) {
      for (auto &yv : alt.seq->getValuesInRange(m.traceBeginTs, m.traceEndTs)) {
        if (lastTimeSet) {
          peak = max(peak, yv.ts - lastTime);
        }
        lastTime = yv.ts;
        lastTimeSet = true;
      }
    }
    if (peak > 0) {
      tr.scale = quantizeIntervalUp(peak);
    }
  }

};




ScopeTraceRendererMatrixStrip::ScopeTraceRendererMatrixStrip(
  ScopeModel &_m,
  ScopeModelVisTrace &_tr)
  : ScopeTraceRenderer(_m, _tr),
    seqPtr(tr.spec.ref)
{
}

ScopeTraceRendererMatrixStrip::~ScopeTraceRendererMatrixStrip()
{
}


vector<pair<string, PolicyRewardMetric>>
ScopeTraceRendererMatrixStrip::getClickMousePolicyRewardMetrics()
{
  vector<pair<string, PolicyRewardMetric>> ret;
  auto t = tr.spec.ref.itemType->asMatrix();
  if (t->elementType->isReal()) {
    for (int ri = 0; ri < min(4, t->rows); ri++) {
      for (int ci = 0; ci < min(4, t->cols); ci++) {
        ret.emplace_back("[" + repr(ri) + "," + repr(ci) + "]", PolicyRewardMetric{
          tr.spec.ref.member("r"+repr(ri) + "c" + repr(ci)),
          clickMouseTime,
          clickMouseVal
        });
      }
    }
  }
  return ret;
}

vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>>
ScopeTraceRendererMatrixStrip::getClickMouseGradBases()
{
  vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>> ret;

  auto elt = tr.spec.ref.itemType->asMatrix();
  if (elt && elt->elementType->isReal()) {
    for (int ci = 0; ci < elt->cols; ci++) {
      for (int ri = 0; ri < elt->rows; ri++) {
        auto label = "\ue004["s + repr(ri) + "," + repr(ci) + "]";

        ret.emplace_back(label, yss->darkGraphColor(ci * elt->rows + ri),
          [ri, ci, elt, this](YogaContext const &ctx) {
            auto deltaValue = ctx.mkValue(tr.spec.ref.seqType);
            YogaPtrAccessor acc(tr.spec.ref.seqType, tr.spec.ref.subName);
            auto p = acc.ptr(deltaValue);
            ((R *)p.buf)[ci * elt->rows + ri] = clickMouseVal;
            return deltaValue;
          });
      }
    }
  }

  return ret;
}


void ScopeTraceRendererMatrixStrip::drawData(RenderQueue &q)
{
  auto t = tr.spec.ref.itemType->asMatrix();
  for (auto &alt : m.alts(tr.spec.ref)) {
    int els = t->cols * t->rows;
    vector<PolylineBuf> pts(els);

    if (t->elementType->isReal()) {
      auto yvs = alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs());
      for (int eli = 0; eli < els; eli++) {
        pts[eli].reserve(yvs.size());
      }
      for (auto &yv : yvs) {
        auto p = seqPtr.ptr(yv);
        R x = lo.convTimeToX(yv.ts);
        for (int eli = 0; eli < els; eli++) {
          R value = ((R *)p.buf)[eli];
          R y = convDataToY(value);
          pts[eli](x, y);
        }
      }
    }
    else {
      throw runtime_error("ScopeTraceRendererMatrixStrip: WRITEME: elementType="s + repr_type(t->elementType));
    }

    for (int eli = 0; eli < els; eli++) {
      pts[eli].draw(alt.adjustColor(yss->darkGraphColor(eli)), false, 1.0);
    }
  }
}

void ScopeTraceRendererMatrixStrip::drawValues(RenderQueue &q)
{
  R textY = lo.plotT + 2;
  auto t = tr.spec.ref.itemType->asMatrix();
  for (auto &alt : m.alts(tr.spec.ref)) {
    if (alt.isPredict) continue;
    auto yv = alt.seq->getEqualBefore(m.visTime);
    auto p = seqPtr.ptr(yv);
    if (t->elementType->isReal()) {
      R elWidth = 50.0, elHeight = GetTextLineHeight();
      for (int ci = 0; ci < t->cols; ci++) {
        for (int ri = 0; ri < t->rows; ri++) {
          R value = ((R *)p.buf)[ci * t->rows + ri];
          auto label = fmtValue(value);

          // WRITEME: if lo.labelOnLeft, then right-align
          drawText(lo.infoX + ci * elWidth, 
            textY + ri * elHeight,
            alt.adjustColor(yss->darkGraphColor(ci * t->rows + ri)),
            label);
        }
      }
      textY += (t->rows + 0.5) * elHeight;
    }
  }
}


ScopeTraceRendererOnehot::ScopeTraceRendererOnehot(
  ScopeModel &_m,
  ScopeModelVisTrace &_tr)
  : ScopeTraceRenderer(_m, _tr),
    seqPtr(tr.spec.ref)
{
}

ScopeTraceRendererOnehot::~ScopeTraceRendererOnehot()
{
}

bool ScopeTraceRendererOnehot::positiveOnly()
{
  return true;
}

vector<pair<string, PolicyRewardMetric>>
ScopeTraceRendererOnehot::getClickMousePolicyRewardMetrics()
{
  vector<pair<string, PolicyRewardMetric>> ret;
  auto t = tr.spec.ref.itemType->asStruct();
  if (t) {
    for (auto &mem : t->members) {
      ret.emplace_back("." + mem->memberName, PolicyRewardMetric{
        tr.spec.ref.member(mem->memberName),
        clickMouseTime,
        clickMouseVal
      });
    }
  }
  return ret;
}


vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>>
ScopeTraceRendererOnehot::getClickMouseGradBases()
{
  vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>> ret;

  auto elt = tr.spec.ref.itemType->asStruct();
  for (size_t eli = 0; eli < elt->members.size(); eli++) {
    auto mem = elt->members[eli];

    ret.emplace_back("\ue004."s + mem->memberName, yss->darkGraphColor(eli),
      [this, mem](YogaContext const &ctx) {
        auto deltaValue = ctx.mkValue(tr.spec.ref.seqType);
        YogaValueAccessor<R> acc(deltaValue.type, dotJoin(tr.spec.ref.subName, mem->memberName));
        acc.wr(deltaValue, clickMouseVal);
        return deltaValue;
      });
  }
  return ret;
}


void ScopeTraceRendererOnehot::drawData(RenderQueue &q)
{
  auto elt = tr.spec.ref.itemType->asStruct();
  for (auto &alt : m.alts(tr.spec.ref)) {
    auto const elCount = elt->members.size();

    if (0) cerr << "Onehot " + repr(elCount) + "\n";

    auto yvs = alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs());
    vector<PolylineBuf> lines(elCount);
    vector<PolybandBuf> bands(elCount);
    for (size_t eli = 0; eli < elCount; eli++) {
      lines[eli].reserve(yvs.size());
      bands[eli].reserve(yvs.size());
    }

    for (auto &yv : yvs) {
      auto p = seqPtr.ptr(yv);
      R x = snap5(lo.convTimeToX(yv.ts));

      R tot = 0.0;
      for (size_t eli = 0; eli < elCount; eli++) {
        tot += ((R *)p.buf)[eli];
      }
      tot = max(tot, 1.0);

      R totFrac = 0.0;
      for (size_t eli = 0; eli < elCount; eli++) {
        R frac = ((R *)p.buf)[eli] / tot;
        R y0 = convDataToY(totFrac);
        totFrac += frac;
        R y1 = convDataToY(totFrac);

        lines[eli](x, y1);
        bands[eli](x, y0, y1);
      }
    }

    for (size_t eli = 0; eli < elCount; eli++) {
      bands[eli].fill(alt.adjustColor(yss->lightGraphColor(eli)));
    }
    for (size_t eli = 0; eli < elCount; eli++) {
      lines[eli].draw(alt.adjustColor(yss->darkGraphColor(eli)), false, 1.0);
    }
  }
}

void ScopeTraceRendererOnehot::drawValues(RenderQueue &q)
{
  auto elt = tr.spec.ref.itemType->asStruct();
  for (auto &alt : m.alts(tr.spec.ref)) {
    if (alt.isPredict) continue;
    if (alt.isSecondary) continue;
    auto yv = alt.seq->getEqualBefore(m.visTime);
    auto p = seqPtr.ptr(yv);
    auto const elCount = elt->members.size();

    R elHeight = GetTextLineHeight();
    for (ssize_t eli = elCount - 1; eli >= 0; eli--) {
      R value = ((R *)p.buf)[eli];
      auto label = elt->members[eli]->memberName + " " + fmtValue(value);

      // WRITEME: if lo.labelOnLeft, then right-align
      drawText(
        lo.infoX, 
        lo.plotT + 2 + eli * elHeight,
        yss->darkGraphColor(eli),
        label);
    }
  }
}

R ScopeTraceRendererOnehot::getInitialVisHeight()
{
  return 0.5;
}



ScopeTraceRendererStruct::ScopeTraceRendererStruct(
  ScopeModel &_m,
  ScopeModelVisTrace &_tr)
  : ScopeTraceRenderer(_m, _tr)
{
  auto t = tr.spec.ref.itemType->asStruct();
  for (auto &memIt : t->members) {
    if (memIt->memberType->isScalar() || memIt->memberType->isBool()) {
      valueGetters.emplace_back(
        memIt->memberName,
        YogaValueAccessor<R>(tr.spec.ref.member(memIt->memberName)));
    }
  }
}

ScopeTraceRendererStruct::~ScopeTraceRendererStruct()
{
}

vector<pair<string, PolicyRewardMetric>>
ScopeTraceRendererStruct::getClickMousePolicyRewardMetrics()
{
  vector<pair<string, PolicyRewardMetric>> ret;
  auto t = tr.spec.ref.itemType->asStruct();
  if (t) {
    for (auto &mem : t->members) {
      ret.emplace_back("." + mem->memberName, PolicyRewardMetric{
        tr.spec.ref.member(mem->memberName),
        clickMouseTime,
        clickMouseVal
      });
    }
  }
  return ret;
}

vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>>
ScopeTraceRendererStruct::getClickMouseGradBases()
{
  vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>> ret;

  auto t = tr.spec.ref.itemType->asStruct();
  for (size_t eli = 0; eli < t->members.size(); eli++) {
    auto mem = t->members[eli];
    if (mem->memberType->isScalar() || mem->memberType->isBool()) {
      ret.emplace_back("\ue004."s + mem->memberName, yss->darkGraphColor(eli),
        [this, mem](YogaContext const &ctx) {
          auto deltaValue = ctx.mkValue(tr.spec.ref.seqType);
          YogaValueAccessor<R> acc(deltaValue.type, dotJoin(tr.spec.ref.subName, mem->memberName));
          return deltaValue;
        });
    }
  }
  return ret;
}

void ScopeTraceRendererStruct::drawData(RenderQueue &q)
{
  for (auto &alt : m.alts(tr.spec.ref)) {
    auto const elCount = valueGetters.size();

    auto yvs = alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs());
    vector<PolylineBuf> pts(elCount);
    for (size_t eli = 0; eli < elCount; eli++) {
      pts.reserve(yvs.size());
    }
    for (auto &yv : yvs) {
      R x = lo.convTimeToX(yv.ts);

      for (size_t eli = 0; eli < elCount; eli++) {
        R val = valueGetters[eli].second.rd(yv);
        R y = convDataToY(val);
        pts[eli](x, y);
      }
    }
    for (size_t eli = 0; eli < elCount; eli++) {
      pts[eli].draw(alt.adjustColor(yss->darkGraphColor(eli)), false, 1.0);
    }
  }
}

void ScopeTraceRendererStruct::drawValues(RenderQueue &q)
{
  R textY = lo.plotT + 2;
  for (auto &alt : m.alts(tr.spec.ref)) {
    if (alt.isSecondary) continue;
    if (alt.isPredict) continue;
    auto yv = alt.seq->getEqualBefore(m.visTime);
    auto const elCount = valueGetters.size();

    R elHeight = GetTextLineHeight();
    for (ssize_t eli = elCount - 1; eli >= 0; eli--) {
      R value = valueGetters[eli].second.rd(yv);
      auto label = valueGetters[eli].first + " " + fmtValue(value);

      // WRITEME: if lo.labelOnLeft, then right-align
      drawText(
        lo.infoX,
        textY + eli * elHeight,
        yss->darkGraphColor(eli),
        label);
    }
    textY += elCount * elHeight;
  }
}

void ScopeTraceRendererStruct::doAutoScale()
{
  R peak = 0.0;
  for (auto &alt : m.alts(tr.spec.ref)) {
    for (auto &getter : valueGetters) {
      for (auto &yv : alt.seq->getValuesInRange(m.traceBeginTs, m.traceEndTs)) {
        auto value = getter.second.rd(yv);
        peak = max(peak, abs(value));
      }
    }
  }
  if (peak > 0) {
    tr.scale = quantizeIntervalUp(peak);
  }
}




static map<string, ScopeTraceRendererRegister *> *renderersByType;

ScopeTraceRendererRegister::ScopeTraceRendererRegister(string const &_clsName,
  std::function<void(ScopeModel &m, ScopeModelVisTrace &tr)> _f)
    : clsName(_clsName),
      f(_f)
{
  if (!renderersByType) renderersByType = new map<string, ScopeTraceRendererRegister *>();
  (*renderersByType)[clsName] = this;
}


void ScopeModelVisTrace::setRenderer(ScopeModel &m)
{
  auto seq = m.trace->getTimeseq(spec.ref.seqName);
  if (!seq) {
    renderer = make_shared<ScopeTraceRendererMissing>(m, *this);
    cerr << spec.ref.fullName + ": setRenderer: no seq " + spec.ref.seqName + "\n";
    return;
  }

  if (spec.ref.typeName == "interval") {
    renderer = make_shared<ScopeTraceRendererInterval>(m, *this);
  }
  else {
    auto t = spec.ref.itemType;
    if (!t) {
      cerr << spec.ref.fullName + ": setRenderer: no type " + spec.ref.typeName + "\n";
      return; // WRITEME: indicate error somewhere
    }

    if (t->isScalar() || t->isBool()) {
      renderer = make_shared<ScopeTraceRendererRealStrip>(m, *this);
    }
    else if (t->asMatrix() && t->asMatrix()->elementType->isReal()) {
      renderer = make_shared<ScopeTraceRendererMatrixStrip>(m, *this);
    }
    else if (t->asStruct()) {
      if (auto rendererMaker = renderersByType ? (*renderersByType)[t->clsName] : nullptr) {
        (rendererMaker->f)(m, *this);
      }
      else if (t->asStruct()->isOnehot) {
        renderer = make_shared<ScopeTraceRendererOnehot>(m, *this);
      }
      else {
        renderer = make_shared<ScopeTraceRendererStruct>(m, *this);
      }
    }
    else {
      renderer = make_shared<ScopeTraceRenderer>(m, *this);
    }
  }
  if (renderer) {
    visHeightTarget = renderer->getInitialVisHeight();
  }
  if (0) cerr << spec.uniqueKey + ": setRenderer: ok\n";
}
