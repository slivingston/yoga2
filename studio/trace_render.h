#pragma once
#include "./ysgui.h"
#include "common/std_headers.h"
#include "numerical/polyfit.h"
#include "./yogastudio_ui.h"
#include "../jit/type.h"
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "../jit/runtime.h"

struct ScopeModel;
struct ScopeGraphLayout;
struct ScopeModelVisTrace;
struct YogaTimeseq;
struct YogaType;
struct YogaTypeStruct;

struct ScopeGraphLayout {
  ImRect plotBox;
  Polyfit1 convTimeToX;
  Polyfit1 convXToTime;
  double selX{0.0};
  double selTimeWindow{0.01};
  double boxL{0.0}, boxR{0.0}, boxT{0.0}, boxB{0.0};
  double plotL{0.0}, plotR{0.0}, plotT{0.0}, plotB{0.0}, textL{0.0}, infoX{0.0};
  double lineHeight{0.0};
  bool labelOnLeft{false};
};

struct ScopeTraceRenderer {
  ScopeTraceRenderer(ScopeModel &_m, ScopeModelVisTrace &_tr);
  virtual ~ScopeTraceRenderer();

  // Stable
  ScopeModel &m;
  ScopeModelVisTrace &tr;

  // Persistent between draws
  R clickMouseTime, clickMouseVal;

  // Refreshed every use
  ScopeGraphLayout lo;
  double rangeT{0.0}, rangeB{0.0};
  double xAxisY{0.0}, scaleY{0.0};
  double labelT{0.0}, labelH{0.0};
  float winopT{0.0}, winopL{0.0}, winopX{0.0};
  Polyfit1 convDataToY, convYToData;

  virtual void setupLayout(ScopeGraphLayout &_lo);
  virtual void drawAll(RenderQueue &q);
  virtual void drawYAxisLabels(RenderQueue &q);
  virtual void drawWinopButtons(RenderQueue &q);
  virtual void drawYAxis(RenderQueue &q);
  virtual void drawXAxis(RenderQueue &q);
  virtual void drawXAxisTics(RenderQueue &q);
  virtual void drawPending(RenderQueue &q);
  virtual void drawData(RenderQueue &q);
  virtual void drawValues(RenderQueue &q);
  virtual void drawDragGradients(RenderQueue &q, R ts, R yVal, bool showHover);
  virtual void drawOneDragGradient(RenderQueue &q, R ts, R gradVal,
    string const &label, U32 labelColor);
  virtual void drawPopupMenu(RenderQueue &q);
  virtual void drawPolicyTerrainYardstick(RenderQueue &q);
  virtual void drawPolicySquiggleYardstick(RenderQueue &q);

  virtual string fmtTopLabel();
  virtual string fmtBotLabel();
  virtual string fmtScale();
  virtual string fmtTypeStr();
  virtual string fmtValue(R value);

  virtual bool supportsScale();
  virtual bool positiveOnly();
  virtual void doAutoScale();
  virtual R getInitialVisHeight();

  virtual vector<pair<string, PolicyRewardMetric>> getClickMousePolicyRewardMetrics();
  virtual vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>> getClickMouseGradBases();
  bool areTerrainAndSquiggleSame();

};


struct ScopeTraceRendererMissing : ScopeTraceRenderer {
  ScopeTraceRendererMissing(
    ScopeModel &_m,
    ScopeModelVisTrace &_tr);
  virtual ~ScopeTraceRendererMissing();

  string fmtTopLabel() override;
  string fmtBotLabel() override;
  void drawYAxis(RenderQueue &q) override;
  void drawXAxis(RenderQueue &q) override;
  void drawXAxisTics(RenderQueue &q) override;

};



struct ScopeTraceRendererRealStrip : ScopeTraceRenderer {
  ScopeTraceRendererRealStrip(
    ScopeModel &_m,
    ScopeModelVisTrace &_tr);
  virtual ~ScopeTraceRendererRealStrip();

  YogaValueAccessor<double> seqValue;

  void drawData(RenderQueue &q) override;
  void drawValues(RenderQueue &q) override;
  void doAutoScale() override;
  virtual U32 lineColor();
  virtual U32 lineColor(ScopeModelAlt &alt);

  vector<pair<string, PolicyRewardMetric>> getClickMousePolicyRewardMetrics() override;
  vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>> getClickMouseGradBases() override;

};



struct ScopeTraceRendererMatrixStrip : ScopeTraceRenderer {
  ScopeTraceRendererMatrixStrip(
    ScopeModel &_m,
    ScopeModelVisTrace &_tr);
  virtual ~ScopeTraceRendererMatrixStrip();

  YogaPtrAccessor seqPtr;

  void drawData(RenderQueue &q) override;
  void drawValues(RenderQueue &q) override;

  vector<pair<string, PolicyRewardMetric>> getClickMousePolicyRewardMetrics() override;
  vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>> getClickMouseGradBases() override;

};



struct ScopeTraceRendererOnehot : ScopeTraceRenderer {
  ScopeTraceRendererOnehot(
    ScopeModel &_m,
    ScopeModelVisTrace &_tr);
  virtual ~ScopeTraceRendererOnehot();

  YogaPtrAccessor seqPtr;

  bool positiveOnly() override;

  void drawData(RenderQueue &q) override;
  void drawValues(RenderQueue &q) override;
  R getInitialVisHeight() override;

  vector<pair<string, PolicyRewardMetric>> getClickMousePolicyRewardMetrics() override;
  vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>> getClickMouseGradBases() override;

};



struct ScopeTraceRendererStruct : ScopeTraceRenderer {
  ScopeTraceRendererStruct(
    ScopeModel &_m,
    ScopeModelVisTrace &_tr);
  virtual ~ScopeTraceRendererStruct();

  vector<pair<string, YogaValueAccessor<R>>> valueGetters;

  void drawData(RenderQueue &q) override;
  void drawValues(RenderQueue &q) override;
  void doAutoScale() override;

  vector<pair<string, PolicyRewardMetric>> getClickMousePolicyRewardMetrics() override;
  vector<tuple<string, U32, function<YogaValue(YogaContext &ctx)>>> getClickMouseGradBases() override;

};


struct ScopeTraceRendererRegister {
  ScopeTraceRendererRegister(string const &_clsName, std::function<void(
    ScopeModel &m, 
    ScopeModelVisTrace &tr)> _f);
  
  string clsName;
  std::function<void(
    ScopeModel &m, 
    ScopeModelVisTrace &tr)> f;

};
