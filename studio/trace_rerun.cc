#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "common/uv_wrappers.h"
#include "../comm/engine.h"
#include "./scope_model.h"
#include "../jit/runtime.h"
#include "../jit/context.h"
#include "../jit/compilation.h"
#include "../jit/yoga_engine.h"
#include "../timeseq/timeseq_pipe.h"

void ScopeModel::updateAltTraces()
{
  if (!trace) return;
  if (traceLoadActive || traceSimActive) return; // wait until done
  if (paramAltValueThreadActive) return;
  if (liveClient || liveSim) return;

  if (!paramTrace ||
      paramTrace->paramValueEpoch != ctx.reg->paramValueEpoch ||
      (paramTrace->gradientSpec ?
        paramTrace->gradientSpec->gradientEpoch != desiredGradientSpec.gradientEpoch :
        !desiredGradientSpec.empty())) {

    paramAltValueThreadActive = true;

    auto newTraceInfo = mkAltTraceForGrads();

    auto t0 = clock();
    uvWork(
        [newTraceInfo](string &error, shared_ptr<void> &result) {
          rerunTrace(newTraceInfo.get());
        },
        [this, t0, newTraceInfo, this1=shared_from_this()](string const &error, shared_ptr<void> const &result) {
          paramTrace = newTraceInfo;
          paramAltValueThreadActive = false;
          uvUiActive = true;
          auto t1 = clock();
          if (0) cerr << "Reran "s + repr_0_3f(traceEndTs) + " S of trace in " + to_string(t1-t0) + " uS\n";
        });
  }
}


shared_ptr<AltTraceInfo> ScopeModel::mkAltTraceForGrads()
{
  auto ret = mkAltTrace();
  if (!desiredGradientSpec.empty()) {
    ret->gradientSpec = make_shared<PolicyGradientSpec>(desiredGradientSpec);
  }

  return ret;
}


void ScopeModel::rerunTrace(AltTraceInfo *newTraceInfo)
{
  int verbose = 0;

  bool doGrads = !!newTraceInfo->gradientSpec;

  auto newTrace = make_shared<Trace>(newTraceInfo->baseTrace->rti->reg);
  if (!newTraceInfo->overrideParamValues.empty()) {
    newTrace->rti->paramValues = newTraceInfo->overrideParamValues.data();
  }
  if (newTraceInfo->durationLimit > 0.0) {
    newTrace->rti->hardHaltTs = newTraceInfo->durationLimit;
  }
  auto oldDur = newTraceInfo->baseTrace->getEndTs();
  if (oldDur > 0.0 && (newTrace->rti->hardHaltTs == 0.0 || oldDur < newTrace->rti->hardHaltTs)) {
    newTrace->rti->hardHaltTs = oldDur;
  }

  newTraceInfo->trace = newTrace;
  auto gradFwdTrace = doGrads ? make_shared<GradFwdTrace>() : nullptr;

  auto reg = newTraceInfo->baseTrace->rti->reg;

  if (!setupYogaEngines(reg->mkCtx("rerunTrace"), newTrace.get(), true)) {
    reg->checkpointDiagLog();
    throw runtime_error("setupYogaEngines failed");
  }

  vector<TimeseqPipe> pipes;

  for (auto &seqIt : newTraceInfo->baseTrace->timeseqs) {
    if (!seqIt.timeseq) continue;

    if (seqIt.timeseq->isExtIn) {
      if (verbose) cerr << "rerun " + seqIt.timeseq->seqName + "\n";

      auto replayFrom = seqIt.timeseq;
      auto replayTo = newTrace->getTimeseq(replayFrom->seqName);
      if (!replayTo) {
        if (verbose) cerr << "  no destination " + seqIt.timeseq->seqName + "\n";
        continue;
      }
      size_t remaining = replayFrom->sampleCount();
      vector<pair<TimeseqChange, std::function<double(double t)>>> localChanges;

      pipes.push_back(TimeseqPipe{
        replayFrom.get(),
        replayFrom->values_.begin(),
        replayTo.get(),
        localChanges,
        remaining
      });

      if (verbose) {
        cerr << "rerunFrom: seq " + replayFrom->seqName +
            ": isExtIn=" + repr(replayFrom->isExtIn) + 
            ": isExtOut=" + repr(replayFrom->isExtOut) +
            " sampleCount=" + repr(replayFrom->sampleCount()) +
            " -- piping " + repr(remaining) + "\n";
        for (auto &changeit : localChanges) {
          cerr << "    change " + changeit.first.changeKey +
            " value=" + repr_0_3f(changeit.first.value) +
            " weight=" + changeit.first.windowFuncName +
            "(" + repr_0_3f(changeit.first.beginTs) +
            " - " + repr_0_3f(changeit.first.endTs) + ")\n";
        }
      }
    }
    else {
      if (verbose) cerr << "norerun " + seqIt.timeseq->seqName + "\n";
    }
  }


  if (verbose) cerr << "startEngines\n";
  newTrace->startEngines();
  auto realtimeTs = realtime();
  while (true) {

    double bestTs{1e10};
    bool haveBestTs{false};

    for (auto &pipeit : pipes) {
      if (pipeit.valid()) {
        if (!haveBestTs) {
          bestTs = pipeit.frontTime();
          haveBestTs = true;
        }
        else if (pipeit.frontTime() < bestTs) {
          bestTs = pipeit.frontTime();
        }
      }
    }
    if (!haveBestTs) break;
    auto rtFence = bestTs+999.0;
    for (auto &pipeit : pipes) {
      if (pipeit.valid() && pipeit.frontTime() == bestTs) {
        pipeit.advance();
        if (pipeit.valid()) {
          rtFence = min(rtFence, pipeit.frontTime());
        }
      }
    }

    EngineUpdateContext ctx(realtimeTs);
    ctx.rtFence = rtFence;
    ctx.gradFwdTrace = gradFwdTrace.get();
    newTrace->updateIter(ctx);
    if (newTrace->rti->hardHalt) break;
  }

  newTrace->updateIter(gradFwdTrace.get());

  newTrace->stopEngines();
  if (verbose) {
    cerr << "stopEngines\n";
    for (auto &seqIt : newTrace->timeseqs) {
      if (!seqIt.timeseq) continue;
      cerr << "  "s + seqIt.timeseq->desc() + "\n";
    }
  }

  if (doGrads) {
    if (verbose) cerr << "Grads:\n";
    auto grads = make_shared<GradientSet>(reg->mkCtx("rerunTrace"));

    for (auto &change : newTraceInfo->gradientSpec->desiredChanges) {
      if (auto seq = newTrace->getTimeseq(change.ref.seqName)) {
        if (auto yv = seq->getEqualBefore(change.ts)) {
          grads->addGrad(yv.buf, change.deltaValue.buf, 1.0, yv.type);

          if (verbose) cerr << "  Set grad on " + repr(yv) + " = " + repr(change.deltaValue) + "\n";
        }
        else {
          if (verbose) cerr << "no pt "s + repr(change.ts) + " in seq " + shellEscape(change.ref.seqName) + "\n";
        }
      }
      else {
        if (verbose) cerr << "no seq "s + shellEscape(change.ref.seqName) + "\n";
      }
    }
    if (grads->nanFlag) cerr << "  ** NaN in grads\n";
    if (grads->errFlag) cerr << "  ** Err in grads\n";
    if (verbose) {
      cerr << "  grad fwd size: "s + repr(gradFwdTrace->size()) + "\n";
      cerr << "  grad set size: "s + repr(grads->grads.size()) + "\n";
    }
    newTraceInfo->grads = grads;

    // WRITEME: multithread?
    newTrace->runBackprop(*grads, 0.0, gradFwdTrace.get());

    if (verbose) {
      cerr << "  after backprop grad set size: "s + repr(grads->grads.size()) + "\n";

      int nonzeroGradCount = 0;
      for (auto param : reg->paramsByIndex) {
        auto g = grads->paramGradAccum[param->paramIndex];
        if (g != 0.0) {
          nonzeroGradCount++;
          cerr << "  param " + param->paramName + " " + repr(g) + "\n";
        }
      }
      cerr << "  (" + repr(nonzeroGradCount) + "/" + repr(reg->paramsByIndex.size()) + " params nonzero)\n";
    }
  }

}



// --------------

void Trace::runBackprop(GradientSet &grads, double minTs, GradFwdTrace *gradFwdTrace)
{
  if (!gradFwdTrace) return;
  for (auto it = gradFwdTrace->rbegin(); it != gradFwdTrace->rend(); it++) {
    if (grads.nanFlag) break;
    (*it)(grads);
  }
}
