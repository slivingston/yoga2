#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "common/uv_wrappers.h"
#include "./scope_model.h"
#include "../timeseq/timeseq_pipe.h"
#include "common/asyncjoy.h"




PolicySquiggleMap *ScopeModel::getSquiggleMapForParam(YogaParamInfo *desiredParam, int priority)
{
  auto paramIndex = desiredParam->paramIndex;
  if (paramIndex < policySquiggleMapZooms.size() && 
      policySquiggleMapZooms[paramIndex] &&
      policySquiggleMapZooms[paramIndex]->paramValueEpoch == ctx.reg->paramValueEpoch &&
      policySquiggleMapZooms[paramIndex]->spec.policySquiggleEpoch == desiredPolicySquiggleSpec.policySquiggleEpoch) {
    return policySquiggleMapZooms[paramIndex].get();
  }

  if (priority > desiredPolicySquiggleZoomParamPriority) {
    desiredPolicySquiggleZoomParam = desiredParam;
    desiredPolicySquiggleZoomParamPriority = priority;
  }

  return nullptr;
}


shared_ptr<PolicySquiggleMap> ScopeModel::mkSquiggleMapFromSpec(PolicySquiggleSpec const &spec)
{
  auto sf = ctx.reg->sources->requiredFilesByName[spec.sourceFileName];
  if (!sf) return nullptr;

  vector<YogaParamInfo *> relevantParams;
  for (auto &param : ctx.reg->paramsByIndex) {
    if (param && param->sourceLoc.file && param->sourceLoc.file == sf) {
      relevantParams.push_back(param);
    }
  }

  auto newMap = make_shared<PolicySquiggleMap>(ctx, spec, relevantParams.size());

  for (size_t vi = 0; vi < relevantParams.size(); vi++) {
    auto param = relevantParams[vi];
    PolicyParamVariation var(param->paramName, spec.paramRange);
    auto varBound = var.bound(ctx, spec.resolution); // could be null if something fails
    newMap->varBounds[vi] = varBound;
    newMap->varsByParamIndex.at(param->paramIndex) = vi;
  }

  return newMap;
}


shared_ptr<PolicySquiggleMap> ScopeModel::mkSquiggleMapFromSpecForParam(PolicySquiggleSpec const &spec, YogaParamInfo *param)
{
  auto newMap = make_shared<PolicySquiggleMap>(ctx, spec, 1);

  PolicyParamVariation var(param->paramName, spec.paramRange);
  auto varBound = var.boundFullRange(ctx, spec.resolution); // could be null if something fails
  newMap->varBounds[0] = varBound;
  newMap->varsByParamIndex.at(param->paramIndex) = 0;

  return newMap;
}



shared_ptr<AltTraceInfo> ScopeModel::mkAltTraceForPoint(PolicySquiggleMap *newMap, size_t varIndex, size_t xi)
{
  auto vb = newMap->varBounds[varIndex];
  if (!vb) return nullptr;

  auto ret = make_shared<AltTraceInfo>();
  ret->baseTrace = trace;
  ret->paramValueEpoch = ctx.reg->paramValueEpoch;
  ret->durationLimit = newMap->spec.rMetric.sampleTime + 0.01;
  ret->overrideParamValues = ctx.reg->paramValues; // copy, then munge below

  vb->param->setNormValue(ctx, vb->paramBase + vb->paramRange * vb->values[xi], &ret->overrideParamValues);

  if (0) {
    cerr << "Eval squiggle at var=" + repr(varIndex) + " xi=" + repr(xi) +
      " overrideParamValues=" + repr(ret->overrideParamValues) + "\n";
  }

  return ret;
}

void ScopeModel::calcPolicySquiggleMap(
  shared_ptr<PolicySquiggleMap> newMap,
  std::function<void (shared_ptr<PolicySquiggleMap>)> onDone,
  std::function<void (R)> onProgress)
{
  auto t0 = clock();
  auto aj = make_shared<AsyncJoy>([newMap, t0, onDone](string const &err) {
    newMap->calcStats();                
    onDone(newMap);
    auto t1 = clock();
    if (0) cerr << "Calculated "s + repr(newMap->rs.size()) + " policy squiggle points in " + repr_clock(t1-t0) + "\n";
  });

  for (size_t varIndex = 0; varIndex < newMap->varBounds.size(); varIndex++) {
    for (size_t xi = 0; xi < newMap->spec.resolution; xi++) {
      auto newTraceInfo = mkAltTraceForPoint(newMap.get(), varIndex, xi);

      aj->start();
      uvWork(
          [newTraceInfo](string &error, shared_ptr<void> &result) {
            rerunTrace(newTraceInfo.get());
          },
          [newMap, newTraceInfo, aj, varIndex, xi, onProgress](string const &error, shared_ptr<void> const &result) {
            newMap->rat(varIndex, xi) = calcMetric(newTraceInfo->trace.get(), newMap->spec.rMetric);
            aj->end();
            if (onProgress) {
              onProgress(aj->fractionDone());
            }
          });
    }
  }
  aj->end();
}

void ScopeModel::updatePolicySquiggles()
{
  if (!trace || traceLoadActive || traceSimActive) return;
  if (policySquiggleThreadActive) return;

  auto &spec = desiredPolicySquiggleSpec;
  if (!spec.isValid() ||
      visSourceFile.empty()) {
    policySquiggleMap = nullptr;
    return;
  }

  if (visSourceFile != spec.sourceFileName) {
    spec.sourceFileName = visSourceFile;
    spec.policySquiggleEpoch ++;
  }

  if (!policySquiggleMap ||
      policySquiggleMap->paramValueEpoch != ctx.reg->paramValueEpoch ||
      policySquiggleMap->spec.policySquiggleEpoch != spec.policySquiggleEpoch) {

    auto newMap = mkSquiggleMapFromSpec(spec);
    if (!newMap) return;

    policySquiggleThreadActive ++;
    calcPolicySquiggleMap(newMap, 
      [this, this1=shared_from_this()](shared_ptr<PolicySquiggleMap> doneMap) {
        policySquiggleMap = doneMap;
        policySquiggleThreadActive --;
        uvUiActive = true;
      },
      nullptr);
  }

  if (auto todoParam = desiredPolicySquiggleZoomParam) {
    if (!(desiredPolicySquiggleZoomParam->paramIndex < policySquiggleMapZooms.size() &&
      policySquiggleMapZooms[desiredPolicySquiggleZoomParam->paramIndex] &&
      policySquiggleMapZooms[desiredPolicySquiggleZoomParam->paramIndex]->paramValueEpoch == ctx.reg->paramValueEpoch &&
      policySquiggleMapZooms[desiredPolicySquiggleZoomParam->paramIndex]->spec.policySquiggleEpoch == spec.policySquiggleEpoch)) {

      auto zoomSpec = spec;
      zoomSpec.resolution *= 5;
      auto newMap = mkSquiggleMapFromSpecForParam(zoomSpec, todoParam);
      if (!newMap) return;

      policySquiggleThreadActive ++;
      calcPolicySquiggleMap(newMap, 
        [this, this1=shared_from_this(), todoParam](shared_ptr<PolicySquiggleMap> doneMap) {
          if (!(todoParam->paramIndex < policySquiggleMapZooms.size())) {
            policySquiggleMapZooms.resize(todoParam->paramIndex+1);
          }
          policySquiggleMapZooms[todoParam->paramIndex] = doneMap;
          policySquiggleThreadActive --;
          uvUiActive = true;
        },
        nullptr);
    }
    desiredPolicySquiggleZoomParam = nullptr;
    desiredPolicySquiggleZoomParamPriority = 0;
  }

}
