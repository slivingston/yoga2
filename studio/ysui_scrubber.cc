#include "./yogastudio_ui.h"
#include "./scope_model.h"
#include "./ysgui.h"
#include "./main_window.h"
#include "numerical/polyfit.h"

void YogaStudioView::drawScopeTimeScrubber(ScopeModel &m, ImRect bb)
{
  ImGuiWindow* window = GetCurrentWindow();
  ImGuiContext& g = *GImGui;
  //const ImGuiStyle& style = g.Style;
  ImGuiID id = window->GetID("scrubberKnob");

  SetCursorPos(bb.Min);
  //const float w = CalcItemWidth();

  ImRect frameBB(bb);
  if (!ItemAdd(bb, id, &frameBB)) {
    return;
  }

  bool hovered = ItemHoverable(frameBB, id);
  RenderNavHighlight(frameBB, id);

  const bool focus_requested = FocusableItemRegister(window, id);
  const bool clicked = (hovered && g.IO.MouseClicked[0]);
  if (focus_requested || clicked || g.NavActivateId == id || g.NavInputId == id) {
    SetActiveID(id, window);
    SetFocusID(id, window);
    FocusWindow(window);
    // FIXME g.ActiveIdAllowNavDirFlags = (1 << ImGuiDir_Up) | (1 << ImGuiDir_Down);
    if (focus_requested || (clicked && g.IO.KeyCtrl) || g.NavInputId == id) {
      FocusableItemUnregister(window);
    }
  }

  ImRect grabBB;
  bool changed = SliderBehavior(frameBB, id, ImGuiDataType_Double, &m.visTime, &m.traceBeginTs, &m.traceEndTs, "%0.3f", 1.0f, ImGuiSliderFlags_None, &grabBB);
  if (0) cerr << "scrubber id="s + to_string(id) + " activeID=" + to_string(g.ActiveId) + " changed=" + to_string(changed) + " hovered=" + to_string(hovered) + " frameBB=" + repr(frameBB) + " grabBB=" + repr(grabBB) + " visTime=" + to_string(m.visTime) + "\n";
  if (changed) MarkItemEdited(id);

  R boxT = bb.Min.y;
  R boxR = bb.Max.x;
  R boxB = bb.Max.y;
  R boxL = bb.Min.x;

  R innerL = boxL;
  R innerR = boxR;
  R knobL = grabBB.Min.x;
  R knobR = grabBB.Max.x;

  R middleY = bb.Min.y + 0.5*(bb.Max.y - bb.Min.y);

  // antialiased lines and fills seem to be on by default
  if (0) cerr << "drawScopeTimeScrubber flags="s + repr_08x(window->DrawList->Flags) + "\n";

  if (1) { // drawShading
    window->DrawList->AddRectFilled(ImVec2(boxL, boxT), ImVec2(boxR, boxB), yss->scrubShade3);
    window->DrawList->AddRectFilled(ImVec2(innerL, boxB-1), ImVec2(innerR, boxB), yss->scrubShade2);
    window->DrawList->AddRectFilled(ImVec2(innerL, boxT), ImVec2(innerL+1, boxB), yss->scrubShade2);
    window->DrawList->AddRectFilled(ImVec2(innerL, boxT), ImVec2(innerR, boxT+1), yss->scrubShade1);
    window->DrawList->AddRectFilled(ImVec2(innerR-1, boxT), ImVec2(innerR, boxB), yss->scrubShade1);
  }

  if (0) {
    auto drawHscrollArrow = [window, middleY](R arrT, R arrR, R arrB, R arrL, R direction) {
      window->DrawList->AddRectFilled(ImVec2(arrL, arrT), ImVec2(arrR, arrB), yss->scrubShade1);
      window->DrawList->AddRectFilled(ImVec2(arrL+0.5, arrT+0.5), ImVec2(arrR-0.5, arrB-0.5), IM_COL32(0x61, 0x63, 0x5f, 0xff));
      window->DrawList->AddRectFilled(ImVec2(arrL+1.5, arrT+1.5), ImVec2(arrR-1.5, arrB-1.5), IM_COL32(0xcc, 0xcc, 0xcc, 0xff));

      R centerX = (arrL + arrR) / 2 + direction * 0.5;

      window->DrawList->AddTriangleFilled(
        ImVec2(centerX + direction*3, middleY),
        ImVec2(centerX - direction*3, middleY-5),
        ImVec2(centerX - direction*3, middleY+5),
        IM_COL32(0x00, 0x00, 0x00, 0xff));
    };
    drawHscrollArrow(boxT, innerL, boxB, boxL, -1.0);
    drawHscrollArrow(boxT, boxR, boxB, innerR, +1.0);
  }

  if (1) { // drawGroove
    window->DrawList->AddRectFilled(ImVec2(innerL+1, middleY-1.5), ImVec2(innerR-1, middleY+1.5), yss->scrubGroove1);
    window->DrawList->AddRectFilled(ImVec2(innerL+1, middleY-0.5), ImVec2(innerR-1, middleY+0.5), yss->scrubGroove2);
  }

  if (1) { // drawTrough
    // WRITEME: ctx.fillStyle = scope_ui.mkTroughPatternH(ctx, lo.boxT+1, lo.innerR-1, lo.boxB-1, lo.innerL+1); 
    window->DrawList->AddRectFilled(ImVec2(innerL+1, boxT+1), ImVec2(innerR-1, boxB-1), yss->scrubTrough);
  }

  if (1) { // drawKnob
    auto drawOutline = [window, m, knobL, knobR, boxT, boxB, middleY](R inset, ImU32 fillColor) {
      if (m.autoScroll > 0) {
        array pts {
          ImVec2(knobL + inset, boxT + 1 + inset),
          ImVec2(knobR - inset, boxT + 1 + inset),
          ImVec2(knobR + (boxB - boxT) - inset, middleY),
          ImVec2(knobR - inset, boxB - 1 - inset),
          ImVec2(knobL + inset, boxB - 1 - inset),
        };
        window->DrawList->AddConvexPolyFilled(pts.data(), pts.size(), fillColor);
      }
      else if (m.autoScroll < 0) {
        array pts {
          ImVec2(knobL + inset, boxT + 1 + inset),
          ImVec2(knobR - inset, boxT + 1 + inset),
          ImVec2(knobR - inset, boxB - 1 - inset),
          ImVec2(knobL + inset, boxB - 1 - inset),
          ImVec2(knobL - (boxB - boxT) - inset, middleY),
        };
        window->DrawList->AddConvexPolyFilled(pts.data(), pts.size(), fillColor);
      }
      else {
        window->DrawList->AddRectFilled(ImVec2(knobL + inset, boxT + 1 + inset), ImVec2(knobR - inset, boxB - 2 - 2*inset), fillColor);
      }
    };

    drawOutline(0.0, yss->scrubKnob1);
    drawOutline(1.0, yss->scrubKnob2);
    drawOutline(1.5, yss->scrubKnob3);
    // WRITEME: pat = ctx.createLinearGradient(lo.boxT+1, lo.knobL, lo.boxB-1, lo.knobL);
    drawOutline(3.0, yss->scrubTrough);
  }
}
