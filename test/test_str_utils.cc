#include "common/std_headers.h"
#include "./test_utils.h"

TEST_CASE("startsWith", "[lib]") {
  CHECK(startsWith("foobar", "f"));
  CHECK(startsWith("foobar", "foo"));
  CHECK(!startsWith("foobar", "x"));
  CHECK(startsWith("foobar", ""));
  CHECK(!startsWith("foobar", "foobart"));
}
TEST_CASE("endsWith", "[lib]") {
  CHECK(endsWith("foobar", "r"));
  CHECK(endsWith("foobar", "bar"));
  CHECK(!endsWith("foobar", "x"));
  CHECK(endsWith("foobar", ""));
  CHECK(!endsWith("foobar", "tfoobar"));
}

TEST_CASE("splitChar works", "[lib]") {
  auto fb = splitChar("foo,bar", ',');
  REQUIRE(fb.size() == 2);
  CHECK(fb[0] == "foo");
  CHECK(fb[1] == "bar");
}

TEST_CASE("splitChar preserves trailing sep", "[lib]") {
  auto fb = splitChar(",", ',');
  REQUIRE(fb.size() == 2);
  CHECK(fb[0] == "");
  CHECK(fb[1] == "");
}

TEST_CASE("splitChar preserves leading sep", "[lib]") {
  auto fb = splitChar(",x", ',');
  REQUIRE(fb.size() == 2);
  CHECK(fb[0] == "");
  CHECK(fb[1] == "x");
}


TEST_CASE("joinChar(splitChar) is identity", "[lib]") {
  CHECK(joinChar(splitChar("foo,bar", ','), ',') == "foo,bar");
}



TEST_CASE("splitString \",\" works", "[lib]") {
  auto fb = splitString("foo,bar", ",");
  REQUIRE(fb.size() == 2);
  CHECK(fb[0] == "foo");
  CHECK(fb[1] == "bar");
}

TEST_CASE("splitString \",\" preserves trailing sep", "[lib]") {
  auto fb = splitString(",", ",");
  REQUIRE(fb.size() == 2);
  CHECK(fb[0] == "");
  CHECK(fb[1] == "");
}

TEST_CASE("splitString \",\" preserves leading sep", "[lib]") {
  auto fb = splitString(",x", ",");
  REQUIRE(fb.size() == 2);
  CHECK(fb[0] == "");
  CHECK(fb[1] == "x");
}

TEST_CASE("joinString(splitString, \",\") is identity", "[lib]") {
  CHECK(joinString(splitString("foo,bar", ","), ",") == "foo,bar");
}



TEST_CASE("splitString \"::\" works", "[lib]") {
  auto fb = splitString("foo::bar", "::");
  REQUIRE(fb.size() == 2);
  CHECK(fb[0] == "foo");
  CHECK(fb[1] == "bar");
}

TEST_CASE("splitString \"::\" preserves trailing sep", "[lib]") {
  auto fb = splitString("::", "::");
  REQUIRE(fb.size() == 2);
  CHECK(fb[0] == "");
  CHECK(fb[1] == "");
}

TEST_CASE("splitString \"::\" preserves leading sep", "[lib]") {
  auto fb = splitString("::x", "::");
  REQUIRE(fb.size() == 2);
  CHECK(fb[0] == "");
  CHECK(fb[1] == "x");
}

TEST_CASE("joinString(splitString, \"::\")) is identity", "[lib]") {
  CHECK(joinString(splitString("foo::bar", "::"), "::") == "foo::bar");
}

