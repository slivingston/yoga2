#pragma once
#include <regex>

#include "../jit/compilation.h"
#include "../jit/runtime.h"

/*
  See https://github.com/catchorg/Catch2/blob/master/docs/Readme.md#top
  https://github.com/catchorg/Catch2/blob/master/docs/assertions.md#top
 */
#define CATCH_CONFIG_FAST_COMPILE 1
#define CATCH_CONFIG_ENABLE_PAIR_STRINGMAKER 1
#include "catch2/catch.hpp"

extern ofstream testlog;

bool isMatch(string const &l, string const &re);
string atline(int line, string const &error);

shared_ptr<YogaCompilation> yogac(string const &code, char const *file, int line);
#define YOGAC(X) yogac(X, __FILE__, __LINE__)

bool checkerrs(YogaCompilation *reg, vector<string> const &errors);
bool checkerrs(shared_ptr<YogaCompilation> reg, vector<string> const &errors);
bool checkerrs(YogaContext const &ctx, vector<string> const &errors);

bool checkGradient(YogaCaller f, vector<YogaValue> const &inVals,
  string const &outName, string const &inName, R expectedGrad = numeric_limits<R>::quiet_NaN());
  

struct YogaFoobar {
  R foo;
  R bar;
};

