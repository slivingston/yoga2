#include "common/std_headers.h"
#include "./test_utils.h"
#include "common/uv_wrappers.h"

TEST_CASE("UvMainThreadMutex", "[uv]") {

  UvMainThreadMutex mtm;
  vector<string> events;

  for (int muti=1; muti<=3; muti++) {
    mtm.withMutex([&events, muti](std::function<void()> onDone) {
      events.push_back("start" + repr(muti));
      auto tm = new UvTimer();
      tm->timer_init();
      tm->timer_start([onDone, &events, muti]() {
        events.push_back("done"s + repr(muti));
        onDone();
      }, 50, 0);
    });
  }

  uvRunMainThread();

  CHECK(joinSpaces(events) == "start1 done1 start2 done2 start3 done3");

}
