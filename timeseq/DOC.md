### Trace, Timeseq, Engine, RuntimeInfo

A Timeseq<T> is a growable sequence of (timestamp, T) pairs.
It's used to track all the realtime data in the robot.
While the robot is running, it accumulates in memory, discarding old samples to limit memory use.
After a run, it saves to disk as JSON files.

An Engine (subclass of `GenericEngine`) consumes data from some Timeseqs and supplies data to other Timeseqs.

A Trace is a collection of Timeseqs and Engines,
a mechanism for storing and reloading Timeseqs and a schema to disk,
and a scheduler for the Engines.

For example, the Dex4KinEngine consumes joint angles and IMU coordinates
and creates kinematic coordinates. The juicy part looks like:

```C++
void Dex4KinEngine::update()
{
  // Get the timestamp of the last data I generated, or 0 at beginning
  double ts0 = kinSeq->endTs();

  auto inertial1 = inertialSeq->getAfter(ts0); // Get later inertial data
  if (!inertial1) return; // nothing later available, do nothing

  auto joints1 = jointsSeq->getAfter(ts0); // Get joint angle data
  if (!joints1) return;

  // New timestamp. At least 1 mS in the future, and the later of the
  // two input streams. Other policies are possible.
  double ts1 = max(ts0+0.001, max(inertial1.ts, joints1.ts));
  auto kin1 = kinSeq->getNew(ts1);

  // Fill in the fields of kin1 from inertial1, llState1.
  kin1->foo = f(inertial1->bar, joints->buz);

  // Add it to kinSeq. We set the timestamp (=ts1) back when we allocated kin1.
  kinSeq->add(kin1);
}
```


Most of a Trace's runtime state is held in a `RuntimeInfo` struct, which
all the Timeseqs and Engines also have a pointer to.


###
