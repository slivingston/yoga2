#include "./gradient_set.h"
#include "../jit/compilation.h"
#include "../jit/value.h"


GradientSet::GradientSet(YogaContext const &_ctx)
  :ctx(_ctx)
{
  mem = new_yogic_pool(ctx.reg->mem, "grads");
  paramGradAccum.resize(ctx.reg->paramsByIndex.size());
}

GradientSet::~GradientSet()
{
  if (mem) {
    free_yogic_pool(mem);
    mem = nullptr;
  }
}

void GradientSet::addGrad(U8 *value, U8 *g, double coeff, YogaType *t)
{
  const int verbose = 0;
  auto &gradSlot = grads[value];
  if (gradSlot) {
    if (!t->linearOp) {
      if (!errFlag) {
        errFlag = true;
        cerr << "No linearOp for " + repr_type(t) + "\n";
      }
      return;
    }
    auto newOb = t->mkInstance(mem);
    t->linearOp(newOb, 1.0, gradSlot, coeff, g, mem);
    gradSlot = newOb;
  }
  else if (coeff == 1.0) {
    gradSlot = g;
  }
  else {
    if (!t->linearOp) {
      if (!errFlag) {
        errFlag = true;
        cerr << "No linearOp for " + repr_type(t) + "\n";
      }
      return;
    }
    auto newOb = t->mkInstance(mem);
    t->linearOp(newOb, 0.0, t->zeroValue, coeff, g, mem);
    gradSlot = newOb;
  }
  if (verbose) {
    YogaValue yv(ctx.reg, t, value);
    YogaValue yg(ctx.reg, t, gradSlot);
    if (verbose >= 2 || yg.nonZeroValue()) {
      cerr << "  V=" + repr(yv) + "\n  G=" + repr(yg) + "\n";
    }
  }
}

void GradientSet::addWindowedGrad(U8 *value, U8 *g, double coeff, double ts, YogaType *t)
{
  coeff *= windowFunc((ts - windowBeginTs) / (windowEndTs - windowBeginTs));
  addGrad(value, g, coeff, t);
}

U8 *GradientSet::getGrad(U8 *value, YogaType *t)
{
  auto &gradSlot = grads[value];
  if (0) cerr << "Get grad " + repr_ptr(value) + "->" + repr_ptr(gradSlot) + " " + repr_type(t) + "\n";
  if (!gradSlot) {
    // mutated by ScopeModel::rerunTrace.
    // perhaps most of these could be t->zeroValue if we did something special there
    gradSlot = t->mkInstance(mem);
  }
  return gradSlot;
}

YogaValue GradientSet::getGrad(YogaValue const &value)
{
  if (!value.type) return YogaValue();
  auto bufGrad = getGrad(value.buf, value.type);
  return YogaValue(ctx.reg, value.type, bufGrad, value.ts);
}
