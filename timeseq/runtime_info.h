#pragma once
#include "common/std_headers.h"

#include "./gradient_set.h"
#include "../jit/compilation.h"

struct RuntimeInfo {
  RuntimeInfo(shared_ptr<YogaCompilation> _reg);
  double realTimeOffset{0.0};
  double timeWindow{0.0};
  double hardHaltTs{0.0}, softHaltTs{0.0};
  bool hardHalt{false}, softHalt{false};

  string traceName;
  string traceDir;

  shared_ptr<YogaCompilation> reg;
  R *paramValues{nullptr};

  R yogaParamsUpdateTs{0.0};

  void requestUpdate();
  struct uv_async_s *updateAsync{nullptr};
  struct uv_timer_s *updateTimer{nullptr};
  int updateAsyncPendingCnt{0};
};
