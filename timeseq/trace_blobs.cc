#include "common/std_headers.h"

#include <atomic>
#include <typeinfo>
#include <apr_random.h>
#include "./runtime_info.h"
#include "./trace_blobs.h"



U64 parseChunkId(string const &chunkName)
{
  return stoull(chunkName, nullptr, 16);
}

string fmtChunkId(U64 chunkId)
{
  char chunkName[128];
  snprintf(chunkName, sizeof(chunkName), "%016llx", (unsigned long long)chunkId);
  return chunkName;
}

struct BlobFileInfo {
  int fd{0};
  atomic<U64> writePos{0};
};

static map<U64, BlobFileInfo> blobFiles;

static size_t roundUp(size_t baseSize)
{
  return (baseSize + 7) & ~7;
}


static bool openFile(BlobFileInfo &fileInfo, U64 chunkId)
{
  string fn = YogaLayout::instance().blobFilename(chunkId);
  int fd = open(fn.c_str(), O_CREAT | O_RDWR, 0666);
  if (fd < 0) {
    cerr << fn + ": " + strerror(errno) + "\n";
    return false;
  }
  fileInfo.fd = fd;
  return true;
}

static BlobFileInfo &getBlobInfo(U64 chunkId)
{
  auto &fileInfo = blobFiles[chunkId];
  if (!fileInfo.fd) {
    openFile(fileInfo, chunkId);
  }
  return fileInfo;
}

bool loadBlobFile(U64 chunkId, Blob &data)
{
  auto &fileInfo = getBlobInfo(chunkId);
  if (!fileInfo.fd) {
    return false;
  }

  struct stat st;
  if (fstat(fileInfo.fd, &st) < 0) {
    return false;
  }
  auto size = st.st_size;

  data.alloc(size);

  auto nr = pread(fileInfo.fd, data.data(), size, 0);

  if (nr < 0) {
    cerr << "Loading blobfile " + repr(chunkId) + ": " + strerror(errno) + "\n";
    return false;
  }
  else if (nr == 0) {
    if (0) cerr << "No data in blobfile " + repr(chunkId) + "\n";
    return false;
  }
  else if (nr < size) {
    cerr << "Partial load from blobfile " + repr(chunkId) + 
      ": " + to_string(nr) + "/" + to_string(size) +
      "\n";
    return false;
  }
  return true;
}

bool loadBlob(BlobRef const &id, Blob &data)
{
  if (id.partSize > 0xfffffffLL || id.partOfs > 0xfffffffffLL) {
    throw runtime_error("Silly "s + repr(id));
  }
  auto &fileInfo = getBlobInfo(id.chunkId);
  if (!fileInfo.fd) {
    return false;
  }
  data.alloc(id.partSize);
  auto nr = pread(fileInfo.fd, data.data(), id.partSize, id.partOfs);
  if (nr < 0) {
    cerr << "Loading from " + repr(id) + ": " + strerror(errno) + "\n";
    return false;
  }
  else if (nr == 0) {
    if (0) cerr << "No data for " + repr(id) + "\n";
    return false;
  }
  else if (nr < id.partSize) {
    cerr << "Partial load from " + repr(id) + 
      ": " + to_string(nr) + "/" + to_string(id.partSize) +
      "\n";
    return false;
  }
  return true;
}

BlobRef mkBlob(U64 chunkId, U8 const *data, size_t dataSize)
{
  auto &fileInfo = getBlobInfo(chunkId);
  if (!fileInfo.fd) {
    return BlobRef();
  }
  off_t partOfs = fileInfo.writePos.fetch_add(roundUp(dataSize));

  auto rc = pwrite(fileInfo.fd, data, dataSize, partOfs);
  if (rc < 0) {
    cerr << "write chunk: "s + strerror(errno) + "\n";
    return BlobRef();
  }
  return BlobRef(chunkId, partOfs, dataSize);
}

BlobRef mkBlob(U64 chunkId, Blob const &data)
{
  return mkBlob(chunkId, data.data(), data.size());
}

void saveBlob(BlobRef const &id, U8 const *data, size_t dataSize)
{
  if (dataSize != id.partSize) {
    throw runtime_error("saveBlob mismatch dataSize="s + to_string(dataSize) + " partSize=" + to_string(id.partSize));
  }
  auto &fileInfo = blobFiles[id.chunkId];
  if (!fileInfo.fd) {
    if (!openFile(fileInfo, id.chunkId)) {
      return;
    }
  }
  auto rc = pwrite(fileInfo.fd, data, dataSize, id.partOfs);
  if (rc < 0) {
    cerr << "write chunk: "s + strerror(errno) + "\n";
  }
}

void saveBlob(BlobRef const &id, Blob const &data)
{
  saveBlob(id, data.data(), data.size());
}

bool saveBlobFile(U64 chunkId, Blob const &data)
{
  auto &fileInfo = getBlobInfo(chunkId);
  if (!fileInfo.fd) {
    return false;
  }

  auto rc = pwrite(fileInfo.fd, data.data(), data.size(), 0);
  if (rc < 0) {
    cerr << "write chunk: "s + strerror(errno) + "\n";
    return false;
  }
  return true;
}

ostream & operator <<(ostream &s, BlobRef const &a)
{
  s << "BlobRef(" << repr_016x(a.chunkId) <<
     ", " << repr(a.partOfs) << 
     ", " << repr(a.partSize) + ")";
  return s;
}


U64 mkBlobChunkId()
{
  U64 ret{0};
  ret = (U64)time(nullptr) << 32;
  U8 sub[4];
  apr_generate_random_bytes(sub, sizeof(sub));

  ret += ((U64)sub[0] << 24) + ((U64)sub[1] << 16);

  return ret;
}


void rotateBlobChunkId(U64 &chunkId)
{
  if (chunkId == 0) chunkId = mkBlobChunkId();
  auto &fileInfo = blobFiles[chunkId];
  if (fileInfo.fd) {
    if (fileInfo.writePos.load() >= 9*1024*1024) { // keep under 10 mb
      chunkId++;
    }
  }
}




namespace packetio {
void packet_wr_value(packet &p, BlobRef const &x)
{
  if (x.partSize > 0xfffffffLL || x.partOfs > 0xfffffffffLL) {
    throw runtime_error("Silly "s + repr(x));
  }
  packet_wr_value(p, x.chunkId);
  packet_wr_value(p, x.partOfs);
  packet_wr_value(p, x.partSize);
}
string packet_get_typetag(BlobRef const & /* x */)
{
  return "BlobRef:1";
}
void packet_rd_value(packet &p, BlobRef &x)
{
  packet_rd_value(p, x.chunkId);
  packet_rd_value(p, x.partOfs);
  packet_rd_value(p, x.partSize);
  if (x.partSize > 0xfffffffLL || x.partOfs > 0xfffffffffLL) {
    throw runtime_error("Silly "s + repr(x));
  }
}

std::function<void(packet &, BlobRef &)> packet_rd_value_compat(BlobRef const &x, string const &typetag)
{
  return static_cast<void (*)(packet &, BlobRef &)>(packet_rd_value);
}
}

