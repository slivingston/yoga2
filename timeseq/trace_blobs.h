#pragma once
#include "common/std_headers.h"
#include "common/packetbuf.h"
#include "../db/blob.h"

struct BlobRef {
  BlobRef()
    : chunkId(0),
      partOfs(0),
      partSize(0)
  {
  }
  BlobRef(
    U64 _chunkId,
    U64 _partOfs,
    U64 _partSize)
    : chunkId(_chunkId),
      partOfs(_partOfs),
      partSize(_partSize)
  {
  }

  bool isValid() { return chunkId != 0; }
  U64 chunkId;
  U64 partOfs;
  U64 partSize;
};
DECL_PACKETIO(BlobRef);

ostream & operator <<(ostream &s, BlobRef const &a);

inline bool operator <(BlobRef const &a, BlobRef const &b)
{
  if (a.chunkId < b.chunkId) return true;
  if (a.chunkId > b.chunkId) return false;
  if (a.partOfs < b.partOfs) return true;
  if (a.partOfs > b.partOfs) return false;
  if (a.partSize < b.partSize) return true;
  if (a.partSize > b.partSize) return false;
  return false;
}

bool loadBlob(BlobRef const &id, Blob &data);


BlobRef mkBlob(U64 chunkId, U8 const *data, size_t dataSize);
BlobRef mkBlob(U64 chunkId, Blob const &data);

void saveBlob(BlobRef const &id, U8 const *data, size_t dataSize);
void saveBlob(BlobRef const &id, Blob const &data);
bool saveBlobFile(U64 chunkId, Blob const &data);

U64 mkBlobChunkId();
void rotateBlobChunkId(U64 &chunkId);

U64 parseChunkId(string const &chunkName);
string fmtChunkId(U64 chunkId);

bool loadBlobFile(U64 chunkId, Blob &data);
